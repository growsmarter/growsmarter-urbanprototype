var rModule = angular.module('result', []);

rModule.controller('resultController',
		[ '$scope', function($scope) {
			$scope.init = function () {
			    // check if there is query in url
			    // and fire search in case its value is not empty
				AjaxGetLabelTable();
//				AjaxGetUrbanConcepts();
				mapInit();
//				AjaxGetSparqlPrefixes();
			};
		} ]);

rModule.directive('resultView', function() {
	return {
		restrict : 'E',
		templateUrl : 'result.jsp'
	};
});