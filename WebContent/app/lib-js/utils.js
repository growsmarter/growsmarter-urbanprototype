/**
 * MAP FUNCTIONS
 * 
 */
var map, layer, ctrlDF;
// , vlayer, landlotlayer, blocklayer;

var conceptLayers = new Object(); // Tab1 functionality: Hash table with the
// array of map layers and selected polygons
// associated to each concept (uri, not id)
// in the hierarchy. layer, selectedFeature,
// checked
var popupFeatures = new Object(); // Hash id popup -> {feature, previousStyle}
var existentIndividuals = new Object(); // Hash de las uri de las clases,
// conteniendo un array con las features
// existentes (y visibles según checked
// de concepLayers) en el mapa
// (individuos)
var conceptProperties = new Object(); // Hash de las uri de las clases, con
// las propiedades compartidas por los
// individuos existentes de esa clase
var relatedConcepts = new Object(); // Hash de las uri de las clases. Para cada
// una se almacena un array de las uri de
// las propiedades indizado por tipo de
// propiedad. relatedConcepts[uriClass]->
// properties[type]->[uriprop,...]
var indicatorsHash = null;
var querylist = new Object(); // Has de las queries en tab2.Contiene uriclass,
// uriprop, groupOp, nextbvalue, features,
// result y un objeto con hash de cada condición
// and. {class, prop, op, conditions:{prop, op,
// value}}
var urllabelmap = new Object();
// var geojson_format = new OpenLayers.Format.GeoJSON();
var wkt_format = new OpenLayers.Format.WKT();
var layerSwitcher;
var fixedLayers = new Object(); // {"Base": layer, "Blocks": layer, "Landlots":
// layer, "Area Edition": layer}
var lastPopupWindow = null;
var tab15clicked = false;
var tab2clicked = false;
var projDest = new OpenLayers.Projection('EPSG:900913');
var projSource = new OpenLayers.Projection('EPSG:4326');
var groupOp = ["Avg", "Min", "Max", "Sum", "Count"];
var arithOp = ["+", "-", "*", "/"];
var compOp = ["<", ">", "<=", ">=", "==", "!="];
var ARROW = "\u2192";
var dataLoadingCounter = 0;
// WEB SERVICES TOMCAT SERVLET
// var wsPath_utilsjs = "/stable"; // redrock wsPath
var wsPath_utilsjs = "http://127.0.0.1:58080/UrbanPrototype";
//var wsPath_utilsjs = "http://localhost:8080/UrbanPrototype";
//var wsPath_utilsjs = "http://growsmarter.bsc.es:8080/UrbanPrototype";
// var wsPath_utilsjs = "/SemanticUrbanModel2/"; // local wsPath
var populatedRelConcepts = {
	populated: false
}; // smendoza: param to indicate if populated related concepts to the WS

var messageTab1 = "Select one or more areas on the map and then check the urban concepts to be displayed from the left window. You can hide/display them by using the blue box in the right upper corner";
var messageTab15 = "Select one urban concept to see the related classes in the upper window pane. The property which relate them is also shown. You can choose the type of top properties on the check box. In the window below, introduce a text to find properties that match. Their domain and range are displayed";
var messageTab2 = "Click on the class to explore the properties and values corresponding to the instances in the upper left window pane. Build queries by selecting classes, properties and constraints over the map instances.";
var messageTab3 = "Write a sparql query in the upper window text box. In the window below, you can edit the prefixes needed. The results are displayed in the window at the bottom. The resulting instances that match are highlighted in the text as well as in the map";
var messageTabIndicators = "";

function isMapUndefined() {
	if (map == undefined)
		return true;
	else
		return false;
}
var panelControls = {
	navigation: undefined,
	drawFeature: undefined,
};
var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
var layerResultsOptions = {
	styleMap: new OpenLayers.StyleMap({
		'default': {
			strokeColor: "#65737e", // "#ff4e50",
			strokeOpacity: 1,
			strokeWidth: 3,
			fillColor: "#f9d62e",
			fillOpacity: 0.5,
			pointRadius: 6,
			pointerEvents: "visiblePainted",
			// label with \n linebreaks
			label: "${label}",
			// label : "El_Label",
			fontColor: "#000000",
			fontSize: "12px",
			fontFamily: "Courier New, monospace",
			fontWeight: "bold",
			labelOutlineColor: "white",
			labelOutlineWidth: 3
		},
		'select': {
			strokeColor: "#65737e", // "#ff4e50",
			strokeOpacity: 1,
			strokeWidth: 3,
			fillColor: '#c11d1d',
			fillOpacity: 0.5,
			pointRadius: 6,
			pointerEvents: "visiblePainted",
			label: "${label}",
			fontColor: "#000000",
			fontSize: "12px",
			fontFamily: "Courier New, monospace",
			fontWeight: "bold",
			labelOutlineColor: "white",
			labelOutlineWidth: 3
		},
	}),
	renderers: renderer
};

function mapInit(mapDOMId) {

	var zoom = 16;

	var map_options = {
		eventListeners: {
			"changelayer": mapLayerChanged,
		},
		numZoomLevels: 5,
	};
	map = new OpenLayers.Map(mapDOMId, map_options);

	// Base layer (raster)
	var layer = new OpenLayers.Layer.OSM("Simple OSM Map");
	layer.displayInLayerSwitcher = false;
	map.addLayer(layer);
	fixedLayers["Base"] = layer;

	// Selection layers
	layer = new OpenLayers.Layer.Vector("Area Edition", layerResultsOptions);
	layer.displayInLayerSwitcher = false;
	map.addLayer(layer);
	fixedLayers["Area Edition"] = layer;
	RegisterSelectEvent(layer);

	layer = new OpenLayers.Layer.Vector("Landlots");
	map.addLayer(layer);
	fixedLayers["Landlots"] = layer;
	RegisterSelectEvent(layer);

	layer = new OpenLayers.Layer.Vector("Blocks");
	map.addLayer(layer);
	fixedLayers["Blocks"] = layer;
	RegisterSelectEvent(layer);
	// smendoza
	// When feature selected, offer it at the data query
	// 24/02 added manually to manage multipolygons
	// fixedLayers["Blocks"].events.on({
	// featureselected : function(eventInput) {
	// showCreateAreaForm(eventInput.feature);
	// }
	// });
	// fixedLayers["Blocks"].events.on({
	// featureselected : function(eventInput) {
	// showCreateAreaForm(eventInput.feature);
	// }
	// });

	// Toolbar
	var drawFeature = new OpenLayers.Control.DrawFeature(fixedLayers["Area Edition"], OpenLayers.Handler.Polygon, {
		'displayClass': 'olControlDrawFeaturePolygon',
	});
	// drawFeature.featureAdded = function(input) {
	// // RegisterFeatureAdded(fixedLayers["Area Edition"]);
	// showCreateAreaForm(input); // function defined in angularjs
	// };
	var ctrlNav = new OpenLayers.Control.Navigation({
		zoomWheelEnabled: true,
		defaultDblClick: false,
		mouseWheelOptions: {
			interval: 200, // {Integer} In order to increase server performance, an interval (in
			// milliseconds) can
			// be
			// set to reduce the number of up/down events called.
			// maxDelta : 4, // {Integer} Maximum delta to collect before breaking from the current interval.
			// delta : 1, // {Integer} When interval is set, delta collects the mousewheel z-deltas of the
			// events that
			// // occur within the interval.
			// cumulative : true, // {Boolean} When interval is set: true to collect all the mousewheel
			// z-deltas, false to
			// // only record the delta direction (positive or negative)
		}
	});

	panelControls.navigation = ctrlNav;
	panelControls.drawFeature = drawFeature;

	// var panelControls = [ ctrlNav, drawFeature ];
	// var toolbar = new OpenLayers.Control.Panel({
	// displayClass : 'olControlEditingToolbar',
	// defaultControl : panelControls.navigation,
	// });

	// for ( var key in this.panelControls) {
	// toolbar.addControl(panelControls[key]);
	// }
	// map.addControl(toolbar);
	for (var key in panelControls) {
		map.addControl(panelControls[key]);
	}

	// end Toolbar

	// TODO: modifications through the polygon
	var ctrlMF = new OpenLayers.Control.ModifyFeature(fixedLayers["Area Edition"]);
	map.addControl(ctrlMF);

	// end smendoza
	map.addControl(new OpenLayers.Control.PanZoom());
	map.addControl(new OpenLayers.Control.MousePosition());
	layerSwitcher = new OpenLayers.Control.LayerSwitcher();
	map.addControl(layerSwitcher); // ahora uso un dropdown para mostrar capas

	selectControl = new OpenLayers.Control.SelectFeature([fixedLayers["Area Edition"], fixedLayers["Landlots"],
		fixedLayers["Blocks"]
	], {
		clickout: true,
		toggle: false,
		multiple: false,
		hover: false, // CAMBIANDO multiple:true permite seleccionar varios.
		// Problema: no sincronizaría con el tree
		toggleKey: "ctrlKey", // ctrl key removes from selection
		multipleKey: "shiftKey", // shift key adds to selection

	});
	map.addControl(selectControl);
	selectControl.activate();

	Log("Loading Data...");
	AddDataToSelectLayers();
	// layerSwitcher.maximizeControl();
	// map.setCenter(new OpenLayers.LonLat(236959.00265, 5068571.27635), zoom); // Les Corts
	map.setCenter(new OpenLayers.LonLat(239597.27445, 5069963.86346), zoom); // Eixample

	// map.setCenter(new OpenLayers.LonLat(41.390365, 2.155273), zoom);
	Inf(messageTab1);

	// smendoza setCenter
	// layer = fixedLayers["Landlots"];
	// var extent = OpenLayers.extent.createEmpty();
	// projecten.getLayers().forEach(function(layer) {
	// OpenLayers.extent.extend(extent, layer.getSource().getExtent());
	// });
	// map.getView().fitExtent(extent, map.getSize());

	// var centerBounds = new OpenLayers.Bounds(fixedLayers["Landlots"].getExtent());

	// layer.events.register("loadend", layer, function() {
	// var centerBounds = fixedLayers["Landlots"].getDataExtent();
	// var lonLat = centerBounds.getCenterLonLat();
	// var zoomFE = fixedLayers["Landlots"].getZoomForExtent();
	// map.setCenter(new OpenLayers.LonLat(lonLat.lon, lonLat.lat), zoom);
	// });

	var vecLyr = map.getLayersByName('Area Edition')[0];
	map.raiseLayer(vecLyr, map.layers.length);

	// patch por smendoza
	return map;
}

// smendoza
/**
 * Adds a function to the input layer, defining what to do when the event featureadded is generated @ param layer the
 * layer where the event is added
 */
function RegisterFeatureAdded(layer) {
	layer.events.on({
		"featureadded": function (introparam) {
			showCreateAreaForm(introparam); // function defined in angularjs
		},
	});
}
// end smendoza

function addLineString(p1lon, p1lat, p2lon, p2lat, p3lon, p3lat, rlayer) {

	point1 = new OpenLayers.Geometry.Point(p1lon, p1lat).transform(projSource, projDest);
	point2 = new OpenLayers.Geometry.Point(p2lon, p2lat).transform(projSource, projDest);
	point3 = new OpenLayers.Geometry.Point(p3lon, p3lat).transform(projSource, projDest);

	var pointArray = new Array();
	pointArray.push(point1);
	pointArray.push(point2);
	pointArray.push(point3);

	lineFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString(pointArray), null, 'default');
	rlayer.addFeatures([lineFeature]);
	return lineFeature;
}

function addBox(p1lon, p1lat, p2lon, p2lat, blayer) {
	var point1 = new OpenLayers.Geometry.Point(p1lon, p1lat).transform(projSource, projDest);
	var point2 = new OpenLayers.Geometry.Point(p2lon, p2lat).transform(projSource, projDest);
	bounds = new OpenLayers.Bounds();
	bounds.extend(point1);
	bounds.extend(point2);
	bounds.toBBOX();
	var feature = new OpenLayers.Feature.Vector(bounds.toGeometry());
	blayer.addFeatures([feature]);
	map.addLayer(blayer);
	return feature;
}

function addPoint(lon, lat, player) {
	var point = new OpenLayers.Geometry.Point(lon, lat).transform(projSource, projDest);
	var feature = new OpenLayers.Feature.Vector(point, {
		some: 'data'
	}, {
		externalGraphic: './OpenLayers/img/marker.png',
		graphicHeight: 21,
		graphicWidth: 16
	});
	player.addFeatures([feature]);
	map.addLayer(player);
	return feature;
}

function getPolygonVertices() { // array de OpenLayers.Geometry.Point
	var len = vlayer.features.length;

	for (var i = 0; i < len; i++) {
		var id = vlayer.features[i].geometry.id;
		if (id.indexOf("Polygon") !== -1)
			return vlayer.features[i].geometry.getVertices();
	}
	return null;
}

function addPolygon(p1lon, p1lat, p2lon, p2lat, p3lon, p3lat, polayer) {
	var point1 = new OpenLayers.Geometry.Point(p1lon, p1lat).transform(projSource, projDest);
	var point2 = new OpenLayers.Geometry.Point(p2lon, p2lat).transform(projSource, projDest);
	var point3 = new OpenLayers.Geometry.Point(p3lon, p3lat).transform(projSource, projDest);

	var pointArray = new Array();
	pointArray.push(point1);
	pointArray.push(point2);
	pointArray.push(point3);
	var linear_ring = new OpenLayers.Geometry.LinearRing(pointArray);
	var polygonFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]), null, null);
	polayer.addFeatures([polygonFeature]);
	return polygonFeature;
}

function array2Polygon(stringPoints, polayer) {
	if (stringPoints != null && stringPoints.length % 2 == 0) {
		var pointArray = new Array();
		var projDest = new OpenLayers.Projection('EPSG:900913');
		var projSource = new OpenLayers.Projection('EPSG:4326');
		for (var i = 0; i < stringPoints.length; i += 2) {
			point = new OpenLayers.Geometry.Point(stringPoints[i].lng, stringPoints[i + 1].lat);
			point.transform(projSource, projDest);
			pointArray.push(point);
		}
		var linear_ring = new OpenLayers.Geometry.LinearRing(pointArray);
		polygonFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]), null,
			'default');
		polayer.addFeatures([polygonFeature]);
	}
}

function transformVertices(vertices) { // Cada Point contiene como atributos x
	// e y
	var projSource = new OpenLayers.Projection('EPSG:900913');
	var projDest = new OpenLayers.Projection('EPSG:4326');
	var transfPoints = new Array();
	var len = vertices.length;
	for (var i = 0; i < len; i++) {
		var pointTransf = vertices[i].transform(projSource, projDest);
		transfPoints.push(pointTransf);
	}
	return transfPoints;
}

function getLongitude(point) { // devuelve float
	return point.x;
}

function getLatitude(point) { // devuelve float
	return point.y;
}

function point2String(point) { // representación del punto como string separada
	// por coma y espacio. Ej."2.455666...,
	// 43.448948..."
	return point.toShortString();
}

function vertices2String(vertices) {
	var len = vertices.length;
	var result = "";
	if (len > 0) {
		result = point2String(vertices[0]);
		for (var i = 1; i < len; i++) {
			result += ", ";
			result += point2String(vertices[i]);
		}
	}
	return result;
}

function getPolygonParam() {
	var vertices = getPolygonVertices();
	var param = null;
	if (vertices != null) {
		var transfVertices = transformVertices(vertices);
		param = vertices2String(transfVertices);
	}
	return param;
}

/*
 * OTHER FUNCTIONS
 */

/* Ajax funcions */

function AddDataToSelectLayers() {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var stringJson = xmlhttp.responseText;
			if (stringJson != null) {
				var datalayers = JSON.parse(stringJson);
				if (datalayers != null && datalayers != "") {
					var featuresLandlot = JSON.parse(datalayers["Landlots"]);
					AddLayerFeature(fixedLayers["Landlots"], featuresLandlot);
					var featuresBlock = JSON.parse(datalayers["Blocks"]);
					AddLayerFeature(fixedLayers["Blocks"], featuresBlock);
				}
			}
			Log("Loading Complete");
		}
	};
	xmlhttp.open("POST", wsPath_utilsjs + "/GetDataSelection", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send();
}

function createConceptLayers() {
	var element = document.getElementById("tab1ConceptsForm");

	for (var i = 0; i < element.length; i++) {
		var name = element[i].name;
		conceptLayers[name] = new Array();
	}

}

function Log(info) {
	var prevInfo = "";
	prevInfo += document.getElementById("result").innerHTML;
	document.getElementById("result").innerHTML = info + "<p>" + prevInfo + "</p>";
}

function Inf(info) {
	document.getElementById("result").innerHTML = info;
}

function clickConcepts(IdItem, value) {
	Log("Loading data...");
	dataLoadingCounter = 0;
	var modifiedElements = new Array();
	clickConcept(IdItem, value, modifiedElements);
	for (var i = 0; i < modifiedElements.length; i++) {
		changeConcept(modifiedElements[i], modifiedElements.length);
	}
}

function clickConcept(IdItem, value, modifiedElements) {
	var element = document.getElementById(IdItem);
	modifiedElements.push(IdItem);
	// var parentValue = element.checked;
	// var nestedUL = null;
	var children = element.parentElement.children; // Quitar un nivel de parent
	for (var i = 0; i < children.length; i++) {
		var child = children[i];
		if (child.nodeName == "INPUT") {
			if (child.checked != value) {
				child.checked = value;
				// child.onchange();
			}
		} else if (child.nodeName == "UL") {
			var child_li = child.children;
			for (var j = 0; j < child_li.length; j++) {
				clickConcept(child_li[j].children[0].id, value, modifiedElements);
			}
		}
	}
	var sameUriElements = document.getElementsByName(element.name);
	for (var i = 0; i < sameUriElements.length; i++)
		sameUriElements[i].checked = value;
}

function changeConcept(IdItem, numtotal) {
	var element = document.getElementById(IdItem);
	var uriConcept = element.name;
	var sfeatures = getSelectedFeatures();
	for (var i = 0; i < sfeatures.length; i++) {
		var selectedFeature = sfeatures[i];
		if (!conceptLayers.hasOwnProperty(uriConcept))
			conceptLayers[uriConcept] = new Array();
		var layer = null;
		var concept = LayerSelectedFeature(conceptLayers[uriConcept], selectedFeature);
		if (concept != null) {
			layer = concept.layer;
		}
		if (element.checked) {
			if (layer != null) {
				// layer created before, just show it and don't create it again
				showLayer(layer);
				concept.checked = true;
			} else {
				// the layer has not been created before. Create and add it
				// conceptLayers is actualized inside because the ajax call is
				// asynchronous
				CreateLayer(uriConcept, selectedFeature, numtotal);

			}
		} else if (layer != null) { // element unchecked but created previously
			hideLayer(layer);
			concept.checked = false;
		}
	}
}

function sameGeometry(geometry1, geometry2) {
	var vertices1 = geometry1.getVertices();
	var vertices2 = geometry2.getVertices();
	if (vertices1 == null || vertices2 == null)
		return false;
	if (vertices1.length != vertices2.length)
		return false;
	for (var i = 0; i < vertices1.length; i++) { // the comparison includes
		// the order. It would be
		// better not including it
		if (vertices2[i].x != vertices1[i].x)
			return false;
		if (vertices2[i].y != vertices1[i].y)
			return false;
	}
	return true;
}

/**
 * Adds a new Layer to OpenLayers, containing featureCollection information
 * 
 * @param newlayer
 * @param featureCollection
 */
function AddLayerFeature(newlayer, featureCollection) {
	var concept = false;
	var featuresToInsert = new Array();

	if (!IsBaseLayer(newlayer.id) && !IsFixedLayer(newlayer.id))
		concept = true;
	for (var i = 0; i < featureCollection.length; i++) {
		var featureWKT = featureCollection[i].geometry.wkt.split("> ")[1]; // smendoza 20151126, parliament wkt
		// debugging
		var feature = wkt_format.read(featureWKT);
		var properties = featureCollection[i].properties;
		feature.attributes = properties;
		if (concept)
			feature.style = getFeatureStyle("default");
		featuresToInsert.push(feature);
	}
	newlayer.addFeatures(featuresToInsert);
}

/**
 * Function for downloading ontology info from server to Client, parsing the JSON received
 * 
 * @param IdItem
 * @param selectedFeature
 * @param numtotal
 * @returns {OpenLayers.Layer.Vector}
 */
function CreateLayer(IdItem, selectedFeature, numtotal) {
	var id = selectedFeature.id;
	var idFeature = id.match("\\d+"); // polygon identifier used to
	// differentiate between different
	// layers of the same item on
	// layerSwitch control
	var localUri = FilterURL(IdItem, false);
	var newlayer = new OpenLayers.Layer.Vector(localUri + "-" + idFeature);

	// geoJson
	var g = selectedFeature.geometry;

	// wkt
	var wktSelectedFeature = wkt_format.write(selectedFeature);

	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var stringJson = xmlhttp.responseText;
			time2 = new Date().getTime();
			if (stringJson != null && stringJson != "") {
				var featureCollection = JSON.parse(stringJson); // [geometry,
				// {URI,
				// property1,
				// property2,
				// ...}]
				time3 = new Date().getTime();
				if (featureCollection != null && featureCollection != "") {
					// If features not loaded from server, create a new Layer
					AddLayerFeature(newlayer, featureCollection);
				}
				if (newlayer.features != null && newlayer.features.length > 0) {
					// If features already loaded from server, don't create a
					// new Layer just show the already existing Layer
					var item = conceptLayers[IdItem];
					var index = item.length;
					item[index] = new Object();
					item[index].selectedFeature = selectedFeature;
					item[index].layer = newlayer;
					item[index].checked = true;
					showLayer(item[index].layer);
				} else {
					// If any feature loaded, destroy the layer because it has
					// no info
					newlayer.destroy();
					newlayer = null;
				}
			}
			dataLoadingCounter++;
			if (dataLoadingCounter == numtotal)
				Log("Loading Complete");
			time4 = new Date().getTime();
			console.log('Send:' + time1 + ' Response:' + time2 + ' ParseJson:' + (time3 - time2) + ' Process:' +
				(time4 - time3));
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetFeatures", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var time1, time2, time3, time4;
	time1 = new Date().getTime();
	xmlhttp.send("selectedFeature=" + wktSelectedFeature + "&URIItem=" + IdItem);
	return newlayer;
}

/*
 * Returns the concept item of the concept if it has been selected previously with the same feature. Returns null
 * otherwise.
 */
function LayerSelectedFeature(arrayData, selectedFeature) {
	for (var i = 0; i < arrayData.length; i++) {
		if (arrayData[i].selectedFeature.id == selectedFeature.id)
			return arrayData[i];
	}
	return null;
}

/* General functions */

/**
 * Apply as defaults the specified style for OpenLayers
 * 
 * @param type
 * @returns the symbolizer of the style specified
 */
function getFeatureStyle(type) {
	var symbolizer = null;
	if (type == "default") {
		symbolizer = OpenLayers.Util.applyDefaults({
			fillColor: "yellow",
			fillOpacity: 0.4,
			strokeColor: "red",
			pointRadius: 5,
			strokeWidth: "1.5"
		}, OpenLayers.Feature.Vector.style["default"]);
	} else if (type == "popup") {
		symbolizer = OpenLayers.Util.applyDefaults({
			fillColor: "red ",
			fillOpacity: 1,
			strokeColor: "black",
			pointRadius: 5,
			strokeWidth: "1.5"
		}, OpenLayers.Feature.Vector.style["default"]);
	} else if (type == "queryselection") {
		symbolizer = OpenLayers.Util.applyDefaults({
			fillColor: "green",
			fillOpacity: 1,
			strokeColor: "black",
			pointRadius: 5,
			strokeWidth: "1.5"
		}, OpenLayers.Feature.Vector.style["default"]);
	}

	// var styleMap = new OpenLayers.StyleMap({"default": symbolizer, "select":
	// {pointRadius: 30}});
	return symbolizer;
}

function hideLayer(layer) {
	if (layerOnMap(layer.id) != null) {
		map.removeLayer(layer);
		// layer.setVisibility(false);
		layerSwitcher.redraw();
	}
}

function showLayer(layer) {
	var changes = false;
	if (layerOnMap(layer.id) == null) {
		map.addLayer(layer);
		changes = true;
	}
	if (layer.visibility == false) {
		layer.setVisibility(true);
		changes = true;
	}
	if (changes)
		layerSwitcher.redraw();
}

function IsConceptLayer(id) {
	for (var idItem in conceptLayers) {
		var layers = conceptLayers[idItem];
		for (var i = 0; i < layers.length; i++) {
			if (id == layers[i].layer.id)
				return true;
		}
	}
	return false;
}

function TurnOnSelectedLayers() {
	for (var i = 0; i < layerSwitcher.dataLayers.length; i++) {
		var layer = layerSwitcher.dataLayers[i].layer;
		if (IsSelectLayer(layer.id)) {
			layer.setVisibility(true);
		}
	}
	layerSwitcher.checkRedraw();
}

function TurnOffSelectLayers() {
	for (var i = 0; i < layerSwitcher.dataLayers.length; i++) {
		var layer = layerSwitcher.dataLayers[i].layer;
		if (IsSelectLayer(layer.id)) {
			layer.setVisibility(false);
		}
	}
	layerSwitcher.checkRedraw();
}

function IsSelectLayer(id) {
	if (id == fixedLayers["Landlots"].id || id == fixedLayers["Blocks"].id || id == fixedLayers["Area Edition"].id)
		return true;
	return false;
}

function getLayer(id) {
	for (var i = 0; i < map.layers.length; i++) {
		var lay = map.layers[i];
		if (lay.id == id)
			return lay;
	}
	return null;
}

/**
 * Checks if the Layer with id identifier is a base layer
 */
function IsBaseLayer(id) {
	if (id == fixedLayers["Base"].id)
		return true;
	return false;
}

/**
 * Checks if the Layer with id identifier is a fixed Layer
 */
function IsFixedLayer(id) {
	for (var type in fixedLayers) {
		if (fixedLayers[type].id == id)
			return true;
	}
	return false;
}

function getSelectedFeatures() {
	var output = new Array();
	for (var i = 0; i < map.layers.length; i++) {
		var lay = map.layers[i];
		if (IsSelectLayer(lay.id) && lay.visibility == true) {
			output = output.concat(lay.selectedFeatures);
		}
	}
	return output;
}

function layerOnMap(id) {
	for (var i = 0; i < map.layers.length; i++) {
		var lay = map.layers[i];
		if (lay.id == id)
			return lay;
	}
	return null;
}

function mapClean() {
	map.destroy();
	conceptLayers = null;
	wkt_format = null;
	layerSwitcher = null;
	fixedLayers = null;
	projDest = null;
	projSource = null;
}

function HtmlEncode(s) {
	var el = document.createElement("div");
	el.innerText = el.textContent = s;
	s = el.innerHTML;
	return s;
}
//
// function FilterURL(text, isproperty){
// var time1, time2, time3, time4,time5,time6;
// time1 = new Date().getTime();
// if (text == null)
// return "";
// var datatyperegex = new RegExp("\\^\\^.+");
// if (text.match(datatyperegex)){
// return text.replace(datatyperegex,"");
// }
// var urlregex = new RegExp("[^\\s]+://[^\\s]+","gm");
//	
// var tokens = text.split(" ");
// var labels = new Array();
// var time2 = new Date().getTime();
// //document.getElementById("result").innerHTML+="<p>New filterULR:
// "+(time2-time1)+" ";
// for (var i=0;i<tokens.length;i++){
// var url = tokens[i];
// if (url != null && url!= ""){
// //if (url.match(urlregex) != null &&
// url.indexOf("http://www.ibm.com/BCNEcologyINST")==-1){ //es una url y no es
// una instancia (asumo que las instancias no tienen labels)
// if (url.match(urlregex) != null && url.indexOf("bcnInstance")==-1){ //es una
// url y no es una instancia (asumo que las instancias no tienen labels)}
// var label =urllabelmap[url];
// if ( label != null)
// labels.push(label);
// else {
// time5 = new Date().getTime();
// label = AjaxGetLabel(url);
// time6 = new Date().getTime();
// //document.getElementById("result").innerHTML += "New token:
// "+(time6-time5)+" ";
// if ( label != null)
// urllabelmap[url] = label;
// else
// label = url;
// labels.push(label);
// }
// } else { //not a url
// labels.push(url);
// }
// }
// }
// var time3 = new Date().getTime();
// var regex = new RegExp("([^\\s]+://[^\\s]*\#|[^\\s]+://[^\\s]*/)","gm");
// if (labels.length == 0)
// return text.replace(regex,"").trim();
//
// var output=labels[0].replace(regex,"").trim();
// for (var i=1;i<labels.length;i++){
// if (isproperty)
// output += ARROW;
// else
// output += " ";
// output += labels[i].replace(regex,"").trim();
// }
// var time4 = new Date().getTime();
// //document.getElementById("result").innerHTML+=" Ending filterULR:
// "+(time4-time3)+"</p>";
// return output;
// }

function replacer(match, p1, p2, p3, offset, string) {
	var url = p1 + p2;
	var label = urllabelmap[url];
	if (label == null) {
		if (url.indexOf("bcnInstance") != -1 && url.indexOf("measureInstance") != -1)
			label = p2;
		else {
			label = AjaxGetLabel(url);
		}
		urllabelmap[url] = label;
	}
	if (p3.localeCompare(" ") == 0)
		label += ARROW;
	return label.trim();
}

function FilterURL(text, isproperty) {
	if (text == null)
		return "";
	var datatyperegex = new RegExp("\\^\\^.+", "g");
	if (text.match(datatyperegex)) {
		text = text.replace(datatyperegex, "");
	}
	var regex = new RegExp("(\\w+://[^\\s]*[\#/])([^\\s]+)(\\s?)", "g");
	var output = text.replace(regex, replacer);
	return output;
}

function InstanceInFeatures(instance_uri, features) {
	if (features == null)
		return false;
	for (var i = 0; i < features.length; i++) {
		var feat = features[i];
		var uri = feat.attributes["URI"];
		if (uri != null && uri == instance_uri)
			return true;
	}
	return false;
}

function IsEmpty(object) {
	if (object == null)
		return true;
	if (object.length == 0)
		return true;
	var content = false;
	for (var att in object)
		if (object[att] != null && object[att].length > 0)
			content = true;
	return content;
}

/* Checkbox events */

function SynchronizeCheckboxConceptLayers(selectedFeature) {
	for (var idItem in conceptLayers) {
		// var docElement =document.getElementById(idItem); //all the elements
		// con that uri in name
		var docElements = document.getElementsByName(idItem);
		var arrayLayer = conceptLayers[idItem];
		var exist = false;
		if (arrayLayer != null) {
			for (var i = 0; i < arrayLayer.length; i++) {
				if (arrayLayer[i].selectedFeature.id == selectedFeature.id && arrayLayer[i].checked == true) {
					exist = true;
					for (var j = 0; j < docElements.length; j++) {
						if (!docElements[j].checked) {
							docElements[j].checked = true;
							// docElements[j].changeConcept();
							changeConcept(docElements[j].id);
						}
					}
				}
			}
		}
		if (!exist) {
			for (var j = 0; j < docElements.length; j++) {
				if (docElements[j].checked) {
					docElements[j].checked = false;
					// docElements[j].changeConcept();
					changeConcept(docElements[j].id);
				}
			}
		}
	}
}

/* Selection and Popup Events */

function propertyValues2String(arrayvalues) {
	var stringprop = "";
	if (Object.prototype.toString.call(arrayvalues) != '[object Array]') {
		stringprop = FilterURL(arrayvalues, false);
	} else {
		stringprop += FilterURL(arrayvalues[0], false);
		for (var i = 1; i < arrayvalues.length; i++) {
			var value = FilterURL(arrayvalues[i], false);
			stringprop += ", " + value;
		}
	}
	return stringprop;
}

function getFeatureInfo(feature) {
	var output = "";
	var attributes = feature.attributes;
	var attArray = new Array();
	var cont = 0;
	var URI = null;
	for (var att in attributes) {
		if (att == "URI")
			URI = attributes[att];
		else {
			var arrayvalues = attributes[att];
			var property = FilterURL(att, true);
			if (property.indexOf("hasGeometry") == -1) {
				attArray[cont++] = property + " : " + propertyValues2String(arrayvalues);
			}
		}
	}
	attArray.sort();
	attArray.unshift("<strong>" + URI + "</strong>");
	for (var i = 0; i < attArray.length; i++) {
		output += attArray[i] + "<br/>";
	}
	return output;
}

function featureclick(e) {
	instanceClick(e.feature);
}

function instanceClick(feature) {
	// popup = new
	// OpenLayers.Popup.FramedCloud("chicken",e.feature.geometry.getBounds().getCenterLonLat(),
	// new OpenLayers.Size(200, 200), "<div style=\"overflow:
	// auto\">hello</div>", null, true, test);
	popup = new OpenLayers.Popup.FramedCloud(feature.id, feature.geometry.getBounds().getCenterLonLat(), null,
		"<div style=\"overflow: auto\">" + getFeatureInfo(feature) + "</div>", null, true, onPopupClose);
	popup.panMapIfOutOfView = true;
	popup.minSize = new OpenLayers.Size(50, 50); // Tamaño mínimo de la
	// ventana
	popup.maxSize = new OpenLayers.Size(500, 200); // Tamaño máximo
	popup.autoSize = true; // La ventana a ajusta al texto mientras no supere
	// el tamaño máximo establecido.
	feature.popup = popup;
	map.addPopup(popup);
	var infoPopup = new Object();
	infoPopup.feature = feature;
	infoPopup.previousStyle = feature.style;
	popupFeatures[popup.id] = infoPopup;
	// popupFeatures[popup.id]=feature;
	feature.style = getFeatureStyle("popup");
	// feature.style.strokeColor ="red";
	feature.layer.redraw();
}

function onPopupClose(evt) {
	if (popupFeatures != null && popupFeatures[this.id] != null) {
		var feature = popupFeatures[this.id].feature;
		feature.style = popupFeatures[this.id].previousStyle;
		delete popupFeatures[this.id];
		feature.layer.redraw();
	}
	map.removePopup(this);
	this.destroy();
}

function UnRegisterPopupEvent(layer) {
	var listen = layer.events.listeners;
	if (listen["featureclick"] != null && listen["featureclick"].length > 0)
		layer.events.unregister("featureclick", map, featureclick);
}

function RegisterPopupEvent(layer) {
	var listen = layer.events.listeners;
	if (listen["featureclick"] == null || listen["featureclick"].length == 0)
		layer.events.register("featureclick", map, featureclick, true);
}

function RegisterSelectEvent(layer) {
	layer.events.on({
		"featureselected": function (e) {
			// selectedFeature = e.feature;
			// vControl.clickoutFeature();
			SynchronizeCheckboxConceptLayers(e.feature);
		},
		"featureunselected": function (e) {
			// SynchronizeCheckboxConceptLayers(e.feature);
		}
	});
}

// each time a layer is selected/unselected on Switch control
function mapLayerChanged(event) {
	var activate = false;
	// selectControl.unselectAll();
	var eventlayer = event.layer;
	var layers = layerSwitcher.layerStates;
	if (eventlayer == null) { // se han eliminado todas las capas
		activate = true;
	} else if (eventlayer.getVisibility() == true && IsSelectLayer(eventlayer.id)) {
		activate = true;

	} else {
		if (layers != null) {
			for (var i = 0; i < layers.length; i++) {
				if (IsSelectLayer(layers[i].id)) {
					if (layers[i].visibility == true && layers[i].id != eventlayer.id) {
						activate = true;
						break;
					}
				}
			}
		}
	}
	if (activate && selectControl.active == false) { // active select
		// Feature, deactive
		// popups
		selectControl.activate();
		for (var i = 0; i < layers.length; i++) {
			if (IsConceptLayer(layers[i].id)) {
				UnRegisterPopupEvent(getLayer(layers[i].id));
			}
		}
	} else if (!activate && selectControl.active == true) { // deactive select
		// feature, active
		// popups on click
		// event
		selectControl.deactivate();
		for (var i = 0; i < layers.length; i++) {
			if (IsConceptLayer(layers[i].id)) {
				RegisterPopupEvent(getLayer(layers[i].id));
			}
		}

	}
}

/*
 * Tab1 Urban Concepts
 */
function AjaxGetUrbanConcepts() {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var urbanconcepts = xmlhttp.responseText;
			// var xmlText = document.createTextNode(qresults);
			// document.getElementById("result").appendChild(xmlText);
			var time2 = new Date().getTime();
			document.getElementById("tab1ConceptsForm").innerHTML = urbanconcepts;
			// createConceptLayers();
			var time3 = new Date().getTime();
			// alert('Time1:'+time1+' Time2:'+time2+' Time3:'+time3);
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetUrbanConcepts", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	// xmlhttp.send("selectedFeature="+geojsonSelectedFeature+"&IdItem="+IdItem);
	var time1 = new Date().getTime();
	xmlhttp.send("");
}

/*
 * Tab1 Search concepts
 */
function tab1SearchConcepts() {
	tab1ClearSelection();
	CloseTree();
	var inputText = document.getElementById("tab1inputsearch").value;
	if (inputText != "") {
		var regex = new RegExp(inputText, "i");
		var container = document.getElementById("tab1Concepts");
		var arrayli = container.getElementsByTagName("li");
		var matches = 0;
		for (var i = 0; i < arrayli.length; i++) {
			// var name = arrayli[i].getAttribute("name");
			var name = null;
			var childs = arrayli[i].childNodes;
			for (var j = 0; j < childs.length; j++) {
				if (childs[j].className == "tab1mainconcepts") {
					name = childs[j].innerText;
					break;
				}
				if (childs[j].nodeName == "#text") {
					name = childs[j].data;
					break;
				}
			}
			if (regex.test("*." + name + ".*")) {
				// if (name.indexOf(inputText) != -1){
				ExpandElementTree(arrayli[i]);
				matches++;
				arrayli[i].setAttribute("class", "selected");
				if (matches == 1)
					container.scrollTop = arrayli[i].offsetTop - 30;
				// for (var j=0;j< arrayli[i].children.length;j++){
				// var child = arrayli[i].children[j];
				// if (child.nodeName=="INPUT"){
				// if (child.checked != true){
				// child.checked = true;
				//							
				// child.onclick();
				// }
				// }
				// }
			} else {
				arrayli[i].setAttribute("class", "noselected");
			}
		}

	}
}

function CloseTree() {
	var mcarray = document.getElementsByClassName("tab1mainconcepts");
	if (mcarray != null) {
		for (var i = 0; i < mcarray.length; i++) {
			closeTreeMainConcept(mcarray[i]);
		}
	}
}

function ExpandTree() {
	var mcarray = document.getElementsByClassName("tab1mainconcepts");
	if (mcarray != null) {
		for (var i = 0; i < mcarray.length; i++) {
			expandTreeMainConcept(mcarray[i]);
		}
	}
}

function ExpandElementTree(liElement) {
	var parent = liElement.parentNode;
	while (parent != null && parent != document) {
		if (parent.nodeName != null && parent.nodeName == "LI") {
			var divMc = parent.getElementsByClassName("tab1mainconcepts");
			if (divMc != null && divMc.length == 1) {
				expandTreeMainConcept(divMc[0]);
				return;
			}
		}
		parent = parent.parentNode
	}
}

function closeTreeMainConcept(mc) {
	var arrayul = mc.parentNode.getElementsByTagName("ul");
	for (var j = 0; j < arrayul.length; j++) {
		if (arrayul[j].style.display == "block")
			arrayul[j].style.display = "none";
	}
}

function expandTreeMainConcept(mc) {
	var arrayul = mc.parentNode.getElementsByTagName("ul");
	for (var j = 0; j < arrayul.length; j++) {
		if (arrayul[j].style.display == "none")
			arrayul[j].style.display = "block";
	}
}

function clickTab1MainConcept(mc) {
	var arrayul = mc.parentNode.getElementsByTagName("ul");
	for (var j = 0; j < arrayul.length; j++) {
		if (arrayul[j].style.display == "none")
			arrayul[j].style.display = "block";
		else if (arrayul[j].style.display == "block")
			arrayul[j].style.display = "none";
	}
}

function tab1ClearSelection() {
	var container = document.getElementById("tab1Concepts");
	var arrayli = container.getElementsByTagName("li");
	for (var i = 0; i < arrayli.length; i++) {
		arrayli[i].setAttribute("class", "noselected");
		// for (var j=0;j< arrayli[i].children.length;j++){
		//				
		// var child = arrayli[i].children[j];
		// if (child.nodeName=="INPUT"){
		//					
		// if (child.checked != false){
		//						
		// child.checked = false;
		// child.onclick();
		// }
		// }
		// }
	}
}

/*
 * Select Tab
 */

function FeaturesToDefaultStyle() {
	for (var uri in conceptLayers) {
		var infoclass = conceptLayers[uri];
		for (var i = 0; i < infoclass.length; i++) {
			var item = infoclass[i];
			if (item.checked)
				ShowTab2QueryInstances(item.layer.features, getFeatureStyle("default"));
		}
	}
	ConceptLayersRedraw();
}

function SelectTab(anchor) {
	var tab1 = document.getElementById("tab1");
	var tab15 = document.getElementById("tab15");
	var tab2 = document.getElementById("tab2");
	var tab3 = document.getElementById("tab3");
	var tabIndicators = document.getElementById("tabIndicators");

	var selection_tab1 = document.getElementById("tab1_queryselected");
	var selection_tabIndicators = document.getElementById("tabIndicators_queryselected");
	var selection_tab15 = document.getElementById("tab15_queryselected");
	var selection_tab2 = document.getElementById("tab2_queryselected");
	var selection_tab3 = document.getElementById("tab3_queryselected");

	if (anchor.id == selection_tab1.id) {
		// Urban Concepts Selected
		Inf(messageTab1);
		TurnOnSelectedLayers();
		FeaturesToDefaultStyle();
		tab1.style.display = "block";
		tabIndicators.style.display = "none";
		tab15.style.display = "none";
		tab2.style.display = "none";
		tab3.style.display = "none";
		selection_tab1.attributes.class.value = "selected";
		selection_tabIndicators.attributes.class.value = "noselected";
		selection_tab15.attributes.class.value = "noselected";
		selection_tab2.attributes.class.value = "noselected";
		selection_tab3.attributes.class.value = "noselected";
	} else if (anchor.id == selection_tab15.id) {
		// Related Concepts Selected
		Inf(messageTab15);
		TurnOffSelectLayers();
		FeaturesToDefaultStyle();
		// tab1.style.display = "none";
		// tab15.style.display = "block";
		// tabIndicators.style.display = "none";
		// tab2.style.display = "none";
		// tab3.style.display = "none";
		// selection_tab1.attributes.class.value = "noselected";
		// selection_tabIndicators.attributes.class.value = "noselected";
		// selection_tab15.attributes.class.value = "selected";
		// selection_tab2.attributes.class.value = "noselected";
		// selection_tab3.attributes.class.value = "noselected";
		existentIndividuals_aux = tabListClass("tab15class", "tab15");
		if (!tab15clicked || !SameContent(existentIndividuals_aux, existentIndividuals)) {
			existentIndividuals = existentIndividuals_aux;
			UpdateRelatedClasses(null);
			// CreateTab15PropertyValuesClass();
			relatedConcepts = null;
			// AjaxGetRelatedConcepts();
		}
		tab15clicked = true;
	} else if (anchor.id == selection_tabIndicators.id) {
		// Indicators Selected
		Inf(messageTabIndicators);
		TurnOffSelectLayers();
		FeaturesToDefaultStyle();
		tab1.style.display = "none";
		tabIndicators.style.display = "block";
		tab15.style.display = "none";
		tab2.style.display = "none";
		tab3.style.display = "none";
		selection_tab1.attributes.class.value = "noselected";
		selection_tabIndicators.attributes.class.value = "selected";
		selection_tab15.attributes.class.value = "noselected";
		selection_tab2.attributes.class.value = "noselected";
		selection_tab3.attributes.class.value = "noselected";
		AjaxGetIndicators();
	} else if (anchor.id == selection_tab2.id) {
		// Data Query Selected
		Inf(messageTab2);
		TurnOffSelectLayers();
		FeaturesToDefaultStyle();
		tab1.style.display = "none";
		tab15.style.display = "none";
		tabIndicators.style.display = "none";
		tab2.style.display = "block";
		tab3.style.display = "none";
		selection_tab1.attributes.class.value = "noselected";
		selection_tabIndicators.attributes.class.value = "noselected";
		selection_tab15.attributes.class.value = "noselected";
		selection_tab2.attributes.class.value = "selected";
		selection_tab3.attributes.class.value = "noselected";
		existentIndividuals_aux = tabListClass("tab2class", "tab2");
		if (!tab2clicked || !SameContent(existentIndividuals_aux, existentIndividuals)) {
			existentIndividuals = existentIndividuals_aux;
			UpdateInstancesClass(null);
			createConceptProperties(existentIndividuals);
			createQueryForm(existentIndividuals);
		}
		tab2clicked = true;
		// console.log('Init tab2: '+time0+' TurnOffLayers:'+time05+'
		// StyleFeatures: '+time1+' ExistentIndividuals:'+time2+'
		// Properties:'+time3+' Form:'+time4);
	} else if (anchor.id == selection_tab3.id) {
		// Sparql Query Selected
		Inf(messageTab3);
		TurnOffSelectLayers();
		FeaturesToDefaultStyle();
		tab1.style.display = "none";
		tab15.style.display = "none";
		tabIndicators.style.display = "none";
		tab2.style.display = "none";
		tab3.style.display = "block";
		selection_tab1.attributes.class.value = "noselected";
		selection_tabIndicators.attributes.class.value = "noselected";
		selection_tab15.attributes.class.value = "noselected";
		selection_tab2.attributes.class.value = "noselected";
		selection_tab3.attributes.class.value = "selected";
		existentIndividuals = getExistentIndividuals();
	}
}

function SameContent(indArray1, indArray2) {
	if (Object.keys(indArray1).length != Object.keys(indArray2).length)
		return false;
	for (var uri in indArray1) {
		if (indArray2[uri] == null)
			return false;
		if (indArray2[uri].length != indArray1[uri].length)
			return false;
		var features1 = indArray1[uri];
		var features2 = indArray2[uri];
		for (var i = 0; i < features1.length; i++) {
			if (features1[i].id != features2[i].id)
				return false;
		}
	}
	return true;
}

/*
 * Tab2 Search Concepts
 */

function getExistentIndividuals() {
	delete existentIndividuals;
	existentIndividuals = new Object();
	for (var uri in conceptLayers) {
		var checked = false;
		var array = conceptLayers[uri];
		for (var i = 0; i < array.length; i++) {
			if (array[i].checked == true) {
				if (existentIndividuals[uri] == null) {
					existentIndividuals[uri] = new Array();
				}
				if (array[i].layer.features != null) {
					for (var j = 0; j < array[i].layer.features.length; j++) {
						var instance_uri = array[i].layer.features[j].attributes["URI"];
						if (!InstanceInFeatures(instance_uri, existentIndividuals[uri]))
							existentIndividuals[uri].push(array[i].layer.features[j]);
					}
				}
			}
		}
	}
	return existentIndividuals;
}

function tabListClass(idOutput, tab) {
	// delete existentIndividuals;
	var existentIndividuals_aux = new Object();
	var list = "<table cellspacing='12' class=\"table\"><col width='50%' align='left'><tr><td><strong>Classes</strong>";
	for (var uri in conceptLayers) {
		var checked = false;
		var array = conceptLayers[uri];
		for (var i = 0; i < array.length; i++) {
			if (array[i].checked == true) {
				if (existentIndividuals_aux[uri] == null) {
					existentIndividuals_aux[uri] = new Array();
					var element = FilterURL(uri, false);
					element = "<a class='noselected' name='class" + tab + "' id='" + tab + uri +
						"'onclick='clickClass(\"" + uri + "\",\"" + tab +
						"\")' style='cursor: pointer; cursor: hand;'>" + element + "</a>";
					list = list + " <tr><td>" + element + "</td></tr>";
				}
				if (array[i].layer.features != null) {
					for (var j = 0; j < array[i].layer.features.length; j++) {
						var instance_uri = array[i].layer.features[j].attributes["URI"];
						if (!InstanceInFeatures(instance_uri, existentIndividuals_aux[uri]))
							existentIndividuals_aux[uri].push(array[i].layer.features[j]);
					}
				}
			}
		}
	}
	list = list + " </table>";
	document.getElementById(idOutput).innerHTML = list;
	// UpdateInstancesClass(null);

	return existentIndividuals_aux;
}

function tab2ClickProperty(uriClass) {
	// var list ="<ul class='noindent'>";
	// var list ="<p>";
	// var list = "<table cellspacing='5'>";
	var list = "";
	var property = document.getElementById("tab2selectpropbutton").value;
	if (property != "") {
		var features = existentIndividuals[uriClass];
		if (features != null) {
			for (var i = 0; i < features.length; i++) {
				var feat = features[i];
				var uriIns = feat.attributes["URI"];
				var propvalue = feat.attributes[property];
				var value = "&lt;No Property&gt;";
				if (propvalue != null)
					value = propertyValues2String(propvalue);
				document.getElementById("tab2td" + uriIns).childNodes[1].innerHTML = value;
			}
		}
	}
	// list +="</p>";
	// list = list+" </ul>";
	// document.getElementById("tab2instproplist").innerHTML = list;
}

function featureInQueryList(uriClass, feature) {
	// var foundclass = false;
	for (var index in querylist)
		if (querylist[index].class == uriClass) {
			foundclass = true;
			listf = querylist[index].features;
			if (listf != null)
				if (listf.indexOf(feature) != -1)
					return true;
		}
	// for (var i=0;i<listf.length;i++)
	// if (listf[i].id == feature.id)
	// return true;

	// if (!foundclass)
	// return true;
	return false;
}

function UpdateInstancesClass(uriClass) {
	// var list = "<strong>Instances "+FilterURL(uriClass, false)+"</strong>";
	var list = "<table cellspacing='12' class=\"table\"><col width='50%' align='right'><col width='50%' align='right'><tr><td><strong>Instances</strong></td><td id='tab2instpropbutton'><td></tr>";
	// list += "<p>";
	if (uriClass != null) {
		var features = existentIndividuals[uriClass];
		if (features != null) {
			for (var i = 0; i < features.length; i++) {
				var feat = features[i];
				var uri = feat.attributes["URI"];
				var showinstance = FilterURL(uri, false);
				if (featureInQueryList(uriClass, feat))
					showinstance = "<div class='highlight'>" + showinstance + "</div>";
				element = "<a name='" + uri + "' onclick=\"clickInstance('" + uriClass + "','" + uri +
					"')\" style='cursor: pointer; cursor: hand;'>" + showinstance + "</a>";
				// list = list + " <tr id='tab2td" + uri + "'><td>" + element
				// + "</td><td></td></tr>";
				list = list + " <tr  id='tab2td" + uri + "'><td>" + element + "</td></tr>"; // @smendoza: no se que hace
				// <td></td> aqui
				// console.log('Tab2InsideClickClass_FeatureInit:'+time1+'
				// Tab2InsideClickClass_FilterURL:'+time2+'
				// Tab2InsideClickClass_AskQueryList:'+time3);
			}
		}
	}
	list = list + " </table>";
	document.getElementById("tab2inst").innerHTML = list;
	document.getElementById("tab2inst").setAttribute("name", uriClass);
	UpdatePropertyValuesClass(uriClass);
}

function UpdatePropertyValuesClass(uriClass) {
	var prop = "";
	// prop = "<table cellspacing='5'><tr><td>";
	if (uriClass != null) {
		prop += "<div class='styled-select'><select class='reduce' id='tab2selectpropbutton' onchange=\"tab2ClickProperty('" +
			uriClass + "')\">";
		prop += "<option value=\"\">&lt;Property&gt;</option>";
		var properties = conceptProperties[uriClass];
		if (properties != null) {
			properties.sort();
			for (var i = 0; i < properties.length; i++) {
				prop += "<option value=\"" + properties[i] + "\">" + FilterURL(properties[i], true) + "</option>";
			}
		}
	} else {
		prop += "<div class='styled-select'><select class='reduce' id='tab2selectpropbutton' onchange=\"tab2ClickProperty('" +
			uriClass + "')\">";
		prop += "<option value=\"\">&lt;Property&gt;</option>";
	}
	// prop +="</select><hr/></td></tr></table>";
	prop += "</select></div>";
	document.getElementById("tab2instpropbutton").innerHTML = prop;
	// document.getElementById("tab2instproplist").innerHTML="";
}

function getSelectedTabClass(tab) {
	var elements = document.getElementsByName("class" + tab);
	for (var i = 0; i < elements.length; i++)
		if (elements[i].getAttribute("class") == "selected")
			return elements[i].getAttribute("id").replace(tab, "");
	return null;
}

function clickClass(uriClass, tab) {
	var elements = document.getElementsByName("class" + tab);
	for (var i = 0; i < elements.length; i++)
		elements[i].setAttribute("class", "noselected");
	document.getElementById(tab + uriClass).setAttribute("class", "selected");
	if (tab == "tab2") {
		var time1 = new Date().getTime();
		UpdateInstancesClass(uriClass);
		var time2 = new Date().getTime();
		UpdatePropertyValuesClass(uriClass);
		var time3 = new Date().getTime();
		console.log('Tab2BeforeListInstances:' + time1 + ' Tab2AfterList:' + time2 + ' Tab2AfterListProperties:' +
			time3);
	} else if (tab == "tab15") {
		AjaxGetRelatedConcepts();
	}
}

function clickInstance(uriClass, uriInstance) {
	var features = existentIndividuals[uriClass];
	if (features != null) {
		for (var j = 0; j < features.length; j++) {
			var feat = features[j];
			var uri = feat.attributes["URI"];
			if (uri == uriInstance)
				instanceClick(feat);
		}
	}
}

function createConceptProperties(existentIndividuals) {
	for (var uri in existentIndividuals) {
		conceptProperties[uri] = new Array();
		var features = existentIndividuals[uri];
		if (features != null) {
			for (var j = 0; j < features.length; j++) {
				var feat = features[j];
				var attributes = feat.attributes;
				if (attributes != null) {
					for (var attname in attributes) {
						var name = attname;
						if (conceptProperties[uri].indexOf(name) == -1)
							conceptProperties[uri].push(name);
					}
				}
			}
		}
	}
}

// showProperties('"+idClass+"','"+idProp+"');

function createTab2QueryLineA(number) {
	var form = "<div id='tab2querylineA'>";
	form += "<form onsubmit=\"return false;\">";
	form += "<input type='checkbox' name='tab2qcheck' value='" + number + "' id='tab2qcheck" + number +
		"' onchange='ShowQueryResult()'/>";
	form += "<i>Query " + number + "</i>:&nbsp;";
	form += " ";
	var idClass = "tab2c" + number;
	var idProp = "tab2p" + number;
	form += "<div class='styled-select'><select class='reduce' id='" + idClass + "' onchange=\"tab2AClickSelect('" +
		number + "','class')\">";
	form += "<option value=\"\">&lt;Class&gt;</option>";
	for (var uriClass in existentIndividuals) {
		form += "<option value=\"" + uriClass + "\">" + FilterURL(uriClass, false) + "</option>";
	}
	form += "</select></div>";

	form += " ";
	form += "<div class='styled-select'><select class='reduce' id='" + idProp + "' onchange=\"tab2AClickSelect('" +
		number + "','property');ShowQueryResult()\">";
	form += "<option value=\"\">&lt;Property&gt;</option>";
	form += "</select></div>";

	// form+="<input size='5' type='text' id='"+idValue+"'/>";

	form += " ";
	form += "<div class='styled-select'><select class='reduce' id='tab2o" + number + "' onchange=\"tab2AClickSelect('" +
		number + "','operator');ShowQueryResult()\">";
	form += "<option value=\"\">&lt;Operator&gt;</option>";
	form += "<option value=\"\">No operator</option>";
	for (var i = 0; i < groupOp.length; i++) {
		form += "<option value=\"" + groupOp[i] + "\">" + groupOp[i] + "</option>";
	}
	form += "</select></div>";
	form += "</form>";
	form += "</div>"
	return form;
}

function updateProperties(condnumber) {
	var query = querylist[condnumber];
	showProperties("tab2c" + condnumber, "tab2p" + condnumber);
	for (var condB in query.conditions) {
		showProperties("tab2c" + condnumber, "tab2p" + condB);
	}

}

function tab2AClickSelect(condnumber, type) {
	switch (type) {
		case "class":
			querylist[condnumber].class = document.getElementById("tab2c" + condnumber).value;
			updateProperties(condnumber);
			break;
		case "property":
			querylist[condnumber].prop = document.getElementById("tab2p" + condnumber).value;
			break;
		case "operator":
			querylist[condnumber].op = document.getElementById("tab2o" + condnumber).value;
			break;
	}
	// if (querylist[condnumber].conditions == null ||
	// querylist[condnumber].conditions.length ==0)
	// querylist[condnumber].nextbvalue =
	// document.getElementById("tab2n"+condnumber).value; //It is taken the
	// value of the next button in the last AND condition
}

function tab2BClickSelect(condnumber, type) {
	for (var condA in querylist)
		for (condB in querylist[condA].conditions) {
			if (condB == condnumber)
				switch (type) {
					case "property":
						querylist[condA].conditions[condB].prop = document.getElementById("tab2p" + condnumber).value;
						break;
					case "operator":
						querylist[condA].conditions[condB].op = document.getElementById("tab2o" + condnumber).value;
						break;
					case "value":
						querylist[condA].conditions[condB].value = document.getElementById("tab2v" + condnumber).value;
						break;
				}
			// querylist[condA].nextbvalue =
			// document.getElementById("tab2n"+condB).value; //It is taken the
			// value of the next button in the last AND condition
		}
}

function tab2NextClickSelect(condnumber) {
	for (var condA in querylist) {
		if (condnumber == condA &&
			(querylist[condA].conditions == null || Object.keys(querylist[condA].conditions).length == 0)) {
			querylist[condA].nextbvalue = document.getElementById("tab2n" + condnumber).value; // It is taken the value
			// of the next
			// button in the last AND condition
			return;
		}
		for (condB in querylist[condA].conditions) {
			if (condB == condnumber)
				if ((parseInt(condA) + Object.keys(querylist[condA].conditions).length) == condnumber) {
					querylist[condA].nextbvalue = document.getElementById("tab2n" + condnumber).value; // It
					// is
					// taken
					// the
					// value
					// of
					// the
					// next
					// button
					// in
					// the
					// last
					// AND
					// condition
					return;
				}
		}
	}
}

function createTab2QueryLineB(number, uriclass) {
	var form = "<div id='tab2querylineB'>";
	form += "<form onsubmit=\"return false;\">";
	form += "<input type='checkbox' name='tab2qcheck' value='" + number + "' id='tab2qcheck" + number +
		"' onchange='ShowQueryResult()'/>";
	form += "<i>Condition " + number + "</i>:&nbsp";
	form += " ";

	form += "<div class='styled-select'><select class='reduce' id='tab2p" + number + "' onchange=\"tab2BClickSelect('" +
		number + "','property');ShowQueryResult()\">";
	form += "<option value=\"\">&lt;Property&gt;</option>";
	var properties = conceptProperties[uriclass];
	if (properties != null) {
		properties.sort();
		for (var i = 0; i < properties.length; i++) {
			form += "<option value=\"" + properties[i] + "\">" + FilterURL(properties[i], true) + "</option>";
		}

	}

	form += "</select></div>";

	var selectClassId = "tab2c" + getConditionA(number);
	// showProperties(selectClassId, "tab2p"+number)
	form += " ";
	form += "<div class='styled-select'><select class='reduce' id='tab2o" + number + "' onchange=\"tab2BClickSelect('" +
		number + "','operator');ShowQueryResult()\">";
	form += "<option value=\"\">&lt;Operand&gt;</option>";
	for (var i = 0; i < compOp.length; i++) {
		form += "<option value=\"" + compOp[i] + "\">" + compOp[i] + "</option>";
	}
	form += "</select></div>";

	form += "<input size='5' type='text' id='tab2v" + number + "' onchange=\"tab2BClickSelect('" + number +
		"','value');ShowQueryResult()\"/>";

	form += "</form>";
	form += "</div>"
	return form;
}

function numberConditions() {
	var count = 0;
	for (var n in querylist) {
		count++;
		for (var c in querylist[n].conditions)
			count++;
	}
	return count;
}

function ShowNextQuery(condnumber) {
	var count = numberConditions();
	if (condnumber == count) {
		var nextselect = document.getElementById("tab2n" + count);
		var selection = nextselect.value;
		var newline;
		if (selection == "AND" || selection == "OR")
			newline = createTab2QueryLineB(count + 1, querylist[getConditionA(count)].class);
		else
			newline = createTab2QueryLineA(count + 1);

		var newNode = document.createElement('div');
		newNode.innerHTML = newline;
		document.getElementById("tab2querylines").appendChild(newNode);

		var next = createQueryNext(count + 1);
		disableSelectElement("tab2n" + count, selection);
		var newNode = document.createElement('div');
		newNode.innerHTML = next;
		document.getElementById("tab2querynext").appendChild(newNode);

		// document.getElementById("tab2querynext").innerHTML += next;

		SaveQuery(count + 1);
	}
}

function disableSelectElement(id, value) {
	// var element = document.getElementById(id);
	// if (value == "AND"){
	// element.value = value;
	// element.disabled = true;
	// }
}

// disableNextSelect("+count+");
function createQueryNext(count) {
	var form = "";
	form += "<div id='tab2querynextline'><div class='styled-select'><select class='reduce' id='tab2n" + count +
		"' onchange=\"tab2NextClickSelect('" + count + "');ShowNextQuery('" + count + "');ShowQueryResult()\">";
	form += "<option value=\"\">&lt;Next&gt;</option>";
	form += "<option value=\"Next\">Next Query</option>";
	form += "<option value=\"AND\">AND</option>";
	form += "<option value=\"OR\">OR</option>";
	for (var i = 0; i < arithOp.length; i++) {
		form += "<option value=\"" + arithOp[i] + "\">" + arithOp[i] + "</option>";
	}
	form += "</select></div></div>";
	return form;
}

function compareValues(value1, operator, value2) {
	var number1 = null;
	var number2 = null;
	if (isNumber(value1) && isNumber(value2)) {
		number1 = parseFloat(value1);
		number2 = parseFloat(value2);
	}
	if (number1 != null && number2 != null) {
		value1 = number1;
		value2 = number2;
	} else {
		value1 = FilterURL(value1);
		value2 = FilterURL(value2);
	}
	switch (operator) {
		case "<":
			if (value1 < value2)
				return true;
			break;
		case ">":
			if (value1 > value2)
				return true;
			break;
		case "<=":
			if (value1 <= value2)
				return true;
			break;
		case ">=":
			if (value1 >= value2)
				return true;
			break;
		case "!=":
			if (value1 !== value2)
				return true;
			break;
		case "==":
			if (value1 == value2)
				return true;
			break;
	}
	return false;
}

function FilterCondition(features, uriprop, compOp, value) {
	if (features == null)
		return null;
	var filter_feat = new Array();
	for (var i = 0; i < features.length; i++) {
		var values_att = features[i].attributes[uriprop];
		if (Object.prototype.toString.call(values_att) != '[object Array]') {
			if (values_att != null && compareValues(values_att, compOp, value))
				filter_feat.push(features[i]);
		} else {
			var check = false;
			for (var j = 0; j < values_att.length; j++) {
				var value_att = values_att[j];
				if (value_att != null && compareValues(value_att, compOp, value)) {
					check = true;
					break;
				}
			}
			if (check)
				filter_feat.push(features[i]);
		}
	}
	return filter_feat;
}

function ConceptLayersRedraw() {
	for (var uriclass in conceptLayers) {
		var infoclass = conceptLayers[uriclass];
		for (var i = 0; i < infoclass.length; i++) {
			var item = infoclass[i];
			if (item.checked)
				item.layer.redraw();

		}
	}
}

function ShowTab2QueryInstances(features, style) {
	var features_id = new Array();
	var layers_id = new Array();
	// var popupStyle = getFeatureStyle("popup");

	if (features != null) {
		for (var i = 0; i < features.length; i++) {
			// if (features_id.indexOf(features[i].id) == -1){
			// features_id.push(features[i].id);
			features[i].style = style;
			// }

			// if (layers_id.indexOf(features[i].layer.id == -1)){
			// layers_id.push(features[i].layer.id);
			// features[i].layer.redraw();
			// }
		}
		// layer.redraw();
	}
}

function ShowQueryDescription() {
	var description = "";
	for (var condA in querylist) {
		if (document.getElementById("tab2qcheck" + condA).checked) {
			var cl = FilterURL(querylist[condA].class, false);
			var propA = FilterURL(querylist[condA].prop, true);
			var opA = querylist[condA].op;
			var conditions = "";
			var lastoperator = document.getElementById("tab2n" + condA).value;
			for (var condB in querylist[condA].conditions) {
				if (document.getElementById("tab2qcheck" + condB).checked) {
					var propB = FilterURL(querylist[condA].conditions[condB].prop, true);
					var opB = querylist[condA].conditions[condB].op;
					var value = querylist[condA].conditions[condB].value;
					conditions += " " + lastoperator + " " + propB + opB + value;
					lastoperator = document.getElementById("tab2n" + condB).value;
				}
			}
			if (conditions == "")
				conditions = "no constraints";
			else
				conditions = "(All Selected Features " + conditions + ")";
			// else
			// conditions = conditions.slice(0,conditions.length-5);
			description += "<p>Query " + condA + ": " + opA + " (" + propA + " of " + cl + "s which have " + conditions +
				")</p>";
		}
	}

	description += "<p>Total Result: ";
	var totalresults = "";
	var lastoperator = "(";
	for (var condA in querylist) { // THIS MAY NOT WORK IF KEYS ARE NOT
		// ITERATED IN THE ORDER THEY WERE PUSHED
		if (document.getElementById("tab2qcheck" + condA).checked) {
			if (lastoperator == "" || lastoperator == "OR" || lastoperator == "AND") {
				totalresults += ")(";
			} else
				totalresults += " " + lastoperator + " ";
			totalresults += "Query " + condA;
			lastoperator = querylist[condA].nextbvalue;
		}

	}
	if (totalresults != "")
		totalresults += ")";

	description += totalresults + "</p>";
	Log(description);
	// document.getElementById("tab2queryresult").innerHTML = description;
}

function ShowQueryResult() {
	var output = "";
	for (var index in querylist) {
		ShowTab2QueryInstances(querylist[index].features, getFeatureStyle("default"));
	}

	var totalResult = Tab2TotalResult();
	for (var index in querylist) {
		if (document.getElementById("tab2qcheck" + index).checked) {
			var cl = querylist[index].class;
			var pr = querylist[index].prop;
			output += "<p>Query " + index + ": " + querylist[index].result + "</p>";
			ShowTab2QueryInstances(querylist[index].features, getFeatureStyle("queryselection"));
		}
	}

	output += "<p><strong>Total Result :";
	for (var i = 0; i < totalResult.length; i++) {
		output += "(" + totalResult[i] + ")";
	}
	totalResult += "</strong></p>";
	document.getElementById("tab2queryresult").innerHTML = "<div class='sparqlCaption'>Query Results</div>" + output;
	ShowQueryDescription();

	// Highlight instances in upper window
	// var displayClass
	// =document.getElementById("tab2inst").getAttribute("name");;
	// if (displayClass != null)
	// UpdateInstancesClass(displayClass);
	ConceptLayersRedraw();
}

function Tab2TotalResult() {
	var totalvalues = new Array();
	var lastoperator = "+";
	var temporalvalue = 0;
	for (var index in querylist) { // THIS MAY NOT WORK IF KEYS ARE NOT
		// ITERATED IN THE ORDER THEY WERE PUSHED
		if (document.getElementById("tab2qcheck" + index).checked) {
			var query = querylist[index];
			var result = Tab2QueryResult(query, index);
			query.result = result;
			if (lastoperator != null && lastoperator != "NaN" && lastoperator != "" && lastoperator != "AND" &&
				lastoperator != "OR") {
				if (result != null && result != "NaN" && temporalvalue != "NaN") {
					switch (lastoperator) {
						case "+":
							temporalvalue += result;
							break;
						case "-":
							temporalvalue -= result;;
							break;
						case "*":
							temporalvalue = temporalvalue * result;
							break;
						case "/":
							if (result !== 0)
								temporalvalue = temporalvalue / result;
							else
								temporalvalue = "NaN";
							break;
					}
				} else
					temporalvalue = "NaN";
			} else {
				totalvalues.push(temporalvalue);
				temporalvalue = result;
			}
			lastoperator = querylist[index].nextbvalue;
		}
	}
	totalvalues.push(temporalvalue);
	return totalvalues;
}

function addFeatures(existentFeatures, newFeatures) {
	for (var i = 0; i < newFeatures.length; i++)
		if (existentFeatures.indexOf(newFeatures[i]) == -1)
			existentFeatures.push(newFeatures[i]);
}

function Tab2QueryResult(query, condA) {
	if (query == null)
		return null;
	qclass = query.class;
	qprop = query.prop;
	qop = query.op;
	if (qclass == null || qprop == null || qop == null)
		return null;
	var allfeatures = existentIndividuals[qclass];
	query.features = allfeatures;
	var lastoperator = document.getElementById("tab2n" + condA).value;
	for (var condB in query.conditions) {
		if (document.getElementById("tab2qcheck" + condB).checked) {
			var prop = query.conditions[condB].prop;
			var op = query.conditions[condB].op;
			var value = query.conditions[condB].value;
			if (prop !== "" && op !== "" && value !== "") {

				if (lastoperator == "AND")
					query.features = FilterCondition(query.features, prop, op, value);
				else if (lastoperator == "OR")
					addFeatures(query.features, FilterCondition(allfeatures, prop, op, value));
			}

			lastoperator = document.getElementById("tab2n" + condB).value;
		}
	}

	var numericValues = getValues(query.features, qprop, false);

	var result = 0;
	switch (qop) {
		case "Avg":
			result = CalculateAvg(numericValues);
			break;
		case "Min":
			result = CalculateMin(numericValues);
			break;
		case "Max":
			result = CalculateMax(numericValues);
			break;
		case "Count":
			result = CalculateCount(numericValues);
			break;
		case "Sum":
			result = CalculateSum(numericValues);
			break;
		default:
			result = "NaN";
	}
	return result;
}

function SaveQuery(count) {
	var selectClass = document.getElementById("tab2c" + count);
	if (selectClass != null) { // Query line type A
		querylist[count] = new Object();
		querylist[count].class = selectClass.value;
		querylist[count].prop = document.getElementById("tab2p" + count).value;
		querylist[count].op = document.getElementById("tab2o" + count).value;
		querylist[count].nextbvalue = document.getElementById("tab2n" + count).value;

	} else { // Query line type B
		var mainquery = querylist[getConditionA(count)];
		if (mainquery.conditions == null)
			mainquery.conditions = new Object();
		mainquery.conditions[count] = new Object();
		mainquery.conditions[count].prop = document.getElementById("tab2p" + count).value;
		mainquery.conditions[count].op = document.getElementById("tab2o" + count).value;
		mainquery.conditions[count].value = document.getElementById("tab2v" + count).value;
		// CalculateQuery(mainquery);
	}
}

function showProperties(selectClassId, selectPropertyId) {

	var ptext = "<option value=\"\">&lt;Property&gt;</option>";
	var select = document.getElementById(selectClassId);
	if (select.value !== "") {
		var classuri = select.value;
		var properties = conceptProperties[classuri];
		if (properties != null) {
			properties.sort();
			for (var i = 0; i < properties.length; i++) {
				ptext += "<option value=\"" + properties[i] + "\">" + FilterURL(properties[i], true) + "</option>";
			}
		}
	}
	document.getElementById(selectPropertyId).innerHTML = ptext;
}

// Returns the conditionA corresponding to the condition received as parameter,
// or the last ConditionA existent
function getConditionA(condition) {
	var last = null;
	for (var condA in querylist) {
		if (condA == condition)
			return condA;
		for (var condB in querylist[condA].conditions)
			if (condB == condition)
				return condA;
		last = condA;
	}
	return last;
}

function createQueryForm(existentIndividuals) {
	delete querylist;
	querylist = new Object();
	var lines = "";
	lines += createTab2QueryLineA(1);
	// lines +=createTab2QueryLineB();

	document.getElementById("tab2querylines").innerHTML = lines;

	var next = "";
	next += createQueryNext(1);
	// next += createQueryNext("2");
	document.getElementById("tab2querynext").innerHTML = next;
	SaveQuery(1);
}

function isNumber(data) {
	if (Object.prototype.toString.call(data) == '[object Number]')
		return true;
	if (Object.prototype.toString.call(data) == '[object String]') {
		if (data.match(new RegExp("\\d+")) != null && data.match(new RegExp("[a-zA-Z]+")) == null)

			// var number = parseFloat(data); //TODO: esto filtra también cosas
			// que tengan números
			// if (number != null)
			return true;
	}
	return false;
}

function getValues(features, uriProperty, numeric) {
	if (features == null)
		return null;
	var output = new Array();
	for (var i = 0; i < features.length; i++) {
		var att = features[i].attributes;
		var arrayvalues = att[uriProperty];
		if (arrayvalues != null) {
			if (Object.prototype.toString.call(arrayvalues) != '[object Array]') {
				if (isNumber(arrayvalues)) {
					output.push(parseFloat(arrayvalues));
				} else
					output.push(arrayvalues);
			} else {
				if (arrayvalues.length > 1)
					output.push("Set");
				else {
					for (var j = 0; j < arrayvalues.length; j++)
						if (arrayvalues[j] != null) {
							if (isNumber(arrayvalues[j])) {
								output.push(parseFloat(arrayvalues[j]));
							} else
								output.push(arrayvalues[j]);
						}
				}
			}
		}
	}
	return output;
}

function NumericValues(arrayvalues) {
	var total = 0;
	if (arrayvalues == null)
		return total;
	for (var i = 0; i < arrayvalues.length; i++)
		total++;
	return total;
}

function CalculateAvg(arrayvalues) {
	var sum = CalculateSum(arrayvalues);
	var count = CalculateCount(arrayvalues);
	if (count != 0 && sum != "NaN")
		return (parseFloat(sum) / parseFloat(count));
	else
		return "NaN";
}

function CalculateSum(arrayvalues) {
	var sum = 0;
	for (var i = 0; i < arrayvalues.length; i++)
		if (isNumber(arrayvalues[i]))
			sum += parseFloat(arrayvalues[i]);
		else
			return "NaN";
	return sum;
}

function CalculateCount(arrayvalues) {
	if (arrayvalues == null)
		return 0;
	else
		return (arrayvalues.length);
}

function CalculateMax(arrayvalues) {
	if (arrayvalues == null)
		return "NaN";
	var max = 0;
	for (var i = 0; i < arrayvalues.length; i++)
		if (!isNumber(arrayvalues[i]))
			return "NaN";
	if (arrayvalues.length > 0)
		max = arrayvalues[0];
	for (var i = 1; i < arrayvalues.length; i++)
		if (parseFloat(arrayvalues[i]) > max)
			max = arrayvalues[i];
	return max;
}

function CalculateMin(arrayvalues) {
	if (arrayvalues == null)
		return "NaN";
	for (var i = 0; i < arrayvalues.length; i++)
		if (!isNumber(arrayvalues[i]))
			return "NaN";
	var min = 0;
	if (arrayvalues.length > 0)
		min = arrayvalues[0];
	for (var i = 1; i < arrayvalues.length; i++)
		if (parseFloat(arrayvalues[i]) < min)
			min = arrayvalues[i];
	return min;
}

/*
 * General Sparql Query
 */
function AjaxGetSparqlPrefixes() {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var prefixes = xmlhttp.responseText;
			document.getElementById("sparqlPrefixes").innerHTML = prefixes;
			// createConceptLayers();
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetSparqlPrefixes", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("");
}

/*
 * Query Results tab
 */

function clickFeature(idFeature) {
	for (var uriclass in existentIndividuals) {
		if (existentIndividuals[uriclass] != null) {
			var arrayfeat = existentIndividuals[uriclass];
			for (var i = 0; i < arrayfeat.length; i++) {
				var feat = arrayfeat[i];
				if (feat.id == idFeature)
					instanceClick(feat);
			}
		}
	}
}

function FeatureExistentIndividuals(uri) {
	for (var uriclass in existentIndividuals) {
		if (existentIndividuals[uriclass] != null) {
			var arrayfeat = existentIndividuals[uriclass];
			for (var i = 0; i < arrayfeat.length; i++) {
				var feat = arrayfeat[i];
				if (feat.attributes["URI"] == uri)
					return feat;
			}
		}
	}
	return null;
}

function ShowQueryResultsOnMap(qresults) {
	var urlregex = new RegExp("[^\\s]+://[^\\s]+", "gm");
	var output = "<ul>";
	var feat;
	var listIncludedFeatures = new Array();
	for (var i = 0; i < qresults.length; i++) {
		output += "<li>";
		var row = qresults[i];
		for (var j = 0; j < row.length; j++) {
			var included = false;
			var uri = row[j];
			if (uri.match(urlregex) != null) {
				feat = FeatureExistentIndividuals(uri);
				if (feat != null) {
					included = true;
					if (listIncludedFeatures.indexOf(feat) == -1)
						listIncludedFeatures.push(feat);
				}
			}

			if (included) {
				var showinstance = "<span class='highlight'>" + uri + "</span>";
				output += "<a name='" + uri + "' onclick=\"clickFeature('" + feat.id +
					"')\" style='white-space: nowrap; cursor: pointer; cursor: hand;'>" + showinstance + "</a>";
			} else {
				output += uri;
			}
			output += "&nbsp;&nbsp;&nbsp;";
		}
		output += "</li>";
	}
	output += "</ul>";
	ShowTab2QueryInstances(listIncludedFeatures, getFeatureStyle("queryselection"));
	ConceptLayersRedraw();
	return output;
}

/* Tab Instances */

function cleanPopup() {
	if (lastPopupWindow != null)
		lastPopupWindow.close();
	lastPopupWindow = null;
}

function clickIndicator(event, indicatorName) {
	var indicator = indicatorsHash[indicatorName];
	if (indicator == null)
		return;
	var cl = FilterURL(indicator.class);
	// var wind = window.open("", "_blank", "width=300, height=200,
	// top="+event.pageY+", left="+event.pageX);
	var w = 300;
	var h = 200;
	// var posX = (screen.width/2)-(w/2);
	// var posY = (screen.height/2)-(h/2);
	var posX = event.pageX;
	var posY = event.pageY + (h / 2);
	if (lastPopupWindow != null) {
		lastPopupWindow.close();
		lastPopupWindow = null;
	}
	var wind = window.open("", "_blank", "width=" + w + ", height=" + h + ", top=" + posY + ", left=" + posX);
	if (wind == null) {
		return;
	}
	lastPopupWindow = wind;
	var text = "<p>Indicator Type: " + cl + "</p><p>" + indicator.comment + "</p><p>Desirable Values:<ul>";
	var measures = indicator.references;
	for (var j = 0; j < measures.length; j++) {
		var measObject = measures[j];
		text += "<li>" + FilterURL(measObject.measure) + ":" + measObject.value + "</li>";
	}
	text += "</ul></p>";
	wind.document.write("<html><head><title>" + cl + "</title></head><body onclose=cleanPopup();>" + text +
		"</body></html>");
}

function SortIndicators() {
	var indicatorsClassHash = new Object();
	for (var indicatorName in indicatorsHash) {
		var indicator = indicatorsHash[indicatorName];
		for (var i = 0; i < indicator.affects.length; i++) {
			var cl = indicator.affects[i].class;
			if (indicatorsClassHash[cl] == null)
				indicatorsClassHash[cl] = new Array();
			var clArray = indicatorsClassHash[cl];
			clArray.push(indicatorName);
		}
	}
	return indicatorsClassHash;
}

function sortHashKeys(hashVar) {
	var arrayKeys = new Array();
	for (var key in hashVar) {
		arrayKeys.push(key);
	}
	return arrayKeys.sort(function (a, b) {
		var x = FilterURL(a).toLowerCase();
		y = FilterURL(b).toLowerCase();
		return x < y ? -1 : x > y ? 1 : 0;
	});
}

function showIndicatorsSortedByClass() {
	var indicatorsClassHash = SortIndicators();
	var list = "<table border='1'><tr><th style='width:35%;valign:bottom;'><a class='clickable' onclick=showIndicatorsSortedByClass();><strong>Associated Concepts</strong></a></th><th style='width:10%;valign:bottom;'><a class='clickable' onclick=showIndicators();><strong>Indicator</strong></a></th><th style='width:10%;valign:bottom;'><strong>Value (<em>Les Corts</em> Superblock)</strong></th></tr>";
	var arrayKeys = sortHashKeys(indicatorsClassHash);
	for (var i = 0; i < arrayKeys.length; i++) {
		var indicatorClass = arrayKeys[i];
		var cl = FilterURL(indicatorClass);
		list += "<tr>";
		list += "<td>" + cl + "</td>";
		list += "<td><ul>";
		var clArray = indicatorsClassHash[indicatorClass];
		for (var j = 0; j < clArray.length; j++) {
			list += "<li>";
			var indicatorName = clArray[j];
			var name = FilterURL(indicatorName);
			list += "<a class='clickable' onclick=clickIndicator(event,'" + indicatorName + "')>" + name + "</a>";
			list += "</li>";
		}
		list += "</ul></td>";

		list += "<td><ul>";
		var clArray = indicatorsClassHash[indicatorClass];
		for (var j = 0; j < clArray.length; j++) {
			list += "<li>";
			var indicatorName = clArray[j];
			list += indicatorsHash[indicatorName].value;
			list += "</li>";
		}
		list += "</ul></td>";
		list += "</tr>";
		document.getElementById("tabIndicators").innerHTML = list;
	}
	list += "</table>";
	document.getElementById("tabIndicators").innerHTML = list;
}

function showIndicators() {
	var list = "<table border='1'><tr><th style='width:35%;valign:bottom;'><a class='clickable' onclick=showIndicatorsSortedByClass();><strong>Associated Concepts</strong></a></th><th style='width:10%;valign:bottom;'><a class='clickable' onclick=showIndicators();><strong>Indicator</strong></a></th><th style='width:10%;valign:bottom;'><strong>Value (<em>Les Corts</em> Superblock)</strong></th></tr>";
	var arrayKeys = sortHashKeys(indicatorsHash);
	// recorrido de todos los arrays
	for (var i = 0; i < arrayKeys.length; i++) {
		var indicatorName = arrayKeys[i];
		var name = FilterURL(indicatorName);
		var indicator = indicatorsHash[indicatorName];
		var value = indicator.value;
		var affected = indicator.affects;

		list += "<tr>";
		list += "<td><ul>";
		for (var j = 0; j < affected.length; j++) {
			var clName = FilterURL(affected[j].class);
			list += "<li>" + clName + "</li>";
		}
		list += "</ul></td>";
		list += "<td><a class='clickable' onclick=clickIndicator(event,'" + indicatorName + "')>" + name + "</a></td>";
		list += "<td>" + value + "</td>";
		list += "</tr>";
		document.getElementById("tabIndicators").innerHTML = list;
	}
	list += "</table>";
	document.getElementById("tabIndicators").innerHTML = list;
}

function preprocessIndicators() { // If an indicator doesn't affect any class,
	// replace the empty array by N/A
	for (var indicatorName in indicatorsHash) {
		var indicator = indicatorsHash[indicatorName];
		var affected = indicator.affects;
		if (affected.length == 0) {
			var cl = new Object();
			cl.class = "N/A";
			affected.push(cl);
		}
	}
}

function AjaxGetIndicators() {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var indicatorsInfo = xmlhttp.responseText;
			if (indicatorsInfo != null && indicatorsInfo != "") {
				indicatorsHash = JSON.parse(indicatorsInfo);
				preprocessIndicators();
				showIndicators();
			}
		}
	};
	xmlhttp.open("POST", wsPath_utilsjs + "/GetIndicators", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send();
}

function AjaxQueryResults() {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var qresults = xmlhttp.responseText;
			// document.getElementById("result").innerHTML = qresults;
			if (qresults != null && qresults != "") {
				qresults = JSON.parse(qresults);
				FeaturesToDefaultStyle();
				var output = ShowQueryResultsOnMap(qresults);
				document.getElementById("sparqlResults").innerHTML = output;
			}
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetGeneralQuery", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	// xmlhttp.send("selectedFeature="+geojsonSelectedFeature+"&IdItem="+IdItem);
	var prefixes = document.getElementById("sparqlPrefixes").value;
	var sparqlQuery = document.getElementById("sparqlQuery").value;
	xmlhttp.send("sparqlQuery=" + encodeURIComponent(prefixes + sparqlQuery));
}

function AjaxGetLabel(uri) {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.open("POST", wsPath_utilsjs + "/GetLabel", false);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("uri=" + uri);
	var label = xmlhttp.responseText;
	return label;
}

function AjaxGetMatchProperties() {
	var inputprop = document.getElementById("tab15inputsearch").value;
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var qresults = xmlhttp.responseText; // property OPTIONAL(domain)
			// OPTIONAL(range)
			var output = "<ul>";
			if (qresults != null && qresults != "") {
				qresults = JSON.parse(qresults);
				for (var i = 0; i < qresults.length; i++) {
					var prop = qresults[i].label;
					var domain = qresults[i].domain;
					var range = qresults[i].range;
					output += "<li>";
					// output += "<strong>"+HtmlEncode(prop)+"</strong>";
					output += "<i>" + FilterURL(prop) + "</i>";
					if (domain != null) {
						output += "&nbsp;&nbsp;Domain:" + HtmlEncode(FilterURL(domain, false));
					}
					if (range != null) {
						output += "&nbsp;&nbsp;Range:" + HtmlEncode(FilterURL(range, false));
					}
					output += "</li>";
				}
			}
			output += "</ul>";
			document.getElementById("tab15results").innerHTML = "<div class='sparqlCaption'>Properties that match</div>" +
				output;
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetPropertyMatch", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	// xmlhttp.send("selectedFeature="+geojsonSelectedFeature+"&IdItem="+IdItem);
	xmlhttp.send("property=" + inputprop);

}

function tab15ClickProperty(uriproperty, uriclass) {

}

function UpdateRelatedClasses(properties) {
	// var list = "<strong>Instances "+FilterURL(uriClass, false)+"</strong>";
	var propButton = document.getElementById("tab15select");
	var propButtonValue = null;
	if (propButton != null)
		propButtonValue = propButton.value;
	var list = "<table cellspacing='12' class=\"table\"><col width='50%' align='right'><col width='50%' align='right'><tr><td class='right'><strong>Related Classes</strong></td><td id='tab15propbutton'><td></tr>";
	// list += "<p>";
	if (properties != null) {
		var classArray = new Array();
		for (var uriclass in properties) {
			classArray.push(FilterURL(uriclass))
		}
		classArray.sort();
		var outputArray = new Array();
		for (var i = 0; i < classArray.length; i++) {
			for (var uriclass in properties) {
				if (FilterURL(uriclass) == classArray[i])
					if (outputArray.indexOf(uriclass) == -1) {
						outputArray.push(uriclass);
						break;
					}
			}
		}
		for (var i = 0; i < outputArray.length; i++) {
			var uriclass = outputArray[i];
			var elementClass = FilterURL(uriclass);
			var elementProperties = ShowTab15Properties(properties[uriclass], uriclass);
			list = list + " <tr  id='tab15td" + uriclass + "'><td>" + elementClass + "</td><td>" + elementProperties +
				"</td></tr>";
		}
	}
	list = list + " </table>";
	document.getElementById("tab15related").innerHTML = list;
	CreateTab15PropertyValuesClass(propButtonValue);
}

function CreateTab15PropertyValuesClass(value) {
	var prop = "";
	prop += "<div class='styled-select'><select class='reduce' id='tab15select' onchange=\"AjaxGetRelatedConcepts()\">";
	if (value != null && value == "all")
		prop += "<option selected value='all'>All</option>";
	else
		prop += "<option value='all'>All</option>";
	if (value != null && value == "ass")
		prop += "<option selected value='ass'>Associated to</option>";
	else
		prop += "<option value='ass'>Associated to</option>";
	if (value != null && value == "dagg")
		prop += "<option selected value='dagg'>Has aggregate member</option>";
	else
		prop += "<option value='dagg'>Has aggregate member</option>";
	if (value != null && value == "iagg")
		prop += "<option selected value='iagg'>Aggregate member of</option>";
	else
		prop += "<option value='iagg'>Aggregate member of</option>";
	if (value != null && value == "datt")
		prop += "<option selected value='datt'>Has attribute</option>";
	else
		prop += "<option value='datt'>Has attribute</option>";
	if (value != null && value == "iatt")
		prop += "<option selected value='iatt'>Attribute of</option>";
	else
		prop += "<option value='iatt'>Attribute of</option>";
	prop += "</select></div>";
	document.getElementById("tab15propbutton").innerHTML = prop;
}

function ShowTab15Properties(arrayproperties, uriclass) {
	var stringprop = "";
	if (Object.prototype.toString.call(arrayproperties) != '[object Array]') {
		stringprop = "<a onclick=\"tab15ClickProperty(\'" + arrayproperties + "\',\'" + uriclass + "\')\">" +
			FilterURL(arrayproperties, false) + "</a>";
	} else {
		stringprop += "<a onclick=\"tab15ClickProperty(\'" + arrayproperties + "\',\'" + uriclass + "\')\">" +
			FilterURL(arrayproperties[0], false) + "</a>";
		for (var i = 1; i < arrayproperties.length; i++) {
			var value = "<a onclick=\"tab15ClickProperty(\'" + arrayproperties + "\",\'" + uriclass + "\')\">" +
				FilterURL(arrayproperties[i], false) + "</a>";
			stringprop += ", " + value;
		}
	}
	return stringprop;
}

function AjaxGetLabelTable() {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var table = xmlhttp.responseText;
			if (table != null && table != "") {
				urllabelmap = JSON.parse(table);
			}
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetLabelTable", false);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send();
}

function AjaxGetRelatedConcepts() {
	var inputprop = document.getElementById("tab15select").value;
	var selectedClass = getSelectedTabClass("tab15");
	if (selectedClass == null)
		return;
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// xmlhttp.onreadystatechange=function(){drawFeatures(xmlhttp, newlayer);};

	xmlhttp.onreadystatechange = function () {

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var properties = xmlhttp.responseText; // property OPTIONAL(domain)
			// OPTIONAL(range)
			if (properties != null && properties != "") {
				properties = JSON.parse(properties);
				UpdateRelatedClasses(properties);
			}
		}
	};

	xmlhttp.open("POST", wsPath_utilsjs + "/GetRelatedConcepts", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	// document.getElementById("result").innerHTML = "Loading Related Concepts";
	xmlhttp.send("propertytype=" + inputprop + "&uriclass=" + selectedClass + "&populated=" +
		populatedRelConcepts.populated.toString());
}