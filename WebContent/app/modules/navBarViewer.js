/**
 * 
 */
var navBarModule = angular.module('navBarModule', [ 'ngRoute', 'ngAnimate' ]);

navBarModule.config([ '$routeProvider', '$locationProvider',
		function($routeProvider, $locationProvider) {
			var html_templates = 'html-templates/';

			$routeProvider.when('/', {
				templateUrl : html_templates + 'home-page.html',
				controller : 'HomeCtrl',
				controllerAs : 'hc'
			});
			$routeProvider.when('/Home', {
				templateUrl : html_templates + 'home-page.html',
				controller : 'HomeCtrl',
				controllerAs : 'hc'
			});
			$routeProvider.when('/Home/StepByStep', {
				templateUrl : html_templates + 'home-page.html',
				controller : 'HomeCtrl',
				controllerAs : 'hc'
			});
			$routeProvider.when('/NewQuery', {
				templateUrl : html_templates + 'new-query.html',
				controller : 'NewQueryCtrl',
				controllerAs : 'nqc'
			});
			$routeProvider.when('/NewQuery#CreateArea', {
				templateUrl : html_templates + 'new-query.html',
				controller : 'NewQueryCtrl',
				controllerAs : 'nqc'
			});
			$routeProvider.when('/NewQuery/Results/:queyrId', {
				templateUrl : html_templates + 'new-query-results.html',
				controller : 'NewQueryCtrl',
				controllerAs : 'nqc'
			});
			$routeProvider.when('/Indicators', {
				templateUrl : html_templates + 'tab-indicators.html',
				controller : 'IndicatorsCtrl',
				controllerAs : 'ic'
			});
			$routeProvider.otherwise({
				redirectTo : '/Home',
			});

			$locationProvider.html5Mode(true);

		} ]);

navBarModule.controller('MainCtrl', [ '$scope', '$route', '$routeParams',
		'$location', 'NavMmenuFactory',
		function($scope, $route, $routeParams, $location, NavMmenuFactory) {
			this.$route = $route;
			this.$location = $location;
			this.$routeParams = $routeParams;

			// $scope.navmenu = {
			NavMmenuFactory.navmenu = {
				selected : undefined,
				values : [ {
					label : 'Home Page',
					href : 'Home/',
				}, {
					label : 'New Query',
					href : 'NewQuery/',
				}, {
					label : 'Indicators',
					href : 'Indicators/',
				} ],
			};
			$scope.navmenu = NavMmenuFactory.navmenu;

			$scope.redirector = function(href) {
				$location.url(href);
				// update navmenu.selected
				for (var i = 0; i < $scope.navmenu.values.length; ++i) {
					if ($scope.navmenu.values[i].href == href) {
						$scope.navmenu.selected = i;
						break;
					} else {
						// continue the search
					}
				}

			};

		} ]);

navBarModule.factory('NavMmenuFactory', [ function() {
	var navmenu = {
		selected : 0,
		values : [ {
			label : 'Home Page',
			href : 'Home/',
		}, {
			label : 'New Query',
			href : 'NewQuery/',
		}, {
			label : 'Indicators',
			href : 'Indicators/',
		} ],
	};
	return {
		navmenu : navmenu,
	};
} ]);

navBarModule.controller('HomeCtrl', [ '$routeParams', 'NavMmenuFactory',
		function($routeParams, NavMmenuFactory) {
			this.params = $routeParams;
			NavMmenuFactory.navmenu.selected = 0;
		} ]);

navBarModule.controller('NewQueryCtrl', [ '$routeParams', 'NavMmenuFactory',
		function($routeParams, NavMmenuFactory) {
			this.params = $routeParams;
			NavMmenuFactory.navmenu.selected = 1;
		} ]);
navBarModule.controller('IndicatorsCtrl', [ '$routeParams', 'NavMmenuFactory',
		'$scope', function($routeParams, NavMmenuFactory, $scope) {
			this.params = $routeParams;
			NavMmenuFactory.navmenu.selected = 2;
			if (!$scope.mapVisible) {
				$scope.toggleMap(true);
			}
		} ]);
