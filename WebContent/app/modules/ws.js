/**
 * Modulo que implementa llamadas a los WS implementados para utilizar la
 * ontologia y todas las funcionalidades implementadas en el proyecto
 */

var WSModule = angular.module('myWS', []);

// var myParams = {
// queryid : QM.queries.dataquery.selected,
// nodeid : nodeId,
// uriprop : relationuri,
// rangeFrom : range[0],
// rangeTo : range[1],
// filters : filters,
// };

WSModule.factory('WSGetGraphResult', ['$http', function ($http) {

	var GetGraphResultEnergy = function (inputParams) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphResultEnergy',
			data: inputParams
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphResultEnergy");
			return data;
		});

		return WSoutput;
	};

	var GetGraphResultUpdate = function (inputParams) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphResultUpdate',
			data: inputParams
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphResultUpdate");
			return data;
		});

		return WSoutput;
	};


	var GetGraphResultFromCellnex = function (inputParams) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphResultFromCellnex',
			data: inputParams
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphResultFromCellnex");
			return data;
		});

		return WSoutput;
	};

	var GetGraphDataValues = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphDataValues',
			data: params
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphDataValues");
			return data;
		});

		return WSoutput;
	};

	var GetGraphDataURIS = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphDataURIS',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphDataURIS");
			return data;
		});

		return WSoutput;
	};

	var GetGraphResult = function (inputParams) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphResult',
			// params : inputParams
			data: inputParams
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphResult");
		});

		return WSoutput;

	};

	var GetGraphResultUC = function (query) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphResultUC',
			data: query
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphResultUC");
		});

		return WSoutput;

	};

	var GetGraphResultExport = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphResultExport',
			data: params,
			headers: {
				'Content-type': 'application/csv',
			},
			responseType: 'arraybuffer'
		}).success(function (data, status, headers, config) {
			console.log("SUCCESS IN GetGraphResultExport");
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphResultExport");
		});

		return WSoutput;

	};

	var GetGraphCalculus = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetGraphCalculus',
			data: params
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetGraphCalculus");
		});

		return WSoutput;

	};
	var CancelQuery = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/CancelQuery',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN CancelQuery");
			return data;
		});

		return WSoutput;
	};

	return {
		GetGraphResultEnergy: GetGraphResultEnergy,
		GetGraphResultUpdate: GetGraphResultUpdate,
		GetGraphResultFromCellnex: GetGraphResultFromCellnex,
		GetGraphResultExport: GetGraphResultExport,
		GetGraphResult: GetGraphResult,
		GetGraphResultUC: GetGraphResultUC,
		GetGraphDataValues: GetGraphDataValues,
		GetGraphDataURIS: GetGraphDataURIS,
		GetGraphCalculus: GetGraphCalculus,
		CancelQuery: CancelQuery,
	};
}]);

WSModule.factory('WSOntologyAreas', ['$http', function ($http) {

	var CreateArea = function (params) {
		var data = $http({
			url: wsPath_qt + '/CreateArea',
			method: 'POST',
			data: params,
		}).success(function (data, status, headers, config) {
			console.log("WS /CreateArea: Area created successfully");
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN WS /CreateArea");
		});
		return data;
	};

	var IsArea = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/IsArea',
			data: params
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN WSOntologyAreas.IsArea()");
		});

		return WSoutput;

	};

	var IsGeoConcept = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/IsGeoConcept',
			data: params
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN WSOntologyAreas.IsGeoConcept()");
		});

		return WSoutput;

	};
	return {
		CreateArea: CreateArea,
		IsGeoConcept: IsGeoConcept,
		IsArea: IsArea,
	};
}]);

WSModule.factory('WSGetDataProperties', ['$http', function ($http) {

	var GetSubSuperClasses = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetSubSuperClasses',
			dataType: 'json',
			data: params,
			headers: {
				"Content-Type": "application/json"
			}
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			return "ERROR en WS /GetSubSuperClasses";
		});

		return WSoutput;

	};

	var GetDataProperties = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetDataProperties',
			dataType: 'json',
			data: params,
			headers: {
				"Content-Type": "application/json"
			}
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			return "ERROR en WS /GetDataProperties";
		});

		return WSoutput;

	};
	return {
		GetDataProperties: GetDataProperties,
		GetSubSuperClasses: GetSubSuperClasses,
	};
}]);

WSModule.factory('WSGetPaths', ['$http', function ($http) {

	var GetPaths = function (params) {

		// GetPaths(IOntoBase domain, IOntoBase range, int maxDeep, boolean
		// populated)
		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetPaths',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			return "ERROR en WS /GetDataProperties";
		});

		return WSoutput;

	};
	return {
		GetPaths: GetPaths,
	};
}]);

WSModule.factory('WSGetLinks', ['$http', function ($http) {

	var GetLinksBoth = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetLinksBoth',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetLinksBoth");
		});

		return WSoutput;

	};

	var WSGetLinksFromTo = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetLinksFromTo',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetLinksFromTo");
		});

		return WSoutput;

	};

	var WSGetLinksFrom = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetLinksFrom',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN WSGetLinksFrom");
		});

		return WSoutput;

	};
	return {
		GetLinksBoth: GetLinksBoth,
		WSGetLinksFromTo: WSGetLinksFromTo,
		WSGetLinksFrom: WSGetLinksFrom,
	};
}]);

WSModule.factory('WSDataQuery', ['$http', function ($http) {

	var GetSnippet = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetSnippet',
			data: params,

		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetSnippet");
		});

		return WSoutput;

	};

	var GetDataQuery = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetDataQuery',
			data: params,
			/*params: {
				params: inputParams
			},
			headers: {
				"Content-Type": "application/json"
			}*/
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetDataQuery");
			return "ERROR en WS /GetSubSuperClasses";
		});

		return WSoutput;

	};

	var GenerateGraphExample = function (inputParams) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GenerateGraphExample',
			data: inputParams,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GenerateGraphExample");
		});

		return WSoutput;

	};

	return {
		GetSnippet: GetSnippet,
		GetDataQuery: GetDataQuery,
		GenerateGraphExample: GenerateGraphExample,
	};
}]);

WSModule.factory('WSIndicators', ['$http', function ($http) {

	var IndicatorUpdateValue = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/IndicatorUpdateValue',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN IndicatorUpdateValue");
		});

		return WSoutput;

	};

	var IndicatorRemove = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/IndicatorRemove',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN IndicatorRemove");
		});

		return WSoutput;

	};
	var GetIndicatorDescriptors = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetIndicatorDescriptors',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetIndicatorDescriptors");
		});

		return WSoutput;

	};

	var GetIndicatorAnchorAreas = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetIndicatorAnchorAreas',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetIndicatorAnchorAreas");
		});

		return WSoutput;

	};

	var GetIndicatorConcepts = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetIndicatorConcepts',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetIndicatorConcepts");
		});

		return WSoutput;

	};

	var GetIndicatorsHierarchy = function (params) {

		var WSoutput = $http({
			method: 'POST',
			url: wsPath_qt + '/GetIndicatorsHierarchy',
			data: params,
		}).success(function (data, status, headers, config) {
			return data;
		}).error(function (data, status, headers, config) {
			console.log("ERROR IN GetIndicatorsHierarchy");
		});

		return WSoutput;

	};

	return {
		IndicatorUpdateValue: IndicatorUpdateValue,
		IndicatorRemove: IndicatorRemove,
		GetIndicatorDescriptors: GetIndicatorDescriptors,
		GetIndicatorAnchorAreas: GetIndicatorAnchorAreas,
		GetIndicatorConcepts: GetIndicatorConcepts,
		GetIndicatorsHierarchy: GetIndicatorsHierarchy,
	};
}]);