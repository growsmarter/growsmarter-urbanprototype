var moduelTabIndicator = angular.module('indicatorTabModule', ['angularTreeview', 'ngSanitize', 'ui.grid',
	'ui.grid.selection', 'ui.grid.edit', 'ui.grid.resizeColumns',]);

moduelTabIndicator.controller('IndicatorsCtrl', ['$scope', function ($scope) {
	$scope.updateIndicators = function () {
		AjaxGetIndicators();
	};
}]);

moduelTabIndicator.controller('importIndicatorsFileCtrl', ['$scope', 'WSIndicators', 'uiGridConstants',
	function ($scope, WSIndicators, uiGridConstants) {

		$scope.import_indicator_showable = false;
		$scope.indicatorImportParams = {};
		$scope.indicatorAnchorAreas = [];
		$scope.indicatorDescriptors = [];

		var params = {};

		WSIndicators.GetIndicatorDescriptors().then(function (promise) {
			$scope.indicatorDescriptors = promise.data['indicator_descriptors'];
			if ($scope.indicatorDescriptors.length > 0) {
				$scope.indicatorImportParams['indicator_descriptor'] = $scope.indicatorDescriptors[0];
			}
		});

		WSIndicators.GetIndicatorAnchorAreas().then(function (promise) {
			$scope.indicatorAnchorAreas = promise.data['anchor_areas'];
			if ($scope.indicatorAnchorAreas.length > 0) {
				$scope.indicatorImportParams['anchor_area'] = $scope.indicatorAnchorAreas[0];
			}
		});
		// Change this to the location of your server-side upload handler:
		var url = wsPath_qt + '/IndicatorUpdateFileUploader';
		$('#fileupload').fileupload({
			url: url,
			dataType: 'json',
			// formData : $scope.indicatorImportParams,
			// dynamicFormData : function() {
			// var data = $scope.indicatorImportParams;
			// return data;
			// },
			done: function (e, data) {
				$scope.getIndicators();
				// $.each(data.result.files, function(index, file) {
				// $('<p/>').text(file.name).appendTo('#files');
				// });
				$scope.gridApiAll.core.notifyDataChange(uiGridConstants.dataChange.ALL);
				$scope.gridApiByAC.core.notifyDataChange(uiGridConstants.dataChange.ALL);
				$scope.gridApiHierarchy.core.notifyDataChange(uiGridConstants.dataChange.ALL);
				$scope.closeDialog("#import-indicators-form");
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('#progress .progress-bar').css('width', progress + '%');
			}
		}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#fileupload').bind('fileuploadsubmit', function (e, data) {
			// The example input, doesn't have to be part of the upload form:
			data.formData = {
				anchor_area: $scope.indicatorImportParams.anchor_area,
				// indicator_descriptor : angular.toJson($scope.indicatorImportParams.indicator_descriptor.uri),
				indicator_descriptor: $scope.indicatorImportParams.indicator_descriptor.uri,
			};
		});

		// $('#fileupload').bind('fileuploaddone', function(e, data) {
		// $scope.getIndicators();
		// $.each(data.result.files, function(index, file) {
		// $('<p/>').text(file.name).appendTo('#files');
		// });
		// $scope.gridApiAll.core.notifyDataChange(uiGridConstants.dataChange.ALL);
		// $scope.gridApiByAC.core.notifyDataChange(uiGridConstants.dataChange.ALL);
		// $scope.gridApiHierarchy.core.notifyDataChange(uiGridConstants.dataChange.ALL);
		// $scope.closeDialog("#import-indicators-form");
		// });

		$scope.openDialog = function (htmlID) {
			$scope.import_indicator_showable = true;
			$(htmlID).dialog({
				title: "Import Indicators",
				autoOpen: false,
				resizable: true,
				maxHeight: 600,
				maxWidth: 800,
				minHeight: 300,
				minWidth: 600,
			});
			$(htmlID).dialog('open');
		};

		$scope.closeDialog = function (htmlID) {
			$(htmlID).dialog('close');
		};

	}]);

moduelTabIndicator
	.controller(
		'IndicatorsCategCtrl',
		[
			'$scope',
			'$http',
			'WSIndicators',
			'MapManagement',
			'uiGridConstants',
			'$q',
			function ($scope, $http, WSIndicators, MapManagement, uiGridConstants, $q) {

				// TODO Areas to filter the Indicators
				$scope.areasToChoose = {
					"type": "select",
					"value": "Les Corts", // selected
					"values": ["Les Corts", "Escola Industrial", "Plaça de les Glòries",
						"Sants-Montjuïc", "Poblenou", "Barcelona"],
				};

				$scope.items = ['HP', 'BSC', 'MicroSoft', 'LG'];
				$scope.mySelections = []; // var where ng-grids selected rows will be stored
				$scope.categoryVisualized = {
					checboxSelectAll: {},
				};

				/**
				 * Get all the indicators
				 */
				$scope.getIndicators = function () {
					// return function() {
					return $q(function (resolve, reject) {

						WSIndicators.GetIndicatorConcepts().then(function (promise) {
							var status = promise.status;
							var data = promise.data;

							$scope.indicatorsConcepts = data;
							$scope.indicators = data;
							$scope.convertIndicatorsToArray();
							$scope.groupByAssociatedConcepts();

							// hashmap that associates category-uri with an array with
							// all indicator-uri inside
							// this category
							$scope.hashMapHierarchyIndicator = {};
							for (var indKey in $scope.indicatorsConcepts) {
								var ind = $scope.indicatorsConcepts[indKey];
								if ($scope.hashMapHierarchyIndicator[ind.categoryuri] == undefined) {
									$scope.hashMapHierarchyIndicator[ind.categoryuri] = [];
								}
								$scope.hashMapHierarchyIndicator[ind.categoryuri].push(ind);
							}

							// Categorize the indicators in
							// $scope.indicatorsCategorized[]
							$scope.indicatorsCategorized = {};
							for (var h in $scope.hierarchy) {
								var cat = $scope.hierarchy[h];
								var indicatorsInside = $scope.getIndicatorsInThisCategory(cat);
							}

							var ac = $scope.getAC();
							$scope.showACIndicators(ac[0]); // actualiza el indicatorsForNgGridGroupedByAC
							$scope.showAllIndicators(); // actualiza el indicatorsForNgGridAll
							$scope.updateHierarchyIndicators($scope.categoryVisualized.selected);
							resolve('data received!');
						});
					});
					// };
					// WSIndicators.GetIndicatorConcepts().then(function(promise) {
					// var status = promise.status;
					// var data = promise.data;
					//
					// $scope.indicatorsConcepts = data;
					// $scope.indicators = data;
					// console.log(wsPath_qt + '/GetIndicatorConcepts -> status', status, '=', data);
					// $scope.convertIndicatorsToArray();
					// $scope.groupByAssociatedConcepts();
					//
					// // hashmap that associates category-uri with an array with all indicator-uri inside
					// // this category
					// $scope.hashMapHierarchyIndicator = {};
					// for ( var indKey in $scope.indicatorsConcepts) {
					// var ind = $scope.indicatorsConcepts[indKey];
					// if ($scope.hashMapHierarchyIndicator[ind.categoryuri] == undefined) {
					// $scope.hashMapHierarchyIndicator[ind.categoryuri] = [];
					// }
					// $scope.hashMapHierarchyIndicator[ind.categoryuri].push(ind);
					// }
					//
					// // Categorize the indicators in $scope.indicatorsCategorized[]
					// $scope.indicatorsCategorized = {};
					// for ( var h in $scope.hierarchy) {
					// var cat = $scope.hierarchy[h];
					// var indicatorsInside = $scope.getIndicatorsInThisCategory(cat);
					// }
					//
					// });
				};

				$scope.contstructOntologyHierarchy = function () {

					WSIndicators.GetIndicatorsHierarchy().then(function (promise) {
						var status = promise.status;
						var data = promise.data;

						console.log(wsPath_qt + '/GetIndicatorsHierarchy -> status', status, '=', data);
						$scope.hierarchy = data;
						$scope.convertToTreeView($scope.hierarchy);
					});
				};

				$scope.removeIndicators = function () {
					var urisToRemove = [];
					var apis = [$scope.gridApiAll, $scope.gridApiByAC, $scope.gridApiHierarchy];
					for (var x in apis) {
						var selectedrows = apis[x].selection.getSelectedRows();
						console.log("$scope.mySelections: ", selectedrows);
						for (var k in selectedrows) {
							// TODO: Remove $scope.mySelections[k];
							urisToRemove.push(selectedrows[k].uri);
						}
					}

					var params = {
						indicator_uris: urisToRemove,
					};

					WSIndicators.IndicatorRemove(angular.toJson(params)).then(function (promise) {
						console.log("Removed indicators: ", params);
						$scope.getIndicators().then(function (promise) {
							// $scope.gridApiAll.core.refresh();
							// $scope.gridApiAll.core.queueGridRefresh();
							$scope.gridApiAll.core.refresh();
						});
						// $scope.gridOptions.ngGrid.buildColumns();
						// $scope.gridApiAll.core.notifyDataChange(uiGridConstants.dataChange.ALL);
						// $scope.gridApiAll.core.refresh();
					});

				};

				$scope.convertToTreeView = function (hierarchy) {

					// TreeView data Pattern:
					// $scope.treedata = [ {
					// "label" : "Cat1",
					// "id" : "c1",
					// collapsed : true, // if collapsed, the hierarchy inside won't be open
					// "children" : [ {
					// "label" : "Cat1.1",
					// "id" : "c1.1",
					// "children" : []
					// } ]
					// } ];

					// 'Overall...' ha de ir la primera
					// sort by guiordertag
					// var newTree = [ {
					// label : 'Overall sustainability guide function',
					// id : '"http://www.ibm.com/BCNEcology#OverallSustainabilityGuideFunction"',
					// collapsed : true,
					// children : [],
					// } ];
					var newTree = [];

					var sortingfunction = function (a, b) {
						return a.guiordertag - b.guiordertag;
					};

					for (var h1 in hierarchy) {
						var subCatLvl1 = hierarchy[h1];
						// if (subCatLvl1.label != 'Overall sustainability guide function') {
						var cat1 = {
							label: subCatLvl1.label,
							id: subCatLvl1.uri,
							guiordertag: subCatLvl1.guiordertag,
							collapsed: true,
							children: [],
						};

						for (var h2 in subCatLvl1.subcat) {
							var subCatLvl2 = subCatLvl1.subcat[h2];
							var cat2 = {
								label: subCatLvl2.label,
								id: subCatLvl2.uri,
								guiordertag: subCatLvl2.guiordertag,
								collapsed: true,
								children: [],
							};
							for (var h3 in subCatLvl2.subcat) {
								var subCatLvl3 = subCatLvl2.subcat[h3];
								var cat3 = {
									label: subCatLvl3.label,
									id: subCatLvl3.uri,
									guiordertag: subCatLvl3.guiordertag,
									collapsed: true,
									children: [],
								};
								cat2.children.push(cat3);
								cat2.children.sort(sortingfunction);
							}
							cat1.children.push(cat2);
							cat1.children.sort(sortingfunction);
						}
						newTree.push(cat1);
						newTree.sort(sortingfunction);
						// }
					}

					// TODO: hacer el sort por la categoria .categoryguiordertag

					$scope.treedata = newTree;
				};

				/**
				 * what to do, when category is selected
				 */
				$scope.$watch('OntCat.currentNode', function (newObj, oldObj) {
					if ($scope.OntCat && angular.isObject($scope.OntCat.currentNode)) {
						// TODO: show the indicators on this category on the right
						// get the indicators of the category $scope.OntCat.currentNode.id
						// $scope.updateHierarchyIndicators($scope.OntCat.currentNode.id);
					}
				}, false);

				$scope.getIndicatorsInThisCategory = function (category) {
					var indicators = [];
					// hacer una recursiva

					if (category == undefined) {
						// this is a sublist that was empty
					} else {

						if (category.subcat == undefined || Object.keys(category.subcat).length <= 0) {
							// this is an category, leaf in the tree
							var indicatorsFromThisCategory = $scope.hashMapHierarchyIndicator[category.uri];
							if (indicatorsFromThisCategory != undefined) {
								for (var i = 0; i < indicatorsFromThisCategory.length; i++) {
									// the indicators of this category leaf are at hashMapHierarchyIndicator
									var ind = $scope.hashMapHierarchyIndicator[category.uri][i];
									indicators.push(ind);
								}
								$scope.indicatorsCategorized[category.uri] = indicators;
							}
						} else {

							// this is a category
							// continue recursively with its subcategories
							var indicatorsInAllChilds = [];
							for (var h in category.subcat) {
								var child = category.subcat[h];

								var indicatorsInside = $scope.getIndicatorsInThisCategory(child);

								// if ($scope.indicatorsCategorized[child.uri] == undefined) {
								// $scope.indicatorsCategorized[child.uri] = [];
								// }
								// $scope.indicatorsCategorized[child.uri] =
								// $scope.indicatorsCategorized[child.uri].concat(indicatorsInside);
								// $scope.indicatorsCategorized[child.uri] = indicatorsInside;
								indicatorsInAllChilds = indicatorsInAllChilds.concat(indicatorsInside);
								// indicators.push(child.uri);
								indicators = indicators.concat(indicatorsInside);
							}
							$scope.indicatorsCategorized[category.uri] = indicatorsInAllChilds;
						}
					}

					return indicators;
				};

				$scope.updateHierarchyIndicators = function (categoryUri) {
					// TODO update the categories at the treeview

					// $scope.indicatorsForNgGridHierarchy = [];
					var newGrid = [];
					var indicatorsFromThisCategory = $scope.indicatorsCategorized[categoryUri];

					// TODO: a mejorar que se sincronice con la uri de la categoria que se muestra
					$scope.categoryVisualized.selected = categoryUri;

					if (indicatorsFromThisCategory != undefined) {
						// > 1 indicators in this category

						for (var i = 0; i < indicatorsFromThisCategory.length; i++) {
							// var ind = $scope.hashMapHierarchyIndicator[categoryUri][i];
							var ind = indicatorsFromThisCategory[i];

							/**
							 * Adding Random Grading value/color
							 */
							var randomGrading = Math.floor((Math.random() * 10) + 1);
							var randomColor = gradingColors[randomGrading];
							// ending random color

							var row = ind;

							row.indicator = ind.label;
							row.comment = ind.comment;
							row.AssociatedConcepts = ind.dentass.join(', ');
							row.value = ind.value;
							row.grading = randomGrading;
							row.gradingcolor = randomColor;
							row.maplink = 'https://lh4.googleusercontent.com/DXvuXgKfOk8-MV4rwAr8kDEBGDMSafiuJqhfqF_LTclmAFss3A-Vn_yiQe2XroTV_1v1PaMsCGQQBIatc_Iq3MJZAg=w1656-h794';

							// TODO: when it's AirQuality, add this link

							newGrid.push(row);
						}
						$scope.indicatorsForNgGridHierarchy = newGrid;
					} else {
						// Any indicators in this category, empty data to ngGrid
						$scope.indicatorsForNgGridHierarchy = [];
					}
				};

				// RESTful API calling /GetIndicators
				$scope.getIndicators().then(function () {
					console.log("getIndicators finished");
				});

				// RESTful API calling to get the Hierarchy
				$scope.contstructOntologyHierarchy();

				/**
				 * Converts the $scope.indicators object into an ng-grid readable&valid object
				 */
				$scope.convertIndicatorsToArray = function () {
					console.log('convertIndicatorsToArray()' + $scope.indicators);
					$scope.indicatorsForNgGrid = [];
					for (var k in $scope.indicators) {
						var indicator = $scope.indicators[k];
						var assConcepts = [];
						for (var i = 0; i < indicator.dentass.length; i++) {
							var assC = indicator.dentass[i];
							assConcepts.push(assC);
						}

						/**
						 * Adding Random Grading value/color
						 */
						var randomGrading = Math.floor((Math.random() * 10) + 1);
						var randomColor = gradingColors[randomGrading];
						// ending random color
						var indjson = indicator;
						indjson.indicator = indicator.label;
						indjson.AssociatedConcepts = assConcepts.join(', ');
						indjson.grading = randomGrading;
						indjson.gradingcolor = randomColor;

						// var indjson = {
						// 'indicator' : indicator.label,
						// 'comment' : indicator.comment,
						// 'AssociatedConcepts' : assConcepts.join(', '),
						// 'value' : indicator.value,
						// 'grading' : randomGrading,
						// 'gradingcolor' : randomColor,
						// };
						$scope.indicatorsForNgGrid.push(indjson);
					}
				};

				$scope.groupByAssociatedConcepts = function () {

					console.log('groupByAssociatedConcepts ()' + $scope.indicators);
					// $scope.indicatorsForNgGrid = [];
					$scope.indicatorsByAssociatedConcepts = {};
					for (var k in $scope.indicators) {
						var indicator = $scope.indicators[k];
						var assConcepts = [];
						for (var i = 0; i < indicator.dentass.length; i++) {
							var assC = indicator.dentass[i];
							assConcepts.push(assC);
						}

						for (var i = 0; i < assConcepts.length; i++) {
							var aConcept = assConcepts[i];
							if ($scope.indicatorsByAssociatedConcepts[aConcept] == undefined)
								$scope.indicatorsByAssociatedConcepts[aConcept] = [];

							/**
							 * Adding Random Grading value/color
							 */
							var randomGrading = Math.floor((Math.random() * 10) + 1);
							var randomColor = gradingColors[randomGrading];
							// ending random color

							var tmpInd = indicator;
							var indForRow = {
								'indicator': indicator.label,
								'comment': indicator.comment,
								'areaclass': indicator.areaclass,
								'AssociatedConcepts': assConcepts.join(', '),
								'value': indicator.value,
								'grading': randomGrading,
								'gradingcolor': randomColor,
							};
							$.extend(tmpInd, indForRow);
							$scope.indicatorsByAssociatedConcepts[aConcept].push(tmpInd);
						}
					}
					console.log("done");
				};

				$scope.showAllIndicators = function () {
					console.log('showAllIndicators()' + $scope.indicators);
					var mygrid = [];
					for (var k in $scope.indicators) {
						var indicator = $scope.indicators[k];
						var assConcepts = [];
						for (var i = 0; i < indicator.dentass.length; i++) {
							var assC = indicator.dentass[i];
							assConcepts.push(assC);
						}

						/**
						 * Adding Random Grading value/color
						 */
						var randomGrading = Math.floor((Math.random() * 10) + 1);
						var randomColor = gradingColors[randomGrading];
						// ending random color

						var tmpInd = indicator;
						var indForRow = {
							'indicator': indicator.label,
							'comment': indicator.comment,
							'order': indicator.comment,
							'areaclass': indicator.areaclass,
							'AssociatedConcepts': assConcepts.join(', '),
							'value': indicator.value,
							'grading': randomGrading,
							'gradingcolor': randomColor,
						};
						$.extend(tmpInd, indForRow);

						mygrid.push(tmpInd);
					}
					$scope.indicatorsForNgGridAll = mygrid;
				};

				$scope.showACIndicators = function (ac) {
					console.log('showACIndicators(' + ac + ')=' + ac);
					// TODO: udpate the ng-grid variable indicatorsFornewGridNgGridGroupedByAC to show the
					// indicators of the AC
					// passed as
					// function parameter

					// loop indicators with the Associated Concept ac
					var inds = $scope.indicatorsByAssociatedConcepts[ac];
					for (var i = 0; i < inds.length; i++) {
						console.log('showACIndicators(' + ac + ')=' + inds[i]);
					}
					$scope.indicatorsForNgGridGroupedByAC = inds;

				};

				$scope.getAC = function () {

					if ($scope.indicatorsByAssociatedConcepts != undefined) {
						var k = Object.keys($scope.indicatorsByAssociatedConcepts);
						return k;
					} else {
						return null;
					}
				};

				MapManagement.initIndicatorsLayer('Indicators');

				$scope.selectAllNgGrid = function (api, checked) {
					$scope.categoryVisualized.checboxSelectAll[$scope.categoryVisualized.selected] = checked;
					// $scope.gridOptionsHierarchy.selectAll(checked);
					if (api.selection != undefined) {
						if (checked) {
							api.selection.selectAllVisibleRows();
						} else {
							api.selection.clearSelectedRows();
						}
					}

					// for (var i = 0; i < $scope.indicatorsForNgGridHierarchy.length; ++i) {
					// $scope.mySelections.push($scope.indicatorsForNgGridHierarchy[i]);
					// }

				};

				$scope.selectRow = function (selectedRow, api) {

					$scope.mySelections = api.selection.getSelectedRows();

					// count selected rows of the category visualized
					var selectedFromThisCategory = 0;
					for (var x in $scope.mySelections) {
						if ($scope.mySelections[x].categoryuri == $scope.categoryVisualized.selected) {
							selectedFromThisCategory++;
						}
					}

					if ($scope.indicatorsForNgGridHierarchy.length > selectedFromThisCategory) {
						// deselect the "select all" checkbox
						$scope.categoryVisualized.checboxSelectAll[$scope.categoryVisualized.selected] = false;
					} else {
						// check the "select all" checkbox
						$scope.categoryVisualized.checboxSelectAll[$scope.categoryVisualized.selected] = true;
					}

					for (var i = 0; i < $scope.mySelections.length; ++i) {
						var indicator = $scope.mySelections[i];
						if (selectedRow.entity.uri == indicator.uri) {
							// crear feature en OL con geometria area-wkt
							if (!MapManagement.existsFeatureByUriIndicator(indicator.uri)) {
								MapManagement.addElementIndicator(indicator);
							} else {
								// do nothing, ya existe
							}
						} else {
							// do nothing,
						}
					}

					if (selectedRow.isSelected == true) {
						MapManagement.visibleElementIndicator(selectedRow.entity.uri);
					} else {
						MapManagement.hideElementIndicator(selectedRow.entity.uri);
					}

				};

				$scope.gridOptionsAll = {
					data: 'indicatorsForNgGridAll',
					enableFullRowSelection: true,
					enableSelectAll: true,
					multiSelect: true,
					selectedItems: $scope.mySelections,
					// afterSelectionChange : $scope.selectRow,
					enableColumnResizing: true,
					onRegisterApi: function (gridApi) {
						$scope.gridApiAll = gridApi;
						$scope.gridApiAll.selection.on.rowSelectionChanged($scope, function (rowChanged) {
							$scope.selectRow(rowChanged, $scope.gridApiAll);
						});
						$scope.gridApiAll.selection.on.rowSelectionChangedBatch($scope, function (rows) {
							var allchecked = $scope.gridApiAll.selection.getSelectAllState();
							for (var k in rows) {
								var rowChanged = rows[k];
								$scope.selectRow(rowChanged, $scope.gridApiAll);
							}
						});
					},
					// function para decidir si la columna es editable
					cellEditableCondition: function ($scope) {
						// use $scope.row.entity and $scope.col.colDef to determine if editing is allowed
						// console.log("$scope.row.entity :", $scope.row.entity, ", $scope.col.colDef :
						// ",$scope.col.colDef);
						if ($scope.col.colDef.field == 'value')
							return true;
						else
							return false;
					},
					columnDefs: [
						{
							field: 'indicator',
							displayName: 'Indicator',
							// cellTemplate : '<div class="ngCellText" ng-class="col.colIndex()" ><span
							// ng-cell-text popover-placement="top"
							// popover="{{row.getProperty(\'comment\')}}" popover-append-to-body="true"
							// popover-trigger="mouseenter">
							// {{row.getProperty(col.field)}}</span></div>',
							// cellTemplate : '<div class="ngCellText" ng-class="col.colIndex()" ><span
							// ng-cell-text popover-placement="top"
							// popover="{{row.getProperty(\'comment\')}}" popover-append-to-body="true"
							// popover-trigger="mouseenter"> {{row.getProperty(col.field)}}</span> <a
							// href="" pop-over comment="{{row.getProperty(\'comment\')}}"
							// data-original-title="{{row.getProperty(\'indicator\')}}">Details</a>
							// </div>',
						},
						{
							field: 'areaclass',
							displayName: 'Area',
						},
						{
							field: 'AssociatedConcepts',
							displayName: 'Associated Concepts',
						},
						{
							field: 'value',
							displayName: 'Value',
							width: 90,
						},
						{
							field: 'grading',
							displayName: 'Grading',
							width: 90,
							cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span ng-cell-text><font color="{{row.entity.gradingcolor}}" style="padding: 5px;">{{ row.entity.grading}}</font></span></div>',
							// cellTemplate : '<div class="ui-grid-cell-contents">{{ COL_FIELD
							// }}</div>',
						},
						{
							field: 'maplink',
							displayName: 'Map',
							width: 90,
							cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span ng-cell-text><span data-toggle="modal" data-target=".bs-example-modal-lg">Map</span></span></div>',
						}],
				};

				$scope.gridOptionsIndicators = {
					data: 'indicatorsForNgGridGroupedByAC',
					enableFullRowSelection: true,
					enableSelectAll: true,
					multiSelect: true,
					selectedItems: $scope.mySelections,
					afterSelectionChange: $scope.selectRow,
					enableColumnResizing: true,
					onRegisterApi: function (gridApi) {
						$scope.gridApiByAC = gridApi;
						$scope.gridApiByAC.selection.on.rowSelectionChanged($scope, function (rowChanged) {
							$scope.selectRow(rowChanged, $scope.gridApiByAC);
						});
						$scope.gridApiByAC.selection.on.rowSelectionChangedBatch($scope, function (rows) {
							var allchecked = $scope.gridApiByAC.selection.getSelectAllState();
							for (var k in rows) {
								var rowChanged = rows[k];
								$scope.selectRow(rowChanged, $scope.gridApiByAC);
							}
						});

					},
					cellEditableCondition: function ($scope) {
						// use $scope.row.entity and $scope.col.colDef to determine if editing is allowed
						// console.log("$scope.row.entity :", $scope.row.entity, ", $scope.col.colDef :
						// ",$scope.col.colDef);
						if ($scope.col.colDef.field == 'value')
							return true;
						else
							return false;
					},
					columnDefs: [
						{
							field: 'indicator',
							displayName: 'Indicator',
						},
						{
							field: 'areaclass',
							displayName: 'Area',
						},
						{
							field: 'AssociatedConcepts',
							displayName: 'Associated Concepts',
						},
						{
							field: 'value',
							displayName: 'Value',
							width: 90,
						},
						{
							field: 'grading',
							displayName: 'Grading',
							width: 90,
							cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span ng-cell-text><font color="{{row.entity.gradingcolor}}" style="padding: 5px;">{{ row.entity.grading}}</font></span></div>',
						},
						{
							field: 'maplink',
							displayName: 'Map',
							width: 90,
							cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span ng-cell-text><span data-toggle="modal" data-target=".bs-example-modal-lg">Map</span></span></div>',
						}],
				};

				$scope.indicatorsForNgGridHierarchy = [];

				$scope.gridOptionsHierarchy = {
					data: 'indicatorsForNgGridHierarchy',
					enableFullRowSelection: true,
					enableSelectAll: true,
					multiSelect: true,
					selectedItems: $scope.mySelections,
					afterSelectionChange: $scope.selectRow,
					enableColumnResizing: true,
					onRegisterApi: function (gridApi) {
						$scope.gridApiHierarchy = gridApi;
						$scope.gridApiHierarchy.selection.on.rowSelectionChanged($scope, function (
							rowChanged) {
							$scope.selectRow(rowChanged, $scope.gridApiHierarchy);
						});
						$scope.gridApiHierarchy.selection.on.rowSelectionChangedBatch($scope,
							function (rows) {
								var allchecked = $scope.gridApiHierarchy.selection.getSelectAllState();
								for (var k in rows) {
									var rowChanged = rows[k];
									$scope.selectRow(rowChanged, $scope.gridApiHierarchy);
								}
							});
						$scope.gridApiHierarchy.edit.on.afterCellEdit($scope, function (rowEntity, colDef,
							newValue, oldValue) {
							// console.log('edited row id:' + rowEntity.id + ' Column:' + colDef.name
							// + ' newValue:' + newValue + ' oldValue:' + oldValue);

							var params = {
								indicator_uri: rowEntity.uri,
								value: newValue,
							};

							console.log("params: ", params);
							WSIndicators.IndicatorUpdateValue(params).then(function (promise) {
								// Update success
							});

							$scope.$apply();
						});
					},
					cellEditableCondition: function ($scope) {
						// use $scope.row.entity and $scope.col.colDef to determine if editing is allowed
						// console.log("$scope.row.entity :", $scope.row.entity, ", $scope.col.colDef :
						// ",$scope.col.colDef);
						if ($scope.col.colDef.field == 'value')
							return true;
						else
							return false;
					},
					columnDefs: [
						{
							field: 'indicator',
							displayName: 'Indicator',
						},
						{
							field: 'areaclass',
							displayName: 'Area',
						},
						{
							field: 'AssociatedConcepts',
							displayName: 'Associated Concepts',
						},
						{
							field: 'value',
							displayName: 'Value',
							width: 90,
						},
						{
							field: 'grading',
							displayName: 'Grading',
							width: 90,
							cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span ng-cell-text><font color="{{row.entity.gradingcolor}}" style="padding: 5px;">{{ row.entity.grading}}</font></span></div>',
						},
						{
							field: 'maplink',
							displayName: 'Map',
							width: 90,
							cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span ng-cell-text><span data-toggle="modal" data-target=".bs-example-modal-lg">Map</span></span></div>',
						}],
				};

			}]);