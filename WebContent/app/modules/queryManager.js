var qmModule = angular.module('queryManager', []);

qmModule.factory('QM', ['$http', 'WSGetDataProperties', 'WSOntologyAreas', function ($http, WSGetDataProperties, WSOntologyAreas) {

    var queries;
    var queriesUC;

    var init = function () {
        this.queries = {
            dataquery: {
                selected: undefined,
                queries: {}
            },
            urbanconcepts: {
                selected: undefined,
                queries: {}
            },
            dataproperties: {
                selected: {},
                values: {}
            },
            resultfilters: {
                // $scope.resourceToChoose = [ "Contains", "Range", ];
                // $scope.resourceChoosen = [];
                selected: { // nodeid: {type:X,params:{}}
                },
                types: [{
                    label: 'Contains',
                    params: ['text'],
                    numeric: false,
                }, {
                    label: 'Range',
                    params: ['min', 'max'],
                    numeric: true,
                }, ]
            },
            resultcalculus: {
                // $scope.calculusResult
                selected: { // nodeid: {type:X,result:Y}
                },
                types: [{
                    label: 'Sum',
                    numeric: true,
                }, {
                    label: 'Count',
                    numeric: false,
                }, {
                    label: 'Avg',
                    numeric: true,
                }, {
                    label: 'Min',
                    numeric: true,
                }, {
                    label: 'Max',
                    numeric: true,
                }]
            },
        };
        this.queriesUC = this.queries.urbanconcepts.queries;
    };

    var addQuery = function (p1) {
        var data = p1.data;
        var qid = data['queryid'];
        this.queries.urbanconcepts.selected = qid;
        this.queriesUC[qid] = {};
        // queriesUC[qid] = query;
        this.queriesUC[qid].queryid = qid;
        this.queriesUC[qid].subgraphs = data['graph'];
        // pagination config
        var sgId = 0; // sgId temporal mientras trabajo sin subgrafos
        this.queriesUC[qid].pagination = {
            currentPage: 1,
            maxPagesOffer: 3,
            rowsPerPage: 10,
            bigTotalItems: data['nrows'][sgId],
        };

        // Sparql query showness
        console.log("queryManager - sparql query-> ", data["sparqlquery"]);
        for (var key in data["sparqlquery"]) {
            var sparqlquery = data["sparqlquery"][key];
            Log("queryManager sparql query (subgraph id=" + key + ")=<xmp>" + sparqlquery + "</xmp>");
            this.queriesUC[qid].sparqlquery = sparqlquery;
        }
        console.log("queryManager queries = ", this.queries);
    };
    var rmQuery = function (p1) {
        console.log("rmQuery" + p1);
    };
    var getQuery = function (queryid) {
        console.log("getQuery" + p1);
        return this.queries;
    };
    var updateQuery = function (p1) {
        console.log("updateQuery" + p1);
    };
    var clearQueries = function () {
        console.log("clear");
        this.init();
    };

    var addDataProperty = function (uri, dp) {
        this.queries.dataproperties.values[uri] = dp;
    };

    var addDataProperties = function (uri) {
        if (isArea(uri)) {
            return null; // areas has no properties!
        } else {
            // TODO: check if not exists
            if (this.queries.dataproperties.values[uri] == undefined) {
                // request the data poperties
                var params = {
                    uri: uri,
                };
                var closureThis = this;
                WSGetDataProperties.GetDataProperties(params).success(function (data) {
                    var dataArray = Object.keys(data).map(function (key) {
                        return data[key];
                    });
                    closureThis.queries.dataproperties.values[uri] = dataArray;

                });
            } else {
                // do nothing, properties already at QM
            }
        }
    };
    var getDataProperties = function (uri) {
        if (this.queries.dataproperties.values[uri] != undefined) {
            // if front-end has properties
            return this.queries.dataproperties.values[uri];
        } else {
            // else, request data_properties from WS GetDataProperties

            if (isArea(uri)) {
                return null; // areas has no properties!
            } else {
                // check if not exists
                if (this.queries.dataproperties.values[uri] == undefined) {
                    // request the data poperties
                    var params = {
                        uri: uri,
                    };
                    var closureThis = this;
                    WSGetDataProperties.GetDataProperties(params).success(function (data) {
                        var dataArray = Object.keys(data).map(function (key) {
                            return data[key];
                        });
                        closureThis.queries.dataproperties.values[uri] = dataArray;


                    }).error(function (data, status, headers, config) {
                        console.log("WSGetDataProperties requesting params:", params);
                        return null;
                    }).finally(function () {
                        return this.queries.dataproperties.values[uri];
                    });
                } else {
                    // do nothing, properties already at QM
                    return this.queries.dataproperties.values[uri];
                }
            }
        }
    };

    var getDataPropertySelected = function (nodeid) {
        return this.queries.dataproperties.selected[nodeid];
    };
    var getDataPropertySelectedAll = function () {
        return this.queries.dataproperties.selected;
    };

    var hasDataProperty = function (uri) {
        return this.queries.dataproperties[uri] != undefined;
        // return this.queries.dataproperties.values.indexOf(uri) <= -1;
    };

    var hasGeometry = function (uri) {
        var geouri = 'http://www.ibm.com/ICP/Scribe/CoreV2#hasGeometry.asWKT';
        if (this.hasDP(uri)) {
            // return this.queries.dataproperties.values[uri][geouri] !=
            // undefined;
            return this.queries.dataproperties.values[uri].indexOf(geouri) <= -1;
        } else {
            return 'no dp defined for ' + uri;
        }
    };
    var isArea = function (uri) {
        // solucion temporal...
        // var params = {"uri": uri};
        // WSOntologyAreas.IsArea(params).success(function(data) {
        // var isarea = data.isArea;
        // return isarea;
        // });
        return (uri.indexOf('temporal') > 0);
    };
    var isAttribute = function (node) {
        // solucion temporal...
        return (node.type == 'nodeattribute');
    };


    return {
        init: init,
        add: addQuery,
        get: getQuery,
        remove: rmQuery,
        update: updateQuery,
        clear: clearQueries,
        addDP: addDataProperty,
        addDataProperties: addDataProperties,
        getDP: getDataProperties,
        getDataPropertySelected: getDataPropertySelected,
        getDataPropertySelectedAll: getDataPropertySelectedAll,
        hasDP: hasDataProperty,
        hasGEO: hasGeometry,
        isArea: isArea,
        queries: this.queries,
        isAttribute: isAttribute,
    };

}]);

qmModule.factory('QMGraph', [
    '$http',
    'QM',
    'WSGetDataProperties',
    'WSGetPaths',
    'WSGetLinks',
    '$timeout',
    function ($http, QM, WSGetDataProperties, WSGetPaths, WSGetLinks, $timeout) {
        // qmModule.factory('QMGraph', [ '$scope', '$http', 'QM',
        // function($scope, $http, QM) {

        var nodecounter; // counter for the graph nodes identifier
        var nDS; // graph nodes
        var eDS; // graph edges
        var network;
        var withInstances = true;

        var example = {
            isExample: true,
        };
        var replacesToShow = {
            value: false,
            values: [],
            disabled: false,
        };
        var descomposedPropsToChoose = {
            link: false,
            values: {},
        };

        var initSingleton = function () {
            this.nodecounter = 1; // counter for the graph nodes
            // identifier
            this.nDS = new vis.DataSet(); // init graph nodes
            this.eDS = new vis.DataSet(); // init graph edges
            this.network = new vis.Network(document.getElementById('mynetworkX'), {
                nodes: this.nDS,
                edges: this.eDS
            });
        };

        var addEdge = function (edge) {
            console.log("QMGraph.addEdge(", edge, ")");
            var edgeID = this.eDS.add(edge);
            var node_domain = this.nDS.get(edge.from);
            var node_range = this.nDS.get(edge.to);
            // get the data properties
            if (QM.hasDP(node_domain.uri)) {
                // already got from WS, do nothing
            } else {
                // gotta get from WS
                QM.addDataProperties(node_domain.uri);
            }

            if (QM.hasDP(node_range.uri)) {
                // already got from WS, do nothing
            } else {
                // gotta get from WS
                QM.addDataProperties(node_range.uri);
            }
            return edgeID;

        };

        var editEdge = function (edge) {
            console.log("QMGraph.editEdge(", edge, ")");
            var edgeID = this.eDS.add(edge);
            var node_domain = this.nDS.get(edge.from);
            var node_range = this.nDS.get(edge.to);
            // get the data properties
            if (QM.hasDP(node_domain.uri)) {
                // already got from WS, do nothing
            } else {
                // gotta get from WS
                QM.addDataProperties(node_domain.uri);
            }

            if (QM.hasDP(node_range.uri)) {
                // already got from WS, do nothing
            } else {
                // gotta get from WS
                QM.addDataProperties(node_range.uri);
            }
            return edgeID;

        };



        var initGraph = function (graphScope) {

            if (this.nodecounter == undefined) {
                this.nodecounter = 1; // counter for the graph nodes
                // identifier
                this.nDS = new vis.DataSet(); // init graph nodes
                this.eDS = new vis.DataSet(); // init graph edges
                this.network = new vis.Network(document.getElementById('mynetworkX'), {
                    nodes: this.nDS,
                    edges: this.eDS
                });
            }

            var optionsPhysics = {};

            // var optionsPhysics = {
            // barnesHut : {
            // enabled : true,
            // gravitationalConstant : -2000,
            // centralGravity : 0.1,
            // springLength : 95,
            // springConstant : 0.04,
            // damping : 0.09
            // },
            // repulsion : {
            // centralGravity : 0.1,
            // springLength : 50,
            // springConstant : 0.05,
            // nodeDistance : 100,
            // damping : 0.09
            // },
            // hierarchicalRepulsion : {
            // centralGravity : 0.5,
            // springLength : 150,
            // springConstant : 0.01,
            // nodeDistance : 60,
            // damping : 0.09
            // }
            // };

            var optionsEdges = {
                // inheritColor : false, // don't inherhits and uses the
                // following properties
                // style : 'arrow',
                arrows: {
                    to: {
                        scaleFactor: 0.75,
                    },
                },
                font: {
                    color: '#2A4E6E',
                    size: 10,
                },
                color: {
                    color: '#428CCA',
                    hover: '#2A6496',
                    // highlight : '#2A6496',
                    highlight: '#D16DA4', // lila claro
                },
                hoverWidth: 1.5,
                width: 1,
            };
            var optionsNodes = {
                color: {
                    border: '#269abc',
                    background: '#39b3d7',
                    hover: {
                        border: '#269abc',
                        background: '#39b3d7',
                    },
                    highlight: {
                        border: '#269abc',
                        background: '#39b3d7',
                    },
                },
                font: {
                    color: '#fff',
                    face: 'arial',
                    size: 10,
                },
                borderWidth: 2,
                borderWidthSelected: 5,
                shape: 'box',
                // radius : 5,
            };

            var locales = {
                en: {
                    edit: 'Edit',
                    del: 'Delete selected',
                    back: 'Back',
                    addNode: function () {
                        console.log("SE EJECUTA ADDNODE");
                        return 'Add Node';
                    },
                    addEdge: 'Add Edge',
                    editNode: 'Edit Node',
                    editEdge: 'Edit Edge',
                    addDescription: 'Click in an empty space to place a new node.',
                    edgeDescription: 'Click on a node and drag the edge to another node to connect them.',
                    editEdgeDescription: 'Click on the control points and drag them to a node to connect to it.',
                    createEdgeError: 'Cannot link edges to a cluster.',
                    deleteClusterError: 'Clusters cannot be deleted.',
                    editClusterError: 'Clusters cannot be edited.'
                }
            };

            var options = {
                nds: this.nDS,
                interaction: {
                    hover: true
                },
                width: '100%',
                // height : '400px',
                height: 'auto',
                locales: locales.mylocales,
                manipulation: {
                    enabled: true,
                    initiallyActive: true,
                    addNode: function (data, callback) {

                        // TODO:
                        // 1)obtener los posibles links para el nodo
                        // clickado data.id
                        var span = document.getElementById('operation');
                        var idInput = document.getElementById('node-id');
                        var labelInput = document.getElementById('node-label');
                        var saveButton = document.getElementById('saveButton');
                        var cancelButton = document.getElementById('cancelButton');
                        var div = document.getElementById('network-popUp');
                        span.innerHTML = "Add Node";
                        idInput.value = data.id;
                        labelInput.value = data.label;
                        saveButton.onclick = saveData.bind(this, data, callback);
                        cancelButton.onclick = clearPopUp.bind();
                        div.style.display = 'block';
                    },
                    addEdge: function (data, callback) {
                        console.log("QMGraph", QMGraph.nDS, QMGraph);
                        console.log("addEdge(), data: ", data);
                        console.log("addEdge(), where is nDS", this.nDS, nDS, getnDS(), geteDS(), data);
                        console.log("graphScope.getnDS()", graphScope.getnDS(), graphScope.nDS);

                        var nfrom = graphScope.nDS.get(data.from);
                        var nto = graphScope.nDS.get(data.to);
                        graphScope.onconnectFrom = graphScope.nDS.get(data.from);
                        graphScope.onconnectTo = graphScope.nDS.get(data.to);

                        var params = {
                            domainuri: nfrom.uri,
                            rangeuri: nto.uri,
                            // populated: graphScope.withInstances,
                            populated: $('#querywithinstances').is(':checked'),
                        };

                        if ((nfrom.type != undefined && nfrom.type == "AREA") || (nto.type != undefined && nto.type == "AREA")) {

                            var newEdge = {
                                from: nfrom.id,
                                to: nto.id,
                                label: "click to define",
                                params: [],
                            };
                            var z = graphScope.eDS.add(newEdge);
                            if (z.length > 0) {
                                params.selectededgeid = z[0];
                            }
                            graphScope.pathSelectionVisible = true;

                            var x = WSGetLinks.WSGetLinksFromTo(params).then(function (promise) {
                                console.log("WSGetLinksFromTo(", params, ")=", promise);
                                // TODO:
                                graphScope.edgeFromTo = {
                                    selected: undefined,
                                    values: [],
                                };

                                var edgeid = promise.config.params.params.selectededgeid;
                                for (var linkid in promise.data) {
                                    var link = promise.data[linkid];
                                    link.edgeid = edgeid;
                                    graphScope.edgeFromTo.values.push(link);
                                }

                                if (graphScope.edgeFromTo.values.length > 0) {
                                    graphScope.edgeFromTo.selected = graphScope.edgeFromTo.values[0];
                                }
                                graphScope.disableWSGetLinksFromTo = false;
                                // mddialog to show Paths between 2 nodes
                                $scope.showPathSelection();
                            });

                        } else {
                            // if not AREA, show #path-selection

                            var maxdeep_default = 1;
                            graphScope.selectedDepth = {
                                selected: maxdeep_default,
                                values: [0, 1, 2, 3]
                            };
                            params.maxdeep = graphScope.selectedDepth.selected;

                            var x = WSGetPaths.GetPaths(params).then(function (promise) {
                                QMGraphPaths.show_dquery_graph_edgeConnection = true;
                                QMGraphPaths.pathsFromTo = [];

                                console.log('WSGetPaths.GetPaths(', params, ') : ', promise);
                                var paths = promise.data;
                                var from = promise.config.params.params.domainuri;
                                var to = promise.config.params.params.rangeuri;
                                var depth = promise.config.params.params.rangeuri;
                                graphScope.pathsFromTo = paths;
                                // mddialog to show Paths between 2 nodes
                                $scope.showPathSelection();
                            });
                        }

                    },
                    // deleteNode : function(data, callback) {
                    // TODO: Remove from the list of data properties shown,
                    // stored in $scope.labels
                    // for (var i = 0; i < data.nodes.length; i++) {
                    // var node = data.nodes[i];
                    // $scope.deletelabels(node);
                    // if ($scope.graphResults != undefined &&
                    // $scope.graphResults[node] != undefined) {
                    // delete $scope.graphResults[node]; // remove the node from
                    // graphResults
                    // }
                    // TODO : delete the ndoe results
                    // }
                    // callback(data); // call the callback to delete the
                    // objects.
                    // },
                },

                physics: optionsPhysics,
                edges: optionsEdges,
                nodes: optionsNodes,
            };

            var netw = document.getElementById('mynetworkX');
            var netwJQ = angular.element('mynetworkX');
            this.network = new vis.Network(netw, {
                nodes: this.nDS,
                edges: this.eDS
            }, options);

            return {
                network: this.network,
                nDS: this.nDS,
                eDS: this.eDS,
            };
        };

        var updateNodeRelations = function (domain) {

            var params = {
                domainuri: domain.uri,
                // populated : this.withInstances,
                populated: $('#querywithinstances').is(':checked'),

            };

            var descomposedPropsToChoose = this.descomposedPropsToChoose;
            descomposedPropsToChoose.link = false;
            descomposedPropsToChoose.values = {};
            $("#links-loading").show();
            WSGetLinks.WSGetLinksFrom(params).success(
                function (data) {
                    var nodeRelationsOffered = [];
                    for (var k in data) {
                        var link = data[k];
                        var dom = {
                            label: link.domainlabel,
                            resource: link.domainresource,
                            type: link.domaintype,
                            uri: link.domainuri,
                        };

                        var pr = {
                            label: link.propertylabel,
                            resource: link.propertyresource,
                            type: link.propertytype,
                            uri: link.propertyuri,
                        };

                        var rang = {
                            label: link.rangelabel,
                            resource: link.rangeresource,
                            type: link.rangetype,
                            uri: link.rangeuri,
                        };
                        // solucionando si el label es nulo
                        if (dom.label == undefined) {
                            // dom.label = dom.uri;
                            dom.label = dom.uri.split("#")[1];
                        }
                        if (rang.label == undefined) {
                            // rang.label = rang.uri;
                            rang.label = rang.uri.split("#")[1];
                        }

                        var link = {
                            domain: dom,
                            prop: pr,
                            range: rang
                        };
                        // link.prop.label = link.prop.label
                        // +'('+link.prop.uri+')';
                        // link.range.label = link.range.label
                        // +'('+link.range.uri+')';

                        nodeRelationsOffered.push(link);

                        var key = link.prop.uri;
                        if (descomposedPropsToChoose.values[key] == undefined) {
                            descomposedPropsToChoose.values[key] = [];
                        }
                        descomposedPropsToChoose.values[key].push(link);
                    }
                }).error(function () {
                console.log("ERROR ON WSGetLinks.WSGetLinksFrom");
            }).finally(function () {
                $("#links-loading").hide();
            });
        };


        var updateNodeReplaces = function (node) {

            // TODO: quizás /GetSubSuperClasses usando withinstances
            var params = {
                uri: node.uri,
                withinstances: this.withInstances,
            };

            var replacesToShow = this.replacesToShow;
            console.log("GetSubSuperClasses({", params, ")");
            WSGetDataProperties.GetSubSuperClasses(params).success(function (data) {
                // Anyado concept, para que en la lista al menos haya uno
                var subSuperClasses = [];
                var superc = data.superc;
                var subc = data.subc;
                var subcAndSupercArray = superc.concat(subc);
                replacesToShow.values = subcAndSupercArray;
            }).error(function () {
                console.log("ERROR EN WSGetDataProperties.GetSubSuperClasses()");
            });
        };

        var setNodeOptions = function (nodeid, network) {
            var optionsNode = {
                nodes: {
                    id: nodeid,
                    font: {
                        color: '#FF69B4',
                        face: 'arial',
                        size: 25,
                    },
                },
            };
            network.setOptions(optionsNode);
        };

        var editEdge = function (edge) {
            QMGraph.eDS.update(edge);
            return edge.id;
        };

        var addNode = function (node) {

            // check the id not already in use
            while (this.nDS.get(this.nodecounter) != undefined) {
                this.nodecounter++;
            }

            // TODO: 07/16-setNodeOptions set the options (color, border,
            // etc. depending of the type of node:
            // instantiated, etc.)
            // setNodeOptions(this.nodecounter, this.network);
            var nodeData = {
                uri: node.uri,
                label: node.label,
            };
            nodeData.id = this.nodecounter;
            nodeData.borderWidth = 5;
            nodeData.borderWidthSelected = 10;
            if (node.node != undefined) {
                nodeData.type = node.node.type;
            } else {
                nodeData.type = 'default';
            };

            // if (true) {
            if (QM.isArea(nodeData.uri)) {
                // if is area, then red border
                nodeData.color = {
                    border: 'DarkSeaGreen', // The color of the border of
                    // the node when it is not
                    // selected or
                    // hovered over (assuming hover is enabled in the
                    // interaction module).
                    background: 'DarkGreen', // The color of the
                    // background of the node
                    // when it is not
                    // selected or hovered over (assuming hover is enabled
                    // in the interaction module).
                    highlight: {
                        border: 'DarkSeaGreen', // The color of the
                        // border of the node
                        // when it is selected.
                        background: 'DarkGreen', // The color of the
                        // background of the
                        // node when it is
                        // selected.
                    },
                    hover: {
                        border: 'Green', // The color of the border of
                        // the node when it is selected.
                        background: 'DarkGreen', // The color of the
                        // background of the
                        // node when it is }
                    }
                };
                nodeData.shape = 'image';
                nodeData.image = 'https://cdn0.iconfinder.com/data/icons/ie_Bright/48/blue_area.png';
                // nodeData.image =
                // 'https://cdn2.iconfinder.com/data/icons/cat-power/128/cat_tied.png';
                // nodeData.image =
                // 'http://findicons.com/files/icons/2770/ios_7_icons/128/polygon.png';
                // nodeData.image =
                // 'http://www.fuserealty.ca/wp-content/uploads/2015/03/area-150x150.png';
                // nodeData.image =
                // 'http://www.gayfriendlylawyer.uk/wp-content/uploads/2015/10/iconregion.png';
                nodeData.size = 15;
                nodeData.font = {
                    color: 'Black',
                };
            }
            if (node.node != undefined && node.node.type != undefined && node.node.type == "nodeattribute") {
                // if is area, then red border
                nodeData.color = {
                    border: 'black', // The color of the border of the
                    // node when it is not selected or
                    // hovered over (assuming hover is enabled in the
                    // interaction module).
                    background: 'orange', // The color of the background
                    // of the node when it is not
                    // selected or hovered over (assuming hover is enabled
                    // in the interaction module).
                    highlight: {
                        border: 'white', // The color of the border of
                        // the node when it is selected.
                        background: 'orange', // The color of the
                        // background of the node
                        // when it is
                        // selected.
                    },
                    hover: {
                        border: 'black', // The color of the border of
                        // the node when it is selected.
                        background: 'orange', // The color of the
                        // background of the node
                        // when it is }
                    }
                };
                nodeData.shape = 'image';
                // nodeData.image =
                // 'https://cdn0.iconfinder.com/data/icons/ie_Bright/512/blue_area.png';
                // nodeData.image =
                // 'http://icons.iconarchive.com/icons/martin-berube/flat-animal/96/mouse-icon.png';
                nodeData.image = 'http://icons.iconarchive.com/icons/hydrattz/multipurpose-alphabet/128/Letter-A-lg-icon.png';

                // nodeData.image =
                // 'http://findicons.com/files/icons/2770/ios_7_icons/128/polygon.png';
                // nodeData.image =
                // 'http://www.fuserealty.ca/wp-content/uploads/2015/03/area-150x150.png';
                // nodeData.image =
                // 'http://www.gayfriendlylawyer.uk/wp-content/uploads/2015/10/iconregion.png';
                nodeData.size = 15;
                nodeData.font = {
                    color: 'Black',
                };
            } else {
                // do nothing
                nodeData.color = {
                    // border : '#2B7CE9',// The color of the border of the node
                    // when it is not selected or hovered
                    // over (assuming hover is enabled in the interaction
                    // module).
                    // background : '#D2E5FF', // The color of the background of
                    // the node when it is not selected or
                    // hovered over (assuming hover is enabled in the
                    // interaction module).
                    // highlight : {
                    // border : 'green', // The color of the border of the node
                    // when it is selected.
                    // background : '#D2E5FF', // The color of the background of
                    // the node when it is
                    // selected.
                    // }
                };

            }

            this.nDS.add(nodeData);

            // get the data properties
            if (QM.hasDP(nodeData.uri)) {
                // already got from WS, do nothing
            } else {
                // gotta get from WS
                QM.addDataProperties(nodeData.uri);
            }

            return nodeData.id; // this.nodecounter
        };

        var bindUCToGraph = function () {
            var conceptsCheckedTop = $($scope.UrbanConceptsID).jstree(true).get_top_checked(true);
            var checkedConcepts = conceptsCheckedTop;
            console.log("$scope.bindUCToGraph (): ", "checkedData ", checkedConcepts);
            this.nDS.clear();
            this.eDS.clear();
            for (var concept in checkedConcepts) {
                var ck = checkedConcepts[concept];
                var uri = ck.a_attr.uri;
                var label = ck.text;
                addNode({
                    uri: uri,
                    label: label,
                });
            }
        };

        var getNodesSueltos = function () {
            var nodes_sueltos = [];
            // get nodes in edge
            var edge_nodes = [];
            var edge_ids = this.eDS.getIds();
            for (var i = 0; i < edge_ids.length; ++i) {
                var edgeid = edge_ids[i];
                var edge = this.eDS.get(edgeid);
                edge_nodes.push(edge.from.toString());
                edge_nodes.push(edge.to.toString());
            }

            var all_node_ids = this.nDS.getIds();
            for (var i = 0; i < all_node_ids.length; ++i) {
                var nodeid = all_node_ids[i];
                var node = this.nDS.get(nodeid);
                if (edge_nodes.indexOf(node.id.toString()) < 0) {
                    // node suelto, no esta en ningun edge
                    nodes_sueltos.push(node);
                }
            }
            return nodes_sueltos;
        };

        var cleanNodes = function () {
            if (this.nDS != undefined) {
                this.nDS.clear();
            } else {
                // do nothing
            }

        };

        var cleanEdges = function () {
            if (this.eDS != undefined) {
                this.eDS.clear();
            } else {
                // do nothing
            }
        };

        var getnDS = function () {
            return this.nDS;
        };
        var geteDS = function () {
            return this.eDS;
        };

        var getAttributeNodes = function () {
            var allNodes = this.nDS.get();
            var nonAttributeNodes = []; // clone a node (fastest way)
            for (var index in allNodes) {
                if (!QM.isAttribute(allNodes[index])) {
                    // if not attribute node, remove from array
                    nonAttributeNodes.push(allNodes[index].id);
                } else {
                    // ponerlo como selected?
                }
            }
            // TODO:
            // diff allNodes - nonAttributeNodes
            var attributeNodes = allNodes.filter(function (i) {
                return nonAttributeNodes.indexOf(i.id) < 0;
            });
            return attributeNodes;
        };

        var getAttributeEdges = function () {
            var attributeNodes = this.getAttributeNodes();
            var attributeEdges = [];
            for (var index in attributeNodes) {
                var nodeEdges = this.network.getConnectedEdges(attributeNodes[index].id);
                attributeEdges = attributeEdges.concat(nodeEdges);
            }

            // check not duplicate edges
            var attributeEdgesClone = attributeEdges.slice();
            attributeEdges = attributeEdges.filter(function (item, pos) {
                return attributeEdgesClone.indexOf(item) == pos;
            });

            return attributeEdges;
        };

        // Nodes totally disconnected, nor with attributes
        var getNodesDisconnected = function () {
            var nodesDisconnected = [];
            var nodesAll = this.nDS.get();
            for (nodekey in nodesAll) {
                var connectedNodes = this.network.getConnectedNodes(nodesAll[nodekey].id);
                if (connectedNodes.length <= 0) {
                    nodesDisconnected.push(nodesAll[nodekey]);
                }
            }
            return nodesDisconnected;
        };


        return {
            init: initGraph,
            initSingleton: initSingleton,
            addNode: addNode,
            removeNode: undefined,
            updateNode: undefined,
            cleanNodes: cleanNodes,
            addEdge: addEdge,
            removeEdge: undefined,
            editEdge: editEdge,
            updateEdge: undefined,
            cleanEdges: cleanEdges,
            bind: bindUCToGraph,
            nDS: this.nDS,
            eDS: this.eDS,
            network: this.network,
            nodecounter: this.nodecounter,
            getNodesSueltos: getNodesSueltos,
            getnDS: getnDS,
            geteDS: geteDS,
            updateNodeRelations: updateNodeRelations,
            updateNodeReplaces: updateNodeReplaces,
            replacesToShow: replacesToShow,
            descomposedPropsToChoose: descomposedPropsToChoose,
            withInstances: withInstances,
            getAttributeNodes: getAttributeNodes,
            getAttributeEdges: getAttributeEdges,
            getNodesDisconnected: getNodesDisconnected,
            example: example,
        };

    }
]);

qmModule.directive('visjsNetwork', [
    'QMGraph',
    'QM',
    'WSGetPaths',
    'WSGetLinks',
    'QMGraphPaths',
    '$mdDialog',
    function (QMGraph, QM, WSGetPaths, WSGetLinks, QMGraphPaths, $mdDialog) {

        var MAP_DOM_ELEMENT_ID = 'mynetworkX';

        return {

            restrict: 'E',
            // replace : true,
            templateUrl: 'html-templates/query-graph.html',
            link: function postLink(scope, element, attrs) {

                // QMGraph.init(scope);
                // console.log('QMGraph.init(); called in the directive:
                // visjsNetwork');
                var initGraph = function () {

                    if (QMGraph.nodecounter == undefined) {
                        QMGraph.nodecounter = 1; // counter for the graph
                        // nodes identifier
                        QMGraph.nDS = new vis.DataSet(); // init graph
                        // nodes
                        QMGraph.eDS = new vis.DataSet(); // init graph
                        // edges
                        QMGraph.network = new vis.Network(document.getElementById('mynetworkX'), {
                            nodes: QMGraph.nDS,
                            edges: QMGraph.eDS
                        });
                    }
                    var locales = {
                        en: {
                            edit: 'Edit',
                            del: 'Delete selected',
                            back: 'Back',
                            addNode: 'Add Node',
                            addEdge: 'Add Edge',
                            editNode: 'Edit Node',
                            editEdge: 'Edit Edge',
                            addDescription: 'Click in an empty space to place a new node.',
                            edgeDescription: 'Click on a node and drag the edge to another node to connect them.',
                            editEdgeDescription: 'Click on the control points and drag them to a node to connect to it.',
                            createEdgeError: 'Cannot link edges to a cluster.',
                            deleteClusterError: 'Clusters cannot be deleted.',
                            editClusterError: 'Clusters cannot be edited.'
                        }
                    };

                    var optionsPhysics = {};

                    var optionsEdges = {
                        // inheritColor : false, // don't inherhits and uses
                        // the following properties
                        // style : 'arrow',
                        arrows: {
                            to: {
                                scaleFactor: 0.75,
                            },
                        },
                        font: {
                            color: '#2A4E6E',
                            size: 10,
                        },
                        color: {
                            color: '#428CCA',
                            hover: '#2A6496',
                            // highlight : '#2A6496',
                            highlight: '#D16DA4', // lila claro
                        },
                        hoverWidth: 1.5,
                        width: 1,
                    };
                    var optionsNodes = {
                        color: {
                            border: '#269abc',
                            background: '#39b3d7',
                            hover: {
                                border: '#269abc',
                                background: '#39b3d7',
                            },
                            highlight: {
                                border: '#269abc',
                                background: '#39b3d7',
                            },
                        },
                        font: {
                            color: '#fff',
                            face: 'arial',
                            size: 10,
                        },
                        borderWidth: 2,
                        borderWidthSelected: 5,
                        shape: 'box',
                        // shape : 'star',
                        // radius : 5,
                    };

                    var options = {
                        interaction: {
                            hover: true
                        },
                        width: '100%',
                        // height : '400px',
                        height: '100%',
                        locales: locales,
                        manipulation: {
                            enabled: true,
                            initiallyActive: true,
                            addNode: false,
                            addEdge: function (edge, callback) {
                                // var nfrom = QMGraph.nDS.get(data.from);
                                // var nto = QMGraph.nDS.get(data.to);
                                // QMGraph.onconnectFrom =
                                // QMGraph.nDS.get(data.from);
                                // QMGraph.onconnectTo =
                                // QMGraph.nDS.get(data.to);
                                //
                                // var params = {
                                // domainuri : nfrom.uri,
                                // rangeuri : nto.uri,
                                // populated : QMGraph.withInstances,
                                // };
                                //
                                //
                                // if (QM.isArea(nfrom.uri) ||
                                // QM.isArea(nto.uri)) {
                                // var newEdge = {
                                // from : nfrom.id,
                                // to : nto.id,
                                // label : "double click to define",
                                // params : [],
                                // };
                                // var z = QMGraph.eDS.add(newEdge);
                                // if (z.length > 0) {
                                // params.selectededgeid = z[0];
                                // }
                                // scope.pathSelectionVisible = true;
                                // var x =
                                // WSGetLinks.WSGetLinksFromTo(params).then(function(promise)
                                // {
                                // console.log("WSGetLinksFromTo(", params,
                                // ")=", promise);
                                // // TODO:
                                // scope.edgeFromTo = {
                                // selected : undefined,
                                // values : [],
                                // };
                                //
                                // var edgeid =
                                // promise.config.params.params.selectededgeid;
                                // for ( var linkid in promise.data) {
                                // var link = promise.data[linkid];
                                // link.edgeid = edgeid;
                                // scope.edgeFromTo.values.push(link);
                                // }
                                //
                                // if (scope.edgeFromTo.values.length > 0) {
                                // scope.edgeFromTo.selected =
                                // scope.edgeFromTo.values[0];
                                // }
                                // scope.disableWSGetLinksFromTo = false;
                                // // mddialog to show Paths between 2
                                // // nodes
                                // // scope.showPathSelection(scope);
                                // scope.showEdgeSelection(scope);
                                //
                                // });
                                //
                                // } else {
                                // // if not AREA, show #path-selection
                                // var maxdeep_default = 1;
                                // QMGraphPaths.selectedDepth =
                                // {selected:maxdeep_default,values:[0,1,2,3]};
                                // scope.selectedDepth =
                                // QMGraphPaths.selectedDepth;
                                // params.maxdeep =
                                // QMGraphPaths.selectedDepth.selected;
                                // QMGraphPaths.pathsFromTo = params;
                                // scope.updatePaths(params);
                                // // mddialog to show Paths between 2
                                // // nodes
                                // scope.showPathSelection(scope);
                                // }

                                // ****************************************
                                // ADDING EDGE
                                // ****************************************
                                console.log('manipulation.addEdge:', edge);

                                // For later by .addPathEdges()
                                QMGraph.onconnectFrom = QMGraph.nDS.get(edge.from);
                                QMGraph.onconnectTo = QMGraph.nDS.get(edge.to);

                                var edgeData = {
                                    from: edge.from,
                                    to: edge.to,
                                    label: "double click to define",
                                    // prop : undefined,
                                };

                                QMGraph.edgeToAdd = edgeData;

                                // add edge to the dataset
                                edgeData.id = QMGraph.addEdge(edgeData)[0];
                                QMGraph.edgeData = edgeData;

                                var nodeFrom = QMGraph.nDS.get(edge.from);
                                var nodeTo = QMGraph.nDS.get(edge.to);
                                var params = {
                                    domainuri: nodeFrom.uri,
                                    rangeuri: nodeTo.uri,
                                    // populated: true,
                                    populated: $('#querywithinstances').is(':checked'),
                                };

                                QMGraphPaths.pathsFromTo.domainuri = nodeFrom.uri;
                                QMGraphPaths.pathsFromTo.rangeuri = nodeTo.uri;
                                QMGraphPaths.pathsFromTo.domainid = edge.from;
                                QMGraphPaths.pathsFromTo.rangeid = edge.to;

                                scope.disableWSGetLinksFromTo = true;
                                var x = WSGetLinks.WSGetLinksFromTo(params).success(function (data) {
                                    // TODO:
                                    QMGraph.edgeFromTo = {
                                        selected: undefined,
                                        values: [],
                                    };

                                    // var edgeid =
                                    // promise.config.params.params.selectededgeid;
                                    // var edgeid = QMGraph.selectedEdges[0];
                                    for (var linkid in data) {
                                        var link = data[linkid];
                                        link.edgeid = edgeData.id;
                                        QMGraph.edgeFromTo.values.push(link);
                                    }

                                    if (QMGraph.edgeFromTo.values.length > 0) {
                                        QMGraph.edgeFromTo.selected = QMGraph.edgeFromTo.values[0];
                                    }
                                    scope.edgeFromTo = QMGraph.edgeFromTo;

                                    scope.disableWSGetLinksFromTo = false;

                                    scope.showEdgeSelection(scope);

                                });




                            },
                        },

                        physics: optionsPhysics,
                        edges: optionsEdges,
                        nodes: optionsNodes,
                    };

                    var netw = document.getElementById('mynetworkX');
                    var netwJQ = angular.element('mynetworkX');
                    QMGraph.network = new vis.Network(netw, {
                        nodes: QMGraph.nDS,
                        edges: QMGraph.eDS
                    }, options);

                    scope.replacesToShow = QMGraph.replacesToShow;
                    scope.replacesToShow = QMGraph.descomposedPropsToChoose;
                    QMGraph.network.on("selectNode", function (params) {
                        console.log("selectNode.params=", params);
                    });

                    QMGraph.nDS.on("remove", function (event, properties, senderId) {
                        // check no nodeattribute flying alone
                        var allNodes = QMGraph.nDS.get();
                        if (allNodes == null) {
                            // do nothing
                        } else {
                            for (var i in allNodes) {
                                var node = allNodes[i];
                                if (node.type == 'nodeattribute' && QMGraph.network.getConnectedNodes(node.id).length == 0) {
                                    QMGraph.nDS.remove(node.id);
                                }
                            }
                        }

                    });
                    QMGraph.network.on("doubleClick", function (params) {

                        if (params.nodes.length > 0) {
                            // ****************************************
                            // DOUBLE CLICK ON NODE
                            // ****************************************

                            var selectedEdgesIds = params.edges;
                            var selectedNodesIds = params.nodes;
                            if (selectedNodesIds.length > 0) {
                                // TODO:
                                // 1) get possible relations links with
                                // range the selected node
                                // 2) offer possible relations to the user
                                // (hide/block the div)
                                // 3) update the graph with the selected
                                // options
                                var id = selectedNodesIds[0];
                                var node = QMGraph.nDS.get(id);
                                QMGraph.updateNodeRelations(node);
                                QMGraph.updateNodeReplaces(node);
                                scope.showDirectLinksReplace();
                            }

                            if (selectedEdgesIds.length > 0) {
                                for (var i = 0; i < selectedEdgesIds.length; i++) {
                                    var seId = selectedEdgesIds[i];
                                    var edge = QMGraph.eDS.get(seId);
                                    console.log('ed.id=' + edge.id + ',ed.fromId=' + edge.fromId + ',ed.toId=' + edge.toId);
                                }
                            }
                            return;
                        } else {
                            // ****************************************
                            // DOUBLE CLICK ON EDGE
                            // ****************************************
                            console.log('selectEdge EVENT:', params);
                            QMGraph.selectedEdges = params.edges;
                            var edge = QMGraph.eDS.get(QMGraph.selectedEdges[0]);
                            var nodeFrom = QMGraph.nDS.get(edge.from);
                            var nodeTo = QMGraph.nDS.get(edge.to);
                            var params = {
                                domainuri: nodeFrom.uri,
                                rangeuri: nodeTo.uri,
                                // populated : true,
                                // populated: $('#withinstancesradio').is(':checked'),
                                populated: $('#querywithinstances').is(':checked'),
                            };


                            QMGraphPaths.pathsFromTo.domainuri = nodeFrom.uri;
                            QMGraphPaths.pathsFromTo.rangeuri = nodeTo.uri;
                            QMGraphPaths.pathsFromTo.domainid = edge.from;
                            QMGraphPaths.pathsFromTo.rangeid = edge.to;

                            scope.disableWSGetLinksFromTo = true;
                            var x = WSGetLinks.WSGetLinksFromTo(params).success(function (data) {
                                // TODO:
                                QMGraph.edgeFromTo = {
                                    selected: undefined,
                                    values: [],
                                };

                                // var edgeid =
                                // promise.config.params.params.selectededgeid;
                                var edgeid = QMGraph.selectedEdges[0];
                                for (var linkid in data) {
                                    var link = data[linkid];
                                    link.edgeid = edgeid;
                                    QMGraph.edgeFromTo.values.push(link);
                                }

                                if (QMGraph.edgeFromTo.values.length > 0) {
                                    QMGraph.edgeFromTo.selected = QMGraph.edgeFromTo.values[0];
                                }
                                scope.edgeFromTo = QMGraph.edgeFromTo;

                                scope.disableWSGetLinksFromTo = false;

                                scope.showEdgeSelection(scope);

                            });
                        }
                    });

                    QMGraph.network.on('onConnect', function (event, properties, senderId) {
                        console.log('evento network ON-CONNECT', event, properties);
                    });

                    // QMGraph.network.on("selectEdge", function(params) {
                    // if(params.nodes.length > 0){
                    // // user have selected a node and an edge
                    // return;
                    // }
                    // });

                    return {
                        network: QMGraph.network,
                        nDS: QMGraph.nDS,
                        eDS: QMGraph.eDS,
                    };
                };
                initGraph();
            },
        };

    }
]);

qmModule.factory('QMGraphPaths', ['QMGraph', 'WSGetPaths', function (QMGraph, WSGetPaths) {

    var show_dquery_graph_edgeConnection = false;
    var selectedDepth = {
        selected: 0,
        values: [0, 1, 2, 3]
    };
    var pathsFromTo = {};
    var withInstances = QMGraph.withInstances;
    var disableWSGetPathsUpdatePaths = true;
    var propsToChoose;
    var functionParams = [];

    var getNumber = function (num) {
        return new Array(num);
    };

    return {
        show_dquery_graph_edgeConnection: show_dquery_graph_edgeConnection,
        selectedDepth: selectedDepth,
        pathsFromTo: pathsFromTo,
        withInstances: withInstances,
        disableWSGetPathsUpdatePaths: disableWSGetPathsUpdatePaths,
        propsToChoose: propsToChoose,
        functionParams: functionParams,
        getNumber: getNumber,
        algo: "algo para verificar el scope",
    };

}]);

qmModule
    .controller(
        'queryGraphCtrl', [
            '$scope',
            'QM',
            'QMGraphPaths',
            'WSGetPaths',
            'WSGetGraphResult',
            'QMGraph',
            '$mdDialog',
            function ($scope, QM, QMGraphPaths, WSGetPaths, WSGetGraphResult, QMGraph, $mdDialog) {

                $scope.show_dquery_graph_edgeConnection = QMGraphPaths.show_dquery_graph_edgeConnection;
                $scope.selectedDepth = QMGraphPaths.selectedDepth;
                $scope.pathsFromTo = QMGraphPaths.pathsFromTo;
                $scope.withInstances = QMGraphPaths.withInstances;
                $scope.disableWSGetPathsUpdatePaths = QMGraphPaths.disableWSGetPathsUpdatePaths;
                $scope.propsToChoose = QMGraphPaths.propsToChoose;
                $scope.functionParams = QMGraphPaths.functionParams;
                $scope.getNumber = QMGraphPaths.getNumber;
                $scope.algo = QMGraphPaths.algo;
                $scope.nDS = QMGraph.nDS;
                $scope.edgeFromTo = {
                    selected: 1,
                    values: [],
                };

                $scope.disableWSGetPathsUpdatePaths = false;
                $scope.selectedDepth = {
                    selected: 0,
                    values: [0, 1, 2, 3]
                };
                $scope.updatePaths = function (params) {
                    if ($scope.selectedDepth == undefined) {
                        $scope.selectedDepth = {
                            selected: 0,
                            values: [0, 1, 2, 3]
                        };
                    }

                    var paramsGetPaths = {
                        domainuri: QMGraphPaths.pathsFromTo.domainuri,
                        rangeuri: QMGraphPaths.pathsFromTo.rangeuri,
                        maxdeep: $scope.selectedDepth.selected,
                        // populated: $scope.withInstances,
                        populated: $('#querywithinstances').is(':checked'),
                    };
                    $scope.disableWSGetPathsUpdatePaths = true;
                    $scope.show_dquery_graph_edgeConnection = true;
                    var x = WSGetPaths.GetPaths(paramsGetPaths).success(function (data) {
                        $scope.show_dquery_graph_edgeConnection = false;
                        $scope.pathsFromTo = {
                            selected: undefined,
                            paths: data.paths,
                        };

                        if (data.paths.length > 0) {
                            $scope.pathsFromTo.selected = data.paths[0];
                        } else {

                        }

                        $scope.disableWSGetPathsUpdatePaths = false;
                    });
                };

                $scope.addPath = function () {
                    $scope.selectedDepth = {
                        selected: 1,
                        values: [0, 1, 2, 3]
                    };
                    $scope.show_dquery_graph_edgeConnection = false;
                    $scope.pathsFromTo.selected.path;

                    // add nodes intermedios
                    for (var i = 1; i < $scope.pathsFromTo.selected.path.length - 1; ++i) {
                        var node = $scope.pathsFromTo.selected.path[i];
                        // node.id = QMGraph.addNode(node);
                    }
                    // add edges intermedios
                    var node_domain = QMGraph.nDS.get(QMGraphPaths.pathsFromTo.domainid);
                    var node_range = QMGraph.nDS.get(QMGraphPaths.pathsFromTo.rangeid);

                    var edgeDomain = node_domain;
                    for (var i = 1; i < $scope.pathsFromTo.selected.path.length - 1; ++i) {
                        var edgeRange = $scope.pathsFromTo.selected.path[i];
                        edgeRange.id = QMGraph.addNode(edgeRange);
                        var newEdge = {
                            from: edgeDomain.id,
                            to: edgeRange.id,
                            label: "double click to define",
                        };
                        QMGraph.addEdge(newEdge);
                        edgeDomain = edgeRange;
                    }
                    // add last edge intermedio
                    var newEdge = {
                        from: edgeDomain.id,
                        to: node_range.id,
                        label: "double click to define",
                    };
                    QMGraph.addEdge(newEdge);

                    // Elimino el edge original (el path ya está creado)
                    /*
                     * var selectedEdge = QMGraph.selectedEdges[0];
                     * QMGraph.eDS.remove(QMGraph.selectedEdges[0]);
                     */
                    QMGraph.eDS.remove(QMGraph.edgeData.id);
                };

                $scope.addPathEdges = function () {
                    // var connectedEdges =
                    // QMGraph.network.getConnectedEdges(QMGraph.onconnectFrom.id);
                    // var connectedEdgesTo =
                    // QMGraph.network.getConnectedEdges(QMGraph.onconnectTo.id);
                    var selectedEdges = QMGraph.network.getSelectedEdges();
                    if (selectedEdges.length == 0) {
                        // add the edge, it does not already exists!


                        var connectedEdges = QMGraph.network.getConnectedEdges(QMGraph.onconnectFrom.id);
                        for (var i in connectedEdges) {
                            var edgeid = connectedEdges[i];
                            var edge = QMGraph.eDS.get(edgeid);
                            if (edge.to == QMGraph.onconnectTo.id) {
                                QMGraph.network.selectEdges([edge.id]);
                            }
                        }
                    }
                    var selectedEdges = QMGraph.network.getSelectedEdges();
                    for (var i = 0; i < selectedEdges.length; ++i) {
                        var edgeid = selectedEdges[i]; // edgeid
                        var edge = QMGraph.eDS.get(edgeid); // get
                        // the
                        // info
                        edge.prop = $scope.edgeFromTo.selected.propertyuri;
                        edge.label = $scope.edgeFromTo.selected.propertylabel;
                        edge.params = $scope.functionParams;
                        QMGraph.eDS.update(edge);
                    }
                };

                $scope.addEdgeConnectCancel = function () {
                    $scope.show_dquery_graph_edgeConnection = false;
                };

                $scope.cleanAll = function () {
                    QMGraph.cleanNodes();
                    QMGraph.cleanEdges();
                };

                var closeDialog = function () {
                    $mdDialog.hide();
                };

                $scope.checkTheGraph = function (graph) {
                    var msg = 'Define all the properties between nodes before launching the query, please.';
                    for (var i = 0; i < graph.edgesDS.length; ++i) {
                        if (graph.edgesDS[i].label != 'attribute' && graph.edgesDS[i].prop == undefined) {
                            // property undefined, gotta define them
                            $scope.showError(msg);
                            return false;
                        } else {
                            // continue analyzing
                        }
                    }
                    return true;
                };


                /* Execute this method when query-graph execution is clicked */
                $scope.searchQueryConfirmed = function () {
                    // clean resultfilters
                    QM.queries.resultfilters.selected = {};

                    if (QMGraph.nDS.get().length <= 0) {
                        $scope.showError('There must be at least one node to launch a query');
                        return;
                    }


                    $scope.showLoadingShow('Executing the query...', true, 'Abort Query');
                    $scope.gettingData = true;
                    $scope.colToShow = []; // clean the selected
                    // columns
                    $scope.dquery_show_subgraph_data_values = false; // hide
                    // results
                    $scope.rows = [];

                    // Search all the nodes from the network graph,
                    // identify the networks
                    var linksInDaGraph = [];
                    var nodesSueltosInDaGraph = [];
                    // var ids = QMGraph.eDS.getIds(); // get the edges in the
                    // graph
                    var attributeEdges = QMGraph.getAttributeEdges();
                    var allEdges = QMGraph.eDS.get(); // network.getConnectedEdges();
                    var nonAttributeEdges = allEdges.filter(function (i) {
                        return attributeEdges.indexOf(i.id) < 0;
                    });
                    var nodesDisconnected = QMGraph.getNodesDisconnected();
                    var nodesJustConnectedToAttributes = function () {
                        var nodesFiltered = [];
                        for (var i in attributeEdges) {
                            var edge = attributeEdges[i];
                            // check if a unique node
                            var nodeDomain = QMGraph.nDS.get(edge.from);
                            var nodeRange = QMGraph.nDS.get(edge.to);
                            var connected = [];
                            if (!QM.isAttribute(nodeDomain)) {
                                connected = QMGraph.network.getConnectedNodes(nodeDomain.id);
                                if (connected.length == 1) {
                                    nodesFiltered.push(nodeDomain);
                                }
                            } else if (!QM.isAttribute(nodeRange)) {
                                connected = QMGraph.network.getConnectedNodes(nodeRange.id);
                                if (connected.length == 1) {
                                    nodesFiltered.push(nodeRange);
                                }
                            }

                        }
                        return nodesFiltered;
                    };
                    var nodesDisconnectedAttributeCase = nodesJustConnectedToAttributes();
                    // merge
                    var nodesDisconnected = nodesDisconnected.concat(nodesDisconnectedAttributeCase);
                    // filter not repeated
                    var unrepeated = nodesDisconnected.filter(function (item, pos) {
                        return nodesDisconnected.indexOf(item) == pos;
                    });



                    if (unrepeated.length > 0) {
                        // nodos no conectados, o solo conectados a
                        // attributes

                        for (var i in unrepeated) {
                            var node = unrepeated[i];
                            if (!QM.isArea(node.uri) && !QM.isAttribute(node)) {
                                // if not area AND not attribute,
                                // then tener en cuenta
                                nodesSueltosInDaGraph.push(node);
                                var dps = QM.getDP(node.uri);
                                $scope.colToShow[node.id] = dps[0];
                            }

                            if (QM.isAttribute(node)) {
                                // TODO: show el selected attribute
                                var connectedNodes = QMGraph.network.getConnectedNodes(node.id);
                                for (var i in connectedNodes) {
                                    var ncid = connectedNodes[i];
                                    var connectedNode = QMGraph.nDS.get(ncid);
                                    var selecteddps = QM.queries.dataproperties.values[connectedNode.uri];
                                    var keys = Object.keys(selecteddps);
                                    if (keys.length > 0) {
                                        // 1) Always the First DP
                                        var dpPosition = $.grep(selecteddps, function (e) {
                                            return e.uri == node.uri;
                                        });
                                        // $scope.F_QM.queries.dataproperties.selected[ncid]
                                        // =
                                        // QM.queries.dataproperties.values[connectedNode.uri][dpPosition[0]];
                                        $scope.F_QM.queries.dataproperties.selected[ncid] = dpPosition[0];
                                    }
                                    // var selecteddps =
                                    // QM.queries.dataproperties.values[node.uri];
                                    // var keys = Object.keys(selecteddps);
                                    // if (keys.length > 0) {
                                    // // 1) Always the First DP
                                    // var dp = keys[0];
                                    // QM.queries.dataproperties.selected[ncid]
                                    // =
                                    // QM.queries.dataproperties.values[node.uri][dp];
                                    // }
                                }
                            }
                        }
                        // $scope.$apply();
                    }

                    if (nonAttributeEdges.length > 0) {
                        // nodos conectados (a otra cosa que no son
                        // attributes)

                        // var attributeNodes =
                        // QMGraph.getAttributeNodes();
                        // var attributeEdges = QMGraph.getAttributeEdges();
                        // var allEdges = QMGraph.eDS.get(); //
                        // network.getConnectedEdges();
                        // var nonAttributeEdges = allEdges.filter(function(i)
                        // {return
                        // attributeEdges.indexOf(i.id) < 0;});
                        // ids = nonAttributeEdges;
                        // nodos connectados(links)
                        for (var i = 0; i < nonAttributeEdges.length; ++i) {
                            // var id = ids[i];
                            // var edge = QMGraph.eDS.get(id);
                            var edge = nonAttributeEdges[i];
                            var d = QMGraph.nDS.get(edge.from);
                            var r = QMGraph.nDS.get(edge.to);
                            // var d =
                            // $scope.getDataSetWithId(QMGraph.nDS,
                            // edge.from);
                            // var r =
                            // $scope.getDataSetWithId(QMGraph.nDS,
                            // edge.to);

                            // buscar el edge con ese id (solo se
                            // obtiene con getpaths)
                            if (QMGraph.edgeFromTo != undefined && QMGraph.edgeFromTo.values != undefined) {
                                for (var ei in QMGraph.edgeFromTo.values) {
                                    if (QMGraph.edgeFromTo.values[ei].edgeid == edge.id) {
                                        var edgeinfo = QMGraph.edgeFromTo.values[ei];
                                        edge["params"] = $scope.functionParams;
                                        if (edgeinfo.propertytype == "FUNCTION") {
                                            edge["isfunction"] = true;
                                        } else {
                                            edge["isfunction"] = false;
                                        }
                                    }
                                }
                            }

                            var linkdata = {
                                domain: d,
                                relation: edge,
                                range: r
                            };
                            linksInDaGraph.push(linkdata);

                            // Poner unas propiedades default, para cuando haya
                            // respuesta del WS
                            // Si son AREA no hay que ensenyarlas!!
                            // if (d.type == undefined || d.type != 'AREA') {
                            if (!QM.isArea(d.uri)) {
                                // wkt == undefined -> !AREA
                                $scope.colToShow[d.id] = QM.getDP(d.uri)[0];
                            }
                            // if (r.type == undefined || r.type != 'AREA') {
                            if (!QM.isArea(r.uri)) {
                                // wkt == undefined -> !AREA
                                $scope.colToShow[r.id] = QM.getDP(r.uri)[0];
                            }
                        }
                    }

                    var graph = {
                        links: linksInDaGraph,
                        nodesSueltos: nodesSueltosInDaGraph,
                        edgesDS: QMGraph.eDS.get(),
                        nodesDS: QMGraph.nDS.get(),
                        usertoken: $("#my_query_token").val(),
                        synthetic: $("#synthetic_instances").is(":checked"),
                    };

                    if (!$scope.checkTheGraph(graph)) {
                        return false;
                    }


                    // TODO: poner esta llamada, esto es el modo como se muestra
                    $scope.F_QM.queries.resultfilters.selected = [];
                    $scope.showDataCellnex = false;
                    WSGetGraphResult.GetGraphResultEnergy(graph).success(function (data) {

                        $scope.queryDataCellnex = data;

                        var i = 0;
                        for (var i = 0; i < data.attributes.length; i++) {
                            if (data.attributes[i][0] != null) {
                                $scope.F_QM.queries.resultfilters.selected[i] = data.attributes[i][0];
                            } else {
                                //do nothing
                            }
                        }

                        // what to show on the UI
                        if ($scope.queryDataCellnex.data.length == 0) {
                            // no results, warn about it.
                            $scope.showError("No results found");
                        } else {
                            // show results
                            $scope.showDataCellnex = true;
                            $scope.showQueryResultsCellnex($scope);
                        }

                        // console show
                        console.log("querydatacellnex:", $scope.queryDataCellnex);
                    }).error(function () {
                        $scope.showError("Error produced while executing the query");
                        console.log("Error while GetGraphResultEnergy() execution");
                    }).finally(function () {
                        $scope.closeMdDialog();
                    });

                    $scope.updateDataResults = function (colInd) {

                        console.log("updateDataResults() -> Query DATA being UPDATED...");
                        console.log("F_QM.queries.resultfilters.selected: ", $scope.F_QM.queries.resultfilters.selected);
                        graph.attributes = $scope.F_QM.queries.resultfilters.selected;

                        // added to test
                        //$scope.F_QM.queries.resultfilters.selected[colInd].attribute = $scope.F_QM.queries.resultfilters.selected;

                        /**
                         * Pedir por WI solo estos resultados
                         */

                        $scope.showResultsState = true;
                        $scope.showDataCellnex = false;
                        WSGetGraphResult.GetGraphResultUpdate(graph).success(function (data) {
                            $scope.showResultsState = false;
                            $scope.showDataCellnex = true;
                            //$scope.queryDataCellnex = data;

                            //update the data
                            $scope.queryDataCellnex.data = data.data;

                            //$scope.showQueryResultsCellnex($scope);
                            console.log("querydatacellnex:", $scope.queryDataCellnex);
                        }).error(function () {
                            $scope.showResultsState = false;
                            $scope.showError("Error produced while executing the query");
                            console.log("Error while GetGraphResultUpdate() execution");
                        }).finally(function () {
                            $scope.closeMdDialog();
                        });


                    };

                    $scope.viewOnMap = function (ev) {
                        var location = {
                            "coordinates": {
                                "latitud": 41.38076938452482 + Math.random() / 100,
                                "longitud": 2.1229255199432373 + Math.random() / 100,
                            },
                            type: "Point",
                        };

                        console.log("viewOnMap()");

                        // variacion de las coordinadas
                        /*
                         * TODO:
                         * 
                         */

                        /* $scope.showMapViewResults($scope); */
                        var parentEl = angular.element(document.body);
                        $scope.hideDialog = $mdDialog.hide;
                        $mdDialog.show({
                            scope: $scope,
                            parent: parentEl,
                            preserveScope: true,
                            templateUrl: 'html-templates/map-view-results.html',
                            onComplete: function () {
                                // Leaflet creacion del mapa, despues de que se
                                // cree el mdDialog
                                var map = new OpenLayers.Map("mymap");
                                var mapnik = new OpenLayers.Layer.OSM();
                                var fromProjection = new OpenLayers.Projection("EPSG:4326"); // Transform
                                // from
                                // WGS
                                // 1984
                                var toProjection = new OpenLayers.Projection("EPSG:900913"); // to
                                // Spherical
                                // Mercator
                                // Projection
                                var position = new OpenLayers.LonLat(location.coordinates.longitud, location.coordinates.latitud).transform(fromProjection, toProjection);
                                var zoom = 15;

                                var pointLayer = new OpenLayers.Layer.Vector("Point Layer");
                                map.addLayers([mapnik, pointLayer]);

                                var point = new OpenLayers.Geometry.Point(location.coordinates.longitud, location.coordinates.latitud);
                                var selected_polygon_style = {
                                    fillOpacity: 1,
                                    fillColor: '#000000',
                                    strokeWidth: 5,
                                    strokeColor: '#ff0000',
                                    // add more styling key/value pairs as your
                                    // need
                                };
                                point.style = selected_polygon_style;
                                point = point.transform(fromProjection, toProjection);
                                // console.log(point);
                                var pointFeature = new OpenLayers.Feature.Vector(point, null, null);
                                pointLayer.addFeatures([pointFeature]);
                                map.setCenter(position, zoom);

                            },
                        }).finally(function () {
                            // when closing the mdDialog
                        });



                        console.log("$scope.showMapViewResults() scope:", $scope);

                        /*
                         * $mdDialog.show({ controller: DialogController,
                         * templateUrl:
                         * 'html-templates.html/map-view-results.html', parent:
                         * angular.element(document.body), targetEvent: ev,
                         * clickOutsideToClose: true, //fullscreen:
                         * $scope.customFullscreen // Only for -xs, -sm
                         * breakpoints. }) .then(function (answer) {
                         * $scope.status = 'You said the information was "' +
                         * answer + '".'; }, function () { $scope.status = 'You
                         * cancelled the dialog.'; });
                         * 
                         * 
                         * var mymap = L.map('map-instances').setView([51.505,
                         * -0.09], 13); var mapInstancesDialog =
                         * $("#map-instances"); var compiledeHTML = $compile("<div
                         * view-Map-Instance></div>")($scope);
                         * mapInstancesDialog.append(compiledeHTML);
                         * mapInstancesDialog.dialog({ title: "View Instance in
                         * Map", appendTo: parentEl, autoOpen: false, draggable:
                         * true, minHeight: 300, height: 800, minWidth: 400,
                         * open: function (event, ui) { }, beforeClose: function
                         * (event, ui) { }, }).dialogExtend({ "closable": true,
                         * "maximizable": true, "minimizable": true,
                         * "collapsable": true, "dblclick": "collapse", //
                         * "titlebar" : "transparent", "minimizeLocation":
                         * "left", "icons": { "close": ' ui-icon-closethick', //
                         * "ui-icon-circle-close", "maximize":
                         * 'ui-icon-plusthick', // "ui-icon-circle-plus",
                         * "minimize": 'ui-icon-minusthick', //
                         * "ui-icon-circle-minus", "collapse":
                         * "ui-icon-triangle-1-s", "restore": 'ui-icon-extlink', //
                         * "ui-icon-circle-arrow-n" }, });
                         * mapInstancesDialog.dialog("open");
                         */
                    };



                    /*
                     * $scope.gettingData = true;
                     * WSGetGraphResult.GetGraphResult(graph).success(function
                     * (data) { if (data["ERROR-MSG"] != undefined) {
                     * console.log(data["ERROR-MSG"]); } else { // Get the
                     * queryid results var qid = data['queryid']; var querydata = {
                     * queryid: data['queryid'], nrows: data['nrows'],
                     * sparqlquery: data['sparqlquery'], graph: graph,
                     * subgraphs: data['graph'], timeexecution:
                     * data['timeexecution'], currentPage: {}, // pagination //
                     * current page maxPagesOffer: {}, rowsPerPage: {},
                     * bigTotalItems: {}, };
                     * 
                     * QM.queries.selected = qid; // added
                     * QM.queries.dataquery[qid] = querydata; // once
                     * GetGraphResults WS is executed, // show data and
                     * properties // elections
                     * $scope.dquery_show_subgraph_data_values = true;
                     * $scope.subgraphs = data['graph']; $scope.currentPage =
                     * {}; $scope.maxPagesOffer = {}; $scope.rowsPerPage = {};
                     * $scope.bigTotalItems = {};
                     * 
                     * for (var key in data["sparqlquery"]) {
                     * QM.queries.dataquery[qid].sparqlquery =
                     * data["sparqlquery"][key]; $scope.sparqlquery =
                     * data["sparqlquery"][key]; //
                     * $scope.guiLog("GetGraphResult // sparql query (queryid = " // +
                     * QM.queries.dataquery.selected + // ", subgraph id=" + key + //
                     * ")=<xmp>" // + $scope.sparqlquery + "</xmp>"); }
                     * 
                     * if (querydata.nrows['-1'] <= 0) { // do nothing, no
                     * results $scope.showError("No results found"); } else { //
                     * set the pagination options for (var sgId in
                     * $scope.subgraphs) { $scope.currentPage[sgId] = 1; //
                     * pagination // current // page $scope.maxPagesOffer[sgId] =
                     * 3; $scope.rowsPerPage[sgId] = 10;
                     * $scope.bigTotalItems[sgId] = data['nrows'][sgId];
                     * 
                     * if ($scope.bigTotalItems[sgId] > 0) { // if there are
                     * rows, update // the results in the GUI for (var i = 0; i <
                     * QM.queries.dataquery[qid].subgraphs.length; ++i) { var
                     * subgraph = QM.queries.dataquery[qid].subgraphs[i]; for
                     * (var j = 0; j < subgraph.length; ++j) { var nodeid =
                     * subgraph[j]; var x =
                     * $scope.updateGraphDataValues(nodeid); } } } else { // if
                     * no data, notice the // user $scope.showError("Query with
                     * 0 Results"); } } } } }).error(function () {
                     * $scope.showError("Error produced while executing the
                     * query"); console.log("Error while searchQueryConfirmed()
                     * execution"); }).finally(function () {
                     * $scope.closeMdDialog(); });
                     */
                };

                $scope.getSubgraph = function (nodeId) {
                    for (var sgId in $scope.subgraphs) {
                        var arr = $scope.subgraphs[sgId];
                        if (arr.indexOf(parseInt(nodeId)) > -1) {
                            return sgId;
                        }
                    }
                    return -1;
                };

                $scope.updateGraphDataValues = function (nodeid) {
                    var node = QMGraph.nDS.get(nodeid);
                    if (nodeid == undefined || node == undefined) {
                        // if node not in the graph, do nothing
                        return false;
                    } else if (QM.isArea(node.uri)) {
                        // if node is area, no data to show/update
                        return false;
                    }

                    // Filters that must be applied to data update
                    var filters = $scope.getDataFilters(nodeid);

                    // Get the column property uri
                    var relationuri = undefined;
                    var gdps = QM.getDataPropertySelected(node.id);
                    if (gdps == undefined) {
                        // not DP already selected, choose one
                        var dps = QM.getDP(node.uri);
                        if (dps != undefined && dps.length > 0) {
                            relationuri = dps[0].uri;
                        }
                    } else {
                        // DP already selected
                        relationuri = gdps.uri;
                    }

                    var nSg = $scope.getSubgraph(nodeid);
                    var rangeFrom = $scope.rowsPerPage[nSg] * ($scope.currentPage[nSg] - 1);
                    var range = [rangeFrom, rangeFrom + $scope.rowsPerPage[nSg] - 1];

                    var myParams = {
                        queryid: QM.queries.selected,
                        nodeid: nodeid,
                        uriprop: relationuri,
                        rangeFrom: range[0],
                        rangeTo: range[1],
                        filters: filters,
                    };

                    // TODO: implement the http request to call the
                    // WS and offer the results

                    var w = WSGetGraphResult.GetGraphDataValues(myParams).success(function (data) {
                        var node = QMGraph.nDS.get(nodeid);
                        var dataproperty = myParams.uriprop;
                        // var colname =
                        // $scope.colToShow[nodeid].label;

                        if (node.areatype != undefined) {
                            // es un area, no muestro los resultados
                            // de esta columna
                            return;
                        }

                        if ($scope.graphResults == undefined) {
                            $scope.graphResults = {};
                        }

                        // Limpieza atributos, solo voy a mostrar
                        // nodeId.relationuri
                        $scope.graphResults[nodeid] = {};
                        $scope.graphResults[nodeid][dataproperty] = data['rows'];

                        // TODO: devolver en el data, el numero de
                        // rows
                        $scope.bigTotalItems[nSg] = data['nrows'];

                        $scope.dquery_show_subgraph_data_values = true;

                        $scope.updateSearchQueryData(node); // GUI
                        // vars
                        // update
                        $scope.gettingData = false; // finished the
                        // process of
                        // getting data
                        $scope.showQueryResults($scope);
                    }).error(function () {
                        console.log(" Error en GetGraphDataValues. Params:", myParams);
                    }).finally(function () {
                        $scope.closeMdDialog();
                    });

                };

                /**
                 * Get the info from all the filters specified at the
                 */
                $scope.getDataFilters = function (nodeId) {
                    var filters = [];
                    for (var nodeId in QM.queries.resultfilters.selected) {
                        var myfilter = QM.queries.resultfilters.selected[nodeId];
                        if (myfilter != undefined && myfilter.type != undefined) {

                            var filter = {};
                            filter.nodeid = nodeId;
                            filter.attribute = QM.getDataPropertySelected(nodeId);
                            filter.filtertype = myfilter.type;
                            filter.params = myfilter.params;
                            if (filter.filtertype == 'Contains') {
                                if (filter.params != undefined && filter.params.text != undefined && filter.params.text.trim() == "") {
                                    // do nothing, empty text ==
                                    // nothing to filter
                                } else {
                                    filters.push(filter);
                                }
                            }
                            if (filter.filtertype == 'Range') {
                                if (filter.params != undefined && filter.params.min != undefined && filter.params.min.trim() == "" && filter.params.max != undefined && filter.params.max.trim() == "") {
                                    // do nothing, empty text ==
                                    // nothing to filter
                                } else {
                                    filters.push(filter);
                                }
                            }
                        }
                    }
                    return filters;
                };

                $scope.rows = [];
                $scope.updateSearchQueryData = function (mynode) {
                    $scope.cols = {};
                    var colname;
                    var sqd = [];
                    if (QM.queries.dataproperties.selected[mynode.id] == undefined) {
                        var selecteddps = QM.queries.dataproperties.values[mynode.uri];
                        var keys = Object.keys(selecteddps);
                        if (keys.length > 0) {
                            // 1) Always the First DP
                            var dp = keys[0];
                            // QM.queries.dataproperties.selected[mynode.id]
                            // =
                            // QM.queries.dataproperties.values[mynode.uri][dp].uri;
                            QM.queries.dataproperties.selected[mynode.id] = QM.queries.dataproperties.values[mynode.uri][dp];
                            console.log('QM.queries.dataproperties.selected[mynode.id]=', QM.queries.dataproperties.selected[mynode.id]);
                            console.log('QM.queries.dataproperties.values[mynode.uri][dp]=', QM.queries.dataproperties.values[mynode.uri][dp]);

                            // 2) Random DP
                            // QM.queries.dataproperties.selected[mynode.id]
                            // = keys[keys.length *
                            // Math.random() << 0];

                        }
                        colname = QM.queries.dataproperties.selected[mynode.id];

                    } else {
                        colname = QM.queries.dataproperties.selected[mynode.id];
                    }

                    for (var node in $scope.colToShow) {
                        if ($scope.colToShow[node] == undefined) {
                            // data-property list from this node,
                            // must contain empty rows
                            console.log('$scope.colToShow[', node, '] == undefined');
                            var l = node.split("#")[1];
                            $scope.colToShow[node] = {
                                label: l,
                                uri: node
                            };
                            var tmp = [];
                            for (var i = 0; i < rowSizeForUndefined; i++) {
                                tmp.push(l);
                            }
                            $scope.cols[node] = tmp;
                            $scope.graphResults[node] = {};
                            $scope.graphResults[node][l] = tmp;

                        } else {

                            // var colname =
                            // $scope.colToShow[node].label;
                            // var colnameWithoutSpaces =
                            // colname.replace(/\s+/g, ''); //
                            // remove spaces

                            if ($scope.graphResults == undefined || $scope.graphResults[node] == undefined || $scope.graphResults[node][colname] == undefined) {
                                // llamada al WS getGraphResults
                                // (habria que hacerlo desde el
                                // boton)
                                console.log("WARNING: NO DATA YET. PRESS 'GET DATA'");
                            } else {

                                var nrows = $scope.graphResults[node][colname];
                                for (var i = 0; i < nrows.length; i++) {
                                    if (sqd[i] == undefined) {
                                        sqd[i] = {};
                                    }
                                    var val = nrows[i].rowvalues;
                                    if (val == "" || val == undefined) {
                                        val = "empty";
                                    } else {
                                        // sqd[i][String(colnameWithoutSpaces)]
                                        // = String(val); // para
                                        // version angular
                                        sqd[i][String(colname)] = String(val); // para
                                        // version
                                        // angular
                                    }
                                    // ng-grid
                                    nrows[i].rowvalues = val; // para
                                    // version
                                    // HTML
                                    // (hace
                                    // falta?)
                                    // zz.push(val);
                                }
                                $scope.cols[node] = nrows;
                                // $scope.cols[node] = zz;
                            }
                        }
                    }

                    // TODO: generar los ng-grid's respectivos de
                    // cada tabla
                    $scope.rows = [];
                    var rowSize = 10; // hecho manual, tendria que
                    // hacerse paginacion
                    for (var rowid = 0; rowid < rowSize; rowid++) {
                        var row = {};
                        for (var node in $scope.graphResults) {
                            if ($scope.colToShow[node] != undefined) {
                                var colname2 = QM.queries.dataproperties.selected[mynode.id].uri;
                                var colname = QM.queries.dataproperties.selected[node].uri;
                                var x = $scope.graphResults[node];
                                var y = $scope.graphResults[node][colname];
                                if ($scope.graphResults[node] == undefined || $scope.graphResults[node][colname] == undefined) {
                                    row[node] = "was-undefined";
                                    console.log("$scope.graphResults[" + node + "][" + colname + "] is undefined");
                                } else {
                                    // row[node] =
                                    // $scope.graphResults[node][colname][i].rowvalues;
                                    row[node] = $scope.graphResults[node][colname][rowid];
                                    row.id = rowid + (($scope.currentPage[0] - 1) * 10);
                                    if ($scope.graphResults[node][colname][rowid] != undefined) {
                                        row.uri = $scope.graphResults[node][colname][rowid].rowuri;
                                    }
                                    if (row[node] == undefined || row[node].rowvalues == undefined || row[node].rowvalues == '') {
                                        row[node].rowvalues = '(empty)';
                                    }
                                    row.size = $scope.graphResults[node][colname].length;
                                    rowSize = $scope.graphResults[node][colname].length;
                                }
                            } else {
                                // $scope.colToShow[node] is
                                // undefined
                            }
                        }
                        $scope.rows.push(row);
                    }

                    $scope.searchQueryData = sqd; // ng-grid lee
                    // de
                    // searchQueryData

                };

                /**
                 * Check if there's any node that compounds the subgraph in the
                 * vis.js graph
                 */
                $scope.checkShowDataValues = function (subgraph) {
                    for (var key in subgraph) {
                        if (QMGraph.nDS.get(subgraph[key]) != undefined) {
                            return true;
                        }
                    }
                    return false;
                };

                $scope.pageChanged = function () {
                    // update the page shown to the corresponing
                    // $scope.currentPage = this.currentPage;
                    for (var nodeId in $scope.colToShow) {
                        if ($scope.colToShow[nodeId] != undefined) {
                            $scope.updateGraphDataValues(nodeId);
                        } else {
                            // do-nothing, probablemente es un
                            // enum-type y no tiene datos que
                            // mostrar
                        }
                    }

                };

            }
        ]);

qmModule.controller('getDataResultsCtrl', ['$scope', 'QM', function ($scope, QM) {

    $scope.isNumericColumn = function (col) {
        var at_least_a_numeric_cell = false;
        if (col != undefined) {
            for (var i = 0; i < $scope.rows.length; ++i) {
                if ($scope.rows[i][col] != undefined) {
                    // if is numeric
                    at_least_a_numeric_cell = at_least_a_numeric_cell || !(isNaN($scope.rows[i][col].rowvalues));
                } else {
                    console.log("$scope.rows[" + i + "][" + col + "] is undefined !!", $scope.rows[i][col]);
                }
            }
        }
        return at_least_a_numeric_cell;
    };
}]);

qmModule
    .controller(
        'elemsToShowCtrller', [
            '$scope',
            '$http',
            'QM',
            'QMGraph',
            'ElemsToShow',
            'MapManagement',
            'WSOntologyAreas',
            'WSGetGraphResult',
            function ($scope, $http, QM, QMGraph, ElemsToShow, MapManagement, WSOntologyAreas,
                WSGetGraphResult) {
                console.log('elemsToShowCtrller executed');
                // $scope.elemSelected = function(data) {
                // console.log('elemSelected', data);
                // };

                $scope.visible = {};
                $scope.visibleByInstance = {};
                MapManagement.initResultsLayer('Selected Results');
                ElemsToShow.init();
                $scope.elemSelectAllchecked = false;
                // Calculus vars
                // $scope.calculusToChoose = [ "Sum", "Count",
                // "Avg", "Min", "Max" ];
                $scope.calculusToChoose = QM.queries.resultcalculus.types;
                // $scope.calculusChoosen = [];
                $scope.calculusChoosen = QM.queries.resultcalculus.selected;
                // $scope.calculusResult = {};
                $scope.calculusResult = QM.queries.resultcalculus.selected;

                /**
                 * Generate a file to export the data from each subgraph table
                 * subgraph: array with the identifiers of the subgraph nodes
                 */
                $scope.exportTable = function (subgraph) {

                    // Search all the nodes from the network graph,
                    // identify the networks
                    var ids = QMGraph.eDS.getIds();
                    var linksInDaGraph = [];
                    for (var i = 0; i < ids.length; ++i) {
                        var id = ids[i];
                        var edge = QMGraph.eDS.get(id);
                        // if the edge is part of the subgraph
                        if (subgraph.indexOf(edge.from) != -1 || subgraph.indexOf(edge.to) != -1) {
                            // var d = $scope.getDataSetWithId(QMGraph.nDS,
                            // edge.from);
                            // var r = $scope.getDataSetWithId(QMGraph.nDS,
                            // edge.to);
                            var d = QMGraph.nDS.get(edge.from);
                            var r = QMGraph.nDS.get(edge.to);
                            linksInDaGraph.push({
                                domainuri: d.uri,
                                domainid: d.id,
                                relationuri: edge.prop,
                                propid: edge.id,
                                rangeuri: r.uri,
                                rangeid: r.id,
                            });
                        }
                    }
                    var nodes_sueltos = QMGraph.getNodesSueltos();

                    var jsonLinks = angular.toJson(linksInDaGraph);
                    var params = {
                        links: jsonLinks,
                        nodessueltos: nodes_sueltos,
                        format: 'csv',
                        queryid: QM.queries.selected,
                    };

                    // TODO: implement the http request to call the
                    // WS and offer the results

                    WSGetGraphResult.GetGraphResultExport(params).success(function (data) {
                        var filename = Math.floor((Math.random() * 1000000) + 1);
                        var file = new Blob([data], {
                            type: 'application/csv'
                        });
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        // a.href = 'data:attachment/csv,' +
                        // csvString;
                        a.href = fileURL;
                        a.target = '_blank';
                        // a.download = d.label + '-' + d.id +
                        // '.csv';
                        a.download = 'query_exportable_table_' + filename + '.csv';

                        document.body.appendChild(a);
                        a.click();
                    });
                };

                // ngRepeat filter
                $scope.filterIsNotArea = function (nodeid) {
                    var node = QMGraph.nDS.get(nodeid);
                    console.log("qM.filterIsNotArea(", nodeid, ")");
                    if (nodeid != undefined && node != null) {
                        return !QM.isArea(node.uri);
                    } else {
                        return false;
                    }
                };

                $scope.updateAttribute = function (nodeid) {
                    // $scope.resourceChoosen[nodeid] = undefined;
                    // $scope.searchText[nodeid] = undefined;

                    // init filter object
                    QM.queries.resultfilters.selected[nodeid] = {
                        type: undefined,
                        params: {},
                    };
                    $scope.updateGraphDataValues(nodeid);
                };

                $scope.getConceptColor = function (k) {
                    return MapManagement.conceptColors[k];
                };

                /**
                 * Updates all the columns with the results
                 */
                $scope.updateAllGraphDataValues = function () {
                    $scope.showLoadingShow('Updating query results...', true, 'Cancel update');
                    var allNodes = QMGraph.nDS.get();

                    // Loop through all the graph nodes with results
                    // (key at the graphResults object
                    for (var nodeID in $scope.graphResults) {
                        $scope.updateGraphDataValues(nodeID); // Hacer
                        // filtro
                        // añadiendo
                        // filtros
                        if ($scope.calculusChoosen[nodeID] != undefined && $scope.calculusChoosen[nodeID].type != undefined) {
                            // there's a calculus selected
                            $scope.updateCalculus(nodeID);
                        }
                    }

                };

                /**
                 * Updates de values of all the calculus from the whole table
                 * (Avg, Min, Max,...)
                 */
                $scope.updateCalculus = function (nodeID) {

                    // DP selected
                    var dpselected = QM.getDataPropertySelected(nodeID);

                    // selected
                    var calculus = $scope.calculusChoosen[nodeID].type; // What
                    // to
                    // calculate:
                    // [Avg,Min,...]
                    var queryid = QM.queries.selected;
                    var filters = $scope.getDataFilters(nodeID);
                    // Call WS
                    total = $scope.getSubgraphCalculus(nodeID, dpselected.uri, calculus, filters, queryid);

                };

                /**
                 * Devuelve el valor del calculus (avg, count, etc. del tipo
                 * seleccionado)
                 */
                $scope.getSubgraphCalculus = function (nodeID, nodeAttribute, calculus, filters, queryid) {

                    var jsonToSend = {
                        queryid: queryid,
                        node_id: nodeID,
                        node_attribute_uri: nodeAttribute,
                        calculus: calculus,
                        filters: filters,
                    };

                    WSGetGraphResult.GetGraphCalculus({
                        subgraphNodes: angular.toJson(jsonToSend),
                    }).success(function (data) {

                        $scope.calculusResult[nodeID].result = data[calculus];
                        console.log("WSGetGraphResult.GetGraphCalculus: ", data);
                    });

                };

                $scope.elemSelected = function (rowCol, sg) {
                    // show the map
                    if (!$scope.mapVisible) {
                        $scope.toggleMap(true);
                    }
                    for (var i = 0; i < sg.length; ++i) {
                        var nodeID = sg[i];
                        var resultInstance = rowCol[nodeID];
                        var instanceuri = undefined;
                        if (QM.isArea(QMGraph.nDS.get(nodeID).uri)) {
                            // break;
                            // do nothing
                        } else {

                            if (resultInstance != undefined) {
                                instanceuri = resultInstance.rowuri; // uri
                                // from
                                // the
                                // instance
                                // of
                                // the
                                // row
                            }
                            // id from the instance of the row
                            var rowid = rowCol.id;
                            $scope.addElementToTheMap(rowid, instanceuri, nodeID);
                        }
                    }
                };

                $scope.addElementToTheMap = function (rowid, instanceuri, nodeID) {
                    if ($scope.visible[rowid] == undefined || $scope.visible[rowid] == true) {
                        // check if gotta create the instance,
                        // otherwise, it would be already shown
                        if ($scope.visibleByInstance[instanceuri] == undefined) {
                            $scope.visibleByInstance[instanceuri] = [];
                            // MapManagement.initResultsLayer('Selected
                            // Data');
                            WSOntologyAreas.IsGeoConcept({
                                uri: instanceuri,
                                nodeid: nodeID,
                            }).success(function (data) {
                                if (data.isgeoconcept == true) {
                                    var geoWKT = undefined;
                                    if (data.wkt != undefined) {
                                        var cleanWKTSplit = data.wkt.split("> ");
                                        if (cleanWKTSplit.length > 0) {
                                            geoWKT = cleanWKTSplit[1];
                                        }
                                        MapManagement.addElement(data.uri, geoWKT, data.nodeid);
                                    } else {
                                        $scope.showError(QMGraph.nDS.get(nodeID).label + " has no spatial association");
                                    }
                                }
                            }).error(function (data) {
                                $scope.showError(QMGraph.nDS.get(nodeID).label + " has no spatial association");
                                // $scope.showError("Error on IsGeoConcept WS");
                            });
                            // ElemsToShow.add(param);

                        } else {
                            // Make the instance visible
                            if ($scope.visibleByInstance[instanceuri].length > 0) {
                                // do nothing
                            } else {
                                MapManagement.visibleElementIndicator(instanceuri);
                                MapManagement.visibleElementResults(instanceuri);
                            }

                        }
                        $scope.visibleByInstance[instanceuri].push(rowid);

                    } else {
                        var vBI = $scope.visibleByInstance[instanceuri];
                        var pos = vBI.indexOf(rowid);
                        if (pos >= 0) {
                            // remove rowid from the instances
                            vBI.splice(pos, 1);
                        } else {
                            // do nothing
                        }

                        // hide the element?
                        if (vBI.length == 0) {
                            MapManagement.hideElementIndicator(instanceuri);
                            MapManagement.hideElementResults(instanceuri);
                        }
                    }
                };

                $scope.elemSelectAll = function (sg, rows) {

                    // show the map
                    if (!$scope.mapVisible) {
                        $scope.toggleMap(true);
                    }

                    if ($scope.elemSelectAllchecked == true) {
                        // TODO:
                        // Need to obtain the rowid, instanceuri,
                        // nodeID
                        // nodeID: QMGraph.nDS.get();
                        // instanceuri: implementar nueva funcion en
                        // WI

                        // LOOP for all nodes
                        var subgraphs = QM.queries.dataquery[QM.queries.selected].subgraphs;
                        for (var i = 0; i < subgraphs.length; ++i) {
                            var subgraph = subgraphs[i];
                            var nrowsSG = QM.queries.dataquery[QM.queries.selected].nrows[i];
                            for (var j = 0; j < subgraph.length; ++j) {
                                var nodeid = subgraph[j];
                                var node = QMGraph.nDS.get(nodeid);
                                if (QM.isArea(node.uri)) {
                                    // do nothing
                                } else {
                                    var filters = $scope.getDataFilters(nodeid);
                                    // Property selection
                                    var relationuri = undefined;
                                    var gdps = QM.getDataPropertySelected(nodeid);
                                    if (gdps == undefined) {
                                        // not DP already selected,
                                        // choose one
                                        var dps = QM.getDP(node.uri);
                                        if (dps.length > 0) {
                                            relationuri = dps[0].uri;
                                        }
                                    } else {
                                        // DP already selected
                                        relationuri = gdps.uri;
                                    }

                                    var myParams = {
                                        queryid: QM.queries.selected,
                                        nodeid: nodeid,
                                        uriprop: relationuri,
                                        rangeFrom: 1,
                                        rangeTo: nrowsSG,
                                        filters: filters,
                                    };
                                    // TODO: implement the http
                                    // request to call the WS and
                                    // offer the results
                                    var w = WSGetGraphResult.GetGraphDataURIS(myParams).success(function (data) {
                                        for (var rowid in data.uris) {
                                            // for (var rowid = 0; rowid < 5;
                                            // ++rowid){
                                            var rowuri = data.uris[rowid];
                                            $scope.addElementToTheMap(rowid, rowuri, nodeid);
                                            $scope.visible[rowid] = true;
                                        }
                                    }).error(function (data) {
                                        $scope.showError("Error selecting all result rows, elemSelectAll()");
                                    });
                                }
                            }
                        }

                        // END LOOP
                    } else {
                        // deselect all
                        for (var rowid in $scope.visible) {
                            $scope.visible[rowid] = false;
                        }
                        MapManagement.refreshLayers();
                    }

                };
            }
        ]);

qmModule
    .controller(
        'directLinksReplaceCtrl', [
            '$scope',
            '$http',
            'QM',
            'QMGraph', 'WSGetLinks',
            function ($scope, $http, QM, QMGraph, WSGetLinks) {

                $scope.orderLosLinks = function (input) {
                    return input.prop.label;
                };

                $scope.replaceConcept = function () {
                    // TODO: replace the node selected
                    // $scope.replacesToShow.value at the graph
                    var optionChoosed = QMGraph.replacesToShow.value;
                    var selectedNodesIds = QMGraph.network.getSelectedNodes();
                    if (selectedNodesIds.length > 0) {
                        var nodeid = selectedNodesIds[0];
                        var replacingNode = QMGraph.nDS.get(nodeid);
                        // TODO: replace
                        // vis.js .update cannot be done, requires the same
                        // id
                        // subclasses se pueden suprimir todos, superclasses
                        // hay que comprobar

                        var params = {
                            concepturi: replacingNode.uri,
                            withinstances: QMGraph.withInstances,
                        };

                        // var connectedEdges =
                        // QMGraph.getConnectedEdges(replacingNode.id);

                        // QMGraph.nDS.remove(replacingNode.id);
                        // var newNodeID = QMGraph.addNode({
                        // uri : optionChoosed.uri,
                        // label : optionChoosed.label,
                        // });
                        //							
                        // for (var i in connectedEdges ){
                        // var edgeid = connectedEdges[id];
                        // var edge = QMGraph.eDS.get(edgeid);
                        // var nodeTo = edge.to;
                        // QMGraph.eDS.add({
                        // // id : edge.id,
                        // from : newNodeID,
                        // to : nodeTo,
                        // label : edge.label,
                        // prop : edge.prop,
                        // });
                        // }

                        // get links of new node
                        //WSGetLinks.GetLinksBoth(params).success(function (data) {
                        // TODO: Offer client possible links from the
                        // node selected
                        // var selecteNodes =
                        // $scope.network.getSelectedNodes();
                        var selecteEdges = QMGraph.network.getSelectedEdges();

                        //var nodePosibleDestinies = data['asdomain'];
                        //var nodePosibleOrigins = data['asrange'];

                        // REPLACE the node in the graph
                        var newNodeID = QMGraph.addNode({
                            uri: optionChoosed.uri,
                            label: optionChoosed.label,
                        });


                        // REPLACE the node connections in the graph
                        for (var i = 0; i < selecteEdges.length; ++i) {
                            var id = selecteEdges[i];
                            // var edge = $scope.eDS.get(id);
                            var newEdge = {}
                            var edge = QMGraph.eDS.get(id);
                            if (edge.from == replacingNode.id) {
                                var node = QMGraph.nDS.get(edge.to);
                                //if (nodePosibleDestinies.indexOf(node.uri) >= 0) {
                                edge.from = newNodeID;
                                //}
                            } else if (edge.to == replacingNode.id) {
                                var node = QMGraph.nDS.get(edge.from);
                                //if (nodePosibleOrigins.indexOf(node.uri) >= 0) {
                                edge.to = newNodeID;
                            }
                            //QMGraph.editEdge(newEdge);
                            if (optionChoosed.relation.includes("(sub)")) {
                                //update the edge on the graph
                                QMGraph.eDS.update(edge);
                            } else {
                                //Remove the edge from the graph
                                QMGraph.eDS.remove(edge.id);
                            }
                            //Remove the old node, once updated the edge
                            QMGraph.nDS.remove(replacingNode.id);

                            // $('#edges-n-replace').dialog('close');
                            //});
                        }
                    } else {
                        //do-nothing: no selected nodes
                    }

                };

                $scope.addEdgeAccept = function () {
                    // Extract the data from the choosen link
                    var linkToAdd = QMGraph.descomposedPropsToChoose.link;
                    if (linkToAdd == undefined) {
                        console.log("WARNING: Need to select an option to add the Edge");
                        return;
                    }

                    var domain = linkToAdd.domain;
                    var prop = linkToAdd.prop;
                    var range = linkToAdd.range;

                    var rangeid = QMGraph.addNode({
                        uri: range.uri,
                        label: range.label,
                    });

                    var selectedNodesIds = QMGraph.network.getSelectedNodes();
                    if (selectedNodesIds.length > 0) {
                        domainid = selectedNodesIds[0];
                    }

                    // Add the edge in the Graph
                    QMGraph.addEdge({
                        from: domainid,
                        to: rangeid,
                        label: prop.label,
                        prop: prop.uri,
                    });
                };
            }
        ]);


qmModule.directive('viewMapInstance', function () {
    return {
        restrict: 'E',
        templateUrl: 'html-templates/map-view-results.html',
    };
});


qmModule.directive('selectEdge', function () {
    return {
        restrict: 'E',
        templateUrl: 'html-templates/new-query-edge-selection.html'
    };
});

qmModule.directive('selectPath', function () {
    return {
        restrict: 'E',
        templateUrl: 'html-templates/new-query-path-selection.html'
    };
});

qmModule.directive('selectEdgePath', function () {
    return {
        restrict: 'E',
        templateUrl: 'html-templates/new-query-edge-n-path-selection.html'
    };
});