var mapModule = angular.module('mapOL', ['queryTabModule']);


mapModule.controller('mapOLController', ['$scope', function ($scope) {

}]);

mapModule.controller('showMapCtrl', ['$scope', 'MapManagement',
    function ($scope, MapManagement) {

        $scope.$watch($scope.ctrlDF, function () {
            if ($scope.ctrlDF != undefined) {
                $scope.ctrlDF.featureAdded = function (input) {
                    // RegisterFeatureAdded(fixedLayers["Editable"]);
                    $scope.showCreateAreaForm(input); // function defined
                    // in angularjs
                };
            }
        });

        // Hack
        // $($scope.dialogContainer).dialog('open');
        // MapManagement.mapInit();
        // $scope.mapVisible = false;
        // $($scope.dialogContainer).dialog('close');
        // End Hack

        $scope.toggleMap = function (visibility) {
            if (visibility != undefined) {
                $scope.mapVisible = visibility;
            } else {
                $scope.mapVisible = !$scope.mapVisible;
            }
        };
        $scope.openDialog = function () {
            var isOpen = $($scope.dialogContainer).dialog('isOpen');
            if (!isOpen) {
                $($scope.dialogContainer).dialog('open');
                // if ($scope.mapInitialized == false) {
                // $scope.mapInit();
                // $scope.mapInitialized = true;
                // }
            } else {
                $($scope.dialogContainer).dialog('close');
            }
        };
    }]);

/**
 * Map Add Area controller
 */
mapModule
    .controller(
        'AddAreaCtrl', [
            '$scope',
            '$timeout',
            '$location',
            'QueryAreas',
            'MapManagement',
            function ($scope, $timeout, $location, QueryAreas,
                MapManagement) {

                $scope.OLControlActive = MapManagement.OLControlActive;
                $scope.formcreatearea = {}; // contains form data
                // when creating an area
                $scope.multipolygonFeature = [];
                $('#add-area-button').tooltip({
                    animation: true,
                    delay: {
                        show: 10,
                        hide: 5000,
                    },
                    trigger: 'manual',
                });

                $scope
                    .$watch(
                        'MapManagement.map.layers',
                        function () {
                            for (iLayer in MapManagement.map.layers) {
                                var layer = MapManagement.map.layers[iLayer];
                                $scope.layersChecked[layer.id] = true;
                                layer
                                    .setVisibility($scope.layersChecked[layer.id]);
                            }

                        });
                $scope.getSelectedFeatures = MapManagement.getSelectedFeatures;
                $scope.removeSelectedAreas = function () {
                    var sf = MapManagement.getSelectedFeatures();
                    if (sf.length <= 0) {
                        $scope
                            .showError("You must select one or more areas on the map");
                        return;
                    } else {
                        for (k in sf) {
                            var feature = sf[k];
                            $scope.removeArea(feature);
                        }
                    }
                    MapManagement.refreshLayers();
                };

                $scope.layersChecked = {};
                $scope.toggleMyLayer = function (layerID, howToSet) {
                    var vLayer = MapManagement.map.getLayersBy(
                        'id', layerID);

                    if (howToSet == undefined) {
                        var ischecked = $scope.layersChecked[layerID];
                        if (vLayer.length > 0) {
                            vLayer[0].setVisibility(ischecked);
                        }
                    } else {
                        $scope.layersChecked[layerID] = howToSet;
                        vLayer[0].setVisibility(howToSet);
                    }
                };

                $scope.getLayers = MapManagement.getLayers;

                // $scope.toggleControl =
                // MapManagement.toggleControl;
                $scope.toggleControl = function (selectedKey) {
                    for (key in MapManagement.panelControls) {
                        var control = MapManagement.panelControls[key];
                        if (key == selectedKey) {
                            // keep actived the seleced control
                            control.activate();
                        } else {
                            // desactive all non-selected controls
                            control.deactivate();
                        }
                    }
                };

                $scope
                    .$watch(
                        'MapManagement.panelControls.drawFeature',
                        function () {
                            if (MapManagement.panelControls.drawFeature != undefined) {
                                MapManagement.panelControls.drawFeature.events
                                    .register(
                                        'featureadded',
                                        MapManagement.panelControls.drawFeature,
                                        function (
                                            event) {
                                            $scope
                                                .showCreateAreaForm([event.feature]);
                                        });
                            }
                        });
                // define a function that is called to handle a
                // create polygon event from utils.js
                $scope.showCreateAreaForm = function (features) {

                    if (features == undefined) {
                        var sf = $scope.getSelectedFeatures();
                        if (sf.length <= 0) {
                            // abort add, gotta select an area at OL
                            // $('#add-area-button').tooltip('show');
                            // $timeout(function() {
                            // $('#add-area-button').tooltip('hide');
                            // }, 3000);
                            $scope
                                .showError("You must select one or more areas on the map");
                            return;
                        } else {
                            var geometriesArr = [];
                            for (var i in sf) {
                                // get geometries
                                var feature = sf[i];
                                geometriesArr
                                    .push(feature.geometry); // Get
                                // the
                                // geometries
                                // to
                                // generate
                                // the
                                // multipolygon

                                var fgeo = feature.geometry;
                                var fvec = new OpenLayers.Feature.Vector(
                                    fgeo);
                                var el_wkt = $scope.wktFormat
                                    .write(fvec);
                                console.log(
                                    "el wkt de la geometria " + i + ": ", el_wkt);
                            }

                            // Create an OL Feature with those
                            // geometries
                            var mpGeometries = new OpenLayers.Geometry.MultiPolygon(
                                geometriesArr);

                            $scope.multipolygonFeature = new OpenLayers.Feature.Vector(
                                mpGeometries);
                            // layer.addFeatures([feature]);
                            // //useful in the future?
                            var idSplitted = $scope.multipolygonFeature.id
                                .split("_");
                            var label = 'AREA_' + idSplitted[idSplitted.length - 1];
                            $scope.multipolygonFeature.attributes = {
                                id: $scope.multipolygonFeature.id,
                                label: label,
                                wkt: $scope.wktFormat
                                    .write($scope.multipolygonFeature),
                            };

                            var layerEditable = MapManagement.map
                                .getLayersBy('name', 'Editable')[0];
                            layerEditable
                                .addFeatures([$scope.multipolygonFeature]);
                            $scope.formcreatearea = {
                                editableparams: ['label'],
                                id: $scope.multipolygonFeature.attributes.id, // default
                                // id
                                // assigned
                                // by
                                // Openlayers
                                label: $scope.multipolygonFeature.attributes.label,
                                wkt: $scope.multipolygonFeature.attributes.wkt,
                                openlayersobject: $scope.multipolygonFeature, // default
                                // whole
                                // object
                                // assigned
                                // by
                                // Openlayers
                            };
                        }
                    } else {
                        // area created with drawFeature
                        var sf = features;
                        var feature = undefined;
                        for (key in sf) {
                            feature = sf[key];
                            var idSplitted = feature.id.split("_");
                            var label = 'AREA_' + idSplitted[idSplitted.length - 1];
                            feature.attributes = {
                                id: feature.id,
                                label: label,
                                wkt: $scope.wktFormat
                                    .write(feature),
                            };
                        }
                        $scope.formcreatearea = {
                            editableparams: ['label'],
                            id: feature.attributes.id,
                            label: feature.attributes.label,
                            wkt: feature.attributes.wkt,
                            openlayersobject: feature,
                        };
                    }

                    $scope.showAddArea($scope);
                    // $('#modalCreateAreaForm').modal('show');
                    console.log("features=", features, ",sf=", sf);

                    MapManagement.refreshLayers();
                    $scope.OLControlActive.active = 2;
                    $scope.toggleControl('navigation');

                };

                $scope.saveNewArea = function () {
                    // TODO: salvar los que se hayan modificado en
                    // el objeto de openlayers (en principio solo
                    // label)
                    $scope.formcreatearea['openlayersobject'].attributes.label = $scope.formcreatearea.label;
                    QueryAreas.areasimworkingwith.values
                        .push($scope.formcreatearea['openlayersobject']);
                    console.log("QueryAreas.getAllAreas()=",
                        QueryAreas.getAllAreas());
                    if (QueryAreas.areasimworkingwith.value == null) { // Choose
                        // a
                        // default
                        // value
                        QueryAreas.areasimworkingwith.value = QueryAreas.areasimworkingwith.values[0];
                    }

                    $scope.hideDialog();

                    MapManagement.refreshLayers();
                    $scope.OLControlActive.active = 2;

                    // if ($scope.newAreaDoing == true) {
                    // $scope.toggleMap();
                    // $scope.newAreaDoing = false;
                    // }

                };

                $scope.removeArea = function (feature) {
                    if (feature == undefined) {
                        feature = $scope.formcreatearea['openlayersobject'];
                    } else {
                        // already got it
                    }

                    MapManagement
                        .removeFeaturesFromLayer([feature]);
                };

            }]);

mapModule.directive('mapView', function () {
    return {
        restrict: 'E',
        templateUrl: 'map.jsp'
    };
});

mapModule
    .factory(
        'MapManagement', [
            'QMGraph',
            'GeoStyle',
            function (QMGraph, GeoStyle) {
                // var map; // elements to show
                var MapManagement_Functions = {};
                MapManagement_Functions.conceptColors = [];
                MapManagement_Functions.OLControlActive = {
                    active: 2
                };
                MapManagement_Functions.map = undefined;
                MapManagement_Functions.panelControls = {
                    navigation: undefined,
                    drawFeature: undefined,
                };

                MapManagement_Functions.refreshLayers = function (
                    layerid) {
                    if (layerid == undefined) {
                        var layers = map.layers;
                        for (k in layers) {
                            if (layers[k].redraw != undefined) {
                                layers[k].redraw();
                            } else {

                            }
                        }
                    } else {
                        this.map.layers[layerid].redraw();
                    }
                };

                MapManagement_Functions.toggleControl = function (
                    elementKey) {

                    var control = MapManagement_Functions.panelControls[elementKey];
                    if (!control.active) {
                        control.activate();
                    } else {
                        control.deactivate();
                    }

                };

                MapManagement_Functions.IsSelectLayer = function (id) {
                    if (id == MapManagement_Functions.fixedLayers["Landlots"].id || id == MapManagement_Functions.fixedLayers["Blocks"].id || id == MapManagement_Functions.fixedLayers["Editable"].id)
                        return true;
                    return false;
                };

                // each time a layer is selected/unselected on
                // Switch control
                MapManagement_Functions.mapLayerChanged = function (
                    event) {
                    var activate = false;
                    // selectControl.unselectAll();
                    var eventlayer = event.layer;
                    var layers = MapManagement.layerSwitcher.layerStates;
                    if (eventlayer == null) { // se han eliminado
                        // todas las capas
                        activate = true;
                    } else if (eventlayer.geMapManagement_Functions
                        .bility() == true && MapManagement_Functions
                            .IsSelectLayer(eventlayer.id)) {
                        activate = true;

                    } else {
                        if (layers != null) {
                            for (var i = 0; i < layers.length; i++) {
                                if (MapManagement_Functions
                                    .IsSelectLayer(layers[i].id)) {
                                    if (layers[i].visibility == true && layers[i].id != eventlayer.id) {
                                        activate = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (activate && MapManagement_Functions.selectControl.active == false) { // active
                        // select
                        // Feature, deactive
                        // popups
                        MapManagement_Functions.selectControl
                            .activate();
                        for (var i = 0; i < layers.length; i++) {
                            if (IsConceptLayer(layers[i].id)) {
                                UnRegisterPopupEvent(getLayer(layers[i].id));
                            }
                        }
                    } else if (!activate && MapManagement_Functions.selectControl.active == true) { // deactive
                        // select
                        // feature, active
                        // popups on click
                        // event
                        MapManagement_Functions.selectControl
                            .deactivate();
                        for (var i = 0; i < layers.length; i++) {
                            if (MapManagement_Functions
                                .IsConceptLayer(layers[i].id)) {
                                MapManagement_Functions
                                    .RegisterPopupEvent(getLayer(layers[i].id));
                            }
                        }

                    }
                };

                MapManagement_Functions.RegisterPopupEvent = function (
                    layer) {
                    var listen = layer.events.listeners;
                    if (listen["featureclick"] == null || listen["featureclick"].length == 0)
                        layer.events.register("featureclick", map,
                            featureclick, true);
                };

                MapManagement_Functions.getLayer = function (id) {
                    for (var i = 0; i < MapManagement_Functions.map.layers.length; i++) {
                        var lay = MapManagement_Functions.map.layers[i];
                        if (lay.id == id)
                            return lay;
                    }
                    return null;
                };

                // not-used, utilsjs.mapInit() is the used-one
                MapManagement_Functions.mapInit = function () {
                    MapManagement_Functions.fixedLayers = {};
                    var zoom = 16;
                    var map_options = {
                        eventListeners: {
                            "changelayer": mapLayerChanged,
                        },
                        numZoomLevels: 5,
                    };
                    MapManagement_Functions.map = new OpenLayers.Map(
                        'map', map_options);

                    // Base layer (raster)
                    var layer = new OpenLayers.Layer.OSM(
                        "Simple OSM Map");
                    MapManagement_Functions.map.addLayer(layer);
                    fixedLayers["Base"] = layer;

                    // Selection layers
                    layer = new OpenLayers.Layer.Vector("Editable");
                    MapManagement_Functions.map.addLayer(layer);
                    fixedLayers["Editable"] = layer;
                    RegisterSelectEvent(layer);

                    layer = new OpenLayers.Layer.Vector("Landlots");
                    MapManagement_Functions.map.addLayer(layer);
                    fixedLayers["Landlots"] = layer;
                    RegisterSelectEvent(layer);

                    layer = new OpenLayers.Layer.Vector("Blocks");
                    MapManagement_Functions.map.addLayer(layer);
                    fixedLayers["Blocks"] = layer;
                    RegisterSelectEvent(layer);
                    // smendoza
                    // When feature selected, offer it at the data
                    // query
                    // 24/02 added manually to manage multipolygons
                    // fixedLayers["Blocks"].events.on({
                    // featureselected : function(eventInput) {
                    // showCreateAreaForm(eventInput.feature);
                    // }
                    // });
                    // fixedLayers["Blocks"].events.on({
                    // featureselected : function(eventInput) {
                    // showCreateAreaForm(eventInput.feature);
                    // }
                    // });

                    // Toolbar
                    MapManagement_Functions.drawFeature = new OpenLayers.Control.DrawFeature(
                        fixedLayers["Editable"],
                        OpenLayers.Handler.Polygon, {
                            'displayClass': 'olControlDrawFeaturePolygon',
                        });
                    // drawFeature.featureAdded = function(input) {
                    // //
                    // RegisterFeatureAdded(fixedLayers["Editable"]);
                    // showCreateAreaForm(input); // function
                    // defined in angularjs
                    // };
                    MapManagement_Functions.ctrlNav = new OpenLayers.Control.Navigation({
                        zoomWheelEnabled: true,
                        defaultDblClick: function (event) {
                            return;
                        },
                        mouseWheelOptions: {
                            interval: 200, // {Integer} In
                            // order to
                            // increase
                            // server
                            // performance,
                            // an
                            // interval (in
                            // milliseconds) can
                            // be
                            // set to reduce the number of
                            // up/down events called.
                            // maxDelta : 4, // {Integer}
                            // Maximum delta to collect before
                            // breaking from the
                            // current interval.
                            // delta : 1, // {Integer} When
                            // interval is set, delta collects
                            // the mousewheel
                            // z-deltas of the
                            // events that
                            // // occur within the interval.
                            // cumulative : true, // {Boolean}
                            // When interval is set: true to
                            // collect all the
                            // mousewheel
                            // z-deltas, false to
                            // // only record the delta
                            // direction (positive or negative)
                        }
                    });

                    MapManagement_Functions.panelControls.navigation = ctrlNav;
                    MapManagement_Functions.panelControls.drawFeature = drawFeature;

                    // var panelControls = [ ctrlNav, drawFeature ];
                    var toolbar = new OpenLayers.Control.Panel({
                        displayClass: 'olControlEditingToolbar',
                        defaultControl: panelControls[navigation],
                    });

                    for (var key in MapManagement_Functions.panelControls) {
                        toolbar
                            .addControl(MapManagement_Functions.panelControls[key]);
                    }

                    MapManagement_Functions.map.addControl(toolbar);
                    // end Toolbar

                    // TODO: modifications through the polygon
                    var ctrlMF = new OpenLayers.Control.ModifyFeature(
                        fixedLayers["Editable"]);
                    MapManagement_Functions.map.addControl(ctrlMF);

                    // end smendoza
                    MapManagement_Functions.map
                        .addControl(new OpenLayers.Control.PanZoom());
                    MapManagement_Functions.map
                        .addControl(new OpenLayers.Control.MousePosition());
                    MapManagement_Functions.layerSwitcher = new OpenLayers.Control.LayerSwitcher();
                    MapManagement_Functions.map
                        .addControl(MapManagement_Functions.layerSwitcher);

                    MapManagement_Functions.selectControl = new OpenLayers.Control.SelectFeature(
                        [fixedLayers["Editable"],
                        fixedLayers["Landlots"],
                        fixedLayers["Blocks"]], {
                            clickout: true,
                            toggle: false,
                            multiple: false,
                            hover: false, // CAMBIANDO
                            // multiple:true
                            // permite
                            // seleccionar
                            // varios.
                            // Problema: no sincronizaría con el
                            // tree
                            toggleKey: "ctrlKey", // ctrl key
                            // removes
                            // from
                            // selection
                            multipleKey: "shiftKey", // shift
                            // key
                            // adds
                            // to
                            // selection

                        });
                    MapManagement_Functions.map
                        .addControl(MapManagement_Functions.selectControl);
                    MapManagement_Functions.selectControl
                        .activate();

                    // Log("Loading Data...");
                    // AddDataToSelectLayers();
                    MapManagement_Functions.layerSwitcher
                        .maximizeControl();
                    MapManagement_Functions.map.setCenter(
                        new OpenLayers.LonLat(236959.00265,
                            5068571.27635), zoom);
                    // Inf(messageTab1);
                };

                MapManagement_Functions.initResultsLayer = function (
                    layerName) {
                    MapManagement_Functions.wkt = new OpenLayers.Format.WKT();
                    // Create OL Layer
                    if (MapManagement_Functions.layerResults == undefined) {
                        // var renderer =
                        // OpenLayers.Util.getParameters(window.location.href).renderer;
                        // renderer = (renderer) ? [ renderer ] :
                        // OpenLayers.Layer.Vector.prototype.renderers;
                        var layerResultsOptions = {
                            styleMap: new OpenLayers.StyleMap({
                                'default': {
                                    strokeColor: "#00FF00",
                                    strokeOpacity: 1,
                                    strokeWidth: 3,
                                    fillColor: "#FF5500",
                                    fillOpacity: 0.5,
                                    pointRadius: 6,
                                    pointerEvents: "visiblePainted",
                                    // label with \n
                                    // linebreaks
                                    // label : "Label:
                                    // ${uri}",
                                    label: "LabelRR",
                                    fontColor: "#000000",
                                    fontSize: "24px",
                                    fontFamily: "Courier New, monospace",
                                    fontWeight: "bold",
                                    // labelAlign :
                                    // "${align}",
                                    // labelXOffset :
                                    // "${xOffset}",
                                    // labelYOffset :
                                    // "${yOffset}",
                                    labelOutlineColor: "white",
                                    labelOutlineWidth: 3
                                }
                            }),
                            // renderers : renderer
                        };

                        MapManagement_Functions.layerResults = new OpenLayers.Layer.Vector(
                            layerName, layerResultsOptions);
                        // Add Layer to the OL Map
                        // MapManagement_Functions.map = map;
                        MapManagement_Functions.map
                            .addLayer(MapManagement_Functions.layerResults);
                    } else {
                        MapManagement_Functions.layerResults
                            .removeAllFeatures(); // clear the
                        // features
                        // on
                        // the layer
                    }
                    MapManagement_Functions.conceptColors = [];
                };

                MapManagement_Functions.initIndicatorsLayer = function (
                    layerName) {
                    MapManagement_Functions.wkt = new OpenLayers.Format.WKT();
                    // Create OL Layer
                    if (MapManagement_Functions.layerIndicators == undefined) {
                        MapManagement_Functions.layerIndicators = new OpenLayers.Layer.Vector(
                            layerName);
                        // Add Layer to the OL Map
                        // this.map = map;
                        MapManagement_Functions.map
                            .addLayer(MapManagement_Functions.layerIndicators);
                    } else {
                        MapManagement_Functions.layerIndicators
                            .removeAllFeatures(); // clear the
                        // features on the
                        // layer
                    }
                    MapManagement_Functions.conceptColors = [];
                };

                MapManagement_Functions.hideElement = function (
                    layer, featureInstanceUri) {
                    var tmpFeature = layer.getFeaturesByAttribute(
                        'uri', featureInstanceUri);
                    for (var i = 0; i < tmpFeature.length; ++i) {
                        tmpFeature[i].style.display = 'none';
                        layer.redraw();
                    }
                };

                MapManagement_Functions.hideElementIndicator = function (
                    instanceuri) {
                    if (MapManagement_Functions.layerIndicators == undefined) {
                        // do nothing
                        return;
                    } else {
                        MapManagement_Functions
                            .hideElement(
                                MapManagement_Functions.layerIndicators,
                                instanceuri);
                    }

                };
                MapManagement_Functions.hideElementResults = function (
                    instanceuri) {
                    if (MapManagement_Functions.layerResults == undefined) {
                        // do nothing
                        return;
                    } else {
                        MapManagement_Functions
                            .hideElement(
                                MapManagement_Functions.layerResults,
                                instanceuri);
                    }
                };
                MapManagement_Functions.visibleElement = function (
                    layer, instanceuri) {
                    var tmpFeature = layer.getFeaturesByAttribute(
                        'uri', instanceuri);
                    for (var i = 0; i < tmpFeature.length; ++i) {
                        tmpFeature[i].style.display = 'inline';
                        // tmpFeature[i].style.display = undefined;
                        layer.redraw();
                    }
                };
                MapManagement_Functions.visibleElementIndicator = function (
                    instanceuri) {
                    if (MapManagement_Functions.layerIndicators == undefined) {
                        // do nothing
                        return;
                    } else {
                        MapManagement_Functions
                            .visibleElement(
                                MapManagement_Functions.layerIndicators,
                                instanceuri);
                    }
                };
                MapManagement_Functions.visibleElementResults = function (
                    instanceuri) {
                    if (MapManagement_Functions.layerResults == undefined) {
                        // do nothing
                        return;
                    } else {
                        MapManagement_Functions
                            .visibleElement(
                                MapManagement_Functions.layerResults,
                                instanceuri);
                    }
                };

                MapManagement_Functions.getRandomColor = function (
                    nodeid) {
                    if (MapManagement_Functions.conceptColors[nodeid] == undefined) {
                        var letters = '0123456789ABCDEF'.split('');
                        var color = '#';
                        for (var i = 0; i < 6; i++) {
                            color += letters[Math.floor(Math
                                .random() * 16)];
                        }
                        MapManagement_Functions.conceptColors[nodeid] = color;
                    } else {
                        // do nothing, color already assigned
                    }
                    return MapManagement_Functions.conceptColors[nodeid];

                };

                MapManagement_Functions.existsFeatureByUri = function (
                    layer, instanceuri) {
                    return (layer.getFeaturesByAttribute('uri',
                        instanceuri).length > 0);
                };

                MapManagement_Functions.existsFeatureByUriResults = function (
                    instanceuri) {
                    return MapManagement_Functions
                        .existsFeatureByUri(
                            MapManagement_Functions.layerResults,
                            instanceuri);
                };
                MapManagement_Functions.existsFeatureByUriIndicator = function (
                    instanceuri) {
                    return MapManagement_Functions
                        .existsFeatureByUri(
                            MapManagement_Functions.layerIndicators,
                            instanceuri);
                };

                MapManagement_Functions.addElement = function (
                    instanceuri, geoWKT, nodeid) {
                    // adding results from the data query
                    if (!MapManagement_Functions
                        .existsFeatureByUriResults(instanceuri)) {
                        var OLfeatures = this.wkt.read(geoWKT); // OpenLayers.Feature.Vector
                        OLfeatures.attributes['uri'] = instanceuri;
                        var fc = MapManagement_Functions
                            .getRandomColor(nodeid);
                        var selected_polygon_style = GeoStyle
                            .geoStyle(geoWKT, fc);
                        // {
                        // fillColor : fc,
                        // fillOpacity : 0.4, // default
                        // strokeWidth : 2,
                        // strokeColor : 'black',
                        // };
                        OLfeatures.style = selected_polygon_style;
                        MapManagement_Functions.layerResults
                            .addFeatures([OLfeatures]);
                        console
                            .log(
                                "MapManagement elemSelected GEO for concept:",
                                instanceuri,
                                ", OpenLayers.Feature.Vector:",
                                OLfeatures);
                        MapManagement_Functions.layerResults
                            .addFeatures([OLfeatures]);
                    } else {
                        // do nothing, don't create again

                    }

                };

                MapManagement_Functions.addElementIndicator = function (
                    indicator) {
                    var instanceuri = indicator.uri;
                    var geoWKT = indicator.areawkt;
                    var fc = indicator.gradingcolor;

                    // adding results from the data query
                    if (!MapManagement_Functions
                        .existsFeatureByUriIndicator(instanceuri) && geoWKT != undefined) {
                        var test_wkt = "POLYGON ((236785.89965335 5068913.190225,236623.47096827 5068738.8182543,236678.41008234 5068578.7782263,236953.1056527 5068562.0576264,236907.72116716 5068992.0159104,236785.89965335 5068913.190225))";
                        var test_OLfeatures = this.wkt
                            .read(test_wkt); // OpenLayers.Feature.Vector

                        var cleanWKTSplit = geoWKT.split("> ");
                        if (cleanWKTSplit.length > 0) {
                            geoWKT = cleanWKTSplit[1];
                        }
                        var OLfeatures = this.wkt.read(geoWKT); // OpenLayers.Feature.Vector
                        OLfeatures.attributes['uri'] = instanceuri;
                        // All in LINESTRING
                        var selected_polygon_style = GeoStyle
                            .geoStyle(geoWKT, fc);
                        // Curve, LinearRing, LineString,
                        // MultiLineString, MultiPoint,
                        // MultiPolygon, Point,
                        // Polygon

                        OLfeatures.style = selected_polygon_style;
                        MapManagement_Functions.layerIndicators
                            .addFeatures([OLfeatures]);
                        console
                            .log(
                                "MapManagement elemSelected GEO for concept:",
                                instanceuri,
                                ", OpenLayers.Feature.Vector:",
                                OLfeatures);
                        MapManagement_Functions.layerIndicators
                            .addFeatures([OLfeatures]);
                    } else {
                        // do nothing, don't create again

                    }

                };

                MapManagement_Functions.removeFeaturesFromLayer = function (
                    features) {
                    var layer = MapManagement_Functions.map
                        .getLayersBy('name', 'Editable');
                    if (layer.length > 0) {
                        layer[0].removeFeatures(features);
                        // layer[0].destroyFeatures(features);
                        MapManagement_Functions.refreshLayers();
                    } else {
                        // Houston, we have a problem: the layer was
                        // hard-coded and now incorrect
                        console
                            .log("Houston, we have a problem: layer hard-coded and incorrect");
                    }

                };

                MapManagement_Functions.getLayers = function (
                    daClass) {
                    if (daClass == undefined) {
                        daClass = "OpenLayers.Layer.Vector";
                    }
                    var vectorLayers = MapManagement_Functions.map
                        .getLayersByClass(daClass);
                    return vectorLayers;
                };

                MapManagement_Functions.getSelectedFeatures = function () {
                    var selectedFeatures = [];
                    var layers = MapManagement_Functions
                        .getLayers("OpenLayers.Layer.Vector");
                    for (key in layers) {
                        var vLayer = layers[key];
                        selectedFeatures = selectedFeatures
                            .concat(vLayer.selectedFeatures);
                    }
                    return selectedFeatures;
                };

                return MapManagement_Functions;

            }]);

mapModule.factory('GeoStyle', [function () {
    var geoStyle = function (wkt, color) {
        var selected_polygon_style = {};
        if (wkt.indexOf('LINESTRING') > -1) {
            selected_polygon_style = {
                strokeWidth: 8,
                strokeColor: color,
            };
        } else if (wkt.indexOf('POLYGON') > -1) {
            selected_polygon_style = {
                fillColor: color,
                fillOpacity: 0.4, // default
                strokeWidth: 2,
                strokeColor: 'black',
            };
        } else if (wkt.indexOf('POINT') > -1) {
            selected_polygon_style = {
                pointRadius: 6,
                fillColor: color,
                fillOpacity: 1, // default
            };
        } else {

            // OTHER GEOMETRIES?
        }
        return selected_polygon_style;
    };
    return {
        geoStyle: geoStyle,

    };
}]);

mapModule.factory('ElemsToShow', ['QMGraph', function (QMGraph) {
    var ets; // elements to show

    var init = function () {
        this.ets = [];
    };

    var elemSelected = function (param) {
        // var node = QMGraph.nDS.get(nodeid);

        this.ets.push(param);
        console.log("elemSelected called", this.ets);

    };
    //
    // var addElement = function(element) {
    // ets.push(element);
    // };
    //
    // var elementsToShow = function() {
    //
    // };

    return {
        add: elemSelected,
        init: init,
        ets: this.ets,
    };
}]);
