var ucModule = angular.module('urbanConceptsModule', []);

ucModule.controller('uCController', ['$scope', '$http', function ($scope, $http) {
	$scope.UrbanConceptsID = '#jstreeUrbanConcepts';
}]);

ucModule.controller('uCJsTreeController', ['$scope', '$http', function ($scope, $http) {
	var data = [];

	$($scope.UrbanConceptsID).jstree({
		'core': {
			'themes': {
				'variant': 'large',
				'icons': false,
			},
			'data': data,
		},
		'search': {
			'show_only_matches': true,
		},
		'types': {
			'default': {
				'max_children': 50,
				'max_depth': 50,
				'icon': 'https://cdn2.iconfinder.com/data/icons/starwars/icons/32/clone-old.png',
			},
			'my_type': {
				'max_children': 50,
				'max_depth': 20,
				'icon': 'https://cdn2.iconfinder.com/data/icons/starwars/icons/32/clone-old.png',
			},
			'#': {
				'max_children': 50,
				'max_depth': 50,
				'icon': 'https://cdn2.iconfinder.com/data/icons/starwars/icons/32/clone-old.png',
			},
			// 'demo' : {
			// 'icon' : 'glyphicon glyphicon-ok'
			// }
		},
		'checkbox': {
			'keep_selected_style': true,
			'tie_selection': false,
		},
		'plugins': ['wholerow', 'checkbox', 'types', 'sort', 'search']
	});

	// listen when the state of a node checkbox changes
	// $($scope.UrbanConceptsID).on('check_node.jstree', function(e, data) {
	// // get all the checked nodes
	// var checkedConcepts = $($scope.UrbanConceptsID).jstree("get_checked", true, true);
	// console.log("$scope.loadData (): ", "checkedData ", checkedConcepts);
	// for ( var concept in checkedConcepts) {
	// var ck = checkedConcepts[concept];
	// console.log("$scope.loadData (", ck, "): ");
	// $scope.loadData(ck);
	// }
	// });

	var to = false;
	$('#jstreeUCSearchInput').keyup(function () {
		if (to) {
			clearTimeout(to);
		}
		to = setTimeout(function () {
			var v = $('#jstreeUCSearchInput').val();
			$($scope.UrbanConceptsID).jstree(true).search(v);
		}, 250);
	});

	$http({
		method: 'POST',
		url: wsPath_qt + '/GetUrbanConceptsJsTree',
		params: {},
	}).success(function (data, status, headers, config) {
		$($scope.UrbanConceptsID).jstree(true).settings.core.data = data;
		$($scope.UrbanConceptsID).jstree(true).refresh();
	}).error(function (data, status, headers, config) {
		console.log(' ERROR IN GetUrbanConceptsJsTree');
	});

	// $($scope.UrbanConceptsID).on('changed.jstree', function(e, data) {
	// console.log(data.selected);
	// });
	// $('button').on('click', function() {
	// $($scope.UrbanConceptsID).jstree(true).select_node('child_node_1');
	// $($scope.UrbanConceptsID).jstree('select_node', 'child_node_1');
	// $.jstree.reference($scope.UrbanConceptsID).select_node('child_node_1');
	// });

}]);

ucModule.controller('loadDataOntoOLCtrl', ['$scope', '$http', 'WSGetDataProperties', 'WSGetGraphResult', 'QM',
	'QMGraph', function ($scope, $http, WSGetDataProperties, WSGetGraphResult, QM, QMGraph) {
		console.log("loadDataOntoOLCtrl defined");

		$scope.loadData = function (node) {
			console.log("queryGraphModule.scope.network: ", $scope.network);
			console.log("queryGraphModule.scope.nDS: ", $scope.nDS);
			console.log("queryGraphModule.scope.eDS: ", $scope.eDS);

			console.log("queryGraphModule.QMGraph.network: ", QMGraph.network);
			console.log("queryGraphModule.QMGraph.nDS: ", QMGraph.nDS);
			console.log("queryGraphModule.QMGraph.eDS: ", QMGraph.eDS);

			console.log("System Loading Data at OpenLayers... (", node.a_attr.uri, ")");
			var uriConcept = node.a_attr.uri;
			var nodeChecked = node.state.checked;

			var sfeatures = getSelectedFeatures();
			for (var i = 0; i < sfeatures.length; i++) {
				var selectedFeature = sfeatures[i];
				if (!conceptLayers.hasOwnProperty(uriConcept))
					conceptLayers[uriConcept] = new Array();
				var layer = null;
				var concept = LayerSelectedFeature(conceptLayers[uriConcept], selectedFeature);
				if (concept != null) {
					layer = concept.layer;
				}
				if (nodeChecked) {
					if (layer != null) {
						// layer created before, just show it and don't create it again
						showLayer(layer);
						concept.checked = true;
					} else {
						// the layer has not been created before. Create and add it
						// conceptLayers is actualized inside because the ajax call is
						// asynchronous
						var numtotal = 5; // por poner algo...
						CreateLayer(uriConcept, selectedFeature, numtotal);

					}
				} else if (layer != null) { // element unchecked but created previously
					hideLayer(layer);
					concept.checked = false;
				}
			}
		};

		$scope.loadCheckedConcepts = function () {
			var checkedConcepts = $($scope.UrbanConceptsID).jstree("get_checked", true, true);

			if (checkedConcepts.length > 0) {
				for (var concept in checkedConcepts) {
					var ck = checkedConcepts[concept];
					var uri = ck.a_attr.uri;
					console.log("$scope.loadData (", ck, "): ");
					$scope.loadData(ck);
				}
			} else {
				var msg = 'Select at least one concept, please.';
				$scope.showError(msg);

			}
		};

		$scope.doTheQuery = function () {
			// TODO: create a graph
			// 1) Create the Area selected at the ontology (point, line, polygon, multipolygon...)
			// 2) Create the links between selected concepts (solo el concept selected, más "padre")
			// and the areas
			// 3) Execute the WS GetGraphResult();

			// 1) Create the Area selected at the ontology (point, line, polygon, multipolygon...)
			var sf = $scope.getSelectedFeatures();
			var geometriesArr = [];
			for (var i in sf) {
				var feature = sf[i]; // get geometries
				geometriesArr.push(feature.geometry); // Get the geometries to generate the
				// multipolygon
			}

			if (sf.length == 0) {
				// $scope.mapErrorVisible = true;
				var errorMsg = 'Too much information to show in the map. You should select or define an area.';
				$scope.showDialog(errorMsg);
			} else {
				$scope.mapErrorVisible = false;
			}

			// Create an OL Feature with those geometries
			var mpGeometries = new OpenLayers.Geometry.MultiPolygon(geometriesArr);
			var multipolygonFeature = new OpenLayers.Feature.Vector(mpGeometries);
			multipolygonFeature.wkt = $scope.wktFormat.write(multipolygonFeature)
			// TODO: check if area with this wkt already exists (no imprescindible ahora, una misma
			// query tendrá un
			// mismo
			// area)

			var areaObject = {
				label: multipolygonFeature.id,
				wkt: multipolygonFeature.wkt,
			};

			// 2) Create the links between selected concepts (solo el concept selected, más "padre")
			// and the areas
			// var checkedConcepts = $($scope.UrbanConceptsID).jstree("get_checked", true, true);

			var nodesInTheGraph = [];
			var conceptsCheckedTop = $($scope.UrbanConceptsID).jstree(true).get_top_checked(true);
			// var conceptsCheckedBottom =
			// $($scope.UrbanConceptsID).jstree(true).get_bottom_checked(true);
			var checkedConcepts = conceptsCheckedTop;

			console.log("checkedConcepts=", checkedConcepts);
			for (var concept in checkedConcepts) {
				var ck = checkedConcepts[concept];
				var conceptUri = ck.a_attr.uri;
				var node = {
					// id : ck.a_attr.id,
					uri: conceptUri,
				};
				nodesInTheGraph.push(node);
			}

			// 3) Execute the WS GetGraphResult();
			var query = {
				area: areaObject,
				nodes: nodesInTheGraph,
			};

			// 1) WSGetGraphResultUC: Generate the graph results
			var z = WSGetGraphResult.GetGraphResultUC(query).then(function (promise) {
				console.log("WSGetGraphResultUC(", query, ").then => promise=", promise);
				QM.add(promise);
				var queryid = promise.data['queryid'];

				for (var x in promise.data.nodes) {
					var eachnode = promise.data.nodes[x];
					// chapuza para comprobar si es un AREA (waiting for isArea()
					// if (eachnode.uri.indexOf('temporal') < 0) {
					if (!QM.isArea(eachnode.uri)) {

						var x = WSGetDataProperties.GetDataProperties(eachnode).then(function (promise) {
							var conceptUri = promise.config.params.prop;
							QM.addDP(conceptUri, promise.data);
							console.log("WSGetDataProperties(", conceptUri, ").then => promise=", promise);

							var dataProperties = QM.getDP(conceptUri);
							var dataPropertiesKeys = Object.keys(dataProperties);
							if (dataProperties != undefined && dataPropertiesKeys.length > 0) {
								var myParams = {
									queryid: queryid,
									nodeid: eachnode.id,
									uriprop: dataProperties[dataPropertiesKeys[0]].uri,
									rangeFrom: 0,
									rangeTo: 9,
									filters: [],
								};
								var paramsJSON = angular.toJson(myParams);
								var w = WSGetGraphResult.GetGraphDataValues(paramsJSON).then(function (promise) {
									console.log("GetGraphDataValues(", paramsJSON, ").then => promise=", promise);
									return promise;
								});
							}
							return promise;
						});

					}
				}

				return promise;
			});

		};

		$scope.bindUCToGraph = function () {
			var conceptsCheckedTop = $($scope.UrbanConceptsID).jstree(true).get_top_checked(true);
			var checkedConcepts = conceptsCheckedTop;
			console.log("$scope.bindUCToGraph (): ", ", checkedData: ", checkedConcepts);
			// QMGraph.nDS.clear();
			// QMGraph.eDS.clear();
			for (var concept in checkedConcepts) {
				var ck = checkedConcepts[concept];
				var uri = ck.a_attr.uri;
				var label = ck.text;
				QMGraph.addNode({
					uri: uri,
					label: label,
				});
			}
			// $scope.$apply();
		};

	}]);
