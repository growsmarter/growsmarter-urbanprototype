//Angularjs.module queryTabModule
var gradingColorsRGB = {
    1: 'rgb(0,0,0)',
    2: 'rgb(99,67,54)',
    3: 'rgb(216,49,9)',
    4: 'rgb(253,233,71)',
    5: 'rgb(236,233,71)',
    6: 'rgb(214,205,64)',
    7: 'rgb(197,212,0)',
    8: 'rgb(164,199,0)',
    9: 'rgb(0,166,76)',
    10: 'rgb(19,110,0)'
};
var gradingColors = {
    1: '#000000',
    2: '#634336',
    3: '#D83109',
    4: '#FDE947',
    5: '#ECE947',
    6: '#D6CD40',
    7: '#C5D400',
    8: '#A4A900',
    9: '#00A64C',
    10: '#136E00',
};

// Web-Services path
//var wsPath_qt = "..";
//var wsPath_qt = "http://localhost:58080/UrbanPrototype";
var wsPath_qt = "http://127.0.0.1:58080/UrbanPrototype";
//var wsPath_qt = "http://growsmarter.bsc.es:8080/UrbanPrototype";
//var wsPath_qt = "http://localhost:8080/UrbanPrototype";

var tabsModule = angular.module('queryTabModule', ['ui.grid', 'angularTreeview',]);

tabsModule.directive('urbanConcepts', function () {
    return {
        restrict: 'E',
        templateUrl: 'tab-urban-concepts.html'
    };
});

tabsModule.filter('avgFilter', function () {
    return function (arr) {
        var sumTotals = 0;
        for (var i = 0; i < arr.length; i++) {
            sumTotal = sumTotal + arr[i].taxes;
        }
        var avg = sumTotals / arr.length;
        return avg;
    };
});

tabsModule.filter('orderObjectBy', function () {
    return function (input, attribute) {
        if (!angular.isObject(input))
            return input;

        var array = [];
        for (var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function (a, b) {
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return a - b;
        });
        return array;
    }
});

// directive para el template sobre para el template de los
tabsModule.directive('popOver', function ($compile) {
    var itemsTemplate = "<ul class='unstyled'><li ng-repeat='item in items'>{{item}}</li></ul>";
    var getTemplate = function (contentType) {
        var template = '';
        switch (contentType) {
            case 'items':
                template = itemsTemplate;
                break;
        }
        return template;
    };
    dalink = function (scope, element, attrs) {
        var popOverContent;
        if (scope.items) {
            var html = getTemplate("items");
            popOverContent = $compile(html)(scope);
        } else {
            popOverContent = scope.items;
            console.log("scope.items");
        }
        var options = {
            trigger: 'hover',
            container: 'body',
            // content : popOverContent,
            content: scope.comment,
            placement: 'right',
            html: true,
            title: scope.title
        };
        console.log("options=" + options);
        $(element).popover(options);
    };
    return {
        restrict: "A",
        transclude: true,
        template: "<span ng-transclude></span>",
        link: dalink,
        scope: {
            // items : '=',
            comment: '@',
            title: '@'
        }
    };
});
/**
 * Urban concept Add Area controller
 */
tabsModule.controller('urbanConceptsCtrl', ['$scope', function ($scope) {
    // AjaxGetUrbanConcepts();
}]);

tabsModule.controller('LoadedConceptsCtrl', ['$scope', function ($scope) {
    $scope.conceptsLoaded = ["Concepto1", "Concepto2", "Concepto3"];
}]);

tabsModule.controller('sparqlQueryCtrl', ['$scope', function ($scope) {
    AjaxGetSparqlPrefixes();
}]);

// angular-ui popover patch
tabsModule.directive('bsPopover', function () {
    return function (scope, element, attrs) {
        element.find("button[rel=popover]").popover({
            html: 'true'
        });
    };
});
// end angular-ui popover patch

tabsModule.controller('dQueryController', [
    '$scope',
    '$compile',
    '$http',
    '$sce',
    'QM',
    'QMGraph',
    'WSGetLinks',
    'WSGetPaths',
    'WSDataQuery',
    'WSGetGraphResult',
    'WSGetDataProperties',
    'QueryAreas',
    'QMGraphPaths',
    function ($scope, $compile, $http, $sce, QM, QMGraph, WSGetLinks, WSGetPaths, WSDataQuery, WSGetGraphResult,
        WSGetDataProperties, QueryAreas, QMGraphPaths) {

        // Controller variables (when modified->html updates)
        $scope.queryResults = [];
        $scope.limitToResults = 10; // Max results to be shown
        $scope.conceptsFoundShown = $scope.limitToResults;
        $scope.qm = QM;

        $scope.descomposedPropsToChoose = {};
        $scope.searchText = {};

        // pagination vars
        $scope.currentPage = {}; // pagination current page
        $scope.maxPagesOffer = {};
        $scope.rowsPerPage = {};
        $scope.bigTotalItems = {};
        $scope.numPages = {}; // Math.floor($scope.bigTotalItems /
        // $scope.rowsPerPage);

        $scope.searchQueryData = [];
        $scope.gridOptionsSearchQuery = {
            data: 'searchQueryData',
            enableColumnResize: true,
        };
        $scope.rows = [];
        $scope.conceptColors = {};

        $scope.withInstances = QMGraph.withInstances; // put the reference
        // to the factory
        // QMGraph

        $scope.typeColors = {
            'EINDIVIDUAL': '#1E90FF',
            'CLASS': '#8FBC8F',
            'a': '#20B2AA',
            'b': '#CD5C5C',
            'c': '#FF8C00',
            'd': '#B22222',
            'e': '#FF69B4',
            'f': '#DA70D6',
            'g': '#CDC0B0',
            'h': '#D2B48C',
            'i': '#ADFF2F'
        };

        $scope.clusteringColors = {
            'People and legal entities': 'red',
            'Administrative areas': 'pink', // #634336
            'Buildings and facilities': 'GreenYellow',
            'Energy and environment': '#FDE947',
            'Land use': 'BlueViolet',
            'Mobility Network': 'GoldenRod',
            'Public services': '#C5D400',
            'Asset use': 'HotPink',
            'Indicators and Measurements': '#00A64C',
            'Urban Concept Dimension': 'grey',
            'Other': '#136E00',
        };
        $scope.nDS = undefined;
        $scope.eDS = undefined;
        $scope.initNetworkGraph = function () {

            // QMGraph.init($scope);
            // $('#mynetworkX').appendTo($('#dquery-graph')); //Para mover
            // el map al dialog

            $scope.nDS = QMGraph.nDS;
            $scope.eDS = QMGraph.eDS;
            $scope.network = QMGraph.network;

            var onSelect = function (properties) {

                var selectedEdgesIds = this.getSelectedEdges();
                var selectedNodesIds = this.getSelectedNodes();

                if (selectedNodesIds.length > 0) {
                    // TODO:
                    // 1) get possible relations links with range the
                    // selected node
                    // 2) offer possible relations to the user (hide/block
                    // the div)
                    // 3) update the graph with the selected options
                    var id = selectedNodesIds[0];
                    var node = $scope.nDS.get(id);
                    console.log("URI selected node=" + node.uri);
                    $scope.updateNodeRelations(node, $scope.withInstances);
                    $scope.updateNodeReplaces(node);
                    $('#edges-n-replace').dialog('open');
                }

                if (selectedEdgesIds.length > 0) {
                    for (var i = 0; i < selectedEdgesIds.length; i++) {
                        var seId = selectedEdgesIds[i];
                        var edge = this.edges[seId];
                        console.log('ed.id=' + edge.id + ',ed.fromId=' + edge.fromId + ',ed.toId=' + edge.toId);
                    }
                }
            };

            $scope.network.on('doubleClick', function (properties) {
                console.log("double click made");
            });

            $scope.network.on('select', function (properties) {
                $scope.showPathEdges(properties);
            });
            $scope.network.on('onConnect', function (event, properties, senderId) {
                console.log('evento network ON-CONNECT', event, properties);
            });

            $scope.nDS.on('add', function (event, properties, senderId) {
                console.log('event NODE ADDED: ', $scope.nDS, event, properties);

                $('#adding-node').modal('show');

                // after adding a node to the dataset, get its properties
                for (var i = 0; i < properties.items.length; ++i) {
                    var nodeid = properties.items[i];
                    var node = $scope.nDS.get(nodeid);
                    QM.addDataProperties(node.uri);
                }

            });
            $scope.eDS.on('add', function (event, properties, senderId) {
                console.log('event EDGE ADDED: ', $scope.eDS, event, properties);
            });
            $scope.nDS.on('*', function (event, properties, senderId) {
                console.log('event', $scope.nDS, event, properties);
            });
            $scope.eDS.on('*', function (event, properties, senderId) {
                console.log('event', $scope.eDS, event, properties);
            });
        };

        /**
         * Resets all the variables when an search is done
         */
        $scope.initDataQuery = function () {
            QMGraph.cleanNodes();
            QMGraph.cleanEdges();
            $scope.queryResults = [];
            $scope.colToShow = []; // clean the selected columns
            $scope.rows = []; // data showed
            $scope.clusteringResults = {};
            $scope.nodeRelationsOffered = [];
            $scope.descomposedPropsToChoose = {
                link: false,
                values: {}
            };
            $scope.propertyLabels = {}; // data properties the nodes can
            // offer in data tables

            $scope.searchText = {}; // search for each table column of the
            // data query results
            QMGraphPaths.functionParams = []; // parameters required to
            // add the function

            // javascript functions declared in the scope to be called from
            // the html
            $scope.parseFloat = parseFloat;
            $scope.parseInt = parseInt;
            $scope.isNaN = isNaN;
            $scope.guiLog = Log; // utils.js

            // Visibilities
            $scope.dquery_show_subgraph_data_values = false; // hide
            // results
            $scope.dquery_results_visible = false;
            $scope.dquery_relations_visible = false;
            $scope.dquery_graph_visible = false;
            $scope.dquery_show_subgraph_data_values = false;
            $scope.dquery_show_attr_options = false; // the data
            // properties
            // options, after
            // adding
            // a link
            QMGraphPaths.dquery_graph_edgeConnection = false; // nodes
            // conections
            // visibility
        };
        $scope.dQueryExecute = function (q) {
            $scope.showLoadingShow('Searching concepts...', true, 'Ok');

            if (q != undefined) {
                // visibilities
                $scope.dquery_relations_visible = false;
                $scope.dquery_show_subgraph_data_values = false;
                $scope.dquery_show_attr_options = false; // the data
                // properties
                // options,
                // after adding
                // a link
                QMGraphPaths.dquery_graph_edgeConnection = false; // nodes
                // conections
                // visibility
                // $scope.genGraph($scope.nDS, $scope.eDS); // create the
                // network graph

                // REST API search
                // (Async call!!, dQueryExecute continues execution
                // independently of the RESTCall)
                // $scope.restDataQueryCall('Hotels in the same street as
                // hospitals');
                $scope.restDataQueryCall(q);
                $scope.limitToResults = 10; // results shown willl be
                $scope.conceptsFoundShown = $scope.limitToResults;
            } else {
                // not query, nothing to do
            }
            $scope.$evalAsync(function () {
                // QMGraph.network.fit({nodes:QMGraph.nDS.getIds()});
                // QMGraph.network.fit();
                //					QMGraph.network.focus(QMGraph.nDS.getIds()[0]);
            });

        };

        /**
         * Updates the $scope.selectedConcepts array with the selected
         * concepts
         */
        $scope.updateRelations = function (concept) {

            $scope.getProperties(concept.label, concept.uri);

            var nodeid = QMGraph.addNode(concept);

            $scope.selectedConcepts = [];
            for (var r in $scope.queryResults) {
                // if result selected, add it as selected
                if ($scope.queryResults[r].selected) {
                    $scope.selectedConcepts.push($scope.queryResults[r].uri);
                }
            }

            if ($scope.selectedConcepts.length > 0)
                $scope.dquery_relations_visible = true;
            else
                $scope.dquery_relations_visible = false;

            return nodeid;

        };

        /**
         * Called when a link (domain, property, range)must be added to the
         * graph
         */
        $scope.addRelationToGraph = function () {
            var d = $scope.relSelected.domain;
            var p = $scope.relSelected.prop;
            var r = $scope.relSelected.range;
            console.log("addRelationToGraph()->" + d.label);
        };

        /**
         * Called when concept is clicked. Generates graph using vis.js
         * library.
         */
        $scope.generateGraph = function (concepts) {
            var nodes = [];
            // get the selected concepts
            for (var r in $scope.queryResults) {
                var result = $scope.queryResults[r];

                if (result.selected == true) {
                    nodes.push(result);
                }
            }

            // create an array with edges, if at least one concept selected
            var edges = [];
            if (0 < nodes.length) {
                var nedges = 5;
                for (var i = 0; i < nedges; i++) {
                    var src = Math.floor(Math.random() * nodes.length);
                    var dst = Math.floor(Math.random() * nodes.length);
                    console.log('src=' + src + ',dst=' + dst + ',nodes.length=' + nodes.length);
                    var vfrom = nodes[src].id;
                    var vto = nodes[dst].id;

                    edges.push({
                        from: vfrom,
                        to: vto,
                        label: nodes[src].label + '-' + nodes[dst].label,
                    });
                }
            }

            if ((0 < nodes.length) || (concepts != undefined)) {
                // if nodes, graph div visible & generate the graph
                $scope.dquery_graph_visible = true;
                // $scope.genGraph($scope.nDS, $scope.eDS);
            } else {
                // if no nodes, graph div not visible
            }
        };

        /**
         * Converts object obj to an array. Used for the orderBy similarity
         * at queryresults
         */
        $scope.convertArray = function (obj) {
            var arr = [];
            for (k in obj) {
                arr.push(obj[k]);
            }
            return arr;
        };

        $scope.getNumber = function (num) {
            return new Array(num);
        };

        // when "get data" pressed, clean the resources inputs
        $scope.cleanSearchText = function () {
            $scope.searchText = {};
        };
        // when "get data" pressed, clean the resources inputs
        $scope.cleanSearchText = function (nodeUri) {
            if ($scope.searchText == undefined || nodeUri == undefined) {
                $scope.searchText = {};
            } else {
                delete $scope.searchText[nodeUri];
            }
        };

        function clearPopUp() {
            var saveButton = document.getElementById('saveButton');
            var cancelButton = document.getElementById('cancelButton');
            saveButton.onclick = null;
            cancelButton.onclick = null;
            var div = document.getElementById('network-popUp');
            div.style.display = 'none';
        };

        $scope.saveData = function (data, callback) {
            var idInput = document.getElementById('node-id');
            var labelInput = document.getElementById('node-label');
            var div = document.getElementById('network-popUp');
            data.id = idInput.value;
            data.label = labelInput.value;
            clearPopUp();
            callback(data);
        };

        /**
         * subgraphNodes: array of nodes identifiers that compund the
         * subgraph whose values will be requested
         */
        $scope.updateGraphAllDataValues = function (subgraphNodes) {

            var jsonSubgraphNodes = angular.toJson(subgraphNodes);

            // TODO: implement the http request to call the WS and offer the
            // results
            $http({
                method: 'POST',
                url: wsPath_qt + '/GetGraphAllDataValues',
                params: {
                    subgraphNodes: jsonSubgraphNodes,
                }
            }).success(function (data, status, headers, config) {

                console.log("/GetGraphAllDataValues");
                // if ($scope.dataSet == undefined)
                // $scope.dataSet = [];
                // $scope.dataSet.push([ 'Trident', 'Internet Explorer 4.0',
                // 'Win 95+', '4', 'X' ]);
                // // $(document).ready(function() {
                //
                // if ($scope.dqueryDT == undefined) {
                // $scope.dqueryDT = $('#table_id').dataTable({
                // "data" : $scope.dataSet,
                // "columns" : [ {
                // "title" : "Engine"
                // }, {
                // "title" : "Browser"
                // }, {
                // "title" : "Platform"
                // }, {
                // "title" : "Version",
                // "class" : "center"
                // }, {
                // "title" : "Grade",
                // "class" : "center"
                // } ]
                // });
                // } else {
                // $scope.dqueryDT.fnClearTable();
                // $scope.dqueryDT.fnAddData([ 'Trident', 'Internet Explorer
                // 4.0', 'Win 95+', '4',
                // 'Y' ]);
                // $scope.dqueryDT.fnDraw();
                // }
                // } );

                // move udpate
                // $scope.graphResults[node][colname] = data;

                // $scope.updateSearchQueryData(); // GUI vars update

                // $scope.dquery_show_subgraph_data_values = true;

            }).error(function (data, status, headers, config) {
                console.log(" ERROR IN /GetGraphAllDataValues");
            });

        };

        /**
         * Actualiza en $scope.graphResults[] los datos referentes al nodo
         * pasado como parametro. La petición se hace al WS
         * /GetGraphDataValues pasando como parametros el identificador del
         * nodo y la property que sale del nodo.
         */

        // Get the Dropdown menu
        $scope.getDropdown = function (r) {

            $scope.subSuperClasses = [];

            QM.addDataProperties(r.uri); // add to propertyLabels the
            // label of the node
            var params = {
                uri: r.uri,
                withinstances: $scope.withInstances,
            };

            // Get the sub/super classes of the concept r
            WSGetDataProperties.GetSubSuperClasses(params).then(function (promise) {
                var data = promise.data;
                // Anyado concept, para que en la lista al menos haya uno
                $scope.subSuperClasses.push(r);
                // Anyado sub/super clases devueltas por el WS
                for (var k in data) {
                    var concept = data[k];
                    for (var i = 0; i < concept.categories.length; i++) {
                        concept['catcolor'] = $scope.getCategoriesColor(concept.categories);
                        concept.genBorderColor = function () {
                            return $scope.getCategoriesColor(this.categories);
                        };
                    }
                    $scope.subSuperClasses.push(concept);
                }

                // add all the subsuperclasses to the query results
                for (var key in $scope.subSuperClasses) {
                    var x = $scope.subSuperClasses[key];
                    var z = $scope.queryResults[x.uri];
                    // Si ya está en la lista, no es necesario pedirla
                    if ($scope.queryResults[x.uri] == undefined) {
                        $scope.queryResults[x.uri] = x;
                    }
                }
            });

        };

        $scope.getDataSetWithId = function (ds, id) {
            var dsIdentifier = ds.get({
                filter: function (item) {
                    return item.id == id;
                }
            });
            if (0 < dsIdentifier.length) {
                return dsIdentifier[0];
            } else {
                return null;
            }

        };

        $scope.assignNodeIDs = function () {
            var ids = $scope.nDS.getIds();
            for (var i = 0; i < ids.length; ++i) {
                var id = ids[i];
                var nodeToUpdate = $scope.nDS.get(id);
                nodeToUpdate.nodeID = i + 1;
                // update de nodes DataSet
                $scope.nDS.update(nodeToUpdate);
            }
        };

        $scope.searchQuery = function () {

            $scope.gettingData = true;
            $scope.colToShow = []; // clean the selected columns
            $scope.dquery_show_subgraph_data_values = false; // hide
            // results
            $scope.rows = [];
            $scope.cleanSearchText(); // clean resource inputs

            // assign nodeIDs to each graph node, required by
            // GetGraphResults WS
            $scope.assignNodeIDs();

            // Search all the nodes from the network graph, identify the
            // networks
            var linksInDaGraph = [];
            var nodesSueltosInDaGraph = [];
            var ids = $scope.eDS.getIds(); // get the edges in the graph

            if (ids.length <= 0) {
                // nodesSueltos
                var nIds = $scope.nDS.getIds();
                for (var i = 0; i < nIds.length; ++i) {
                    var id = nIds[i];
                    var node = $scope.nDS.get(id);
                    nodesSueltosInDaGraph.push(node);
                    if (node['type'] == undefined || node['type'] != 'AREA') {
                        // $scope.colToShow[id] =
                        // $scope.propertyLabels[id][0];
                        $scope.colToShow[id] = QM.getDP(node.uri)[0];
                    }
                }
            } else {
                // nodos connectados(links)
                for (var i = 0; i < ids.length; ++i) {
                    var id = ids[i];
                    var edge = $scope.eDS.get(id);
                    var d = $scope.getDataSetWithId($scope.nDS, edge.from);
                    var r = $scope.getDataSetWithId($scope.nDS, edge.to);

                    // buscar el edge con ese id (solo se obtiene con
                    // getpaths)
                    if ($scope.edgeFromTo != undefined && $scope.edgeFromTo.values != undefined) {
                        for (var ei in $scope.edgeFromTo.values) {
                            if ($scope.edgeFromTo.values[ei].edgeid == edge.id) {
                                var edgeinfo = $scope.edgeFromTo.values[ei];
                                edge["params"] = QMGraphPaths.functionParams;
                                if (edgeinfo.propertytype == "FUNCTION") {
                                    edge["isfunction"] = true;
                                } else {
                                    edge["isfunction"] = false;
                                }
                            }
                        }
                    }

                    var linkdata = {
                        domain: d,
                        relation: edge,
                        range: r
                    };
                    linksInDaGraph.push(linkdata);

                    // Poner unas propiedades default, para cuando haya
                    // respuesta del WS
                    // Si son AREA no hay que ensenyarlas!!
                    if (d['type'] == undefined || d['type'] != 'AREA') {
                        $scope.colToShow[d.id] = QM.getDP(d.uri)[0];
                    }
                    if (r['type'] == undefined || r['type'] != 'AREA') {
                        $scope.colToShow[r.id] = QM.getDP(d.uri)[0];
                    }

                    console.log("(d=" + edge.from + ",p=" + edge.prop + ",r=" + edge.to + ")");
                }
            }

            var graph = {
                links: linksInDaGraph,
                nodesSueltos: nodesSueltosInDaGraph,
                edgesDS: $scope.eDS.get(),
                nodesDS: $scope.nDS.get(),
            };

            // var jsonGraph = angular.toJson(graph);

            // TODO: implement the http request to call the WS and offer the
            // results

            // Create popup with results (or loading while waiting the WS)
            $("#get-data-results").dialog({
                autoOpen: false,
                resizable: true,
                maxHeight: 600,
                maxWidth: 800,
                minHeight: 200,
                minWidth: 600,
            }).dialogExtend({
                "maximizable": true,
                "dblclick": "maximize",
                "icons": {
                    "maximize": "ui-icon-arrow-4-diag"
                }
            });
            $('#get-data-results').dialog('open');

            WSGetGraphResult.GetGraphResult(graph).success(
                function (data) {
                    // var data = promise.data;
                    $scope.gettingData = false;
                    // The WS call, generated the results (hosted in the
                    // server)

                    /*
                     * // Offer client possible links from the node
                     * selected $scope.graphResults = data; // TODO:
                     * obtener lista de nodos $scope.graphNodes = [];
                     * for ( var node in data) {
                     * $scope.graphNodes.push(node); }
                     */

                    if (data["ERROR-MSG"] != undefined) {
                        console.log(data["ERROR-MSG"]);
                    } else {
                        // Get the queryid results
                        var qid = data['queryid'];
                        var querydata = {
                            queryid: data['queryid'],
                            nrows: data['nrows'],
                            sparqlquery: data['sparqlquery'],
                            graph: graph,
                            subgraphs: data['graph'],
                            currentPage: {}, // pagination
                            // current page
                            maxPagesOffer: {},
                            rowsPerPage: {},
                            bigTotalItems: {},
                        };

                        QM.queries.selected = qid; // added
                        QM.queries.dataquery[qid] = querydata;
                        // QM.queries.dataquery[qid] = querydata;
                        // QM.queries.dataquery["selected"] = qid; //
                        // queryid visible now
                        // QM.queries.dataquery[qid].QM.queries.dataquery[qid].currentPage
                        // = {}; // pagination
                        // // current page
                        // QM.queries.dataquery[qid].maxPagesOffer = {};
                        // QM.queries.dataquery[qid].rowsPerPage = {};
                        // QM.queries.dataquery[qid].bigTotalItems = {};

                        // once GetGraphResults WS is executed, show
                        // data and properties
                        // elections
                        $scope.dquery_show_subgraph_data_values = true;
                        $scope.subgraphs = data['graph'];

                        // set the pagination options
                        for (var sgId in $scope.subgraphs) {
                            // var sg = $scope.subgraphs[sgId];
                            $scope.currentPage[sgId] = 1; // pagination
                            // current
                            // page
                            $scope.maxPagesOffer[sgId] = 3;
                            $scope.rowsPerPage[sgId] = 10;
                            $scope.bigTotalItems[sgId] = data['nrows'][sgId];
                            // $scope.numPages[sgId] =
                            // Math.floor($scope.bigTotalItems[sgId] /
                            // $scope.rowsPerPage[sgId]);

                            QM.queries.dataquery[qid].currentPage[sgId] = 1; // pagination
                            // current
                            // page
                            QM.queries.dataquery[qid].maxPagesOffer[sgId] = 3;
                            QM.queries.dataquery[qid].rowsPerPage[sgId] = 10;
                            QM.queries.dataquery[qid].bigTotalItems[sgId] = data['nrows'][sgId];
                        }

                        // update the results in the GUI
                        for (var i = 0; i < QM.queries.dataquery[qid].subgraphs.length; ++i) {
                            var subgraph = QM.queries.dataquery[qid].subgraphs[i];
                            for (var j = 0; j < subgraph.length; ++j) {
                                var nodeid = subgraph[j];
                                var x = $scope.updateGraphDataValues(nodeid);
                            }
                        }

                        console.log(" SUCCESS IN GetGraphResult");
                        console.log("GetGraphResult - sparql query-> ", data["sparqlquery"]);
                        console.log("GetGraphResult - all queries(QM.queries) -> ", QM.queries);

                        for (var key in data["sparqlquery"]) {
                            QM.queries.dataquery[qid].sparqlquery = data["sparqlquery"][key];
                            $scope.sparqlquery = data["sparqlquery"][key];
                            $scope.guiLog("GetGraphResult sparql query (queryid = " + QM.queries.dataquery.selected + ", subgraph id=" + key + ")=<xmp>" + $scope.sparqlquery + "</xmp>");

                        }
                    }
                });
        };

        /**
         * Filters the front-end with types OPROPERTY && DPROPERTY
         */
        $scope.myCleaning = function (conceptObject) {

            if (conceptObject != undefined && conceptObject.type == "CLASS" && $scope.lastDataQueryResults.indexOf(conceptObject.uri) >= 0) {
                return true;
            } else {
                return false;
            }
        };


        $scope.updatePosibleRanges = function () {

            var links = $scope.descomposedPropsToChoose.prop;
            // get all the labels
            for (var i = 0; i < links; ++i) {
                var l = links[i];
                console.log("algo: ", l);
            }

        };

        $scope.searchQueryData = [];
        $scope.updateSearchQueryData = function (mynode) {
            // TODO: generar los ng-grid's respectivos de cada tabla
            // $scope.graphAtributes = {};
            // for ( var node in $scope.graphNodes) {
            // $scope.graphAtributes[node];
            // }

            // $scope.searchQueryData = [];
            // $scope.searchQueryData = [ {
            // name : "Moroni",
            // age : 50
            // }, {
            // name : "Tiancum",
            // age : 43
            // }, {
            // name : "Jacob",
            // age : 27
            // }, {
            // name : "Nephi",
            // age : 29
            // }, {
            // name : "Enos",
            // age : 34
            // } ];
            var rowSizeForUndefined = 0;
            for (var node in $scope.colToShow) {
                if ($scope.colToShow[node] != undefined) {
                    var colname = $scope.colToShow[node].label;
                    if ($scope.graphResults != undefined && $scope.graphResults[node] != undefined && $scope.graphResults[node][colname] != undefined) {
                        rowSizeForUndefined = $scope.graphResults[node][colname].length;
                    }
                }
            }

            // Los keys son nodes, contienen como cols los attr que se han
            // de mostrar
            $scope.cols = {};
            var sqd = [];
            for (var node in $scope.colToShow) {
                if ($scope.colToShow[node] == undefined) {
                    // data-property list from this node, must contain empty
                    // rows
                    console.log('$scope.colToShow[', node, '] == undefined');
                    var l = node.split("#")[1];
                    $scope.colToShow[node] = {
                        label: l,
                        uri: node
                    };
                    var tmp = [];
                    for (var i = 0; i < rowSizeForUndefined; i++) {
                        tmp.push(l);
                    }
                    $scope.cols[node] = tmp;
                    $scope.graphResults[node] = {};
                    $scope.graphResults[node][l] = tmp;

                } else {

                    var colname = $scope.colToShow[node].label;
                    var colnameWithoutSpaces = colname.replace(/\s+/g, ''); // remove
                    // spaces

                    if ($scope.graphResults == undefined || $scope.graphResults[node] == undefined || $scope.graphResults[node][colname] == undefined) {
                        // llamada al WS getGraphResults (habria que hacerlo
                        // desde el boton)
                        console.log("WARNING: NO DATA YET. PRESS 'GET DATA'");
                    } else {

                        var nrows = $scope.graphResults[node][colname];
                        for (var i = 0; i < nrows.length; i++) {
                            if (sqd[i] == undefined) {
                                sqd[i] = {};
                            }
                            var val = nrows[i].rowvalues;
                            if (val == "" || val == undefined) {
                                val = "empty";
                            } else {
                                sqd[i][String(colnameWithoutSpaces)] = String(val); // para
                                // version
                                // angular
                            }
                            // ng-grid
                            nrows[i].rowvalues = val; // para version HTML
                            // (hace falta?)
                            // zz.push(val);
                        }
                        $scope.cols[node] = nrows;
                        // $scope.cols[node] = zz;
                    }
                }
            }
            $scope.rows = [];
            var rowSize = 10; // hecho manual, tendria que hacerse
            // paginacion
            for (var rowid = 0; rowid < rowSize; rowid++) {
                var row = {};
                for (var node in $scope.graphResults) {
                    if ($scope.colToShow[node] != undefined) {
                        var colname = $scope.colToShow[node].label;
                        var x = $scope.graphResults[node];
                        var y = $scope.graphResults[node][colname];
                        if ($scope.graphResults[node] == undefined || $scope.graphResults[node][colname] == undefined) {
                            row[node] = "was-undefined";
                            console.log("$scope.graphResults[" + node + "][" + colname + "] is undefined");
                        } else {
                            // row[node] =
                            // $scope.graphResults[node][colname][i].rowvalues;
                            row[node] = $scope.graphResults[node][colname][rowid];
                            row.id = rowid + (($scope.currentPage[0] - 1) * 10);
                            if ($scope.graphResults[node][colname][rowid] != undefined) {
                                row.uri = $scope.graphResults[node][colname][rowid].rowuri;
                            }
                            row.size = $scope.graphResults[node][colname].length;
                            rowSize = $scope.graphResults[node][colname].length;
                        }
                    } else {
                        // $scope.colToShow[node] is undefined
                    }
                }
                $scope.rows.push(row);
            }

            $scope.searchQueryData = sqd; // ng-grid lee de
            // searchQueryData

        };

        $scope.jstreeNodesByFilter = {};
        $scope.lastDataQueryResults = [];
        $scope.untrunkeddataquerysize = 0;
        $scope.isExample = QMGraph.example.isExample;
        $scope.restDataQueryCall = function (q) {
            var currentdate = new Date();
            var inittime = currentdate.getMinutes() + ":" + currentdate.getSeconds();
            var work_with_instances = $("#querywithinstances").is(':checked');
            //var work_with_instances = $scope.withInstances;

            var params = {
                query: q,
                withinstances: work_with_instances,
            };

            WSDataQuery.GetDataQuery(params).then(function (promise) {
                var data = promise.data;

                /*                $scope.jstreeNodesByFilter = [];
                                for (var i in data.conceptsfiltered) {
                                    $scope.jstreeNodesByFilter[i] = [];
                                    for (var j in data.conceptsfiltered[i]) {
                                        $scope.jstreeNodesByFilter[i] = $scope.jstreeNodesByFilter[i].concat(data.conceptsfiltered[i][j]);
                                    }
                                }*/

                $scope.jstreeNodesByFilter = data.conceptsfiltered;

                //$scope.updateJSTree(['CLASS'], true);
                //$scope.loading_dquery = true;


                $scope.untrunkeddataquerysize = data.untrunkedsize;

                //merge data into queryResults
                jQuery.extend($scope.queryResults, data.valuestrunked);

                $scope.lastDataQueryResults = [];
                $scope.lastResultSize = 0;
                $scope.jstreeGPH = [];
                $scope.jstreeToFilter = {};
                $scope.searchresultssize = Object.keys(data.valuestrunked).length;

                for (var r in data.valuestrunked) {
                    // get uri's from the last results
                    $scope.lastDataQueryResults.push(r);

                    // set all results as non-selected
                    $scope.queryResults[r].selected = false;
                    // set id with its URI. Needed when generating nodes
                    $scope.queryResults[r].id = $scope.queryResults[r].uri;

                    // WITH THIS FILTERING

                    /*                    if ($scope.queryResults[r].type == "CLASS") {
                                            $scope.lastResultSize++;
                                        }
                                        for (var i = 0; i < data.valuestrunked[r].jstreenodes.length; ++i) {
                                            var jstreenode = data.valuestrunked[r].jstreenodes[i];
                                            $scope.jstreeGPH.push(jstreenode);
                                            $scope.jstreeToFilter[jstreenode.id] = jstreenode;
                                        }*/
                }

                $scope.conceptsFoundShown = Math.min($scope.limitToResults, $scope.lastResultSize);

                // Clustering results by categories
                $scope.getResultCategories();

                for (var r in data.valuestrunked) {
                    $scope.queryResults[r].genBorderColor = function () {
                        return $scope.getCategoriesColor(this.categories);
                    };
                }

                // if there are results, show them out
                $scope.dquery_results_visible = true;

                console.log('status' + status + 'data.valuestrunked=' + data.valuestrunked);

                var endttime = currentdate.getMinutes() + ":" + currentdate.getSeconds();
                console.log("inittime: " + inittime + ', endttime: ' + endttime);


                var paramsGenerateGraphExample = {
                    query: q,
                    concepts: data.valuestrunked,
                };

                // if no example, create an example
                if (QMGraph.nDS.length <= 0 || QMGraph.example.isExample == true) {
                    QMGraph.cleanNodes();
                    QMGraph.cleanEdges();
                    QMGraph.example.isExample = 'loading';
                    WSDataQuery.GenerateGraphExample(paramsGenerateGraphExample).then(function (promise) {
                        var data = promise.data;
                        QMGraph.example.isExample = true;
                        console.log("GenerateGraphExample Testing: ", data);
                        // Generate a graph with data.nodes
                        for (var i in data.nodes) {
                            for (var j in data.nodes[i]) {
                                var nodeuri = data.nodes[i][j];
                                var concept = {
                                    uri: nodeuri,
                                    label: paramsGenerateGraphExample.concepts[nodeuri].label,
                                };
                                var nodeid = $scope.updateRelations(concept);
                            }
                        }

                        // TODO: Generate the edges in the example
                        var nodeids = QMGraph.nDS.getIds();
                        if (nodeids.length == 1) {
                            // do nothing?
                        } else if (nodeids.length > 1) {
                            for (var k = 1; k < nodeids.length; ++k) {
                                var newEdge = {
                                    from: nodeids[0],
                                    to: nodeids[k],
                                    label: "double click to define",
                                };
                                QMGraph.addEdge(newEdge);
                            }
                            // var newEdge = {
                            // from : nodeids[1],
                            // to : nodeids[(Math.random()*10 ) % nodeids.length],
                            // label : "double click to define",
                            // };
                            // QMGraph.addEdge(newEdge);

                        } else { }



                        // QMGraph.network.fit({nodes:QMGraph.nDS.getIds()});
                        // QMGraph.network.fit();


                    }).finally(function () {
                        //F_QMGraph.example.isExample = true;
                    });
                } else {
                    // example already created
                }


                // $scope.closeMdDialog();
            }).finally(function () {
                $scope.closeMdDialog();
            });

        };

        $scope.getCategoriesColor = function (categories) {
            var colors = [];
            for (var i = 0; i < categories.length; i++) {
                var caturi = categories[i].uri;
                // var catsResults = jQuery.grep($scope.clusteringResults,
                // function(e){ return e.uri
                // ==
                // caturi;});
                var catsResults = $scope.clusteringResults[caturi];
                if (catsResults.selected) {
                    colors.push(catsResults.getColor());
                }
            }
            // TODO: now it returns the first color, in the future must
            // return a mixture of all the
            // categories
            if (colors.length > 0) {
                return colors[0];
            } else {
                return '#adadad';
            }
        };

        $scope.selectEdge = function () {
            console.log("selectEdge");
            // TODO: cambiar el id

        };

        $scope.showPathEdges = function (properties) {
            // TODO: get posible edges
            if (properties.edges.length > 0 && properties.nodes.length == 0) {
                $scope.pathSelectionVisible = true;
                $("#path-proposal").dialog({
                    autoOpen: false,
                    resizable: true,
                    maxHeight: 600,
                    maxWidth: 800,
                    minHeight: 200,
                    minWidth: 600,
                });
                $('#path-proposal').dialog('open');
                // TODO:
                for (var i = 0; i < properties.edges.length; ++i) {
                    var selectedEdge = QMGraph.eDS.get(properties.edges[i]);
                    console.log("properties.edges: ", properties.edges[i], ", selectedEdge: ", selectedEdge);
                    var nfrom = QMGraph.nDS.get(selectedEdge.from);
                    var nto = QMGraph.nDS.get(selectedEdge.to);
                    var params = {
                        domainuri: nfrom.uri,
                        rangeuri: nto.uri,
                        populated: $scope.withInstances,
                        selectededgeid: selectedEdge.id,
                    };
                    $scope.disableWSGetLinksFromTo = true;

                    WSGetLinks.WSGetLinksFromTo(params).then(function (promise) {
                        console.log("WSGetLinksFromTo(", params, ")=", promise);
                        // TODO:
                        $scope.edgeFromTo = {
                            selected: undefined,
                            values: [],
                        };

                        var edgeid = promise.config.params.params.selectededgeid;
                        for (var linkid in promise.data) {
                            var link = promise.data[linkid];
                            link.edgeid = edgeid;
                            $scope.edgeFromTo.values.push(link);
                        }

                        if ($scope.edgeFromTo.values.length > 0) {
                            $scope.edgeFromTo.selected = $scope.edgeFromTo.values[0];
                        }
                        $scope.disableWSGetLinksFromTo = false;

                    });
                }

            }
        };

        $scope.updatePaths = QMGraphPaths.updatePaths;

        $scope.addPathEdges = function () {
            console.log("addPathEdges()");
            var edgeid = $scope.edgeFromTo.selected.edgeid;
            var edge = QMGraph.eDS.get(edgeid);
            edge.prop = $scope.edgeFromTo.selected.propertyuri;
            edge.label = $scope.edgeFromTo.selected.propertylabel;
            $scope.pathSelectionVisible = false;
            QMGraph.eDS.update(edge);
            $('#path-proposal').dialog('close');

        };

        $scope.addEdgeConnectAccept = function () {
            QMGraphPaths.dquery_graph_edgeConnection = false;

            // Extract the data from the choosen link
            var domain = QMGraphPaths.propsToChoose.value.domain;
            var prop = QMGraphPaths.propsToChoose.value.prop;
            var range = QMGraphPaths.propsToChoose.value.range;
            var newEdge = {
                from: domain.domainid,
                to: range.rangeid,
                label: prop.label,
                prop: prop.uri,
            };
            if (QMGraphPaths.propsToChoose.value.prop.type == "FUNCTION") {
                // isfunction, add its parameters (introduced by the user)
                newEdge["params"] = QMGraphPaths.functionParams;
                newEdge["isfunction"] = true;
            } else {
                newEdge["params"] = [];
                newEdge["isfunction"] = false;
            }
            // Add the edge in the Graph
            $scope.eDS.add(newEdge);

            // TODO: add the labels of the properties of the nodes in the
            // graph
            QM.addDataProperties(domain.uri);
            QM.addDataProperties(range.uri);

            // Algo mejor que nada, cojo el label para ponerlo en el select
            // de los botones "Get
            // Data"
            if ($scope.queryResults[domain.uri] == undefined) {
                $scope.queryResults[domain.uri] = {
                    label: domain.label,
                    type: domain.type,
                    uri: domain.uri,
                };
                $scope.queryResults[domain.uri].selected = true;
            }
            if ($scope.queryResults[range.uri] == undefined) {
                $scope.queryResults[range.uri] = {
                    label: range.label,
                    type: range.type,
                    uri: range.uri,
                };
                $scope.queryResults[range.uri].selected = true;
            }
        };

        $scope.addEdgeConnectCancel = function () {
            QMGraphPaths.dquery_graph_edgeConnection = false;
        };

        /**
         * Check if the node with the id specified in the graph already
         * exists
         */
        $scope.existsInTheGraph = function (candidateID) {
            var idsInTheGraph = $scope.nDS.getIds();
            if (idsInTheGraph.indexOf(candidateID) >= 0) {
                return true;
            } else {
                return false;
            }
        };

        /**
         * Get the labels of the concept with the uri specified by parameter
         */

        // DEPRECATED!! 2015/10/06
        $scope.addDataProperties = function (concept) {

            if (QM.getDP(concept.uri) == undefined) {
                // pide attributos del concepto

                WSGetDataProperties.GetDataProperties(concept).then(function (promise) {
                    var data = promise.data;
                    var conceptUri = promise.config.params.prop;
                    $scope.propertyLabels[concept.uri] = [];
                    for (var d in data) {
                        var dataProperties = data[d];
                        $scope.propertyLabels[concept.uri].push(dataProperties);
                    }
                    $('#adding-node').modal('hide');
                });
            } else {
                // do nothing: Si ya tengo la info del concepto,
                // unncesessary doing WS request again
                $('#adding-node').modal('hide');
            }
        };

        $scope.myFilterFunction = function (row) {

            var validRow = true;

            for (var k in row) {
                var columnVal = row[k];
                var option = $scope.resourceChoosen[k]; // select which
                if (option == 'Contains') {
                    if ($scope.searchText[k] == undefined) {
                        validRow = validRow && true;
                    } else {
                        var input = $scope.searchText[k].text;
                        validRow = validRow && (columnVal.toLowerCase().indexOf(input.toLowerCase()) > -1);
                    }
                } else if (option == 'Range') {
                    var cint = parseFloat(columnVal);
                    if ($scope.searchText[k] == undefined) {
                        validRow = validRow && true;
                    } else if (($scope.searchText[k].min == undefined || $scope.searchText[k].min == "") && ($scope.searchText[k].max == undefined || $scope.searchText[k].max == "")) {
                        validRow = validRow && true;
                    } else if (($scope.searchText[k].min == undefined || $scope.searchText[k].min == "")) {
                        var max = parseFloat($scope.searchText[k].max);
                        validRow = validRow && (cint < max);
                    } else if (($scope.searchText[k].max == undefined || $scope.searchText[k].max == "")) {
                        var min = parseFloat($scope.searchText[k].min);
                        validRow = validRow && (min < cint);
                    } else {
                        var min = parseFloat($scope.searchText[k].min);
                        var max = parseFloat($scope.searchText[k].max);
                        validRow = validRow && (min < cint && cint < max);
                    }
                }

                if (validRow == false) {
                    // if any column is invalid, not necessary to continue
                    // analyzing columns
                    return false;
                }
            }

            return validRow;
        };

        /**
         * get all categories from data query results, for clustering
         * results
         */
        $scope.getResultCategories = function () {
            var categories = {};
            for (var qr in $scope.queryResults) {
                var cats = $scope.queryResults[qr];
                if (cats.categories != undefined && cats.categories.length > 0) {
                    for (var i = 0; i < cats.categories.length; i++) {
                        var cat = cats.categories[i];
                        if (categories[cat.uri] == undefined) {
                            categories[cat.uri] = {
                                uri: cat.uri,
                                label: cat.label,
                                selected: false,
                                results: [],
                                color: $scope.clusteringColors[cat.label],
                                getColor: function () {
                                    return this.color;
                                },
                            };
                        }
                        categories[cat.uri].results.push($scope.queryResults[qr]);
                    }
                } else {
                    // TODO: put result at the category "OTHER"

                }

            }
            $scope.clusteringResults = categories;
        };

        /**
         * what to do when a category is clicked
         */
        $scope.clickCategory = function (catUri) {
            // var cat = $scope.clusteringResults[catUri];
            // cat.selected = !cat.selected;
            console.log("clickCategory(" + catUri + ")");

        };

        /**
         * With the info passed by parameter, the function adds the most
         * simillar to the selected
         */
        $scope.getProperties = function (labelToSearch, uriToChoose) {
            if (!(uriToChoose in $scope.queryResults)) {
                var params = {
                    params: {
                        query: labelToSearch,
                        withinstances: $scope.withInstances,
                    }
                };
                $http({
                    method: 'POST',
                    url: wsPath_qt + '/GetProperties',
                    params: params,
                }).success(function (data, status, headers, config) {
                    // getting the most similar
                    var highest = 0;
                    var valid = undefined;
                    for (var k in data) {
                        console.log("/GetProperties: data[" + k + "].label=" + data[k].label);
                        if (uriToChoose == data[k].uri) {
                            valid = k;
                            break;
                        } else if (data[k].similarity > highest) {
                            valid = k;
                            highest = data[k].similarity;
                        }
                    }
                }).error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            }
        };

        $scope.getSelectedAreas = function () {
            $scope.selectedAreas = {};
            $scope.selectedAreas["values"] = $scope.getSelectedFeatures();

            for (var i = 0; i < $scope.selectedAreas.values.length; i++) {
                // generate the wkt attribute
                // $scope.selectedAreas.values[i].attributes.wkt =
                // wkt_format.write($scope.selectedAreas.values[i]);
                // generate a defautl label
                // $scope.selectedAreas.values[i].attributes.label =
                // 'defaultmio';
                if ($scope.selectedAreas.values[i].attributes.URI == undefined) {
                    $scope.selectedAreas.values[i].attributes.URI = $scope.selectedAreas.values[i].id;
                }
                // put a default label
                if ($scope.selectedAreas.values[i].attributes.label == undefined) {
                    $scope.selectedAreas.values[i].attributes.label = $scope.selectedAreas.values[i].id;
                }
            }

            if ($scope.selectedAreas.values.length) {
                // Choose a default option for the interface
                $scope.selectedAreas.value = $scope.selectedAreas.values[0];
                $scope.selectedAreas.disabled = false; // Enable the add
                // add area option
            } else {
                $scope.selectedAreas.disabled = true; // Disable the add
                // add area option
            }

            console.log("selectedAreas=", $scope.selectedAreas);
        };

        // $scope.addAreaToTheOntology = QueryAreas.addAreaToTheOntology;
        // $scope.addAreaToTheOntology = function() {
        //
        // // 24/02 unica seleccion, implementado el multipolygon
        // var areas = [ {
        // wkt : QueryAreas.areasimworkingwith.value.attributes.wkt,
        // label : QueryAreas.areasimworkingwith.value.attributes.label,
        // } ];
        //
        // // check if area already exists, does not have URI
        // var areaExists = false;
        // for ( var areaKey in QueryAreas.areasimworkingwith.values) {
        // var areaIWW = QueryAreas.areasimworkingwith.values[areaKey];
        // if (areaIWW.attributes.wkt == areas[0].wkt) {
        // areaExists = true;
        // break;
        // }
        // }
        //
        // if (areaExists &&
        // QueryAreas.areasimworkingwith.value.attributes.wkt != undefined)
        // {
        //
        // // Area not exists in the ontology, gotta create it
        // $http({
        // url : wsPath_qt + '/CreateArea',
        // method : 'POST',
        // params : {
        // polygons : angular.toJson(areas),
        // },
        // }).success(function(data, status, headers, config) {
        // var nodeData = {
        // uri : data.uri,
        // label : QueryAreas.areasimworkingwith.value.attributes.label,
        // wkt : QueryAreas.areasimworkingwith.value.attributes.wkt,
        // type : 'AREA',
        // };
        // QMGraph.addNode(nodeData);
        // // add to the QueryAreas var
        // for ( var areaKey in QueryAreas.areasimworkingwith.values) {
        // var areaIWW = QueryAreas.areasimworkingwith.values[areaKey];
        // if (areaIWW.attributes.wkt == areas[0].wkt) {
        // areaIWW = nodeData;
        // break;
        // }
        // }
        //
        // console.log(data);
        //
        // }).error(function(data, status, headers, config) {
        // console.log("ERROR IN WS /CreateArea");
        // });
        // } else {
        // // Area already exists in the ontology, not necessary to add it
        // again
        // QMGraph.addNode({
        // uri : QueryAreas.areasimworkingwith.value.attributes.URI,
        // label : QueryAreas.areasimworkingwith.value.attributes.label,
        // wkt : QueryAreas.areasimworkingwith.value.attributese.wkt,
        // type : 'AREA',
        // });
        // }
        // };

        // TODO: update the nodes binding the checked with the network query
        // graph
        $scope.bindUCToGraph = function () {
            var conceptsCheckedTop = $('#dqResultsJSTree').jstree(true).get_top_checked(true);
            var checkedConcepts = conceptsCheckedTop;
            $scope.nDS.clear();
            $scope.eDS.clear();
            for (var concept in checkedConcepts) {
                var ck = checkedConcepts[concept];
                var uri = ck.a_attr.uri;
                var label = ck.text;
                QMGraph.addNode({
                    uri: uri,
                    label: label,
                });
            }
            // TODO: add node de la areas
        };

        // init the network graph
        // $scope.initNetworkGraph();
        // init all the data before doing nothing
        $scope.initDataQuery();

    }]);

tabsModule.controller('jstreefilterCtrller', [
    '$scope',
    '$compile',
    '$timeout',
    'QMGraph',
    function ($scope, $compile, $timeout, QMGraph) {
        $scope.tabsFilters = [{
            title: 'Entities',
            active: true,
            parentfilter: ['CLASS'],
        }, {
            title: 'Indicators',
            active: false,
            parentfilter: ['IND'],
        }, {
            title: 'Descriptors', // 'Enum-Types',
            active: false,
            parentfilter: ['ECLASS'],
        }, {
            title: 'All',
            active: false,
            parentfilter: ['ALL'],
            //parentfilter: undefined,
        },];

        $scope.$watch(function () {
            return $scope.jstreeNodesByFilter;
        }, function () {
            // defult is Entities selected
            $scope.updateJSTree(['CLASS'], true);
            $scope.loading_dquery = true;
        });


        $scope.loading_dquery = false;

        // Recorrer todo el arbol y estilizar los nodos del array con la uri
        // que contienen
        $scope.jstreeStyleNodes = function () {
            if ($scope.jstreeToFilterArray != undefined) {
                for (i in $scope.jstreeToFilterArray) {
                    var node = $scope.jstreeToFilterArray[i];
                    $scope.jstreeStyleNode(node);
                }
            } else {
                // do nothing, no results already found
                console.log("");

            }
        };
        $scope.jstreeStyleNode = function (node) {
            console.log('node a hacer ReStyle: ', node);
        };

        $scope.filterjstreeByTypes = function (filterType) {

            // console.log("$scope.jstreeToFilter: ",
            // $scope.jstreeToFilter);

            for (var i in $scope.jstreeToFilter) {
                var node = $scope.jstreeToFilter[i];
                console.log('filterjstreeByTypes node: ', node);
                console.log('uri: ', node.data.uri, ', enum-type-category:', node.data.enum_type_category);
            }

            if (filterType == undefined || (filterType.indexOf('IND') < 0 && filterType.indexOf('CLASS') < 0 && filterType
                .indexOf('ECLASS') < 0)) {
                $scope.jstreeToFilterArray = $scope.jstreeGPH;
            } else {
                // adapt the tree filtering by type
                var tokeep = {};
                for (var i in $scope.jstreeToFilter) {
                    var tokeepnodepath = [];
                    var node = $scope.jstreeToFilter[i];
                    if (node.id == node.parent) {
                        // do nothingtab.
                        console.log("****************** nodeid=", node.id, ', node.parent=', node.parent);
                    }
                    node.parent = filterjstreeRecursiveGetBranches($scope.jstreeToFilter[node.id], filterType,
                        tokeepnodepath);
                    if (tokeepnodepath.length > 0) {
                        tokeep[node.id] = tokeepnodepath;
                    } else {
                        // dontkeep ;)
                    }
                }
                $scope.jstreeToFilterArray = [];
                // filtrar ramas
                var ramasValidas = {};
                for (var nodepathid in tokeep) {
                    var containsallfilters = true;
                    for (var x in filterType) {
                        var filter = filterType[x];
                        // he de comprobar que el path contiene todos los
                        // filtros
                        // pq necesito el nodo entero? para usar todos los
                        // links del path!!
                        var containfilter = false;
                        for (var nid in tokeep[nodepathid]) {
                            var pathnode = tokeep[nodepathid][nid];
                            if (filter == pathnode.data.type) {
                                // if el nodo de ese tipo, me vale el path
                                containfilter = true;
                            } else {
                                // do nothing, if node not of this type
                                console.log('nodo del path no del tipo que busco', tokeep[node.id]);
                            }
                        }
                        containsallfilters = containsallfilters && containfilter;
                    }
                    if (containsallfilters) {
                        ramasValidas[nodepathid] = tokeep[nodepathid];
                    }
                }

                // clean ramas validas, no repetir nodos
                var defNodes = {};
                for (var nid in ramasValidas) {
                    for (var rnid in ramasValidas[nid]) {
                        var validNode = ramasValidas[nid][rnid];
                        defNodes[validNode.id] = validNode;
                    }
                }

                for (var i in defNodes) {
                    $scope.jstreeToFilterArray.push(defNodes[i]);
                }

            }

            return $scope.jstreeToFilterArray;
        };

        var filterjstreeRecursive = function (node, filterType, tokeep) {
            console.log('node id: ', node.id, ', node parent:', node.parent, 'node uri:', node.data.uri);
            if (filterType.indexOf(node.data.type) >= 0) {
                if (node.parent != '#') {
                    var parent_id = filterjstreeRecursive($scope.jstreeToFilter[node.parent], filterType, tokeep);
                } else {
                    // stop recursivity, keep ROOT(#) as parent
                    // TODO: if valid path, keep it
                }
                tokeep.push(node);
                return node.parent;
            } else {
                console.log("NOT IN TYPES ", filterType, ", NODE:", node);
                if (node.parent != '#') {
                    var parent_id = filterjstreeRecursive($scope.jstreeToFilter[node.parent], filterType, tokeep);
                    return parent_id;
                } else {
                    // stop recursivity, keep ROOT(#) as parent
                    return '#';
                }
            }
        };
        var filterjstreeRecursiveGetBranches = function (node, filterType, branchNodes) {
            console.log('node id: ', node.id, ', node parent:', node.parent, 'node uri:', node.data.uri);
            if (node.parent != '#') {
                var parent_id = filterjstreeRecursiveGetBranches($scope.jstreeToFilter[node.parent], filterType,
                    branchNodes);
            } else {
                // stop recursivity, keep ROOT(#) as parent
                // TODO: if valid path, keep it
            }
            branchNodes.push(node);
            return node.parent;
        };

        $scope.jsTreeNodeCategorize = function () {
            var allNodes = $('#dqResultsJSTree').jstree(true).get_json('#', {
                flat: true,
            });
            for (var k in allNodes) {
                var node = allNodes[k];
                console.log("CATEGORIZE NODE:", node);
                for (var cat in node.data.categories) {
                    var catLabel = node.data.categories[cat].label;
                    var ngstyle = {
                        width: 15,
                        height: 15,
                        border: '2px solid black',
                        'background-color': $scope.clusteringColors[catLabel],
                        display: 'inline-block',
                        margin: '10px 5px 0px 5px',
                    };
                    var ngstyleJSON = angular.toJson(ngstyle);
                    var tagid = node.a_attr.id + "_" + catLabel;
                    tagid = tagid.replace(" ", "");
                    var ngstyleHtmlTag = "<div id='" + tagid + "' ng-style='" + ngstyleJSON + "'></div>";

                    var x = angular.element(document.getElementById(tagid));
                    if (x.length == 0) {
                        angular.element(document.getElementById(node.a_attr.id)).append(
                            $compile(ngstyleHtmlTag)($scope));
                    } else {
                        // do nothing
                    }
                }
            }
            $timeout(function () {
                $scope.$apply();
            });
        };

        $scope.addtoqueryByContextMenu = function (data) {
            // var concepts = [];
            // var concept = {
            // label : data.node.data.label,
            // uri : data.node.data.uri,
            // };
            // $scope.updateRelations(concept);
            // concepts.push(concept);
            // $scope.generateGraph(concepts);
            console.log("addtoqueryByContextMenu ", data);

        };

        var addtoquery = function () {
            var checkedConcepts = $('#dqResultsJSTree').jstree("get_checked", true, true);
            if (checkedConcepts.length > 0) {
                if ($scope.mapVisible == true) {
                    $scope.toggleMap();
                }
                var concepts = [];
                var map_jstreeID_visjsID = {};
                // add nodes
                for (var i = 0; i < checkedConcepts.length; ++i) {
                    var node = checkedConcepts[i];
                    var concept = {
                        label: node.data.label,
                        uri: node.data.uri,
                        node: node,
                    };
                    var nodeid = $scope.updateRelations(concept);
                    map_jstreeID_visjsID[node.id] = nodeid;

                    concepts.push(concept);
                }

                // add edges between attribute and its concept
                for (var i = 0; i < checkedConcepts.length; ++i) {
                    var node = checkedConcepts[i];
                    if (node.type != undefined && node.type == "nodeattribute") {
                        // add edge with its parent
                        console.log("nodeattributeXX node.type=", node, "\n map_jstreeID_visjsID=", map_jstreeID_visjsID);
                        var parentjstreeID = node.parent;
                        var edge = {
                            from: map_jstreeID_visjsID[parentjstreeID],
                            to: map_jstreeID_visjsID[node.id],
                            label: 'attribute',
                            color: {
                                color: '#FF803E', // ocre, orange
                                highlight: '#FF803E',
                                hover: '#FF803E',
                            },
                            font: {
                                color: '#FF803E',
                            },
                        };
                        QMGraph.addEdge(edge);
                    }
                }

                //					$timeout(function(){
                //						QMGraph.network.fit({
                //							animation: {
                //								duration: 500,
                //								easingFunction: "linear",
                //							}
                //						});	
                //					});
                // $scope.generateGraph(concepts);
            } else {
                var msg = 'Select at least one concept, please.';
                $scope.showError(msg);
            }
            var uncheckAll = $('#dqResultsJSTree').jstree("uncheck_all"); // uncheck
            // all
            // checkboxes
        };
        $scope.addtoquery = addtoquery;

        $scope.filterstoshow = true;
        // execute filter through jstree. Returns -1 if nothing to show or
        // error, different to -1 otherwise
        $scope.updateJSTree = function (filter, isdefault) {
            console.log("to filter by type: ", filter);

            if ($scope.jstreeNodesByFilter[filter[0]] == undefined) {
                //do nothing
            } else {
                var result = $scope.jstreeNodesByFilter[filter[0]].jstree;
                $scope.searchresultssize = $scope.jstreeNodesByFilter[filter[0]].jstreenodes;

                //var result = $scope.filterjstreeByTypes(filter);
                $scope.jstreeStyleNodes();
                $('#dqResultsJSTree').jstree(true).settings.core.data = result;
                $('#dqResultsJSTree').jstree(true).refresh();

                if (result == undefined || result.length == undefined || result.length <= 0) {
                    $scope.filterstoshow = false;
                } else {
                    $scope.filterstoshow = true;
                }

                if (isdefault) {
                    for (var i in $scope.tabsFilters) {
                        var tab = $scope.tabsFilters[i];
                        if (tab.parentfilter != undefined && tab.parentfilter[0] == filter[0]) {
                            tab.active = true;
                        } else {
                            tab.active = false;
                        }
                    }
                    // Mark entities as default
                    // $scope.tabsFilters[0].active = true;
                }
            }
        };

        $scope.jsTreeNodeHover = function (e, data) {
            // console.log('HOVER- e: ', e, ', data: ', data);

            $scope.hoverNodeSnippet = {
                uri: undefined,
                label: undefined,
                properties: [],
                related: [],
            };
            var nodeUri = data.node.data.uri;
            var snippet = $scope.queryResults[nodeUri].snippet;
            var snippetLabel = undefined;
            var related = $scope.hoverNodeSnippet.related;
            var properties = $scope.hoverNodeSnippet.properties;
            if (snippet.label != undefined) {
                snippetLabel = snippet.label[nodeUri];
            }
            if (snippet.property != undefined) {
                for (var x in snippet.property) {
                    properties.push(snippet.property[x]);
                }
            }
            if (snippet.related != undefined) {
                for (var x in snippet.related) {
                    related.push(snippet.related[x]);
                }
            }

            $scope.hoverNodeSnippet = {
                uri: nodeUri,
                label: snippetLabel,
                properties: properties,
                related: related
            };

            $scope.$apply(); // THE APPLY TO THE VIEW!!!

            // console.log("SNIPPETs(", nodeUri, ")",
            // $scope.hoverNodeSnippet);

            var popoverid = "#" + data.node.id + "_anchor";

            $(popoverid).popover({
                html: true,
                delay: {
                    'show': 500,
                    'hide': 0,
                },
                trigger: 'hover', // click | hover | focus |
                // manual
                placement: 'right',
                content: function () {
                    // return $('#qresults-snippet-content').html();
                    var html_label = '';
                    var html_properties = '';
                    var html_related = '';

                    if (snippetLabel != undefined) {
                        html_label = '<div><label>Label: </label>' + snippetLabel + '</div>';
                    }

                    if (properties.length > 0) {
                        html_properties = '<div><label>Property: </label>' + properties[0] + '</div>';
                    }
                    if (related.length > 0) {
                        html_related = '<div><label>Related: </label>' + related[0] + '</div>';
                    }
                    var html_snippet = '<div style="white-space:pre">' + html_label + html_properties + html_related + '</div>';

                    return html_snippet;

                },
                title: function () {
                    // return $('#qresults-snippet-title').html();
                    var html_title = '<label>Snippet</label>';
                    return html_title;
                }
            });

            $(popoverid).popover('show');

            // $(popoverid).on('inserted.bs.popover', function() {
            // $(popoverid).popover('show');
            // });

        };

        var maxSimilarityInsideTheBranch = function (nodeid) {
            var node = $('#dqResultsJSTree').jstree(true).get_node(nodeid);
            var max = node.data.similarity;
            var children = node.children;
            if (children.length == 0) {
                max = node.data.similarity;
            } else {
                for (var x in children) {
                    var child_id = children[x];
                    max_children = maxSimilarityInsideTheBranch(child_id);
                    if (max_children > max) {
                        max = max_children;
                    }
                }
            }

            return max;
        };

        $scope.jsTreeCheckNode = function (e, data) {
            // if attribute, check parent. Nothing otherwise
            var node = data.node;
            if (node.type == "nodeattribute") {
                // if attribute, check parent
                $('#dqResultsJSTree').jstree(true).check_node([node.parent]);
            } else {
                // nothing otherwise
            }
        };

        $scope.jsTreeUnCheckNode = function (e, data) {
            // if attribute, check parent. Nothing otherwise
            var node = data.node;
            if (node.type == "nodeattribute") {
                // if attribute, check parent
                $('#dqResultsJSTree').jstree(true).uncheck_node([node.parent]);
            } else {
                // nothing otherwise
            }
        };

        $('#dqResultsJSTree').on('hover_node.jstree', $scope.jsTreeNodeHover);
        // $('#dqResultsJSTree').on('refresh.jstree',
        // $scope.jsTreeNodeCategorize);
        $('#dqResultsJSTree').jstree({
            'core': {
                'themes': {
                    'variant': 'large',
                    'icons': true,
                },
                // 'data' : $scope.jstreeGPH,
                'data': [],
            },
            'sort': function (a, b) {
                var node_a = this.get_node(a);
                var node_b = this.get_node(b);
                // console.log("SORTING..... ", this.get_node(a),
                // this.get_node(b));
                // sort by max of the branch
                var max_a = maxSimilarityInsideTheBranch(a);
                var max_b = maxSimilarityInsideTheBranch(b);
                if (max_a == max_b) {
                    // sort alpha
                    if (node_a.text > node_b.text) {
                        return 1;
                    }
                } else if (max_a < max_b) {
                    return 1;
                } else {
                    return -1;
                }
            },
            'types': {
                'default': {
                    'icon': false,
                },
                'nodeattribute': {
                    // 'icon' :
                    // 'http://icons.iconarchive.com/icons/hydrattz/multipurpose-alphabet/24/Letter-A-lg-icon.png',
                },
            },
            'contextmenu': {
                items: function ($node) {
                    return {
                        add: {
                            label: 'Add to Query',
                            // action :
                            // $scope.addtoqueryByContextMenu, //
                            // done while handling jstree
                            // show_contextmenu.jstree
                            // Event
                            action: function ($node) {
                                console.log('addtoqueryByContextMenu callback', $node);
                                console.log('addtoqueryByContextMenu callback node', $('#dqResultsJSTree')
                                    .jstree(true).get_selected($node));
                            },
                            icon: 'https://cdn3.iconfinder.com/data/icons/musthave/16/Add.png',
                        },
                    };
                },
            },
            'checkbox': {
                // 'keep_selected_style' : true,
                cascade: "",
                three_state: false,
                'tie_selection': false,
            },
            'plugins': ['sort', 'contextmenu', 'checkbox', 'types'],
        });

        $('#dqResultsJSTree').on('check_node.jstree', $scope.jsTreeCheckNode);
        $('#dqResultsJSTree').on('uncheck_node.jstree', $scope.jsTreeUnCheckNode);

        $('#dqResultsJSTree').on('show_contextmenu.jstree', function (e, data) {
            // addtoquery habria que hacerlo desde aqui porque asi
            console.log("testing show_contextmenu.jstree", e, data);
        });

        $('#analyze-button').on({
            click: function () {
                $scope.loading_dquery = false;
                $scope.tabsFilters[0].active = true;
                console.log('analyze-button clicked+++++++++++++++++++++++++++++++');
            }
        });

        $('.populated-button').change(function () {
            $scope.loading_dquery = false;
            $scope.tabsFilters[0].active = true;
            console.log('populated-button-button clicked+++++++++++++++++++++++++++++++');
        });

        /**
         * Function to load the selected data at the map
         */
        $scope.loadCheckedConcepts = function () {
            // Visualize & Init the Map
            if ($scope.mapVisible == false) {
                $scope.toggleMap();
            }

            var sfeatures = getSelectedFeatures();
            if (sfeatures.length == 0) {
                // Error 1: no area selected
                var msg = 'Select at least one area, please.';
                $scope.showError(msg);
                return;
            }

            var checkedConcepts = $('#dqResultsJSTree').jstree("get_checked", true, true);
            if (checkedConcepts.length <= 0) {
                // Error 2: no area selected
                var msg = 'Select at least one concept, please.';
                $scope.showError(msg);
                return;
            } else {
                for (var concept in checkedConcepts) {
                    var ck = checkedConcepts[concept];
                    var uri = ck.data.uri;
                    console.log("$scope.loadData (", ck, "): ");
                    $scope.loadData(ck);
                }
            }
        };

        /**
         * Load data on the map of the node passed by parameter
         */
        $scope.loadData = function (node) {
            var uriConcept = node.data.uri;
            var nodeChecked = node.state.checked;
            // var nodeChecked = node.state.selected;

            var sfeatures = getSelectedFeatures();
            for (var i = 0; i < sfeatures.length; i++) {
                var selectedFeature = sfeatures[i];
                if (!conceptLayers.hasOwnProperty(uriConcept))
                    conceptLayers[uriConcept] = new Array();
                var layer = null;
                var concept = LayerSelectedFeature(conceptLayers[uriConcept], selectedFeature);
                if (concept != null) {
                    layer = concept.layer;
                }
                if (nodeChecked) {
                    if (layer != null) {
                        // layer created before, just show it and don't
                        // create it again
                        showLayer(layer);
                        concept.checked = true;
                    } else {
                        // the layer has not been created before. Create and
                        // add it
                        // conceptLayers is actualized inside because the
                        // ajax call is
                        // asynchronous
                        var numtotal = 5; // por poner algo...
                        CreateLayer(uriConcept, selectedFeature, numtotal);

                    }
                } else if (layer != null) { // element unchecked but created
                    // previously
                    hideLayer(layer);
                    concept.checked = false;
                }
            }
        };

        // $scope.jstreeToFilterArray = $scope.updateJSTree([]);
        // $('#dqResultsJSTree').jstree(true).settings.core.data =
        // $scope.jstreeToFilterArray;
        // $('#dqResultsJSTree').jstree(true).refresh();

        // var jstreedatafiltered = $scope.filterjstreeByTypes([]);
        // $('#dqResultsJSTree').jstree(true).settings.core.data =
        // jstreedatafiltered;
        // $('#dqResultsJSTree').jstree(true).refresh();
        // $timeout(function() {
        // $scope.$apply();
        // })
    }]);

// Related Concepts Controller
tabsModule.controller('relConceptsCtrl', ['$scope', function ($scope) {
    $scope.relConcepts = {};
    $scope.relConcepts.populated = false;
    $scope.populatedRelConcepts = populatedRelConcepts; // variable populated
    // usada al llamar al WS
    // de
    // GetRelatedConcepts

}]);
// End Related Concepts Controller

// factory to manage the areas used while making queries
tabsModule.factory('QueryAreas', ['$http', 'QMGraph', 'WSOntologyAreas', '$timeout', function ($http, QMGraph, WSOntologyAreas, $timeout) {

    var areasimworkingwith = {
        value: null,
        values: [],
    };

    var getArea = function () {
        return this.areasimworkingwith.value;
    };

    var getAllAreas = function () {
        return this.areasimworkingwith.values;
    };
    var setArea = function (x) {
        this.areasimworkingwith.value = x;
        return true;
    };

    var setAllAreas = function (x) {
        this.areasimworkingwith.values = x;
        return true;
    };

    var areaExists = function (areas, areasimworkingwithX) {
        var areaExists = false;
        for (var areaKey in areasimworkingwith.values) {
            var areaIWW = areasimworkingwith.values[areaKey];
            if (areaIWW.attributes.uri == undefined) {
                return false;
            } else if (areaIWW.attributes.wkt == areas[0].wkt) {
                areaExists = true;
                break;
            }
        }
        return areaExists;
    };

    var addAreaToTheOntology = function () {

        var xy = this.areasimworkingwith;

        // 24/02 unica seleccion, implementado el multipolygon
        var areas = [{
            wkt: this.areasimworkingwith.value.attributes.wkt,
            label: this.areasimworkingwith.value.attributes.label,
        }];

        // check if area already exists, does not have URI
        // used with this.areaExists(areas)

        if (!areaExists(areas) && this.areasimworkingwith.value.attributes.wkt != undefined) {

            // Area not exists in the ontology, gotta create using this WS
            // /CreateArea
            // TODO: mostrar mddialog creating area...

            // @date: 2020-APR-06
            // @smendoza: area contains the polygons, for being introduced at the ontology through the CreateArea Service
            var params = areas;

            // WS call to create area on the ontology
            WSOntologyAreas.CreateArea(params).then(function (promise) {
                var nodeData = promise.data;
                QMGraph.addNode(nodeData);
                // add to the QueryAreas var
                for (var areaKey in areasimworkingwith.values) {
                    var areaIWW = areasimworkingwith.values[areaKey];
                    if (areaIWW.attributes.wkt == areas[0].wkt) {
                        areaIWW.data = nodeData;
                        break;
                    }
                }
            });

            // console.log("area created addAreaToTheOntology() with uri: ",
            // data.uri);
            // return data.uri;

        } else {
            // Area already exists in the ontology, not necessary to add it
            // again
            QMGraph.addNode({
                uri: this.areasimworkingwith.value.attributes.URI,
                label: this.areasimworkingwith.value.attributes.label,
                wkt: this.areasimworkingwith.value.attributes.wkt,
                type: 'AREA',
            });
        }

        $timeout(function () {
            QMGraph.network.fit({
                animation: {
                    duration: 500,
                    easingFunction: "linear",
                }
            });
        });
    };

    return {
        getArea: getArea,
        getAllAreas: getAllAreas,
        setArea: setArea,
        setAllAreas: setAllAreas,
        areasimworkingwith: areasimworkingwith,
        addAreaToTheOntology: addAreaToTheOntology,

    };
}]);
