// create application module with dependencies
var logModule = angular.module('LogModule', []);

logModule.controller('logCtrl', [ '$scope', function($scope) {

	$("#log-container").dialog({
		autoOpen : false,
		resizable : true,
		maxHeight : 600,
		maxWidth : 1000,
		minHeight : 200,
		minWidth : 800,
	});

	$scope.openLog = function() {
		$('#log-container').dialog('open');
	};

} ]);
