var qgModule = angular.module('queryGraphModule', [ 'ui.grid', 'queryTabModule', 'urbanConceptsModule' ]);

qgModule.controller('queryGraphCtrl', 'QMGraph', [ '$scope', '$http', function($scope, $http, QMGraph) {
	var UrbanConceptsID = '#jstreeUrbanConcepts';
	console.log("queryGraphModule network (QMGraph.network) =  , ", QMGraph.network);

	// TODO: update the nodes binding the checked with the network query graph
	// $scope.bindUCToGraph = function() {
	// // var conceptsCheckedAll = $($scope.UrbanConceptsID).jstree("get_checked", true, true);
	// var conceptsCheckedTop = $(UrbanConceptsID).jstree(true).get_top_checked(true);
	// var checkedConcepts = conceptsCheckedTop;
	// console.log("bindUCToGraph", $scope.network);
	// console.log("$scope.bindUCToGraph (): ", "checkedData ", checkedConcepts);
	// $scope.nDS.clear();
	// $scope.eDS.clear();
	// for ( var concept in checkedConcepts) {
	// var ck = checkedConcepts[concept];
	// var uri = ck.a_attr.uri;
	// var label = ck.text;
	// $scope.addNode({
	// uri : uri,
	// label : label,
	// });
	// }
	// // TODO: tener en cuenta las relaciones y areas!!
	// };

	$scope.bindUCToGraph = function() {
		var conceptsCheckedTop = $($scope.UrbanConceptsID).jstree(true).get_top_checked(true);
		var checkedConcepts = conceptsCheckedTop;
		console.log("$scope.bindUCToGraph (): ", ", checkedData: ", checkedConcepts);
		// QMGraph.nDS.clear();
		// QMGraph.eDS.clear();
		for ( var concept in checkedConcepts) {
			var ck = checkedConcepts[concept];
			var uri = ck.a_attr.uri;
			var label = ck.text;
			QMGraph.addNode({
				uri : uri,
				label : label,
			});
		}
		// $scope.$apply();
	};

} ]);
