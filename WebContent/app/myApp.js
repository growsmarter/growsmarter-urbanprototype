// create application module with dependencies
var myapp = angular.module('smartCitiesApp', ['ngMaterial', 'ui.bootstrap', 'ngRoute', 'ngSanitize', 'mapOL',
    'queryTabModule', 'LogModule', 'indicatorTabModule', 'urbanConceptsModule', 'myWS', 'queryManager',
    'navBarModule', 'ngAnimate', 'ngMaterial', 'ngMessages', 'material.svgAssetsCache']);

// myapp.run(function(QMGraph, $timeout) {
// $timeout(QMGraph.init(), 3000);
// });

myapp.controller('myAppCtrl', [
    '$scope',
    'QM',
    'QMGraph',
    '$mdSidenav',
    '$mdUtil',
    '$mdDialog',
    'QueryAreas',
    'MapManagement',
    '$timeout',
    'WSGetGraphResult',
    function ($scope, QM, QMGraph, $mdSidenav, $mdUtil, $mdDialog, QueryAreas, MapManagement, $timeout,
        WSGetGraphResult) {

        // OpenLayers env
        $scope.OpenLayers = OpenLayers; // get el openlayers de mmarrero
        // utils.js
        $scope.OLPanel = $scope.OpenLayers.Control.Panel;
        // OpenLayers env, utils.js methods for OL
        $scope.getSelectedFeatures = getSelectedFeatures; // make the
        // funcion
        // available
        // from
        // functions
        $scope.wktFormat = new OpenLayers.Format.WKT();
        $scope.mapInitialized = false;
        $scope.mapInit = mapInit;
        $scope.panelControls = panelControls;
        $scope.map = map; // get el map de openlayers de mmarrero utils.js
        MapManagement.map = map;
        $scope.mapVisible = false;
        $scope.mapErrorVisible = true;
        $scope.mapErrorMessage = 'A Map Error happened (default message)';
        $scope.F_QMGraph = QMGraph;
        $scope.F_QM = QM;
        $scope.F_MapManagement = MapManagement;
        // $scope.nodecounter = 0; // counter for the graph nodes identifier
        // $scope.nDS = new vis.DataSet(); // init graph nodes
        // $scope.eDS = new vis.DataSet(); // init graph edges
        // $scope.network = null;
        $scope.QMGraphInit = QMGraph.init;

        QM.init();

        // $scope.$watch(MapManagement.map.controls, function(x){
        // $scope.ctrlDF.featureAdded = function(){
        // console.log("$scope.ctrlDF.featureAdded DrawFeature");
        // };
        // });


        $scope.errorsAccumlated = [];
        $scope.errorDialogIsOpen = false;

        $scope.refreshOLMap = function () {
            MapManagement.map.updateSize();
            MapManagement.map.baseLayer.redraw();
        };

        $scope.closeMdDialog = function () {
            // Easily hides most recent dialog shown...
            // no specific instance reference is needed.
            $mdDialog.hide();
        };

        $scope.showQueryResultsCellnex = function (myscope) {
            var parentEl = angular.element(document.body);
            myscope.hideDialog = function () {
                $scope.mddialog_showQueryResults_open = false;
                $("#get-data-results-cellnex").dialog("close");
            };
            if (!$scope.mddialog_showQueryResults_open) {
                var newDialog = $("#get-data-results-cellnex");
                $scope.isClosing = false;
                newDialog.dialog({
                    title: "Query Results",
                    appendTo: parentEl,
                    autoOpen: false,
                    draggable: true,
                    minHeight: 300,
                    height: 800,
                    minWidth: 400,
                    open: function (event, ui) {
                        $scope.closeResultsDialog = false;
                    },
                    beforeClose: function (event, ui) {
                        if ($scope.closeResultsDialog) {
                            //Ok, close the dialog
                            return true;
                        } else {

                            var mdDialog = $mdDialog.show({
                                scope: $scope,
                                parent: parentEl,
                                preserveScope: true,
                                templateUrl: 'html-templates/query-results-closing.html',
                                controller: function ($scope, $mdDialog) {
                                    $scope.closeResults = function () {
                                        $scope.closeResultsDialog = true;
                                        $mdDialog.hide();
                                    };
                                    $scope.cancelDialog = function () {
                                        $mdDialog.cancel();
                                        //$mdDialog.hide();
                                    }
                                },
                            }).then(closeResults, cancelDialog);

                            function closeResults() {
                                $("#get-data-results-cellnex").dialog('close');
                                $scope.closeResultsDialog = true;
                            };

                            function cancelDialog() {
                                $scope.closeResultsDialog = false;
                            };
                            return false;
                        }
                    },
                    // maxHeight: 100,
                    // maxWidth: 100,
                }).dialogExtend({
                    "closable": true,
                    "maximizable": true,
                    "minimizable": true,
                    "collapsable": true,
                    "dblclick": "collapse",
                    // "titlebar" : "transparent",
                    "minimizeLocation": "left",
                    "icons": {
                        "close": ' ui-icon-closethick', // "ui-icon-circle-close",
                        "maximize": 'ui-icon-plusthick', // "ui-icon-circle-plus",
                        "minimize": 'ui-icon-minusthick', // "ui-icon-circle-minus",
                        "collapse": "ui-icon-triangle-1-s",
                        "restore": 'ui-icon-extlink', // "ui-icon-circle-arrow-n"
                    },
                });
                newDialog.dialog("open");
            }
            console.log("$scope.showPathSelection scope:", myscope);
        };

        $scope.showLoadingShow = function (loading_message, cancelable, cancel_button) {

            var parentEl = angular.element(document.body);
            $mdDialog.show({
                parent: parentEl,
                clickOutsideToClose: false,
                escapeToClose: false,
                templateUrl: 'html-templates/loading-data.html',
                locals: {
                    message: loading_message,
                    cancelable: cancelable,
                    cancel_button: cancel_button,
                    closeMdDialog: $scope.closeMdDialog(),
                },
                controller: function ($scope, message, cancelable, cancel_button, closeMdDialog) {
                    $scope.message = message;
                    $scope.cancelable = cancelable;
                    $scope.cancel_button = cancel_button;
                    $scope.closeMdDialog = function () {
                        // Easily hides most recent dialog shown...
                        // no specific instance reference is needed.
                        $mdDialog.hide();

                        // vars to cancel query
                        var query_message = 'Executing the query...';
                        var query_cancel_button = 'Abort Query';
                        if (message == query_message && cancel_button == query_cancel_button) {
                            WSGetGraphResult.CancelQuery({}).success(function () {
                                console.log("CancelQuery SUCCESS!");
                            }).error(function () {
                                console.log("CancelQuery ERROR!");
                            }).finally(function () {
                                console.log("CancelQuery FINALLY!");
                            });
                        } else {
                            // do nothing
                        }
                    };
                },
            });
            // return $mdDialog;
        };

        $scope.mddialog_showQueryResults_open = false;
        $scope.showQueryResults = function (myscope) {
            var parentEl = angular.element(document.body);
            myscope.hideDialog = function () {
                $scope.mddialog_showQueryResults_open = false;
                $("#get-data-results").dialog("close");
                // $mdDialog.hide();
            };
            if (!$scope.mddialog_showQueryResults_open) {
                // var newDialog = $("#get-data-results").clone(); //
                // .appendTo( ".goodbye" );
                // var dialogID = 'get-data-results' + QM.queries.selected;
                // newDialog.appendTo("#query-dialogs");
                // newDialog.attr("id", dialogID);

                var newDialog = $("#get-data-results");
                $scope.isClosing = false;
                newDialog.dialog({
                    title: "Query Results (#" + QM.queries.selected + ")",
                    appendTo: parentEl,
                    autoOpen: false,
                    draggable: true,
                    minHeight: 300,
                    height: 800,
                    minWidth: 400,
                    open: function (event, ui) {
                        $scope.closeResultsDialog = false;
                    },
                    beforeClose: function (event, ui) {
                        if ($scope.closeResultsDialog) {
                            //Ok, close the dialog
                            return true;
                        } else {

                            var mdDialog = $mdDialog.show({
                                scope: $scope,
                                parent: parentEl,
                                preserveScope: true,
                                templateUrl: 'html-templates/query-results-closing.html',
                                controller: function ($scope, $mdDialog) {
                                    $scope.closeResults = function () {
                                        $scope.closeResultsDialog = true;
                                        $mdDialog.hide();
                                    };
                                    $scope.cancelDialog = function () {
                                        $mdDialog.cancel();
                                        //$mdDialog.hide();
                                    }
                                },
                            }).then(closeResults, cancelDialog);

                            function closeResults() {
                                $("#get-data-results").dialog('close');
                                $scope.closeResultsDialog = true;
                            };

                            function cancelDialog() {
                                $scope.closeResultsDialog = false;
                            };
                            return false;
                        }
                    },
                    // maxHeight: 100,
                    // maxWidth: 100,
                }).dialogExtend({
                    "closable": true,
                    "maximizable": true,
                    "minimizable": true,
                    "collapsable": true,
                    "dblclick": "collapse",
                    // "titlebar" : "transparent",
                    "minimizeLocation": "left",
                    "icons": {
                        "close": ' ui-icon-closethick', // "ui-icon-circle-close",
                        "maximize": 'ui-icon-plusthick', // "ui-icon-circle-plus",
                        "minimize": 'ui-icon-minusthick', // "ui-icon-circle-minus",
                        "collapse": "ui-icon-triangle-1-s",
                        "restore": 'ui-icon-extlink', // "ui-icon-circle-arrow-n"
                    },
                });
                newDialog.dialog("open");
                // $scope.mddialog_showQueryResults_open = true;
                // var myDialog = $mdDialog.show({
                // scope : myscope,
                // parent : parentEl,
                // hasBackdrop : false,
                // preserveScope : true,
                // templateUrl : 'html-templates/new-query-results.html',
                // }).then(function(result) {
                // console.log("$mdDialog.show -> then success");
                // return result;
                // }, function() {
                // console.log("$mdDialog.show -> then error?");
                // return false;
                // });
            }
            console.log("$scope.showPathSelection scope:", myscope);
        };

        $scope.showMapViewResults = function (myscope) {
            var parentEl = angular.element(document.body);
            myscope.hideDialog = $mdDialog.hide;
            $mdDialog.show({
                scope: myscope,
                parent: parentEl,
                preserveScope: true,
                templateUrl: 'html-templates/map-view-results.html',
            });
            console.log("$scope.showMapViewResults() scope:", myscope);
        };

        $scope.showAddArea = function (myscope) {
            var parentEl = angular.element(document.body);
            myscope.hideDialog = $mdDialog.hide;
            $mdDialog.show({
                scope: myscope,
                parent: parentEl,
                preserveScope: true,
                templateUrl: 'html-templates/new-query-add-area.html',
            });
            console.log("$scope.showAddArea scope:", myscope);
        };

        $scope.showEdgeSelection = function (myscope) {
            var parentEl = angular.element(document.body);
            myscope.hideDialog = $mdDialog.hide;
            myscope.whattoshow = 0;
            $mdDialog.show({
                scope: myscope,
                parent: parentEl,
                preserveScope: true,
                // templateUrl : 'html-templates/new-query-edge-selection.html',
                templateUrl: 'html-templates/new-query-edge-n-path-selection.html',
            });
            console.log("$scope.showEdgeSelection scope:", myscope);
        };

        $scope.replacesToShow = QMGraph.replacesToShow;
        $scope.descomposedPropsToChoose = QMGraph.descomposedPropsToChoose;
        $scope.showDirectLinksReplace = function () {
            var parentEl = angular.element(document.body);
            $scope.hideDialog = $mdDialog.hide;

            $mdDialog.show({
                scope: $scope,
                parent: parentEl,
                preserveScope: true,
                templateUrl: 'html-templates/node-directlinks-replace.html',
            });
        };

        $scope.showPathSelection = function (myscope) {
            var parentEl = angular.element(document.body);

            myscope.hideDialog = $mdDialog.hide;
            myscope.whattoshow = 0;
            $mdDialog.show({
                scope: myscope,
                parent: parentEl,
                preserveScope: true,
                // templateUrl : 'html-templates/new-query-path-selection.html',
                templateUrl: 'html-templates/new-query-edge-n-path-selection.html',
            });
            console.log("$scope.showPathSelection scope:", myscope);
        };

        $scope.showDialog = function showDialog(errorMsg) {
            $scope.errorsAccumlated = $scope.errorsAccumlated.concat([errorMsg]);
            if ($scope.errorDialogIsOpen == false) {
                $scope.errorDialogIsOpen == true;
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    clickOutsideToClose: true,
                    templateUrl: 'html-templates/dialog-error.html',
                    locals: {
                        items: $scope.items,
                        errorMsg: errorMsg,
                        errorsAccumlated: $scope.errorsAccumlated,
                    },
                    controller: DialogController,
                    // onRemoving : $scope.removeFunc,
                });
            } else {
                // if already open, don't open it again...

            }

            function DialogController($scope, $mdDialog, items, errorMsg, errorsAccumlated) {
                $scope.items = items;
                $scope.errorMsg = errorMsg;
                $scope.errorsAccumlated = errorsAccumlated;
                if ($scope.errorsAccumlated == undefined) {
                    $scope.errorsAccumlated = [];
                } else {
                    $scope.errorsAccumlated.push(errorMsg);
                }

                $scope.closeDialog = function () {
                    $mdDialog.hide();
                    $scope.errorDialogIsOpen = false;
                };
            };
        };
        $scope.showError = function (errorMsg) {
            $scope.showDialog(errorMsg);
        };

        // Areas saved by the user on the OpenLayers
        // $scope.areasimworkingwith = {
        // value : null,
        // values : [],
        // };
        $scope.areasimworkingwith = QueryAreas.areasimworkingwith;
        $scope.addAreaToTheOntology = QueryAreas.addAreaToTheOntology;
        $scope.addAreaToGraph = function () {

            // if (QueryAreas.areasimworkingwith.values.length <= 0) {
            // // gotta define an area so, show the Map
            // $scope.toggleMap();
            // return;
            // } else {
            $scope.hideDialog = $mdDialog.hide;
            var parentEl = angular.element(document.body);
            $mdDialog.show({
                scope: $scope,
                parent: parentEl,
                preserveScope: true,
                templateUrl: 'html-templates/new-query-area.html',
            });
            // QueryAreas.addAreaToTheOntology();
            // }
        };

        $("#tab2_queryselected").attr('active', true);

        $scope.toggleRight = buildToggler('right');
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildToggler(navID) {
            var debounceFn = $mdUtil.debounce(function () {
                $mdSidenav(navID).toggle().then(function () {
                    console.log("toggle " + navID + " is done");
                });
            }, 200);

            return debounceFn;
        };

        $scope.toggleMap = function (visibility) {
            // if(!$scope.mapVisible){
            if ($('#map-panel').is(':hidden') && (visibility == true || (visibility == undefined && $scope.mapVisible == false))) {
                // slide to show
                $scope.mapVisible = true;
                MapManagement.OLControlActive.active = 2;
                $('#map-panel').show("slide", {
                    direction: "up",
                    complete: $scope.refreshOLMap
                }, 400);
                // $('#map-panel').slideDown(600,function(){
                // $scope.refreshOLMap();
                // $scope.mapVisible = !$scope.mapVisible;
                // });
            } else {
                // slide to hide
                $scope.mapVisible = false;
                // $('#map-panel').slideUp(600,function(){
                // $scope.refreshOLMap();
                // $scope.mapVisible = !$scope.mapVisible;
                // });
                $('#map-panel').hide("slide", {
                    direction: "up",
                    complete: $scope.refreshOLMap
                }, 400);
            }
            // $scope.mapVisible = !$scope.mapVisible;
        };

    }]);

myapp.controller('RightCtrl', function ($scope, $timeout, $mdSidenav) {
    $scope.close = function () {
        $mdSidenav('right').close().then(function () {
            console.log("close RIGHT is done");
        });
    };
});

myapp.directive('openlayersMap', ['QMGraph', 'MapManagement', '$timeout', function (QMGraph, MapManagement, $timeout) {

    return {

        restrict: 'E',
        // replace : true,
        // template : '<div id="map" style="height: 600px;" ng-style="{height:
        // \'600px\'}"></div>',
        templateUrl: 'html-templates/query-map.html',
        link: function postLink(scope, element, attrs) {
            if (MapManagement.map == undefined) {
                var pc1 = scope.panelControls;
                MapManagement.map = scope.mapInit('myOSMMap');
                var pc2 = scope.panelControls;
                var pc3 = MapManagement.panelControls;
                MapManagement.panelControls = scope.panelControls;
                var pc4 = MapManagement.panelControls;
                console.log("pc1=", pc1, ", pc2=", pc2, ", pc3=", pc3, ", pc4=", pc4);
                layerSwitcher = MapManagement.map.getControlsByClass("OpenLayers.Control.LayerSwitcher")[0];
                // layerSwitcher.baseLbl.innerText = "YOUR NEW TEXT";
                layerSwitcher.dataLbl.innerText = "Map Layers";
                console.log("directive openlayersMap - MapManagement.map UNDEFINED");
                $('#map-panel').hide(1);
            } else {
                scope.map = MapManagement.map;
                console.log("directive openlayersMap - MapManagement.map already DEFINED");
            }
            console.log("directive openlayersMap - MapManagement.map: ", MapManagement.map);
        },
    };
}]);

myapp
    .directive(
        'mapError',
        function () {

            return {
                restrict: 'E',
                replace: true,
                template: '<div class="alert alert-danger" role="alert"> <span class="glyphicon glyphicon-exclamation-sign"></span> <span class="sr-only">Error:</span> {{mapErrorMessage}}</div>',
                link: function postLink(scope, element, attrs) { },
            };
        });

myapp.directive('showDataCellnex', function () {
    return {
        restrict: 'E',
        templateUrl: 'html-templates/query-data-table.html',
        link: function (scope, element, attrs) {
            $scope = scope;
        }
    };
});


myapp.animation('.myslide', [function () {
    console.log("myslide entered?");
    return {
        // make note that other events (like addClass/removeClass)
        // have different function input parameters
        enter: function (element, doneFn) {
            console.log("myslide entered?");
            // $(element).fadeIn(10000, doneFn);
            $(element).slideDown();

            // remember to call doneFn so that angular
            // knows that the animation has concluded
        },

        move: function (element, doneFn) {
            console.log("myslide moved?");
            $(element).fadeIn(1000, doneFn);
        },

        leave: function (element, doneFn) {
            console.log("myslide leaved?");
            // $(element).fadeOut(10000, doneFn);
            $(element).slideUp();
        }
    };
}]);
