package ontology_commons.tests.serviceInterface;

import static org.junit.Assert.*;

import org.json.JSONObject;
import org.junit.Test;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class TestWebInterface {

	// @Ignore
	@Test
	public void test() {

		createOntologyBroker();

		String value = "http://www.bsc.es/Energy#COSensor";
		String value2 = "http://www.ibm.com/ICP/Scribe/CoreV2#Building";
		testDataProperties(value);
		testDataProperties(value2);
	}

	public void testDataProperties(String value) {
		JSONObject paramsJSON = new JSONObject();
		// key value: uri
		String key = "uri";
		// value: entity uri
		paramsJSON.put(key, value);

		String response = WebInterface.GetDataProperties(paramsJSON.toString());
		JSONObject responseJSON = new JSONObject(response);
		System.out.println("testDataProperties[" + paramsJSON.toString() + "]:\n" + responseJSON.toString(4));

	}

	public void createOntologyBroker() {

		String configfile = "/home/smendoza/UrbanPrototypeResources-mc/UrbanPrototypeResources/config.properties";
		// services.ServiceInterface.CreateOntBroker(NAMEBROKER, ONTURI,
		// ONTFILE, POLICYFILE, resourcesPath,
		// INSTFILES, INDFILES, MODELMAPPATH, SEARCHPATH);

		try {
			ServiceInterface.CreateOntBroker(configfile);
			System.out.println("Created OntoBroker");
			System.out.println("App ready");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
