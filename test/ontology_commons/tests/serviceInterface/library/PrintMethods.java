package ontology_commons.tests.serviceInterface.library;

import java.io.FileWriter;
import java.io.IOException;

public class PrintMethods {

	public static void printFile(FileWriter fw, long init, long end) {
		PrintMethods.printFile(fw, init, end, "");
	}

	public static void printFile(FileWriter fw, long init, long end, String msg) {
		try {
			fw.write("Timing[" + msg + "]:" + ((end - init)) + " msec.\n");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
