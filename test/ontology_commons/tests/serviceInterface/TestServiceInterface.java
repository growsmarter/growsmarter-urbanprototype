package ontology_commons.tests.serviceInterface;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class TestServiceInterface {

	@Ignore
	@Test
	public void testGetGraphResult() {

		createOntologyBroker();

		String conceptURI = "http://www.ibm.com/ICP/Scribe/CoreV2#Building";
		int nodeID = 1;
		try {
			Set<IOntoBase> nodesSueltos = new HashSet<>();
			List<IOntoLink> links = new ArrayList<>();

			IOntoBase nodeSueltoIOB = ServiceInterface.GetConcept(conceptURI);
			nodeSueltoIOB.setId(nodeID);
			nodesSueltos.add(nodeSueltoIOB);

			Integer queryID = ServiceInterface.GetGraphResult(nodesSueltos, links, WebInterface.sessionID);
			
			if(queryID == null){
				fail("Problem executing the query");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void testGetGraphDataValues() {

		createOntologyBroker();

		String conceptURI = "http://www.ibm.com/ICP/Scribe/CoreV2#Building";
		int nodeID = 1;
		try {
			Set<IOntoBase> nodesSueltos = new HashSet<>();
			List<IOntoLink> links = new ArrayList<>();

			IOntoBase nodeSueltoIOB = ServiceInterface.GetConcept(conceptURI);
			nodeSueltoIOB.setId(nodeID);
			nodesSueltos.add(nodeSueltoIOB);

			Integer queryID = ServiceInterface.GetGraphResult(nodesSueltos, links, WebInterface.sessionID);

			JSONObject jOut = new JSONObject();
			JSONObject jSparqQL = new JSONObject();
			JSONObject jrows = new JSONObject();

			// NODES SUELTOS
			for (IOntoBase nodeSuelto : nodesSueltos) {
				int subgraphId = ServiceInterface.getSubgraphId(nodeSuelto.getId(), queryID);
				int nrows = ServiceInterface.getRowsNumber(subgraphId, queryID);
				String sparql = ServiceInterface.getSparql(subgraphId, queryID);
				jrows.put(String.valueOf(subgraphId), nrows);
				jSparqQL.put(String.valueOf(subgraphId), sparql);
				List<HashMap<String, Set<String>>> rows = ServiceInterface.GetGraphDataValues(nodeID, 0, nrows - 1,
						queryID);
				printRows(rows);
			}

			// LINKS
			// for (int i = 0; i < jgraph.length(); ++i) {
			// JSONArray subgraph = (JSONArray) jgraph.get(i);
			// for (int j = 0; j < subgraph.length(); ++j) {
			// Integer nodeid = (Integer) subgraph.get(j);
			// int subgraphId = ServiceInterface.getSubgraphId(nodeid, queryID);
			// int rows = ServiceInterface.getRowsNumber(subgraphId, queryID);
			// jrows.put(String.valueOf(subgraphId), rows);
			// String sparql = ServiceInterface.getSparql(subgraphId, queryID);
			// jSparqQL.put(String.valueOf(subgraphId), sparql);
			// // unique call enough to get sparql and nrows
			// break;
			// }
			// }

			jOut.put("nrows", jrows);
			jOut.put("sparqlquery", jSparqQL);
			jOut.put("queryid", queryID);

			System.out.println("Results \n" + jOut.toString(4));

			// RESULTS:

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	

	public void createOntologyBroker() {

		String configfile = "/home/smendoza/UrbanPrototypeResources-mc/UrbanPrototypeResources/config.properties";
		// services.ServiceInterface.CreateOntBroker(NAMEBROKER, ONTURI,
		// ONTFILE, POLICYFILE, resourcesPath,
		// INSTFILES, INDFILES, MODELMAPPATH, SEARCHPATH);

		try {
			ServiceInterface.CreateOntBroker(configfile);
			System.out.println("Created OntoBroker");
			System.out.println("App ready");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printRows(List<HashMap<String, Set<String>>> rows) {
		int i = 0;
		for (HashMap<String, Set<String>> row : rows) {
			System.out.println("******************** ROW " + i + " ********************");
			printRow(row);
			i++;
			System.out.println("***************************************************");
		}
	}

	public void printRow(HashMap<String, Set<String>> row) {
		for (Entry<String, Set<String>> column : row.entrySet()) {
			String colName = column.getKey();
			System.out.print("colName[" + colName + "] -> ");
			Set<String> colValues = column.getValue();
			System.out.print("Values[");
			for (String colValue : colValues) {
				System.out.print(colValue + " ||  ");
			}
			System.out.println("]");

		}
	}

}
