package smartcities.semanticweb.ontologycommons;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.sparql.engine.http.Service;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class App {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// GetSnippetTest();
		GenerateResourcesTest();
		// GetDataQueryTest();
	}

	public static void GenerateResourcesTest() {

		// String configFileEnvName = "CONFIG_FILE_URBANPROJECT";
		// String configFileEnvName = "CONFIG_FILE_URBANPROJECT_MC";
		// Map<String, String> env = System.getenv();
		// String configfile = env.get(configFileEnvName);
		// PATCH para usar maven:
		// if (configfile == null) {
		// configfile =
		// "/home/smendoza/case-smart-cities/UrbanPrototypeResources/config.properties";
		// configfile =
		// "/home/smendoza/UrbanPrototypeResources-mc/UrbanPrototypeResources/config.properties";
		//
		// System.out.println(configFileEnvName + "->null (default: " +
		// configfile + ")");
		// } else {
		// System.out.println(configFileEnvName + "->" + configfile);
		// }

		String configfile = "/home/smendoza/UrbanPrototypeResources-mc/UrbanPrototypeResources/config.properties";
		// String configfile =
		// "/home/smendoza/case-smart-cities/UrbanPrototypeResources/config.properties";

		System.out.println("Generating Resources. config.properties(" + configfile + ")...");

		try {
			ServiceInterface.GenerateResources(configfile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Generating Resources... FINISHED");
	}

	public static void GetSnippetTest() {

		String uri = "http://www.ibm.com/ICP/Scribe/CoreV2#LegalEntity";
		String query = "legal entities next to bike parking adjacent to accomodation food service";

		try {
			// SearchConcepts before having the GetSnippet useful
			List<ISearchResult> resultsUntrunked = ServiceInterface.SearchConcepts("5", query, true);
			Map<String, Map<String, String>> snippets = ServiceInterface.GetSnippet(uri, query, "<label>", "</label>");
			for (String k : snippets.keySet()) {
				System.out.print(k + ": ");
				for (String k2 : snippets.get(k).keySet()) {
					System.out.print("(" + k2 + "," + snippets.get(k).get(k2) + ")");
				}
				System.out.println();

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void GetDataQueryTest() {
		String configfile_sm = "/home/smendoza/case-smart-cities/UrbanPrototypeResources/config.properties";
		String configfile_mc = "/home/smendoza/UrbanPrototypeResources-mc/UrbanPrototypeResources/config.properties";
		try {
			ServiceInterface.CreateOntBroker(configfile_mc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("calling WebInterface.GetDataQuery(params)...");
		String params = "{query:'energy',withinstances:'true'}";
		WebInterface.GetDataQuery(params);
		System.out.println("finished WebInterface.GetDataQuery(params)");
	}

}
