package smartcities.semanticweb.ontologycommons.ontoconcepts;

import java.util.List;

public interface IOntoFunction extends IOntoRelationship {
	
	final public String INTERSECTION_URI ="http://www.opengis.net/def/function/geosparql/sfIntersects";
	final public String DISTANCE_URI ="http://www.opengis.net/def/function/geosparql/distance";
	final public String BUFFER_URI ="http://www.opengis.net/def/function/geosparql/buffer";
	final public String WITHIN_URI ="http://www.opengis.net/def/function/geosparql/sfWithin";
	
	public enum OntFunction {INTERSECTION, WITHIN, DISTANCE, BUFFER, OTHER}
	
	public List<String> getParams();
	
	public OntFunction getFunctionType();
	
	public int getParamNumber();
	
	

}
