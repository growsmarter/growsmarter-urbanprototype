package smartcities.semanticweb.ontologycommons.ontoconcepts;

import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Resource;

import smartcities.semanticweb.ontologycommons.services.UrbanOntology;

public interface IOntoBase {
	
	public enum OntType {CLASS, ECLASS, PROPERTY, APROPERTY, DINDIVIDUAL, EINDIVIDUAL, OTHER, FUNCTION, DATATYPE}
	
	
	Resource getResource();
		
	public String getURI();
	
	public OntType getType();
	
	public String getLabel();
	
	public void setLabel(String label);
	
	public int getId();
	
	public void setId(int id);

	public boolean isProperty();
	
	public boolean isFunction();
	
	public String getPrefix();
	
	public Set<IOntoBase> getInstances(boolean inference);
	
	public List<IOntoBase> getSubConcepts(boolean inference);
	
	public List<IOntoBase> getSuperConcepts(boolean inference);
	
	public Set<IOntoBase> getInstances(); //inference = false
	
	public List<IOntoBase> getSubConcepts(); //inference = false
	
	public List<IOntoBase> getSuperConcepts(); //inference = false
	
	public String getComment();
	
	public Set<IOntoProperty> getObjectProperties();
	
	public Set<IOntoProperty> getDataProperties();
	
	public Set<IOntoBase> getClassConcept(boolean inference);
	
	public boolean isGeoConcept();
	
	public boolean hasDataInstances();
	
	public String getOntologyIdentifier();
	
	public boolean isIndicator();
	
	public boolean isIndicatorDescriptor();
	
	public boolean isIndicatorClass();
	
	public boolean isIndicatorDescriptorClass();
	
	public Set<String> getData(IOntoProperty property);
	
	public Set<Resource> getObjects(IOntoProperty property);
	
	public boolean isAreaInstance();
	
	public boolean isEnumType();
}
