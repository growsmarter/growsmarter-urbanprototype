package smartcities.semanticweb.ontologycommons.ontoconcepts;

import smartcities.semanticweb.ontologycommons.services.UrbanOntology;

public interface IIndicator extends IOntoBase {
	
	public final String ROAD_SEGMENT = "http://www.ibm.com/ICP/Scribe/CoreV2#RoadSegment";
	public final String SUPERBLOCK = "http://www.ibm.com/ICP/Scribe/CoreV2#Superblock";
	public final String LANDLOT = "http://www.ibm.com/ICP/Scribe/CoreV2#LandLot";
	public final String MESH = "http://www.ibm.com/BCNEcology#Mesh_200";
	public final String NEIGHBORHOOD = "http://www.ibm.com/ICP/Scribe/CoreV2#Neighborhood";
	public final String CITY = "http://www.ibm.com/ICP/Scribe/CoreV2#City";
	
	
	public enum ANCHOR_AREAS {ROAD_SEGMENT, SUPERBLOCK, LANDLOT, MESH, NEIGHBORHOOD,CITY}
	
	public IIndicatorDescriptor getIndDescriptor();
	
	public float getIndValue();
	
	public IOntoBase getIndArea();
	
	public void editIndicatorValue(float newValue);
	

}
