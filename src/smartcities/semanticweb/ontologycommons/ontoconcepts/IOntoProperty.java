package smartcities.semanticweb.ontologycommons.ontoconcepts;

import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Resource;

public interface IOntoProperty extends IOntoBase, IOntoRelationship {
	
	//Actually the type of a property is a class, not a property itself
	public final String SCRIBE_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#scribeProperty";
	public final String SOBJECT_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#scribeObjectProperty";
	public final String SDATA_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#scribeDataProperty";
	public final String SAGGREGATE1_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#aggregateMemberOf";
	public final String SAGGREGATE2_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#hasAggregateMember";
	public final String SASSOCIATED_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#associatedTo";
	public final String SATTRIBUTE1_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#attributeOf";
	public final String SATTRIBUTE2_URI = "http://www.ibm.com/ICP/Scribe/CoreV2#hasAttribute";
	public final String ALL_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#Property";
	public final String OBJECT_URI = "http://www.w3.org/2002/07/owl#ObjectProperty";
	public final String DATA_URI = "http://www.w3.org/2002/07/owl#DatatypeProperty";
	public final String ANNOTATION_URI ="http://www.w3.org/2002/07/owl#AnnotationProperty";
	public final String TRANSITIVE_URI = "http://www.w3.org/2002/07/owl#TransitiveProperty";

	
	public enum OntProperty {ALL,SCRIBE, OBJECT, DATA, SOBJECT, SDATA, SAGGREGATE, SASSOCIATED, SATTRIBUTE, ANNOTATION, OTHER}
	public enum OntPropertyGeneral {OBJECT, DATA, OTHER}

	
	public Set<Resource> getDomain();
	
	public Set<Resource> getRange();
	
	public OntProperty getPropertyType();
	
	public OntPropertyGeneral getGeneralPropertyType();

}
