package smartcities.semanticweb.ontologycommons.ontoconcepts;

/*
 * Type of relationship between two concepts: it can be a property (IOntoProperty) or a function (IOntoFunction)
 */

public interface IOntoRelationship extends IOntoBase {
	
	public String getURI();

}
