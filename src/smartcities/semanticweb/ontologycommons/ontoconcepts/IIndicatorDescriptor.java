package smartcities.semanticweb.ontologycommons.ontoconcepts;

import java.util.List;


public interface IIndicatorDescriptor extends IOntoBase {
	
	public IIndicatorType getIndicatorType();
	
	public List<IOntoBase> getEntitiesAssoc();
	
	public List<String> getBestValues();
	
	public List<String> getMinValues();
	
	
}
