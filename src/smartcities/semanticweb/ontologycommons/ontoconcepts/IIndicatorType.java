package smartcities.semanticweb.ontologycommons.ontoconcepts;

public interface IIndicatorType extends IOntoBase {
	
	public int getGUIOrderTag();
}
