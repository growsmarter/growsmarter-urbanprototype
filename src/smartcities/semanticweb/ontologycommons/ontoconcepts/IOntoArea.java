package smartcities.semanticweb.ontologycommons.ontoconcepts;

public interface IOntoArea extends IOntoBase {
	public final String WKT_PROPERTY_URI = "http://www.opengis.net/ont/geosparql#asWKT";
	public final String WKT_DATATYPE_URI ="http://www.opengis.net/ont/sf#wktLiteral";
	public final String GEOMETRY_PROPERTY_URI ="http://www.opengis.net/ont/geosparql#hasGeometry";
	//public final String FEATURE ="http://www.opengis.net/ont/geosparql#Feature";
	public final String SPATIAL_OBJECT ="http://www.opengis.net/ont/geosparql#SpatialObject";
		
	public final String POLYGON_URI ="http://www.opengis.net/ont/sf#Polygon";
	public final String GEOMETRY_URI ="http://www.opengis.net/ont/sf#Geometry";
	public final String POINT_URI ="http://www.opengis.net/ont/sf#Point";
	public final String LINESTRING_URI ="http://www.opengis.net/ont/sf#LineString";

	public enum OntArea {GEOMETRY, POLYGON, POINT, LINESTRING, OTHER}

	public String getWKT();
	
	public OntArea getAreaType();
	
	//public void setWkt(String wkt);
	
}
