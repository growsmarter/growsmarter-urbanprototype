package smartcities.semanticweb.ontologycommons.ontoconcepts;

import java.util.Set;


public interface ISearchResult {
	
	public Set<IOntoBase> getCategory();
	
	//public void setCategory(Set<IOntoBase> categories);
	
	public String getSearchSource();
	
	public void setSearchSource(String var);
	
	public IOntoBase getOntoConcept();
	
	public Double getSimilarity();

}
