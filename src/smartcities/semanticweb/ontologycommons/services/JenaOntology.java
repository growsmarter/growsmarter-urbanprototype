package smartcities.semanticweb.ontologycommons.services;

//import org.mindswap.pellet.jena.PelletReasonerFactory;
//import org.apache.jena.query.spatial.EntityDefinition

//import com.hp.hpl.jena.graph.Triple;
//import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

import com.google.common.collect.Sets;
import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.compose.MultiUnion;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.impl.StatementImpl;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.shared.Lock;
import com.hp.hpl.jena.sparql.JenaTransactionException;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.tdb.StoreConnection;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;
import com.hp.hpl.jena.tdb.base.file.Location;
import com.hp.hpl.jena.tdb.store.DatasetGraphTDB;
import com.hp.hpl.jena.tdb.sys.TDBInternal;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.LocationMapper;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;

import java.util.*;
import java.util.Map.Entry;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.commons.io.FileUtils;

public class JenaOntology {
	// final String tdbDirectory = "TDB";
	Map<String, QueryExecution> activeQuery = new HashMap<String, QueryExecution>();
	// Dataset dBaseModel = null;
	// Dataset dDataModel = null;
	// Dataset dInferredModel = null;
	// Dataset dIndModel = null;

	String baseURI = null;
	// OntModel baseModel=null; //Scribe + imported ontologies + dataModel
	// (included as additional model)
	// OntModel dataModel=null; //instances of scribe with the geo information +
	// properties (with no imported closure)
	// //OntModel completeModel = null; //Scribe + imported ontologies +
	// instances
	// Model inferredModel=null; //baseModel inferred + dataModel (not
	// inferred). Both included as external models
	// OntModel indModel = null; //instances of indicators

	// OntModel dataModel = null;

	Model model = null;
	Model infModel = null;
	Dataset dbModel = null;
	Dataset dbInfModel = null;
	String tdbModelLocation, tdbInfModelLocation;

	// enum transactionModes {READ, WRITE};
	// transactionModes tMode;

	Set<Resource> temporalInstances = new HashSet<Resource>();

	boolean createdNewModels;

	public boolean isCreatedNewModels() {
		return createdNewModels;
	}

	// private Model getDataModel() {
	// //return dataModel;
	// //return this.dbModel.getDefaultModel();
	// return this.model;
	// }

	public Model getModel() {
		// return this.model;
		return this.dbModel.getDefaultModel();
	}

	private Model getInferredModel() {
		// return this.infModel;
		return this.dbInfModel.getDefaultModel();
	}

	public String getBaseURI() {
		return baseURI;
	}

	public void printConnectionData() {
		if (this.dbModel.isInTransaction()) {
			System.out.print("In transaction: ");
		} else {
			System.out.println("Not in transaction");

		}
		StoreConnection sc = StoreConnection.getExisting(new Location(this.tdbModelLocation));

		// sc.flush();
		System.out.println("Active Readers: " + sc.getTransMgrState().activeReaders);
		System.out.println("Active Writers: " + sc.getTransMgrState().activeWriters);
		System.out.println("Queued commits: " + sc.getTransMgrState().queuedCommits);
		System.out.println("Commited Writers: " + sc.getTransMgrState().committedWriters);

		// if (sc.getTransMgrState().activeReaders > 0 &&
		// !this.dbModel.isInTransaction()){
		// sc.flush();
		// this.dbModel.end();
		// this.dbModel.begin(ReadWrite.READ);
		// printConnectionData();
		// }

	}

	public void beginRead() {
		int count = 0;
		int maxAttempts = 3;
		boolean exception = false;
		do {
			try {

				this.dbModel.begin(ReadWrite.READ);
				this.dbInfModel.begin(ReadWrite.READ);
				// this.model = this.dbModel.getDefaultModel();
				// this.infModel = this.dbInfModel.getDefaultModel();
				exception = false;
			} catch (JenaTransactionException e) {
				exception = true;
				commitEnd();
				if (++count >= maxAttempts) {
					System.out.print(e.getMessage());
					throw e;
				}
			}
		} while (exception);
	}

	private void beginWrite() {
		int count = 0;
		int maxAttempts = 3;
		boolean exception = false;
		do {
			try {

				this.dbModel.begin(ReadWrite.WRITE);
				this.dbInfModel.begin(ReadWrite.WRITE);
				// this.model = this.dbModel.getDefaultModel();
				// this.infModel = this.dbInfModel.getDefaultModel();
				exception = false;
			} catch (JenaTransactionException e) { // TODO: specific Exception
				exception = true;
				commitEnd();
				if (++count >= maxAttempts) {
					System.out.print(e.getMessage());
					throw e;
				}
			}
		} while (exception);
	}

	public void commitEnd() {
		try {
			this.dbModel.commit();
			this.dbInfModel.commit();
		} catch (Exception e) { // TODO: specific Exception
			System.out.print(e.getMessage());
		} finally {
			this.dbModel.end();
			this.dbInfModel.end();
		}
	}

	public void finalize() {
		beginWrite();
		Resource s = null;
		Property p = null;
		RDFNode o = null;
		for (Resource res : temporalInstances) {
			this.model.removeAll(res, p, o);
			this.model.removeAll(s, p, res);
			this.infModel.removeAll(res, p, o);
			this.infModel.removeAll(s, p, res);
		}
		commitEnd();
		TDBFactory.release(dbModel);
		TDBFactory.release(dbInfModel);
	}

	// INITIALIZE ONTOLOGY MODELS

	private Set<Statement> UrbanInferenceEngine() {
		this.printConnectionData();
		beginRead();
		String core = "http://www.ibm.com/ICP/Scribe/CoreV2#";
		Model infModel = getInferredModel();
		Model model = getModel();
		Set<Statement> newstatements = new HashSet<Statement>();
		Resource subject = null;
		RDFNode object = null;
		Property property = null;

		// CONSTRUCT {?legal CoreV2:legalEntityAssocToAsset ?landlot . ?landlot
		// CoreV2:assetAssocToLegalEntity ?legal} WHERE {?legal
		// CoreV2:legalEntityAssocToAsset ?buseunit . ?buseunit
		// CoreV2:bldgUseUnitInLandLot ?landlot}
		for (Iterator iterator = model.listStatements(subject, model.getProperty(core + "legalEntityAssocToAsset"),
				object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			Resource legal = st.getSubject();
			if (st.getObject().isURIResource()) {
				Resource buseunit = st.getObject().asResource();
				for (Iterator iterator2 = model.listStatements(buseunit,
						model.getProperty(core + "bldgUseUnitInLandLot"), object); iterator2.hasNext();) {
					Statement st2 = (Statement) iterator2.next();
					if (st2.getObject().isURIResource()) {
						Resource landlot = st2.getObject().asResource();
						newstatements.add(new StatementImpl(legal, model.getProperty(core + "legalEntityAssocToAsset"),
								landlot));
						newstatements.add(new StatementImpl(landlot, model
								.getProperty(core + "assetAssocToLegalEntity"), legal));
					}
				}
			}
		}

		// CONSTRUCT {CoreV2:legalEntityAssocToAsset rdfs:range ?range} WHERE
		// {CoreV2:bldgUseUnitInLandLot rdfs:range ?range}
		// CONSTRUCT {CoreV2:assetAssocToLegalEntity rdfs:domain ?domain} WHERE
		// {CoreV2:bldgUseUnitInLandLot rdfs:range ?domain}
		for (Iterator iterator = model.listStatements(model.getResource(core + "bldgUseUnitInLandLot"), RDFS.range,
				object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			if (st.getObject().isURIResource()) {
				Resource range = st.getObject().asResource();
				newstatements.add(new StatementImpl(model.getResource(core + "legalEntityAssocToAsset"), RDFS.range,
						range));
				newstatements.add(new StatementImpl(model.getResource(core + "assetAssocToLegalEntity"), RDFS.domain,
						range));
			}
		}

		// CONSTRUCT {?buseunit CoreV2:locatedIn ?route . ?route
		// CoreV2:locationOf ?buseunit} WHERE {?buseunit
		// CoreV2:bldgUseUnitInLandLot ?landlot . ?landlot
		// CoreV2:areaAdjacentToRoute ?route}
		for (Iterator iterator = model
				.listStatements(subject, model.getProperty(core + "bldgUseUnitInLandLot"), object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			Resource buseunit = st.getSubject();
			if (st.getObject().isURIResource()) {
				Resource landlot = st.getObject().asResource();
				for (Iterator iterator2 = model.listStatements(landlot,
						model.getProperty(core + "areaAdjacentToRoute"), object); iterator2.hasNext();) {
					Statement st2 = (Statement) iterator2.next();
					if (st2.getObject().isURIResource()) {
						Resource route = st2.getObject().asResource();
						newstatements.add(new StatementImpl(buseunit, model.getProperty(core + "locatedIn"), route));
						newstatements.add(new StatementImpl(route, model.getProperty(core + "locationOf"), buseunit));
					}
				}
			}
		}

		// CONSTRUCT {?building CoreV2:locatedIn ?route . ?route
		// CoreV2:locationOf ?building} WHERE {?building
		// CoreV2:builtStructureInLandLot ?landlot . ?landlot
		// CoreV2:areaAdjacentToRoute ?route}
		for (Iterator iterator = model.listStatements(subject, model.getProperty(core + "builtStructureInLandLot"),
				object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			Resource building = st.getSubject();
			if (st.getObject().isURIResource()) {
				Resource landlot = st.getObject().asResource();
				for (Iterator iterator2 = model.listStatements(landlot,
						model.getProperty(core + "areaAdjacentToRoute"), object); iterator2.hasNext();) {
					Statement st2 = (Statement) iterator2.next();
					if (st2.getObject().isURIResource()) {
						Resource route = st2.getObject().asResource();
						newstatements.add(new StatementImpl(building, model.getProperty(core + "locatedIn"), route));
						newstatements.add(new StatementImpl(route, model.getProperty(core + "locationOf"), building));
					}
				}
			}
		}

		// CONSTRUCT {CoreV2:locatedIn rdfs:domain ?domain} WHERE
		// {CoreV2:bldgUseUnitInLandLot rdfs:domain ?domain}
		// CONSTRUCT {CoreV2:locationOf rdfs:range ?range} WHERE
		// {CoreV2:bldgUseUnitInLandLot rdfs:domain ?range}
		for (Iterator iterator = model.listStatements(model.getResource(core + "bldgUseUnitInLandLot"), RDFS.domain,
				object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			if (st.getObject().isURIResource()) {
				Resource domain = st.getObject().asResource();
				newstatements.add(new StatementImpl(model.getResource(core + "locatedIn"), RDFS.domain, domain));
				newstatements.add(new StatementImpl(model.getResource(core + "locationOf"), RDFS.range, domain));
			}
		}

		// CONSTRUCT {CoreV2:locatedIn rdfs:domain ?domain} WHERE
		// {CoreV2:builtStructureInLandLot rdfs:domain ?domain}
		// CONSTRUCT {CoreV2:locationOf rdfs:range ?range} WHERE
		// {CoreV2:builtStructureInLandLot rdfs:domain ?range}
		for (Iterator iterator = model.listStatements(model.getResource(core + "builtStructureInLandLot"), RDFS.domain,
				object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			if (st.getObject().isURIResource()) {
				Resource domain = st.getObject().asResource();
				newstatements.add(new StatementImpl(model.getResource(core + "locatedIn"), RDFS.domain, domain));
				newstatements.add(new StatementImpl(model.getResource(core + "locationOf"), RDFS.range, domain));
			}
		}

		// CONSTRUCT {CoreV2:locatedIn rdfs:range ?range} WHERE
		// {CoreV2:areaAdjacentToRoute rdfs:range ?range}
		// CONSTRUCT {CoreV2:locationOf rdfs:domain ?domain} WHERE
		// {CoreV2:areaAdjacentToRoute rdfs:range ?domain}
		for (Iterator iterator = model.listStatements(model.getResource(core + "areaAdjacentToRoute"), RDFS.range,
				object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			if (st.getObject().isURIResource()) {
				Resource range = st.getObject().asResource();
				newstatements.add(new StatementImpl(model.getResource(core + "locatedIn"), RDFS.range, range));
				newstatements.add(new StatementImpl(model.getResource(core + "locationOf"), RDFS.domain, range));
			}
		}

		// CONSTRUCT {?newprop rdf:type owl:DatatypeProperty . ?newprop
		// rdfs:domain ?domain . ?ind1 ?newprop ?value . ?newprop rdfs:label
		// ?newlabel}
		// WHERE {?p1 rdfs:subPropertyOf CoreV2:hasAttribute . ?ind1 rdf:type
		// ?domain . ?p2 rdf:type owl:DatatypeProperty . ?ind1 ?p1 ?ind2 . ?ind2
		// ?p2 ?value .
		// ?p1 rdfs:label ?p1label . ?p2 rdfs:label ?p2label .
		// BIND(REPLACE(str(?p1), '^.*(#|/)', '') AS ?p1localname) .
		// BIND(REPLACE(str(?p2), '^.*(#|/)', '') AS ?p2localname) .
		// BIND(CONCAT(str(?p1label),'-', str(?p2label)) AS ?newlabel) . BIND
		// (URI(CONCAT('http://www.ibm.com/ICP/Scribe/CoreV2#',str(?p1localname),'.',str(?p2localname)))
		// AS ?newprop)}
		Set<Resource> dataproperties = infModel.listSubjectsWithProperty(RDF.type, OWL.DatatypeProperty).toSet();
		dataproperties.add(model.getResource(IOntoArea.WKT_PROPERTY_URI));
		Set<Resource> hasAttproperties = infModel.listSubjectsWithProperty(RDFS.subPropertyOf,
				model.getProperty(core + "hasAttribute")).toSet();
		hasAttproperties.add(model.getResource(IOntoArea.GEOMETRY_PROPERTY_URI));
		for (Resource p1 : hasAttproperties) {
			for (Resource p2 : dataproperties) {
				boolean existLink = false;
				if (p1.getURI().equals(IOntoArea.GEOMETRY_PROPERTY_URI)
						&& p2.getURI().equals(IOntoArea.WKT_PROPERTY_URI))
					existLink = false;

				String newpropuri = p1.getLocalName() + "." + p2.getLocalName();
				Property newprop = ResourceFactory.createProperty(core + newpropuri);
				// rdf:type owl:DatatypeProperty
				// newstatements.add(new StatementImpl(newprop,RDF.type,
				// OWL.DatatypeProperty));
				for (Iterator iterator = model.listStatements(subject, model.getProperty(p1.getURI()), object); iterator
						.hasNext();) {
					Statement st = (Statement) iterator.next();
					Resource ind1 = st.getSubject();
					if (st.getObject().isURIResource()) {
						Resource ind2 = st.getObject().asResource();
						for (Iterator iterator2 = model.listStatements(ind2, model.getProperty(p2.getURI()), object); iterator2
								.hasNext();) {
							Statement st2 = (Statement) iterator2.next();
							if (st2.getObject().isLiteral()) {
								existLink = true;
								newstatements.add(new StatementImpl(ind1, newprop, st2.getObject().asLiteral()));
							}

						}
					}
				}
				if (existLink) {

					String p1label = GetLabelNotTransact(p1.getURI(), false);
					String p2label = GetLabelNotTransact(p2.getURI(), false);

					String newlabel = "";
					if (p1label != null)
						newlabel += p1label;
					else
						newlabel += p1.getURI();
					newlabel += " - ";
					if (p2label != null)
						newlabel += p2label;
					else
						newlabel += p2.getURI();
					newstatements.add(new StatementImpl(newprop, RDF.type, OWL.DatatypeProperty));
					newstatements.add(new StatementImpl(newprop, model.getProperty(core + "derivedFrom"), p2));
					newstatements.add(new StatementImpl(newprop, RDFS.label, ResourceFactory
							.createPlainLiteral(newlabel)));
					for (Iterator iterator3 = model.listStatements(p1, RDFS.domain, object); iterator3.hasNext();) {
						Statement st3 = (Statement) iterator3.next();
						if (st3.getObject().isURIResource())
							newstatements.add(new StatementImpl(newprop, RDFS.domain, st3.getObject().asResource()));
					}
					for (Iterator iterator3 = model.listStatements(p2, RDFS.range, object); iterator3.hasNext();) {
						Statement st3 = (Statement) iterator3.next();
						if (st3.getObject().isURIResource())
							newstatements.add(new StatementImpl(newprop, RDFS.range, st3.getObject().asResource()));
					}
				}
			}

		}

		commitEnd();
		return newstatements;

	}

	public synchronized void removeStatement(Statement st, boolean inferred) {
		beginWrite();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		try {
			ontology.remove(st);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		commitEnd();
	}

	public synchronized void addNewStatement(Statement newSt, boolean inferred) {
		beginWrite();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		try {
			ontology.remove(newSt);
			if (!ontology.contains(newSt)) // Aparentemente no funciona
				ontology.add(newSt);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		commitEnd();
	}

	public synchronized void removeStatement(Resource subject, Property predicate, RDFNode object, boolean inferred) {
		beginRead();
		Statement newSt = new StatementImpl(subject, predicate, object);
		commitEnd();
		removeStatement(newSt, inferred);
	}

	public synchronized void addNewStatement(Resource subject, Property predicate, RDFNode object, boolean inferred) {
		beginRead();
		Statement newSt = new StatementImpl(subject, predicate, object);
		commitEnd();
		addNewStatement(newSt, inferred);
	}

	public void removeResource(String uri) {
		beginRead();
		Resource res = this.getModel().getResource(uri);
		commitEnd();
		removeResource(res);
	}

	public synchronized void removeResource(Resource res) {
		beginWrite();
		if (res != null) {
			Resource s = null;
			RDFNode o = null;
			Property p = null;
			this.model.removeAll(res, p, o);
			this.model.removeAll(s, p, res);
			this.infModel.removeAll(res, p, o);
			this.infModel.removeAll(s, p, res);
		}
		commitEnd();
	}

	public synchronized Resource newInstance(String uri, Resource cl) {
		beginWrite();
		Resource res = this.getModel().getResource(uri);
		if (res != null) {
			Resource s = null;
			RDFNode o = null;
			Property p = null;
			this.model.removeAll(res, p, o);
			this.model.removeAll(s, p, res);
			this.infModel.removeAll(res, p, o);
			this.infModel.removeAll(s, p, res);
		}
		Resource newInstance = this.getModel().createResource(uri);
		this.temporalInstances.add(newInstance);
		commitEnd();
		addNewStatement(newInstance, RDF.type, cl, false);
		addNewStatement(newInstance, RDF.type, cl, true);
		return newInstance;
	}

	public static Model getTBox(String policyFile, String bcnURI, String bcnIndicatorsURI, String ontologiesPath) {
		// CREATE TBOX IN MEMORY
		OntDocumentManager dm = new OntDocumentManager(policyFile); // if it is
																	// an uri,
																	// the
																	// prefix
																	// must be
																	// "http:",
																	// if it's a
																	// file,
																	// then the
																	// prefix is
																	// "file:"
		dm.addIgnoreImport("http://www.daml.ri.cmu.edu/ont/USRegionState.daml");
		dm.addIgnoreImport("http://purl.org/dc/elements/1.1/");
		OntModelSpec omsNotInferred = new OntModelSpec(OntModelSpec.OWL_DL_MEM);
		omsNotInferred.setDocumentManager(dm);
		UpdatePathImportedFiles(dm.getFileManager(), ontologiesPath);
		OntModel baseModel = ModelFactory.createOntologyModel(omsNotInferred);
		baseModel.read(bcnURI, "TURTLE"); // read tbox

		OntModel indModel = ModelFactory.createOntologyModel(omsNotInferred);
		indModel.read(bcnIndicatorsURI, "TURTLE");
		return baseModel.add(indModel); // add base indicators to tbox
	}

	// create jena model from new resources
	public JenaOntology(Properties properties, Dataset mainmodel, Model tbox) throws IOException {
		String tdbPath = properties.getProperty("tdbPath");
		createdNewModels = true;
		this.tdbModelLocation = tdbPath + "/model";
		this.dbModel = mainmodel;
		this.dbModel.begin(ReadWrite.WRITE);
		this.model = dbModel.getDefaultModel();

		Path path = Paths.get(tdbPath + "/infModel");
		if (Files.notExists(path))
			Files.createDirectories(path);
		else
			FileUtils.cleanDirectory(new File(tdbPath + "/infModel"));
		this.tdbInfModelLocation = tdbPath + "/infModel";
		this.dbInfModel = TDBFactory.createDataset(this.tdbInfModelLocation);
		this.dbInfModel.begin(ReadWrite.WRITE);
		this.infModel = dbInfModel.getDefaultModel();
		if (!infModel.isEmpty()) {
			infModel.removeAll();
		}
		this.infModel.add(model);
		this.infModel.setNsPrefixes(model.getNsPrefixMap());

		// Add tbox to both models
		this.model.add(tbox);
		InfModel tboxInf = ModelFactory.createInfModel(ReasonerRegistry.getRDFSSimpleReasoner(), tbox);
		this.infModel.add(tboxInf);
		commitEnd();

		// Add Urban inference to both models

		Set<Statement> newstatements = UrbanInferenceEngine();
		beginWrite();
		System.out.println("Urban Inference Engine: inserting " + newstatements.size() + " new statements");
		this.infModel = this.getInferredModel();
		this.model = this.getModel();
		for (Statement st : newstatements) {
			if (!this.infModel.contains(st))
				this.infModel.add(st);
			if (!this.model.contains(st))
				this.model.add(st);
		}
		commitEnd();

		CopyTDB(tdbPath); // TRABAJA CON UNA COPIA, LAS TDB ORIGINALES NO SE
							// MODIFICAN

		// DEBUG
		Set<Resource> res = this.GetSubjects(
				this.getResource("http://www.ibm.com/ICP/Scribe/CoreV2#BuiltStructure", true), RDFS.subClassOf, true);
		for (Resource r : res)
			System.out.println(r.getURI());

	}

	// create jena model from existing resources
	public JenaOntology(Properties properties) throws IOException {
		String tdbPath = properties.getProperty("tdbPath");
		this.tdbModelLocation = tdbPath + "/model";
		this.dbModel = TDBFactory.createDataset(this.tdbModelLocation);
		this.model = dbModel.getDefaultModel();

		this.tdbInfModelLocation = tdbPath + "/infModel";
		this.dbInfModel = TDBFactory.createDataset(this.tdbInfModelLocation);
		this.infModel = dbInfModel.getDefaultModel();

		createdNewModels = false;
		CopyTDB(tdbPath); // TRABAJA CON UNA COPIA, LAS TDB ORIGINALES NO SE
							// MODIFICAN
	}

	private void CopyTDB(String tdbPath) throws IOException {
		this.beginRead();

		Path path = Paths.get(tdbPath + "/modelCopy");
		String tdbModelLocationCopy = path.toFile().getAbsolutePath();
		if (Files.notExists(path))
			Files.createDirectories(path);
		else
			FileUtils.cleanDirectory(new File(tdbModelLocationCopy));

		Dataset dbModelCopy = TDBFactory.createDataset(tdbModelLocationCopy);
		dbModelCopy.begin(ReadWrite.WRITE);
		Model modelCopy = dbModelCopy.getDefaultModel();
		modelCopy.add(this.model);
		modelCopy.setNsPrefixes(this.model.getNsPrefixMap());
		dbModelCopy.commit();

		path = Paths.get(tdbPath + "/infModelCopy");
		String tdbInfModelLocationCopy = path.toFile().getAbsolutePath();
		if (Files.notExists(path))
			Files.createDirectories(path);
		else
			FileUtils.cleanDirectory(new File(tdbInfModelLocationCopy));

		Dataset dbInfModelCopy = TDBFactory.createDataset(tdbInfModelLocationCopy);
		dbInfModelCopy.begin(ReadWrite.WRITE);
		Model infModelCopy = dbInfModelCopy.getDefaultModel();
		infModelCopy.add(this.infModel);
		infModelCopy.setNsPrefixes(this.infModel.getNsPrefixMap());
		dbInfModelCopy.commit();

		this.commitEnd();

		createdNewModels = false;
		this.tdbModelLocation = tdbModelLocationCopy;
		this.tdbInfModelLocation = tdbInfModelLocationCopy;
		this.dbModel = dbModelCopy;
		this.dbInfModel = dbInfModelCopy;
		// this.model = dbModel.getDefaultModel();
		// this.infModel =dbInfModel.getDefaultModel();
	}

	// public JenaOntology(Properties properties) throws MalformedURLException,
	// IOException{
	// String tdbPath = properties.getProperty("tdbPath");
	// String ontUri = properties.getProperty("baseuri");
	// String instUri = properties.getProperty("instancesuri");
	// String instFiles = properties.getProperty("instancesontology");
	// String policyFile = properties.getProperty("ontologiespolicy");
	// String ontologiesPath = properties.getProperty("ontologiesPath");
	//
	// this.tdbModelLocation = tdbPath+"/model";
	// this.dbModel = TDBFactory.createDataset(this.tdbModelLocation);
	// this.model = dbModel.getDefaultModel();
	//
	// this.tdbInfModelLocation = tdbPath+"/infModel";
	// this.dbInfModel = TDBFactory.createDataset(this.tdbInfModelLocation);
	// this.infModel =dbInfModel.getDefaultModel();
	//
	// if (this.model.isEmpty() || this.infModel.isEmpty()){
	// //if (true){
	// System.out.println("Creating Models...");
	// createdNewModels = true;
	// OntDocumentManager dm = new OntDocumentManager(policyFile); //if it is an
	// uri, the prefix must be "http:", if it's a file, then the prefix is
	// "file:"
	// dm.addIgnoreImport("http://www.daml.ri.cmu.edu/ont/USRegionState.daml");
	// dm.addIgnoreImport("http://purl.org/dc/elements/1.1/");
	// //OntModelSpec omsInferred = new
	// OntModelSpec(OntModelSpec.OWL_DL_MEM_RDFS_INF);
	// OntModelSpec omsNotInferred = new OntModelSpec(OntModelSpec.OWL_DL_MEM);
	// omsNotInferred.setDocumentManager(dm);
	//
	// //TODO:check if it is neccessary
	// UpdatePathImportedFiles(dm.getFileManager(), ontologiesPath);
	//
	// //CREATE TBOX
	// OntModel baseModel = ModelFactory.createOntologyModel(omsNotInferred);
	// baseModel.read(ontUri, "TURTLE");
	// OntModel indModel = ModelFactory.createOntologyModel(omsNotInferred);
	// indModel.read(instUri, "TURTLE");
	// Model tbox = baseModel.add(indModel);
	//
	// //INFER TBOX
	// Model reasCap =
	// ReasonerRegistry.getRDFSSimpleReasoner().getReasonerCapabilities();
	// System.out.println(ReasonerRegistry.getRDFSSimpleReasoner().toString()+" : Reasoner rules");
	// for (Iterator iterator = reasCap.listStatements(); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// InfModel tboxInf =
	// ModelFactory.createInfModel(ReasonerRegistry.getRDFSSimpleReasoner(),
	// tbox);
	//
	// FileManager fm=FileManager.get();
	// InputStream in1=fm.open(instFiles);
	//
	// //CREATE MODEL IN TDB: TBOX + ABOX
	// this.printConnectionData();
	// // beginWrite();
	// // dbModel.getDefaultModel().removeAll();
	// // commitEnd();
	// beginWrite();
	// DatasetGraphTDB dsg=
	// TDBInternal.getBaseDatasetGraphTDB(dbModel.asDatasetGraph());
	// TDBLoader tdb = new TDBLoader();
	//
	// tdb.loadGraph(dsg.getDefaultGraphTDB(),in1);
	// this.printConnectionData();
	// commitEnd();
	// beginWrite();
	// this.model =this.dbModel.getDefaultModel(); //IMPORTANTE: el modelo con
	// el que se trabaje debe obtenerse TRAS el inicio (begin) de la transacción
	// this.model.add(tbox);
	// //this.model.setNsPrefixes(dbModel.getDefaultModel().getNsPrefixMap());
	// commitEnd();
	// in1.close();
	// System.out.println("TDB model generated in "+tdbPath+"/model");
	//
	// //DEBUG
	// beginRead();
	// model =dbModel.getDefaultModel();
	// System.out.println("Testing NOT inferred model");
	// Resource subject = null;
	// Property p = null;
	// RDFNode object = null;
	// for (Iterator iterator = model.listStatements(subject,
	// model.getProperty("http://www.ibm.com/ICP/Scribe/CoreV2#legalEntityAssocToAsset"),
	// object); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// for (Iterator iterator =
	// model.listStatements(model.getResource("http://www.ibm.com/ICP/Scribe/CoreV2#GreenCoveredBuilding"),
	// RDFS.subClassOf, object); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// for (Iterator iterator =
	// model.listStatements(this.model.getResource("http://www.ibm.com/BCNEcologyINST#bcnInstance8051"),
	// p, object); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// commitEnd();
	// //ENDDEBUG
	//
	// //CREATE INF MODEL IN TDB: TBOX INFERRED + ABOX
	// in1 = fm.open(instFiles);
	// // beginWrite();
	// // dbInfModel.getDefaultModel().removeAll();
	// // commitEnd();
	// beginWrite();
	// dsg= TDBInternal.getBaseDatasetGraphTDB(dbInfModel.asDatasetGraph());
	// tdb.loadGraph(dsg.getDefaultGraphTDB(),in1);
	// commitEnd();
	// beginWrite();
	// this.infModel =this.dbInfModel.getDefaultModel(); //IMPORTANTE: el modelo
	// con el que se trabaje debe obtenerse TRAS el inicio (begin) de la
	// transacción
	// this.infModel.add(tboxInf);
	// //this.infModel.setNsPrefixes(dbInfModel.getDefaultModel().getNsPrefixMap());
	// commitEnd();
	// in1.close();
	// System.out.println("TDB infModel generated in "+tdbPath+"/infModel");
	//
	// //DEBUG
	// beginRead();
	// infModel =dbInfModel.getDefaultModel();
	// System.out.println("Testing inferred model");
	// for (Iterator iterator = infModel.listStatements(subject,
	// infModel.getProperty("http://www.ibm.com/ICP/Scribe/CoreV2#legalEntityAssocToAsset"),
	// object); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// for (Iterator iterator =
	// infModel.listStatements(infModel.getResource("http://www.ibm.com/ICP/Scribe/CoreV2#GreenCoveredBuilding"),
	// RDFS.subClassOf, object); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// for (Iterator iterator =
	// infModel.listStatements(infModel.getResource("http://www.ibm.com/BCNEcologyINST#bcnInstance8051"),
	// p, object); iterator.hasNext();) {
	// Statement st = (Statement) iterator.next();
	// System.out.println(st.toString());
	// }
	// commitEnd();
	// //ENDDEBUG
	//
	//
	// //ADD TO INF MODEL THE URBAN INFERENCE
	// Set<Statement> newstatements = UrbanInferenceEngine();
	// int count = newstatements.size();
	// beginWrite();
	// this.infModel = this.getInferredModel();
	// this.model = this.getModel();
	// for (Statement st: newstatements){
	// if (st.getPredicate().getURI().contains("hasGeometry.asWKT"))
	// st = st;
	// if (!this.infModel.contains(st))
	// this.infModel.add(st);
	// if (!this.model.contains(st))
	// this.model.add(st);
	// //this.addNewStatement(st, false);
	// //this.addNewStatement(st, true);
	// System.out.println(count--);
	// }
	// commitEnd();
	// printConnectionData();
	// System.out.println("TDB models generated in "+tdbPath+"/model");
	// }else {
	// createdNewModels = false;
	// this.model = dbModel.getDefaultModel();
	// this.infModel =dbInfModel.getDefaultModel();
	// System.out.println("Models read from TDB");
	//
	// // Resource object = null;
	// // for (Resource r: this.GetSubjects(object,
	// this.getProperty("http://www.ibm.com/ICP/Scribe/CoreV2#hasArea.abbreviation",
	// false), false))
	// // System.out.println(r.getURI());
	// }
	//
	// }

	// public void JenaOntologyInMemory(String ontUri, String file, String
	// policyFile, String appPath, String pathInstFiles, String pathIndFiles)
	// throws MalformedURLException, IOException{
	// OntDocumentManager dm = new OntDocumentManager(appPath+policyFile); //if
	// it is an uri, the prefix must be "http:", if it's a file, then the prefix
	// is "file:"
	// dm.addIgnoreImport("http://www.daml.ri.cmu.edu/ont/USRegionState.daml");
	// OntModel model, inferredModel;
	// OntModelSpec omsNotInferred = new OntModelSpec(OntModelSpec.OWL_DL_MEM);
	// omsNotInferred.setDocumentManager(dm);
	//
	// OntModelSpec omsInferred = new
	// OntModelSpec(OntModelSpec.OWL_DL_MEM_RDFS_INF);
	// omsInferred.setDocumentManager(dm);
	//
	// this.dataModel = ModelFactory.createOntologyModel(omsNotInferred);
	// UpdatePathImportedFiles(dataModel, appPath);
	// dataModel.read(appPath+pathInstFiles);
	//
	// OntModel baseModel = ModelFactory.createOntologyModel(omsNotInferred);
	// UpdatePathImportedFiles(baseModel, appPath);
	// baseModel.read(appPath+file);
	//
	//
	// OntModel baseModelInferred =
	// ModelFactory.createOntologyModel(omsInferred);
	// UpdatePathImportedFiles(baseModelInferred, appPath);
	// baseModelInferred.read(appPath+file);
	//
	// OntModel indModel = ModelFactory.createOntologyModel(omsNotInferred);
	// UpdatePathImportedFiles(indModel, appPath);
	// indModel.read(appPath+pathIndFiles);
	//
	// OntModel indModelInferred =
	// ModelFactory.createOntologyModel(omsInferred);
	// UpdatePathImportedFiles(indModelInferred, appPath);
	// indModelInferred.read(appPath+pathIndFiles);
	//
	// model = ModelFactory.createOntologyModel(omsNotInferred);
	// model.add(baseModel);
	// model.add(dataModel);
	// model.add(indModel);
	// this.model = model;
	//
	// inferredModel = ModelFactory.createOntologyModel(omsNotInferred);
	// inferredModel.add(baseModelInferred);
	// inferredModel.add(indModelInferred);
	// inferredModel.add(dataModel);
	// this.infModel = inferredModel;
	// }

	private static void UpdatePathImportedFiles(FileManager fm, String ontologiesPath) {
		LocationMapper lm = fm.getLocationMapper();
		HashMap<String, String> map = new HashMap<String, String>();
		for (Iterator<String> iterator = lm.listAltEntries(); iterator.hasNext();) {
			String remoteUri = (String) iterator.next();
			String localUri = lm.altMapping(remoteUri);
			map.put(remoteUri, localUri);
		}
		for (String remoteUri : map.keySet()) {
			String localUri = map.get(remoteUri);
			if (localUri.contains("file:")) {
				localUri = localUri.replace("file:", "file:" + ontologiesPath);
				lm.removeAltEntry(remoteUri);
				lm.addAltEntry(remoteUri, localUri);
			}
		}
	}

	private void UpdatePathImportedFiles(OntModel baseModel, String appPath) {
		FileManager fm = baseModel.getDocumentManager().getFileManager();
		LocationMapper lm = fm.getLocationMapper();
		HashMap<String, String> map = new HashMap<String, String>();
		for (Iterator<String> iterator = lm.listAltEntries(); iterator.hasNext();) {
			String remoteUri = (String) iterator.next();
			String localUri = lm.altMapping(remoteUri);
			map.put(remoteUri, localUri);
		}
		for (String remoteUri : map.keySet()) {
			String localUri = map.get(remoteUri);
			if (localUri.contains("file:")) {
				localUri = localUri.replace("file:", "file:" + appPath);
				lm.removeAltEntry(remoteUri);
				lm.addAltEntry(remoteUri, localUri);
			}
		}
	}

	// GET RESULTS FROM SPARQL QUERIES

	public QueryResults ResultSetQueryOntology(String sparqlQuery, boolean inferred, String idSession) {
		QueryResults qr = null;
		CancelActiveQuery(idSession);
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		// Model ontology = ds.getDefaultModel();
		// QueryExecution qexec;
		try {
			Query q = QueryFactory.create(sparqlQuery);
			// Op op = Algebra.compile(q);
			// System.out.println(op.toString());
			// Query q
			// =QueryFactory.create("SELECT distinct ?a WHERE {?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?b}");
			QueryExecution qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
			NewQuery(qexec, idSession);
			ResultSet results = qexec.execSelect();
			qr = new QueryResults(results);
			qexec.close();
		} catch (Exception e) {
			qr = null;
		}
		commitEnd();
		return qr;
	}

	public void NewQuery(QueryExecution qexec, String idSession) {
		if (OnQuery(idSession)) {
			CancelActiveQuery(idSession);
		}
		this.activeQuery.put(idSession, qexec);
	}

	public boolean OnQuery(String idSession) {
		if (activeQuery.containsKey(idSession)) {
			QueryExecution qexec = activeQuery.get(idSession);
			if (qexec != null && !qexec.isClosed())
				return true;
		}
		return false;
	}

	public boolean CancelActiveQuery(String idSession) {
		if (OnQuery(idSession)) {
			QueryExecution qexec = this.activeQuery.get(idSession);
			qexec.abort();
			qexec.close();
			this.activeQuery.remove(idSession);
		}
		return true;
	}

	public String XMLQueryOntology(String sparqlQuery, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		// Model ontology = ds.getDefaultModel();
		String output = null;
		QueryExecution qexec;
		Query q = QueryFactory.create(sparqlQuery);
		qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
		ResultSet results = qexec.execSelect();
		output = ResultSetFormatter.asXMLString(results);
		qexec.close();
		commitEnd();
		return output;
	}

	public OntModel RDFQueryOntology(String sparqlQuery, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		// Model ontology = ds.getDefaultModel();
		Query q = QueryFactory.create(sparqlQuery);
		QueryExecution qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
		ResultSet results = qexec.execSelect();
		OntModel modelRes = ModelFactory.createOntologyModel();
		ResultSetFormatter.asRDF(modelRes, results);
		qexec.close();
		commitEnd();
		return modelRes;
	}

	public boolean AskQueryOntology(String sparqlQuery, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		// Model ontology = ds.getDefaultModel();
		boolean output;
		QueryExecution qexec;
		Query q = QueryFactory.create(sparqlQuery);
		qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
		output = qexec.execAsk();
		qexec.close();
		commitEnd();
		return output;
	}

	// GENERAL FUNCTIONS OVER ONTOLOGIES
	public Map<String, String> getMapPrefixes(boolean importedClosure, OntModel ontology) {
		Map<String, String> prefixMap = new HashMap<String, String>();
		prefixMap.put("fn", "http://www.w3.org/2005/xpath-functions#");
		prefixMap.put("afn", "http://jena.hpl.hp.com/ARQ/function#"); // sparql
																		// functions
																		// in
																		// jena
		prefixMap = getPrefixBlock(ontology, prefixMap);
		if (importedClosure) {
			Set<String> importedClos = ontology.listImportedOntologyURIs(true);
			for (String uri : importedClos) {
				OntModel impOntModel = ontology.getImportedModel(uri);
				prefixMap = getPrefixBlock(impOntModel, prefixMap);
			}
		}
		return prefixMap;
	}

	public String getPrefix(String namespace) {
		String prefix = "";
		Map<String, String> mapNamespaces = getMappedNamespaces(false);
		// if (mapNamespaces.containsValue(namespace)) {
		// for (String key: mp.keySet())
		// if (mp.get(key).equals(namespace)){
		// prefix = key;
		// break;
		// }
		// }

		// DEBUGGING:
		try {
			FileWriter fw = new FileWriter("/tmp/GS_getPrefix_mappings.csv");
			for (Entry<String, String> entry : mapNamespaces.entrySet()) {
				fw.write(entry.getKey() + "->" + entry.getValue() + "\n");
			}
			fw.flush();
			fw.close();

			// The old get prefixes -> getMapPrefixes()
			Map<String, String> mapPrefixes = getMapPrefixes(false);
			fw = new FileWriter("/tmp/GS_getMapPrefixes_mappings.csv");
			for (Entry<String, String> entry : mapPrefixes.entrySet()) {
				fw.write(entry.getKey() + "->" + entry.getValue() + "\n");
			}
			fw.flush();
			fw.close();

			fw = new FileWriter("/tmp/GS_getMapPrefixes_mappings_difference.csv");
			fw.write("\n");
			Set<String> difference_1 = Sets.symmetricDifference(mapPrefixes.keySet(),
					new HashSet<String>(mapNamespaces.values()));
			for (String elems : difference_1) {
				fw.write(elems + ",");
			}
			fw.flush();
			fw.write("\n");
			fw.write("********************************************************");
			fw.write("\n");

			Set<String> difference_2 = Sets.symmetricDifference(mapPrefixes.keySet(),
					new HashSet<String>(mapNamespaces.values()));
			for (String elems : difference_2) {
				fw.write(elems + ",");
			}
			fw.flush();
			fw.write("\n");
			fw.write("********************************************************");
			fw.write("\n");
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		prefix = mapNamespaces.get(namespace);
		if (prefix != null) {
			return prefix;
		} else {
			return "";
		}
	}

	public String getNamespace(String prefix) {
		String namespace = "";
		if (getMapPrefixes(false).containsKey(prefix))
			namespace = getMapPrefixes(false).get(prefix);
		return namespace;
	}

	public Map<String, String> getMapPrefixes(boolean inferred) {

		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		// Model ontology = ds.getDefaultModel();
		Map<String, String> prefixMap = new HashMap<String, String>();
		prefixMap.put("fn", "http://www.w3.org/2005/xpath-functions#");
		prefixMap.put("afn", "http://jena.hpl.hp.com/ARQ/function#"); // sparql
																		// functions
																		// in
																		// jena
		prefixMap = getPrefixBlock(ontology, prefixMap);
		commitEnd();
		return prefixMap;
	}

	/**
	 * Returns a map ontology_namespace--->prefix
	 * 
	 * @param inferred
	 * @return
	 */
	public Map<String, String> getMappedNamespaces(boolean inferred) {

		Map<String, String> mapPrefixes = getMapPrefixes(inferred);
		Map<String, String> mapNamespaces = new HashMap<>();
		for (Entry<String, String> entry : mapPrefixes.entrySet()) {
			mapNamespaces.put(entry.getValue(), entry.getKey());
		}
		return mapNamespaces;
	}

	/**
	 * Answer the prefix for the given URI, or null if there isn't one. If there
	 * is more than one, one of them will be picked. If possible, it will be the
	 * most recently added prefix. (The cases where it's not possible is when a
	 * binding has been removed.)
	 * 
	 * @author: smendoza
	 * @param namespace
	 *            uri of the namespace which prefix is requested
	 * @return prefix specified in the model (last prefix added to the model in
	 *         case there's more than one)
	 */
	public String getNsURIPrefix(String namespace) {
		beginRead();
		Model ontology = this.getModel();
		String prefixByJenaModel = ontology.getNsURIPrefix(namespace);
		commitEnd();
		return prefixByJenaModel;
	}

	private Map<String, String> getPrefixBlock(Model model, Map<String, String> prefixMap) {
		if (model != null) {

			Map<String, String> actualPrefix = model.getNsPrefixMap();
			for (String key : actualPrefix.keySet()) {
				if (!prefixMap.containsKey(key))
					prefixMap.put(key, actualPrefix.get(key));
			}
		}
		return prefixMap;
	}

	public boolean ValidateInferedModel(InfModel inferredModel) {

		return (inferredModel.validate().isValid());
	}

	public void PrintModel(String outputPath, String format, boolean inferred) throws FileNotFoundException {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		java.io.FileOutputStream ostream;
		ostream = new java.io.FileOutputStream(outputPath);
		PrintWriter pw = new PrintWriter(ostream);
		ontology.write(ostream, format);
		pw.close();
		commitEnd();
	}

	// GET DATA - NEW FUNCTIONS

	public Set<Resource> GetInstances(Resource resClass, boolean inference) {
		Set<Resource> instances = new HashSet<Resource>();
		instances.addAll(GetSubjects(resClass, RDF.type, false));
		if (inference) {
			Set<Resource> subclasses = GetSubHierarchy(resClass, true, false);
			for (Resource sub : subclasses) {
				instances.addAll(GetSubjects(sub, RDF.type, false));
			}
		}
		return instances;
	}

	public Set<Resource> GetSuperHierarchy(Resource res, boolean inferred) {

		Set<Resource> results = new HashSet<Resource>();
		for (RDFNode n : GetObjects(res, RDFS.subClassOf, inferred))
			results.add(n.asResource());
		if (results.size() == 0) {
			for (RDFNode n : GetObjects(res, RDFS.subPropertyOf, inferred))
				results.add(n.asResource());
		}
		results.add(res);
		return results;
	}

	private Set<Resource> GetSubHierarchyClosure(Resource res, boolean inferred) {
		Set<Resource> allResults = new HashSet<Resource>();
		Set<Resource> results = GetSubHierarchy(res, inferred, false);
		while (results != null && !results.isEmpty()) {
			Set<Resource> aux = new HashSet<Resource>();
			for (Resource r : results) {
				if (!inList(res, allResults))
					aux.addAll(GetSubHierarchy(r, inferred, false));
			}
			allResults.addAll(results);
			results = aux;
		}
		return allResults;
	}

	// public Set<Resource> GetSubHierarchyNotTransact(Resource res, boolean
	// inferred){
	// Set<Resource> results =
	// GetSubjectsNotTransact(res,RDFS.subClassOf,inferred);
	// if (results != null && results.size() > 0)
	// return results;
	// return this.GetSubjectsNotTransact(res,RDFS.subPropertyOf,inferred);
	// }

	public Set<Resource> GetSubHierarchy(Resource res, boolean inferred, boolean closure) {
		Set<Resource> results = new HashSet<Resource>();
		if (!closure) {
			results = GetSubjects(res, RDFS.subClassOf, inferred);
			if (results == null || results.size() == 0)
				results = GetSubjects(res, RDFS.subPropertyOf, inferred);
			results.add(res);
			return results;
		} else {
			Set<Resource> allResults = GetSubHierarchyClosure(res, inferred);
			return allResults;
		}

	}

	private Resource getResourceNotTransact(String uri, boolean inferred) {
		Model ontology = this.getModel(); // the models should have been updated
											// after the last begin transaction
		if (inferred)
			ontology = this.getInferredModel();
		return ontology.getResource(uri);
	}

	public Resource getResource(String uri, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Resource result = ontology.getResource(uri);
		commitEnd();
		return result;
	}

	public Property getProperty(String uri, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Property result = ontology.getProperty(uri);
		commitEnd();
		return result;
	}

	public Property getPropertyNotTransact(String uri, boolean inferred) {
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Property result = ontology.getProperty(uri);
		return result;
	}

	public boolean contains(Statement st, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		boolean contains = ontology.contains(st);
		commitEnd();
		return contains;
	}

	public Set<Resource> listSubjects(boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<Resource> result = ontology.listSubjects().toSet();
		commitEnd();
		return result;
	}

	public Set<Resource> GetSubjectsNotTransact(Resource object, Property property, boolean inferred) {
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<Resource> results = new HashSet<Resource>();
		for (Iterator<Resource> iterator = ontology.listSubjectsWithProperty(property, object); iterator.hasNext();) {
			Resource resource = iterator.next();
			if (resource.isURIResource())
				results.add(resource);
		}
		return results;
	}

	public Set<Resource> GetSubjects(Resource object, Property property, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<Resource> results = new HashSet<Resource>();
		for (Iterator<Resource> iterator = ontology.listSubjectsWithProperty(property, object); iterator.hasNext();) {
			Resource resource = iterator.next();
			if (resource.isURIResource())
				results.add(resource);
		}
		commitEnd();
		return results;
	}

	public Resource GetSubject(Resource object, Property property, boolean inferred) {
		Set<Resource> res = GetSubjects(object, property, inferred);
		for (Resource r : res)
			return r;
		return null;
	}

	public Resource GetSubjectNotTransact(Resource object, Property property, boolean inferred) {
		Set<Resource> res = GetSubjectsNotTransact(object, property, inferred);
		for (Resource r : res)
			return r;
		return null;
	}

	public RDFNode GetObject(Resource subject, Property property, boolean inferred) {
		Set<RDFNode> res = GetObjects(subject, property, inferred);
		for (RDFNode n : res)
			return n;
		return null;
	}

	public RDFNode GetObjectNotTransact(Resource subject, Property property, boolean inferred) {
		Set<RDFNode> res = GetObjectsNotTransact(subject, property, inferred);
		for (RDFNode n : res)
			return n;
		return null;
	}

	public Set<RDFNode> GetObjects(Resource subject, Property property, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<RDFNode> results = new HashSet<RDFNode>();
		for (Iterator<RDFNode> iterator = ontology.listObjectsOfProperty(subject, property); iterator.hasNext();) {
			RDFNode node = iterator.next();
			if (node.isLiteral() || node.isURIResource())
				results.add(node);
		}
		commitEnd();
		return results;

	}

	public Set<RDFNode> GetObjectsNotTransact(Resource subject, Property property, boolean inferred) {
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<RDFNode> results = new HashSet<RDFNode>();
		for (Iterator<RDFNode> iterator = ontology.listObjectsOfProperty(subject, property); iterator.hasNext();) {
			RDFNode node = iterator.next();
			if (node.isLiteral() || node.isURIResource())
				results.add(node);
		}
		return results;

	}

	// Always with inference
	public boolean PopulatedRangeProp(IOntoBase range, IOntoBase property) {
		Set<Resource> subr = GetSubHierarchy(range.getResource(), true, false);
		Set<String> subrStr = new HashSet<String>();
		for (Resource sc : subr) {
			subrStr.add(sc.getURI());
		}
		Set<Resource> subp = GetSubHierarchy(property.getResource(), true, false);
		this.beginRead();
		Set<Statement> stProp = new HashSet<Statement>();

		for (Resource r : subp) {
			stProp.addAll(GetStatementsNotTransact(null, getPropertyNotTransact(r.getURI(), false), null, false));
		}

		for (Statement st : stProp) {
			RDFNode n = st.getObject();
			if (n.isURIResource()) {
				for (RDFNode cl : GetObjectsNotTransact(n.asResource(), RDF.type, true)) {
					if (cl.isURIResource() && subrStr.contains(cl.asResource().getURI())) {
						this.commitEnd();
						return true;
					}
				}
			}
		}
		this.commitEnd();
		return false;
	}

	// Always with inference
	public boolean PopulatedLink(IOntoBase domain, IOntoBase property, IOntoBase range) {

		// Set<Resource> subr = GetSubHierarchyNotTransact(range.getResource(),
		// true);
		Set<Resource> subr = GetSubHierarchy(range.getResource(), true, false);
		Set<String> subrStr = new HashSet<String>();
		for (Resource sc : subr) {
			subrStr.add(sc.getURI());
		}

		// Set<Resource> subd = GetSubHierarchyNotTransact(domain.getResource(),
		// true);
		Set<Resource> subd = GetSubHierarchy(domain.getResource(), true, false);
		Set<String> subdStr = new HashSet<String>();
		for (Resource sc : subd) {
			subdStr.add(sc.getURI());
		}

		// Set<Resource> subp =
		// GetSubHierarchyNotTransact(property.getResource(), true);
		Set<Resource> subp = GetSubHierarchy(property.getResource(), true, false);
		this.beginRead();
		Set<Statement> stProp = new HashSet<Statement>();
		for (Resource r : subp) {
			stProp.addAll(GetStatementsNotTransact(null, getPropertyNotTransact(r.getURI(), false), null, false));
		}
		for (Statement st : stProp) {
			Resource r = st.getSubject();
			RDFNode n = st.getObject();
			if (n.isURIResource()) {
				for (RDFNode cl : GetObjectsNotTransact(r, RDF.type, true)) {
					if (cl.isURIResource() && subdStr.contains(cl.asResource().getURI())) {
						for (RDFNode cl2 : GetObjectsNotTransact(n.asResource(), RDF.type, true)) {
							if (cl2.isURIResource() && subrStr.contains(cl2.asResource().getURI())) {
								this.commitEnd();
								return true;
							}
						}
					}
				}
			}
		}
		this.commitEnd();
		return false;
	}

	// Always with inference
	public boolean PopulatedDomainProp(IOntoBase domain, IOntoBase property) {

		// Set<Resource> subd = GetSubHierarchyNotTransact(domain.getResource(),
		// true);
		Set<Resource> subd = GetSubHierarchy(domain.getResource(), true, false);
		Set<String> subdStr = new HashSet<String>();
		for (Resource sc : subd) {
			subdStr.add(sc.getURI());
		}

		// Set<Resource> subp =
		// GetSubHierarchyNotTransact(property.getResource(), true);
		Set<Resource> subp = GetSubHierarchy(property.getResource(), true, false);
		this.beginRead();
		Set<Statement> stProp = new HashSet<Statement>();
		for (Resource r : subp) {
			stProp.addAll(GetStatementsNotTransact(null, getPropertyNotTransact(r.getURI(), false), null, false));
		}
		for (Statement st : stProp) {
			Resource r = st.getSubject();
			RDFNode n = st.getObject();
			if (n.isLiteral()) {
				for (RDFNode cl : GetObjectsNotTransact(r, RDF.type, true)) {
					if (cl.isURIResource() && subdStr.contains(cl.asResource().getURI())) {
						this.commitEnd();
						return true;
					}
				}
			}
		}
		this.commitEnd();
		return false;
	}

	// Always with inference
	public boolean PopulatedConcept(IOntoBase concept) {
		// Set<Resource> subd =
		// GetSubHierarchyNotTransact(concept.getResource(), true);
		Set<Resource> subd = GetSubHierarchy(concept.getResource(), true, false);
		this.beginRead();
		for (Resource s : subd) {
			// if (s.getURI().contains("InformationResourceCategory"))
			// concept = concept;
			if (this.GetSubjectNotTransact(s, RDF.type, true) != null) {
				this.commitEnd();
				return true;
			}
		}
		this.commitEnd();
		return false;
	}

	// public Resource getSuperconcept(Resource res1, Resource res2){
	// Property pClass = RDFS.subClassOf;
	// Property pProp = RDFS.subPropertyOf;
	// beginRead();
	// Set<Resource> subclassOfRes1 = this.GetSubjectsNotTransact(res1, pClass,
	// true);
	// Set<Resource> subclassOfRes2 = this.GetSubjectsNotTransact(res2, pClass,
	// true);
	//
	// if (subclassOfRes1.contains(res2)){
	// commitEnd();
	// return res1;
	// }
	// if (subclassOfRes2.contains(res1)){
	// commitEnd();
	// return res2;
	// }
	//
	//
	// Set<Resource> subpropOfRes1 = this.GetSubjectsNotTransact(res1, pProp,
	// true);
	// Set<Resource> subpropOfRes2 = this.GetSubjectsNotTransact(res2, pProp,
	// true);
	// if (subpropOfRes1.contains(res2)){
	// commitEnd();
	// return res1;
	// }
	// if (subpropOfRes2.contains(res1)){
	// commitEnd();
	// return res2;
	// }
	//
	//
	// commitEnd();
	// return null;
	// }

	public Resource getSuperClass(Resource res1, Resource res2) {
		beginRead();
		Property pClass = RDFS.subClassOf;
		Set<Statement> st = this.GetStatementsNotTransact(res1, pClass, res2, true);
		if (!st.isEmpty()) {
			commitEnd();
			return res2;
		}
		st = GetStatementsNotTransact(res2, pClass, res1, true);
		if (!st.isEmpty()) {
			commitEnd();
			return res1;
		}
		commitEnd();
		return null;
	}

	public Resource getSuperProperty(Resource res1, Resource res2) {
		beginRead();
		Property pProp = RDFS.subPropertyOf;
		Set<Statement> st = GetStatementsNotTransact(res1, pProp, res2, true);
		if (!st.isEmpty()) {
			commitEnd();
			return res2;
		}
		st = GetStatementsNotTransact(res2, pProp, res1, true);
		if (!st.isEmpty()) {
			commitEnd();
			return res1;
		}
		commitEnd();
		return null;
	}

	public Resource getSuperconcept(Resource res1, Resource res2) {
		Resource res = null;
		res = getSuperClass(res1, res2);
		if (res == null)
			res = getSuperProperty(res1, res2);
		return res;
	}

	public Set<Statement> GetStatements(Resource subject, Property property, RDFNode object, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<Statement> results = ontology.listStatements(subject, property, object).toSet();
		commitEnd();
		return results;
	}

	public Set<Statement> GetStatementsNotTransact(Resource subject, Property property, RDFNode object, boolean inferred) {
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		Set<Statement> results = ontology.listStatements(subject, property, object).toSet();
		return results;
	}

	// the inference may not work for instances
	public Set<Resource> GetDirectConceptsOfType(Resource res) {
		beginRead();
		Model ontology = this.getModel();
		Resource subject = null;
		Set<Resource> types = new HashSet<Resource>();
		for (Iterator<Statement> itSt = ontology.listStatements(subject, RDF.type, res); itSt.hasNext();) {
			Statement st = itSt.next();
			Resource sub = st.getSubject();
			if (sub.isURIResource())
				types.add(sub.asResource());
		}
		commitEnd();
		return types;
	}

	// TODO: datatypes

	// if asked about an instances, the inference may not work
	public Set<Resource> GetDirectType(Resource res) {
		beginRead();
		RDFNode object = null;
		Model ontology = this.getModel();
		Set<Resource> types = new HashSet<Resource>();
		for (Iterator<Statement> itSt = ontology.listStatements(res, RDF.type, object); itSt.hasNext();) {
			Statement st = itSt.next();
			RDFNode rdfn = st.getObject();
			if (rdfn.isURIResource())
				types.add(rdfn.asResource());
		}
		// Set<Resource> results = new HashSet<Resource>();
		// Model infModel = this.getInferredModel();
		// for (Resource type:types){
		// boolean isSuperclass = false;
		// for (Resource type2:types)
		// if (type != type2){
		// if (infModel.contains(type2, RDFS.subClassOf, type)){
		// isSuperclass = true;
		// break;
		// }
		// }
		// if (isSuperclass)
		// results.add(type);
		// }
		commitEnd();
		return types;
	}

	// public Resource GetType(Resource res){
	// RDFNode object = null;
	// Model ontology = this.getModel(); //ojo si se hace con el inferido porque
	// puede incluir como tipo nodos vacíos
	// for (Iterator<Statement> itSt = ontology.listStatements(res, RDF.type,
	// object); itSt.hasNext();){
	// Statement st =itSt.next();
	// RDFNode rdfn = st.getObject();
	// Resource result = rdfn.asResource();
	// return result;
	// }
	// //System.out.println("Error: Get Type->null. "+res.getURI());
	// return null;
	// }

	// public Set<Resource> GetTypes(Resource res, boolean inferred){
	// Set<Resource> results = new HashSet<Resource>();
	// RDFNode object = null;
	// Model ontology;
	// if (inferred)
	// ontology = this.getInferredModel(); //ojo si se hace con el inferido
	// porque puede incluir como tipo nodos vacíos
	// else
	// ontology = this.getModel();
	// for (Iterator<Statement> itSt = ontology.listStatements(res, RDF.type,
	// object); itSt.hasNext();){
	// Statement st =itSt.next();
	// RDFNode rdfn = st.getObject();
	// if (rdfn.isURIResource())
	// results.add(rdfn.asResource());
	// return results;
	// }
	// //System.out.println("Error: Get Type->null. "+res.getURI());
	// return null;
	// }

	public static boolean inList(Resource res, List<Resource> resList) {
		for (Resource r : resList) {
			if (res.getURI().equals(r.getURI()))
				return true;
		}
		return false;
	}

	public static boolean inList(Resource res, Set<Resource> resList) {
		for (Resource r : resList) {
			if (res.getURI().equals(r.getURI()))
				return true;
		}
		return false;
	}

	// GET DATA - OLD FUNCTIONS
	private ArrayList<String> GetSubClassesNotTransact(String uri, Model ontology) {
		// beginRead();
		Resource oclass = ResourceFactory.createResource(uri);
		if (!ontology.contains(oclass, null)) {
			return null;
		}
		ArrayList<String> results = new ArrayList<String>();
		for (Resource child : ontology.listResourcesWithProperty(RDFS.subClassOf, oclass).toList()) {
			// results.add(child.getLocalName());
			results.add(child.getURI());
		}
		// commitEnd();
		return results;
	}

	public HashMap<String, ArrayList<String>> GetClassHierarchy(String uri, Model ontology) {
		beginRead();
		ArrayList<String> uriList = new ArrayList<String>();
		HashMap<String, ArrayList<String>> results = new HashMap<String, ArrayList<String>>();

		uriList.add(uri);
		while (uriList != null && uriList.size() > 0) {
			int lastIndex = uriList.size() - 1;
			String actualUri = uriList.get(lastIndex);
			uriList.remove(lastIndex);
			ArrayList<String> childs = GetSubClassesNotTransact(actualUri, ontology);
			results.put(actualUri, childs);
			uriList.addAll(childs);
		}
		commitEnd();
		return results;

	}

	public String GetClass(Resource res, boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		if (res.isURIResource()) {
			try {
				RDFNode rdfn = null;
				for (Iterator<Statement> iteratorSt = ontology.listStatements(res, RDF.type, rdfn); iteratorSt
						.hasNext();) {
					Statement st = iteratorSt.next();
					String uriclass = st.getObject().asResource().getURI();
					if (!uriclass.equals("http://www.w3.org/2000/01/rdf-schema#Resource")) {
						commitEnd();
						return uriclass;
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		commitEnd();
		return null;
	}

	public String GetClass(String uri, boolean inferred) {
		Resource res = this.getResource(uri, inferred);
		return GetClass(res, inferred);
	}

	public List<Resource> GetURIs(boolean inferred) {
		beginRead();
		Model ontology = this.getModel();
		if (inferred)
			ontology = this.getInferredModel();
		ArrayList<String> uriList = new ArrayList<String>();
		List<Resource> results = ontology.listSubjects().toList();
		// for (Iterator<Resource> itRes = ontology.listSubjects();
		// itRes.hasNext();){
		// uriList.add(itRes.next().getURI());
		// }
		commitEnd();
		return results;
	}

	public String GetLabel(String uri, boolean inferred) {
		Resource res = getResource(uri, inferred);
		return GetLabel(res);
	}

	private String GetLabelNotTransact(String uri, boolean inferred) {
		Resource res = getResourceNotTransact(uri, inferred);
		return GetLabelNotTransact(res);
	}

	// public String GetLabel(String uri){
	// beginRead();
	// Resource res = this.getModel().getResource(uri);
	// commitEnd();
	// return GetLabel(res);
	// }

	// private String GetLabelNotTransact(String uri){
	// Resource res = this.getModel().getResource(uri);
	// return GetLabel(res);
	// }

	public QueryResults GetRelatedClasses(String uriProp, String uriClass, String uriUrbanConcepts) {
		beginRead();
		Model inferred = this.getInferredModel();
		Resource resclass = inferred.getResource(uriClass);
		Property prop = inferred.getProperty(uriProp);
		ArrayList<Property> listprop = new ArrayList<Property>();
		Resource resurban = inferred.getResource(uriUrbanConcepts);
		for (Iterator<Resource> iteratorNode = inferred
				.listResourcesWithProperty(RDFS.subPropertyOf, prop.asResource()); iteratorNode.hasNext();) {
			Resource subproperty = iteratorNode.next();
			String subpuri = subproperty.getURI();
			if (!listprop.contains(subpuri))
				listprop.add(inferred.getProperty(subproperty.getURI()));
		}
		Model data = this.getModel();
		HashMap<String, ArrayList<String>> classprop = new HashMap<String, ArrayList<String>>();
		for (Iterator<Resource> iteratorNode = data.listResourcesWithProperty(RDF.type, resclass); iteratorNode
				.hasNext();) {
			// Resource next = iteratorNode.next();
			// if (next.isURIResource()){
			Resource ind = iteratorNode.next();
			RDFNode node = null;
			for (Property p : listprop) {
				for (Iterator<Statement> itSt = data.listStatements(ind, p, node); itSt.hasNext();) {
					Statement st = itSt.next();
					if (st.getObject().isURIResource()) {
						Resource relatedInd = st.getObject().asResource();
						for (Iterator<RDFNode> itClass = data.listObjectsOfProperty(relatedInd, RDF.type); itClass
								.hasNext();) {
							RDFNode relclass = itClass.next();
							// if (relclass.isURIResource() &&
							// inferred.listStatements(relclass.asResource(),
							// RDFS.subClassOf, resurban).hasNext()){
							if (relclass.isURIResource()) {
								String urirel = relclass.asResource().getURI();

								ArrayList<String> relproperties;
								if (classprop.containsKey(urirel))
									relproperties = classprop.get(urirel);
								else {
									relproperties = new ArrayList<String>();
									classprop.put(urirel, relproperties);
								}
								String puri = p.getURI();
								if (!relproperties.contains(puri))
									relproperties.add(puri);
							}
						}

					}
				}
				// }
			}
		}
		ArrayList<String> vars = new ArrayList<String>();
		vars.add("relclass");
		vars.add("relprop");
		ArrayList<HashMap<String, String>> qrData = new ArrayList<HashMap<String, String>>();
		for (String cl : classprop.keySet()) {
			for (String pr : classprop.get(cl)) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("relclass", cl);
				map.put("relprop", pr);
				qrData.add(map);
			}
		}
		commitEnd();
		return new QueryResults(vars, qrData);
	}

	public QueryResults GetInstanceProperties(List<Resource> instances, boolean objectProperties) {
		beginRead();
		Model data = this.getModel();
		ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();
		ArrayList<String> vars = new ArrayList<String>();
		vars.add("f1");
		vars.add("property");
		vars.add("info");
		if (objectProperties) {
			vars.add("p2");
			vars.add("info2");
		}
		Property pgeo = data.getProperty("http://www.opengis.net/ont/geosparql#asWKT");
		Property p = null;
		RDFNode node = null;
		for (Resource r : instances) {
			for (Iterator<Statement> itSt = data.listStatements(r, p, node); itSt.hasNext();) {
				Statement st = itSt.next();
				RDFNode object = st.getObject();
				Property pred = st.getPredicate();
				if (objectProperties && object.isURIResource()) {
					for (Iterator<Statement> itSt2 = data.listStatements(object.asResource(), pgeo, node); itSt2
							.hasNext();) {
						HashMap<String, String> row = new HashMap<String, String>();
						row.put("f1", r.toString());
						Statement st2 = itSt2.next();
						Property pred2 = st2.getPredicate();
						RDFNode object2 = st2.getObject();
						row.put("property", pred.toString());
						row.put("info", object.toString());
						row.put("p2", pred2.toString());
						// String test = "";
						// if (pred2.getLocalName().equals("displayName"))
						// test = "a";
						row.put("info2", object2.toString());
						rows.add(row);
					}
				} else {
					HashMap<String, String> row = new HashMap<String, String>();
					row.put("f1", r.toString());
					row.put("property", pred.toString());
					row.put("info", object.toString());
					rows.add(row);
				}
			}
		}
		commitEnd();
		return (new QueryResults(vars, rows));
	}

	public QueryResults GetInstanceProperties(String uriclass, boolean objectProperties) {
		beginRead();
		ArrayList<Resource> instances = new ArrayList<Resource>();
		Model data = this.getModel();
		Resource classRes = data.getResource(uriclass);
		Resource inst = null;
		for (Iterator<Statement> itSt = data.listStatements(inst, RDF.type, classRes); itSt.hasNext();) {
			Statement st = itSt.next();
			Resource subject = st.getSubject();
			instances.add(subject);
		}
		commitEnd();
		return GetInstanceProperties(instances, objectProperties);
	}

	public QueryResults GetIndDescriptorMeasurements(String uriDescriptor, String relatedMeasurements,
			String relatedValues) {
		beginRead();
		ArrayList<String> vars = new ArrayList<String>();
		vars.add("descriptor");
		vars.add("measurement");
		vars.add("measurement_value");
		ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();

		Model base = this.getModel();
		Resource descriptor = base.getResource(uriDescriptor);
		Property pRelMeas = base.getProperty(relatedMeasurements);
		Property pRelValues = base.getProperty(relatedValues);
		RDFNode object = null;
		for (Iterator<Statement> itSt = base.listStatements(descriptor, pRelMeas, object); itSt.hasNext();) {
			HashMap<String, String> row = new HashMap<String, String>();
			Statement st = itSt.next();
			row.put("descriptor", uriDescriptor);
			row.put("measurement", st.getObject().asResource().getURI());
			Statement staux = st.getObject().asResource().getProperty(pRelValues);
			String value = "";
			if (staux != null)
				value = staux.getObject().asLiteral().getValue().toString();
			row.put("measurement_value", value);
			rows.add(row);
		}
		commitEnd();
		return (new QueryResults(vars, rows));
	}

	public QueryResults GetIndDescriptorClassesAffected(String uriDescriptor, String relatedClasses) {
		beginRead();
		ArrayList<String> vars = new ArrayList<String>();
		vars.add("descriptor");
		vars.add("relatedClass");
		ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();

		Model base = this.getModel();
		Resource descriptor = base.getResource(uriDescriptor);
		Property pRelClass = base.getProperty(relatedClasses);
		RDFNode object = null;
		for (Iterator<Statement> itSt = base.listStatements(descriptor, pRelClass, object); itSt.hasNext();) {
			HashMap<String, String> row = new HashMap<String, String>();
			Statement st = itSt.next();
			row.put("descriptor", uriDescriptor);
			row.put("relatedClass", st.getObject().asResource().getURI());
			rows.add(row);
		}
		commitEnd();
		return (new QueryResults(vars, rows));
	}

	public String GetIndDescriptorComment(String uriDescriptor) {
		beginRead();
		Model base = this.getModel();
		Resource descriptor = base.getResource(uriDescriptor);
		Property pComment = RDFS.comment;
		Statement st = base.getProperty(descriptor, pComment);
		if (st != null) {
			commitEnd();
			return st.getObject().asLiteral().getString();
		}
		commitEnd();
		return "";
	}

	public QueryResults GetIndicators(String hasDescriptor, String hasMeasurement) {
		beginRead();
		ArrayList<String> vars = new ArrayList<String>();
		vars.add("indicator");
		vars.add("descriptor");
		vars.add("measure");
		ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();

		Model indModel = this.getModel();
		Property pDesc = indModel.getProperty(hasDescriptor);
		Property pMeas = indModel.getProperty(hasMeasurement);
		Resource subject = null;
		RDFNode object = null;
		for (Iterator<Statement> itSt = indModel.listStatements(subject, pDesc, object); itSt.hasNext();) {
			Statement st = itSt.next();
			RDFNode measure = null;
			for (Iterator<Statement> itSt2 = indModel.listStatements(st.getSubject(), pMeas, measure); itSt2.hasNext();) {
				HashMap<String, String> row = new HashMap<String, String>();
				Statement st2 = itSt2.next();
				row.put("indicator", st.getSubject().getURI());
				row.put("descriptor", st.getObject().asResource().getURI());
				row.put("measure", st2.getObject().asLiteral().getValue().toString());
				rows.add(row);
			}

		}
		commitEnd();
		return (new QueryResults(vars, rows));
	}

	// public String GetComment(Resource res){
	// RDFNode object = null;
	// for (Iterator<Statement> itSt = this.getBaseModel().listStatements(res,
	// RDFS.comment, object); itSt.hasNext();){
	// return itSt.next().getObject().asLiteral().getValue().toString();
	// }
	// return null;
	// }

	public String GetLabelNotTransact(Resource res) {
		String esLabel = null;
		String defaultLabel = null;

		RDFNode object = null;
		for (Iterator<Statement> itSt = this.getInferredModel().listStatements(res, RDFS.label, object); itSt.hasNext();) {
			Statement st = itSt.next();
			Literal n = st.getObject().asLiteral();
			// Literal n = ((RDFNode) iteratorNode.next()).asLiteral();
			String lang = n.getLanguage();
			String value = n.getLexicalForm();
			if (lang == null || lang.equals("")) {
				defaultLabel = value;
			} else if (lang.equalsIgnoreCase("en"))
				return value;
			else if (lang.equalsIgnoreCase("es"))
				esLabel = value;
		}
		if (defaultLabel != null)
			return defaultLabel;
		return esLabel;
	}

	public String GetLabel(Resource res) {
		beginRead();
		String result = GetLabelNotTransact(res);
		commitEnd();
		return result;
	}

	public String GetComment(Resource res) {
		beginRead();
		String result = GetCommentNotTransact(res);
		commitEnd();
		return result;

	}

	public String GetCommentNotTransact(Resource res) {
		RDFNode comment = null;
		for (Iterator<Statement> itSt2 = this.getInferredModel().listStatements(res, RDFS.comment, comment); itSt2
				.hasNext();) {
			Statement st2 = itSt2.next();
			if ("en".equals(st2.getObject().asLiteral().getLanguage())) {
				return st2.getObject().asLiteral().toString();
			}
		}
		return null;
	}

	//
	// private String getLabel
	// (Resource res){
	// RDFNode label = null;
	// for (Iterator<Statement> itSt2 = indModel.listStatements(res, RDFS.label
	// , label); itSt2.hasNext();){
	// Statement st2 = itSt2.next();
	// String language = st2.getObject().asLiteral().getLanguage();
	// if (language == "" || "en".equals(language)){
	// return st2.getObject().asLiteral().getValue().toString();
	// }
	// }
	// return "";
	// }

	// SPARQL QUERIES

	public String Query_GetMatchProperties(String inputProperty, String uriUrbanConcepts) {
		return ("SELECT DISTINCT ?property ?label ?domain_urban ?range_urban WHERE {?property rdf:type rdf:Property . ?property rdfs:label ?label . OPTIONAL {?property rdfs:domain ?domain . ?domain_urban rdfs:subClassOf ?domain . ?domain_urban rdfs:subClassOf <"
				+ uriUrbanConcepts
				+ "> } . OPTIONAL {?property rdfs:range ?range . ?range_urban rdfs:subClassOf ?range . ?range_urban rdfs:subClassOf <"
				+ uriUrbanConcepts + ">} . FILTER (REGEX (STR (?label), \"" + inputProperty + "\",\"i\"))}");
		// return("SELECT DISTINCT ?property ?label ?domain ?range WHERE {?property rdf:type rdf:Property . ?property rdfs:label ?label . OPTIONAL {?property rdfs:domain ?domain} . OPTIONAL {?property rdfs:range ?range} . FILTER (REGEX (STR (?label), \""+inputProperty+"\",\"i\"))}");

	}

	public String Query_GetMatchConcept(String keyword) {
		return ("SELECT DISTINCT ?concept ?type WHERE {?concept rdf:type ?type . ?concept rdfs:label ?label . FILTER (REGEX (STR (?label),\""
				+ keyword + "\",\"i\"))}");
	}

	// DEBUG

	class CancelQuery implements Runnable {
		long initialTime;
		UrbanOntology uOntology;
		int maxTime;
		String idSession;

		public CancelQuery(UrbanOntology uOntology, int maxTime, String idSession) {
			this.uOntology = uOntology;
			this.maxTime = maxTime;
			this.idSession = idSession;
			this.initialTime = System.currentTimeMillis();
		}

		@Override
		public void run() {
			try {
				long elapsed;
				do {
					elapsed = System.currentTimeMillis() - initialTime;
				} while (elapsed < maxTime);
				uOntology.CancelQuery(this.idSession);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public Object[] ResultSetQueryOntologyDebug(String sparqlQuery, boolean inferred, int maxTime,
			UrbanOntology uOntology, String idSession) throws IOException {
		Object[] result = { -1, (long) -1, (long) -1 };

		Model ontology;
		if (inferred) {
			TDBFactory.release(this.dbInfModel);
			this.dbInfModel = TDBFactory.createDataset(new Location(this.tdbInfModelLocation));
			ontology = this.getInferredModel();
		} else {
			TDBFactory.release(this.dbModel);
			this.dbModel = TDBFactory.createDataset(new Location(this.tdbModelLocation));
			ontology = this.getModel();
		}

		beginRead();
		int nresults = 0;
		long time, cpu;
		ThreadMXBean thbean = ManagementFactory.getThreadMXBean();
		if (!thbean.isCurrentThreadCpuTimeSupported())
			thbean.setThreadCpuTimeEnabled(true);

		// Model ontology = ds.getDefaultModel();

		// QueryTime qt = new QueryTime(sparqlQuery, ontology);
		// Thread thquery = new Thread(qt);
		// CheckTime chkt = new CheckTime(thquery);
		// Thread thcheck = new Thread(chkt);

		long cpu1 = thbean.getCurrentThreadCpuTime();
		long t1 = System.currentTimeMillis();
		this.CancelActiveQuery(idSession);
		try {
			// QueryExecution qexec;

			Query q = QueryFactory.create(sparqlQuery);

			// Op op = Algebra.compile(q);
			// System.out.println(op.toString());
			// Query q
			// =QueryFactory.create("SELECT distinct ?a WHERE {?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?b}");
			if (maxTime != Integer.MAX_VALUE) {
				CancelQuery cquery = new CancelQuery(uOntology, maxTime, idSession);
				Thread th = new Thread(cquery);
				th.start();

			}
			QueryExecution qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
			NewQuery(qexec, idSession);
			activeQuery.put(idSession, qexec);
			ResultSet results = qexec.execSelect();

			// FileWriter fw = new FileWriter("testResults.txt");
			// QueryResults qr = new QueryResults(results);
			// for (HashMap<String,String> row: qr.getResults()){
			// for (String var:row.keySet()){
			// fw.write(var+":"+row.get(var)+" ");
			// }
			// fw.write("\n");
			// }
			// fw.close();
			while (results.hasNext()) {
				results.next();
				nresults++;
			}

			long t2 = System.currentTimeMillis();
			long cpu2 = thbean.getCurrentThreadCpuTime();
			time = t2 - t1;
			cpu = (Math.abs(cpu2 - cpu1)) / 1000000;
			qexec.close();
		} catch (Exception e) {
			time = (long) -1;
			cpu = (long) -1;
			nresults = -1;
		}
		// thquery.run();
		// thcheck.run();
		// while (thquery.isAlive());
		// if (thcheck.isAlive())
		// thcheck.interrupt();
		// long t2 = System.currentTimeMillis();
		// long cpu2 = thbean.getCurrentThreadCpuTime();
		//
		// // if (qt.interrumpted){
		// // time = (long)-1;
		// // cpu = (long)-1;
		// // }else{
		// time = t2-t1;
		// cpu = (Math.abs(cpu2-cpu1)) / 1000000;
		// }
		// nresults = qt.nresults;

		System.out.println(nresults);
		// System.out.println("Total results:"+nresults);
		// System.out.println("Time (millis):"+time);
		// System.out.println("Cpu (millis):"+cpu);
		result[0] = (Integer) nresults;
		result[1] = (Long) time;
		result[2] = (Long) cpu;

		// QueryResults qr = new QueryResults(results);

		commitEnd();
		return result;
	}

	class CheckTime implements Runnable {
		long initialTime;
		Thread thquery;

		public CheckTime(Thread thquery) {
			this.thquery = thquery;
			this.initialTime = System.currentTimeMillis();
		}

		@Override
		public void run() {
			try {
				long elapsed;
				do {
					elapsed = System.currentTimeMillis() - initialTime;
				} while (elapsed < 5000);
				if (thquery.isAlive()) {
					thquery.interrupt();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public class QueryTime implements Runnable {

		Model ontology;
		String query;
		int nresults = 0;
		boolean interrumpted = false;
		long endTime;

		public QueryTime(String query, Model ontology) {
			this.query = query;
			this.ontology = ontology;
		}

		public void run() {
			try {
				QueryExecution qexec;
				Query q = QueryFactory.create(query);
				// Op op = Algebra.compile(q);
				// System.out.println(op.toString());
				// Query q
				// =QueryFactory.create("SELECT distinct ?a WHERE {?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?b}");
				qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
				ResultSet results = qexec.execSelect();

				while (results.hasNext()) {
					results.next();
					nresults++;
				}
				qexec.close();
				endTime = System.currentTimeMillis();
			} catch (Exception e) {
				interrumpted = true;
				e.printStackTrace();
			}

		}

	}

}
