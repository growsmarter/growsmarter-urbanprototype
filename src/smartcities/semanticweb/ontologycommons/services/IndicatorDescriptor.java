package smartcities.semanticweb.ontologycommons.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;

class IndicatorDescriptor extends OntoConcept implements	IIndicatorDescriptor {
//	private List<IOntoBase> entitiesAssoc = new ArrayList<IOntoBase>();
//	private List<String> minValue = new ArrayList<String>();
//	private List<String> bestValue =new ArrayList<String>();
//	private IIndicatorType indType=null;

	public IndicatorDescriptor(Resource resource, UrbanOntology uOntology,
			String label, OntType type) {
		super(resource, uOntology, label, type);
	}

	public IndicatorDescriptor(IOntoBase ob, UrbanOntology uOntology) {
		super(ob, uOntology);
	}
	
	static public Set<IIndicatorDescriptor> GetIndicatorDescriptors(UrbanOntology uOntology){
		String indClassPrefix = UrbanOntology.core;
		String indClassLocalName = "IndicatorType";
		Resource indTypeClass = uOntology.getJenaModel().getResource(indClassPrefix+"#"+indClassLocalName, false);
		Set<Resource> indicators = new HashSet<Resource>();  
		for (Resource res: uOntology.getJenaModel().GetSubjects(indTypeClass,RDFS.subClassOf, false)){
			indicators.addAll(uOntology.getJenaModel().GetSubHierarchy(res, true, false));
		}
		Set<IIndicatorDescriptor> resultingInd = new HashSet<IIndicatorDescriptor>();
		for (Resource resInd: indicators){
			for (Resource indDesc:uOntology.getJenaModel().GetSubjects(resInd,RDF.type, false)){
				IIndicatorDescriptor oc = new IndicatorDescriptor(new OntoConcept(indDesc,uOntology),uOntology);
				resultingInd.add(oc);
			}
		}
		return resultingInd;
	}
	
	@Override
	public IIndicatorType getIndicatorType() {
		Resource descriptor  = this.getResource();
		List<Resource> types = new ArrayList<Resource>(uOntology.getJenaModel().GetDirectType(descriptor));
		if (types.size() == 1)
			return (new IndicatorType(new OntoConcept(types.get(0),uOntology),uOntology));
		return null;
	}
	
	@Override
	public List<IOntoBase> getEntitiesAssoc() {
		Resource descriptor  = this.getResource();
		Property entAssoc = uOntology.getJenaModel().getProperty(UrbanOntology.core +"#indicatorAssociatedToEntity",false);
		Set<RDFNode> entities = uOntology.getJenaModel().GetObjects(descriptor, entAssoc, false);
		List<IOntoBase> listEntity = new ArrayList<IOntoBase>();
		for (RDFNode ent: entities){
			IOntoBase obEntity = new OntoConcept(ent.asResource(),uOntology);
			OntoConcept ocEntity = new OntoConcept(obEntity,uOntology);
			listEntity.add(ocEntity);
		}
		return listEntity;

	}
	
	@Override
	public List<String> getBestValues() {
		List<String> values = new ArrayList<String>();
		Resource descriptor  = this.getResource();
		Property measProperty = uOntology.getJenaModel().getProperty(UrbanOntology.core +"#hasMeasurementDescriptor",false);
		Set<RDFNode> measurements = uOntology.getJenaModel().GetObjects(descriptor, measProperty, false);
		Property ivalue =  uOntology.getJenaModel().getProperty(UrbanOntology.core+"#indicatorValueConstraint",false);
		for (RDFNode meas: measurements){
			if (uOntology.getJenaModel().GetClass(meas.asResource(),false).equals(UrbanOntology.core+"#OptimalValue")){
				Set<RDFNode> objects = uOntology.getJenaModel().GetObjects(meas.asResource(), ivalue, false);
				for (RDFNode value: objects){
					values.add(value.asLiteral().getValue().toString());
				}
			}
		}
		return values;
	}
	
	@Override
	public List<String> getMinValues() {
		List<String> values = new ArrayList<String>();
		Resource descriptor  = this.getResource();
		Property measProperty = uOntology.getJenaModel().getProperty(UrbanOntology.core +"#hasMeasurementDescriptor",false);
		Set<RDFNode> measurements = uOntology.getJenaModel().GetObjects(descriptor, measProperty, false);
		Property ivalue =  uOntology.getJenaModel().getProperty(UrbanOntology.core+"#indicatorValueConstraint",false);
		for (RDFNode meas: measurements){
			if (uOntology.getJenaModel().GetClass(meas.asResource(),false).equals(UrbanOntology.core+"#DesiredMinValue")){
				Set<RDFNode> objects = uOntology.getJenaModel().GetObjects(meas.asResource(), ivalue, false);
				
				for (RDFNode value: objects){
					values.add(value.asLiteral().getValue().toString());
				}
			}
		}
		return values;
	}


}
