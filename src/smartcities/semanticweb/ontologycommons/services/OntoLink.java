package smartcities.semanticweb.ontologycommons.services;

import java.util.List;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoRelationship;


class OntoLink implements IOntoLink {
	private UrbanOntology uOntology;
	private IOntoBase domain;
	
	private IOntoBase range;
	
	private IOntoRelationship relationship;

//	public OntoLink(IOntoProperty property, IOntoBase domain, IOntoBase range){
//		this.domain = domain;
//		this.range = range;
//		this.relationship = property;
//	}
//	
//	public OntoLink(IOntoFunction function, IOntoBase domain, IOntoBase range){
//		this.domain = domain;
//		this.range = range;
//		this.relationship = function;
//	}
	
	
	public OntoLink(IOntoRelationship p, IOntoBase domain, IOntoBase range, UrbanOntology uOntology){
		this.uOntology = uOntology;
		this.domain = domain;
		this.range = range;
		this.relationship = p;
	}

	
	//domain or range could be IOntoArea
	public OntoLink(IOntoFunction geof, IOntoBase domain,  IOntoBase range, UrbanOntology uOntology){
		if (domain.isGeoConcept() || range.isGeoConcept()){
			this.uOntology = uOntology;
			this.domain = domain;
			this.range = range;
			this.relationship = geof;
		}
			//new OntoLink((IOntoRelationship) geof,domain, range, uOntology);
	}

	
//	public OntoLink (IOntoBase domain, IOntoRelationship relationship, IOntoBase range,  UrbanOntology uOntology){
//		if (relationship.isFunction())
//			new OntoLink (domain, (IOntoFunction) relationship, range,uOntology);
//		if (relationship.isProperty())
//			new OntoLink(domain, (IOntoProperty) relationship, range,uOntology);
//	}

	
	public OntoLink(IOntoFunction.OntFunction ftype, List<String> fargs, IOntoBase domain, IOntoBase range, UrbanOntology uOntology) throws Exception{
		if (domain.isGeoConcept() || range.isGeoConcept()){
			IOntoFunction f = new OntoFunction(new OntoConcept(OntoFunction.getFunctionType(ftype),uOntology), fargs,uOntology);
			this.uOntology = uOntology;
			this.domain = domain;
			this.range = range;
			this.relationship = f;

				//new OntoLink(f,domain, range,uOntology);
		}
	}
	


	@Override
	public IOntoRelationship getRelationship() {
		return this.relationship;
	}

	@Override
	public IOntoBase getDomain() {
		return this.domain;
	}

	@Override
	public IOntoBase getRange() {
		return this.range;
	}
	
	
	

}
