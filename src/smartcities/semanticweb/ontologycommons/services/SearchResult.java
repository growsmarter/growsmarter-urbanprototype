package smartcities.semanticweb.ontologycommons.services;

import java.util.HashSet;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.impl.StatementImpl;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;

class SearchResult implements ISearchResult{
	private UrbanOntology uOntology;
	private double similarity;
	private IOntoBase oc;
	private Set<IOntoBase> category;
	private String varSearchSource;

	public IOntoBase getOntoConcept() {
		return oc;
	}
	
	public Set<IOntoBase> getCategory(){
		return category;
	}

	public SearchResult(IOntoBase oc, double similarity, UrbanOntology uOntology){
		this.oc = oc;
		this.uOntology = uOntology;
		setSimilarity(similarity);
		setCategory();
	}
	//Gets search engine similarity of a concept
	public Double getSimilarity(){
		return this.similarity;
	}
	
	public void setSimilarity(double similarity){
		this.similarity = similarity;
	}
	
	private void setCategory(){
		Set <IOntoBase> results = new HashSet<IOntoBase>();
		Set<Resource> allCategories = new HashSet<Resource>();
		for (Resource r: uOntology.getJenaModel().GetSubjects(uOntology.getJenaModel().getResource(UrbanOntology.PLANNING_PERSPECTIVE, false), RDFS.subClassOf, false)){
			if (!r.getURI().equals(UrbanOntology.PLANNING_PERSPECTIVE)){
				allCategories.add(r);
				if (r.getURI().equals(UrbanOntology.CONCEPT_DIMENSION))
					allCategories.addAll(uOntology.getJenaModel().GetSubHierarchy(r, false, false));
			}
		}
		for (Resource s: allCategories){
			if (uOntology.getJenaModel().contains(new StatementImpl(this.getOntoConcept().getResource(), RDFS.subClassOf, s),true))
				results.add(new OntoConcept(s,uOntology));
		}
		this.category = results;
	}
	
	public String getSearchSource(){
		return this.varSearchSource;
	}
	
	public void setSearchSource(String varSearchSource){
		this.varSearchSource = varSearchSource;
	}


	
}
