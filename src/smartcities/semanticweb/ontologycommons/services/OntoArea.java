package smartcities.semanticweb.ontologycommons.services;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.TypeMapper;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea.OntArea;

class OntoArea extends OntoConcept implements IOntoArea {
	private String wkt=null;
	private OntArea atype;


	public void setWkt(String wkt) {
		this.wkt = wkt;
	}

	//if ob is not an area then the method returns null
	public OntoArea(IOntoBase ob, UrbanOntology uOntology) {
		super(ob, uOntology);
//		if (ob.getType() != IOntoBase.OntType.DINDIVIDUAL && !ob.isGeoConcept())
//			return null;
		//OntoConcept oc = new OntoConcept(ob,uOntology);
		//Model ontology = this.getJenaModel().getModel();
		//Dataset ds = this.getJenaModel().getDbModel();
		Property property = uOntology.getJenaModel().getProperty(IOntoArea.GEOMETRY_PROPERTY_URI,false);
		RDFNode geometry = uOntology.getJenaModel().GetObject(ob.getResource(), property, false);
		//RDFNode geometry = this.getJenaModel().GetObject(oc.getResource(), ontology.getProperty(IOntoArea.GEOMETRY_PROPERTY_URI), ontology);
		RDFNode wkt = null;
		if (geometry != null){
			wkt = uOntology.getJenaModel().GetObject(geometry.asResource(), uOntology.getJenaModel().getProperty(IOntoArea.WKT_PROPERTY_URI,false), false);
		} else {
			geometry = uOntology.getJenaModel().GetObject(ob.getResource(), RDF.type, false);
			wkt = uOntology.getJenaModel().GetObject(ob.getResource(), uOntology.getJenaModel().getProperty(IOntoArea.WKT_PROPERTY_URI,false), false);
		}
//		if (geometry== null || wkt ==null)
//			return null;
		setWkt(wkt.asLiteral().getLexicalForm());
		setAreaType(geometry.asResource().getURI());
	}
	
	
	public static IOntoArea CreateArea(IOntoArea.OntArea atype, String wkt, String label, UrbanOntology uOntology) {
		//super(result, uOntology);
		//System.out.println("Entering CreateArea");
		String uriArea = getAreaType(atype);
		OntoConcept ocarea = (OntoConcept) new OntoConcept(uriArea,uOntology);
		RDFDatatype dtype = TypeMapper.getInstance().getSafeTypeByName(IOntoArea.WKT_DATATYPE_URI);
		//ocarea.getResource().addLiteral(ResourceFactory.createProperty(IOntoArea.WKT_PROPERTY_URI), ResourceFactory.createTypedLiteral(wkt, dtype));
		Resource newInst = uOntology.getJenaModel().newInstance(UrbanOntology.bcninst+"#temporal"+UrbanOntology.tempInstCounter++, ocarea.getResource());
		Property p = uOntology.getJenaModel().getProperty(IOntoArea.WKT_PROPERTY_URI, false);
		Literal lit = ResourceFactory.createTypedLiteral("<http://www.opengis.net/def/crs/EPSG/0/3049> "+wkt, dtype);
		uOntology.getJenaModel().addNewStatement(newInst, p, lit, false);
		uOntology.getJenaModel().addNewStatement(newInst, p, lit, true);
		uOntology.getJenaModel().addNewStatement(newInst, RDFS.label, ResourceFactory.createPlainLiteral(label), false);
		uOntology.getJenaModel().addNewStatement(newInst, RDFS.label, ResourceFactory.createPlainLiteral(label), true);
		IOntoArea result = new OntoArea(new OntoConcept(newInst,uOntology),uOntology);
		return result;
	}


	@Override
	public OntArea getAreaType() {
		return this.atype;
	}
	
	@Override
	public String getWKT() {
		return this.wkt;
	}
		
	static public String getAreaType(IOntoArea.OntArea atype){
		switch(atype){
		case POLYGON:return IOntoArea.POLYGON_URI;
		case POINT: return IOntoArea.POINT_URI;
		case LINESTRING: return IOntoArea.LINESTRING_URI;
		case GEOMETRY: return IOntoArea.GEOMETRY_URI;
		case OTHER: return null;
		}
		return null;
	}
	
	public void setAreaType(String uri){
		if (uri.equals(POLYGON_URI))
			this.atype = OntArea.POLYGON;
		else if (uri.equals(GEOMETRY_URI))
			this.atype = OntArea.GEOMETRY;
		else if (uri.equals(POINT_URI))
			this.atype = OntArea.POINT;
		else if (uri.equals(LINESTRING_URI))
			this.atype = OntArea.LINESTRING;
		else
			this.atype = OntArea.OTHER;

	}


}
