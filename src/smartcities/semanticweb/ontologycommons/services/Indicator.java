package smartcities.semanticweb.ontologycommons.services;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.TypeMapper;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.XSD;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;

class Indicator extends OntoConcept implements IIndicator {
	private IIndicatorDescriptor indDescriptor;
	private IOntoBase indArea;
	private float indValue;

		
	public Indicator(Resource resource, UrbanOntology uOntology, String label,
			OntType type) {
		super(resource, uOntology, label, type);
		addIndicator();
	}
	
	public Indicator(IOntoBase ob, UrbanOntology uOntology) {
		super(ob, uOntology);
		addIndicator();
	}
	
	static public List<IIndicator> GetIndicators(UrbanOntology uOntology){
		List<IIndicator> results = new ArrayList<IIndicator>();
		JenaOntology jena = uOntology.getJenaModel();
		//Model indOnt = jena.getModel();
		Set<Resource> indList = jena.GetDirectConceptsOfType(jena.getResource(UrbanOntology.core+"#Indicator",false));
		for (Resource ind:indList){
			OntoConcept oc = (OntoConcept) new OntoConcept(ind.getURI(),uOntology);
			//addIndicator(oc);
			results.add(new Indicator(oc,uOntology));
		}
		return results;
	}
	
	private void addIndicator(){
		RDFNode area = uOntology.getJenaModel().GetObject(this.getResource(), uOntology.getJenaModel().getProperty(UrbanOntology.core+"#IndicatorOf", false) , false);
		if (area != null)
			this.indArea = new OntoConcept(area.asResource(),uOntology);
		RDFNode descriptor = uOntology.getJenaModel().GetObject(this.getResource(), uOntology.getJenaModel().getProperty(UrbanOntology.core+"#hasIndicatorDescriptor",false) , false);
		if (descriptor != null)
			this.indDescriptor= new IndicatorDescriptor(new OntoConcept(descriptor.asResource(),uOntology),uOntology);
		RDFNode value = uOntology.getJenaModel().GetObject(this.getResource(), uOntology.getJenaModel().getProperty(UrbanOntology.core+"#measurementFloatValue",false) ,false);
		if (value != null)
			this.indValue =Float.parseFloat(value.asLiteral().getValue().toString());
	}

	
	static public IIndicator CreateIndicator(IIndicator.ANCHOR_AREAS anchorArea, float value, IIndicatorDescriptor indDesc, String areaIdentifier, UrbanOntology uOntology){
		//areaInst prop areaIdentifier . areaInst type areaTypeURI
		
		IOntoBase areaType = new OntoConcept(getAnchorArea(anchorArea),uOntology,"",IOntoBase.OntType.CLASS);
		//Resource indDescriptor = uOntology.getJenaModel().getResource(indTypeURI, false);
		Resource areaInst = null;
		
		if (areaIdentifier != null){
			//Literal idArea = ResourceFactory.createPlainLiteral(areaIdentifier);
			for(Resource instanceArea: uOntology.getJenaModel().GetSubjects(areaType.getResource(), RDF.type, false)){
				IOntoArea obArea = new OntoArea(new OntoConcept(instanceArea,uOntology,"",IOntoBase.OntType.DINDIVIDUAL), uOntology);
				String id = obArea.getOntologyIdentifier();
				if (id != null && id.equals(areaIdentifier)){
					areaInst = obArea.getResource();
					break;
				}
					
//				for (RDFNode object: uOntology.getJenaModel().GetObjects(instanceArea, null, false)){
//					if (object.isLiteral() && object.asLiteral().sameValueAs(idArea)){
//						areaInst = instanceArea;
//						break;
//					}
//				}
//				if (areaInst != null)
//					break;
			}
//		} else {
//			
//			areaInst = uOntology.getJenaModel().newInstance(UrbanOntology.bcninst+"#temporal"+UrbanOntology.tempInstCounter++,areaType.getResource());
		}
		if (areaInst == null)
			return null;

//		System.out.println("-3");
//		uOntology.printConnectionData();
		
		//newInst type CoreV2:Indicator
		Resource indicator = uOntology.getJenaModel().getResource(UrbanOntology.core+"#Indicator", false);
		Resource newInst = null;
		
		newInst = uOntology.getJenaModel().newInstance(UrbanOntology.bcninst+"#indicator_"+indDesc.getResource().getLocalName()+"_"+areaType.getResource().getLocalName()+"_"+areaIdentifier, indicator);
		
		//newInst coreV2:IndicatorOf indAreaURI
		Property pIndOf = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#IndicatorOf",false);
		uOntology.getJenaModel().addNewStatement(newInst, pIndOf, areaInst, false);
		uOntology.getJenaModel().addNewStatement(newInst, pIndOf, areaInst, true);
		
		Property pValue = null;
		Set<IOntoLink> newTriples = null;

		//indAreaURI CoreV2:hasIndicator newInst
		Property pHasInd = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#hasIndicator",false);
		uOntology.getJenaModel().addNewStatement(areaInst, pHasInd, newInst, false);
		uOntology.getJenaModel().addNewStatement(areaInst, pHasInd, newInst, true);
		
		//newInst CoreV2:hasIndicatorDescriptor indDescriptor
		Property pHasDescr = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#hasIndicatorDescriptor",false);
		uOntology.getJenaModel().addNewStatement(newInst, pHasDescr, indDesc.getResource(), false);
		uOntology.getJenaModel().addNewStatement(newInst, pHasDescr, indDesc.getResource(), true);
		

		//indDescriptor CoreV2:indicatorDescriptorOf newInst
		Property pDescrOf = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#indicatorDescriptorOf",false);
		uOntology.getJenaModel().addNewStatement(indDesc.getResource(), pDescrOf, newInst, false);
		uOntology.getJenaModel().addNewStatement(indDesc.getResource(), pDescrOf, newInst, true);
		
//		System.out.println("3");
//		uOntology.printConnectionData();
		//newInst CoreV2:measurementFloatValue value
		pValue = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#measurementFloatValue",false);
		
		RDFDatatype dtype = TypeMapper.getInstance().getSafeTypeByName(XSD.xfloat.getURI());
		uOntology.getJenaModel().addNewStatement(newInst, pValue, ResourceFactory.createTypedLiteral(Float.toString(value), dtype), false);
		
		
		newTriples = NewRelationsFromIndicator(areaType, indDesc, uOntology);
		if (uOntology.getMapPopulated().UpdateAddNewLinks(newTriples, uOntology))
			try {
				ModelMap.SerializeObject(uOntology.getMapPopulated(), uOntology.getModelMapPopulatedPath());
			} catch (IOException e) {
				System.err.println("Add indicator: error updating internal structures");
				e.printStackTrace();
			}

		
		return new Indicator(newInst,uOntology,"indicator instance",IOntoBase.OntType.DINDIVIDUAL);
	}
	
	
	static private Set<IOntoLink> NewRelationsFromIndicator(IOntoBase anchorArea, IIndicatorDescriptor indDesc, UrbanOntology uOntology){
		//Resource indicator = uOntology.getJenaModel().getResource(UrbanOntology.core+"#Indicator", false);
		//Resource areaType = uOntology.getJenaModel().getResource(areaTypeURI, false);
		//Resource indDescriptorInstance = uOntology.getJenaModel().getResource(indTypeURI, false);
		
		//Resource indDescriptor = uOntology.getJenaModel().getResource(uOntology.getJenaModel().GetClass(indDescriptorInstance, false),false);
		
//		Property pIndOf = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#IndicatorOf",false);
//		Property pHasInd = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#hasIndicator",false);
//		Property pHasDescr = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#hasIndicatorDescriptor",false);
//		Property pDescrOf = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#indicatorDescriptorOf",false);
//		Property pValue = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#measurementFloatValue",false);

		Set<IOntoLink> links = new HashSet<IOntoLink>();
		IOntoBase obIndicator = new OntoConcept(UrbanOntology.core+"#Indicator",uOntology);
		//IOntoBase obAreaType = new OntoConcept(areaType,this);
		//IOntoBase obDescriptor = new OntoConcept(indDescriptor,this);
		IOntoBase obFloatDatatype = new OntoConcept(UrbanOntology.rdfs+"#Datatype",uOntology);
		IOntoProperty opIndOf = new OntoProperty(new OntoConcept(UrbanOntology.core+"#IndicatorOf",uOntology,"",IOntoBase.OntType.PROPERTY),uOntology);
		IOntoProperty opHasInd = new OntoProperty(new OntoConcept(UrbanOntology.core+"#hasIndicator",uOntology,"",IOntoBase.OntType.PROPERTY),uOntology);
		IOntoProperty opHasDescr = new OntoProperty(new OntoConcept(UrbanOntology.core+"#hasIndicatorDescriptor",uOntology,"",IOntoBase.OntType.PROPERTY),uOntology);
		IOntoProperty opDescrOf = new OntoProperty(new OntoConcept(UrbanOntology.core+"#indicatorDescriptorOf",uOntology,"",IOntoBase.OntType.PROPERTY),uOntology);
		IOntoProperty opValue = new OntoProperty(new OntoConcept(UrbanOntology.core+"#measurementFloatValue",uOntology,"",IOntoBase.OntType.PROPERTY),uOntology);
		
		if (anchorArea != null){
			links.add(new OntoLink(opIndOf, obIndicator, anchorArea,uOntology));
			links.add(new OntoLink(opHasInd,anchorArea, obIndicator,uOntology));
		}
		if (indDesc != null){
			IIndicatorType indType = indDesc.getIndicatorType();
			links.add(new OntoLink(opHasDescr,obIndicator,  indType,uOntology));
			links.add(new OntoLink(opDescrOf, indType, obIndicator,uOntology));
		}
		links.add(new OntoLink(opValue, obIndicator, obFloatDatatype,uOntology));
		return links;
	}
	
	public void editIndicatorValue(float newValue){
		Property property = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#measurementFloatValue",false);
		RDFNode value = uOntology.getJenaModel().GetObject(this.getResource(), property ,false);
		uOntology.getJenaModel().removeStatement(this.getResource(), property, value, true);
		uOntology.getJenaModel().removeStatement(this.getResource(), property, value, false);
		RDFDatatype dtype = TypeMapper.getInstance().getSafeTypeByName(XSD.xfloat.getURI());
		uOntology.getJenaModel().addNewStatement(this.getResource(), property, ResourceFactory.createTypedLiteral(Float.toString(newValue), dtype), false);
		this.indValue = newValue;
	}
	
	static public void RemoveIndicator(IIndicator indInstance, UrbanOntology uOntology){
		uOntology.getJenaModel().removeResource(indInstance.getResource());
		IIndicatorDescriptor indDesc = indInstance.getIndDescriptor();
		IOntoBase areaType = indInstance.getIndArea();
		//IOntoBase areaType = new OntoConcept(getAnchorArea(anchorArea),uOntology,"",IOntoBase.OntType.CLASS);
		Set<IOntoLink> newTriples = NewRelationsFromIndicator(areaType, indDesc, uOntology);
		if (uOntology.getMapPopulated().UpdateRemoveLinks(newTriples, uOntology))
			try {
				ModelMap.SerializeObject(uOntology.getMapPopulated(), uOntology.getModelMapPopulatedPath());
			} catch (IOException e) {
				System.err.println("Remove indicator: error updating internal structures");
				e.printStackTrace();
			}
	}
	

	@Override
	public IIndicatorDescriptor getIndDescriptor() {
		return indDescriptor;
	}

	@Override
	public float getIndValue() {
		return indValue;
	}

	@Override
	public IOntoBase getIndArea() {
		return indArea;
	}
//	
//	static public Set<IOntoBase> GetAreasAssociatedToIndicator(UrbanOntology uOntology){
//		Set<IOntoBase> areas = new HashSet<IOntoBase>();
//		areas.add(new OntoConcept(UrbanOntology.core+"#RoadSegment",uOntology));
//		areas.add(new OntoConcept(UrbanOntology.core+"#Superblock",uOntology));
//		areas.add(new OntoConcept(UrbanOntology.core+"#LandLot",uOntology));
//		return areas;
//	}
	
	static public String getAnchorArea(IIndicator.ANCHOR_AREAS area) {
		switch (area){
		case ROAD_SEGMENT: return IIndicator.ROAD_SEGMENT;
		case SUPERBLOCK: return IIndicator.SUPERBLOCK;
		case LANDLOT: return IIndicator.LANDLOT;
		case MESH: return IIndicator.MESH;
		case CITY: return IIndicator.CITY;
		case NEIGHBORHOOD: return IIndicator.NEIGHBORHOOD;
		}
		return null;
	}
	
	
	
}
