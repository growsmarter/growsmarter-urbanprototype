package smartcities.semanticweb.ontologycommons.services;

import java.util.HashSet;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty.OntPropertyGeneral;

class OntoProperty extends OntoConcept implements IOntoProperty {
	private OntProperty ptype;

	public OntoProperty(IOntoBase ob, UrbanOntology uOntology) {
		super(ob, uOntology);
		setPropertyType();
	}

	public OntoProperty(Resource resource, UrbanOntology uOntology) {
		super(resource, uOntology);
		setPropertyType();
	}

	static public Set<IOntoProperty> GetOntoProperty(IOntoProperty.OntProperty ptype, boolean inferProperty,
			boolean includeGenericConcepts, UrbanOntology uOntology) {
		Set<String> uriProps = getPropertyType(ptype);
		Set<Resource> allProps = new HashSet<Resource>();
		for (String uri : uriProps) {
			Resource obp = uOntology.getJenaModel().getResource(uri, false);
			Set<Resource> resProps = uOntology.getJenaModel().GetDirectConceptsOfType(obp);
			for (Resource p : resProps) {
				if (includeGenericConcepts || (!includeGenericConcepts && !UrbanOntology.IsGeneralConcept(p.getURI()))) {
					allProps.add(p);
					if (inferProperty)
						allProps.addAll(uOntology.getJenaModel().GetSubHierarchy(p, true, false));
				}
			}
		}
		Set<IOntoProperty> ontoProps = new HashSet<IOntoProperty>();
		for (Resource op : allProps) {
			if (includeGenericConcepts || (!includeGenericConcepts && !UrbanOntology.IsGeneralConcept(op.getURI()))) {
				OntoConcept pOc = (OntoConcept) new OntoConcept(op, uOntology);
				ontoProps.add(new OntoProperty(pOc, uOntology));
			}
		}
		return ontoProps;
	}

	// Returns subproperties of resProp which have resClassDomain in the domain
	// and resClassRange in the range.
	// Manual inference
	static public boolean IsValidProperty(IOntoProperty p, Resource classDomain, Resource classRange) {
		boolean foundDomain = false;
		boolean foundRange = false;
		if (classDomain != null) {
			if (p.getDomain().contains(classDomain)) {
				// if (inList(classDomain, p.getDomain())){
				foundDomain = true;
			}
		} else
			foundDomain = true;
		if (classRange != null) {
			if (p.getRange().contains(classRange)) {
				// if (inList(classRange, p.getRange())){
				foundRange = true;
			}
		} else
			foundRange = true;
		if (foundDomain && foundRange)
			return true;
		return false;
	}

	public Set<Resource> getDomain() {
		// if (this.getType() == IOntoBase.OntType.DPROPERTY || this.getType()
		// == IOntoBase.OntType.OPROPERTY || this.getType() ==
		// IOntoBase.OntType.APROPERTY || this.getType() ==
		// IOntoBase.OntType.PROPERTY){
		Set<RDFNode> domains = uOntology.getJenaModel().GetObjects(this.getResource(), RDFS.domain, true);
		Set<Resource> domainsres = new HashSet<Resource>();
		for (RDFNode d : domains) {
			if (!d.isAnon()) { // TODO: manage unionOf and other types of
								// composed ranges
				domainsres.addAll(uOntology.getJenaModel().GetSubHierarchy(d.asResource(), true, false));
			}
		}
		return domainsres;
	}

	@Override
	public Set<Resource> getRange() {
		// Set<IOntoBase> ocRanges = new HashSet<IOntoBase>();
		Set<Resource> rangesres = new HashSet<Resource>();
		if (this.getGeneralPropertyType() == IOntoProperty.OntPropertyGeneral.DATA)
			// if (this.getType() == IOntoBase.OntType.DPROPERTY)
			rangesres.add(uOntology.getJenaModel().getResource("http://www.w3.org/2000/01/rdf-schema#Datatype", false));
		// ocRanges.add(new
		// OntoConcept("http://www.w3.org/2000/01/rdf-schema#Datatype",uOntology));
		else {
			Set<RDFNode> ranges = uOntology.getJenaModel().GetObjects(this.getResource(), RDFS.range, true);
			for (RDFNode r : ranges) {
				if (!r.isAnon()) // TODO: manage unionOf and other types of
									// composed ranges
					rangesres.addAll(uOntology.getJenaModel().GetSubHierarchy(r.asResource(), true, false));
			}
		}
		return rangesres;
	}

	@Override
	public OntProperty getPropertyType() {
		return this.ptype;
	}

	// TODO: sólo un tipo y además no subproperties, así que siempre serán data
	// u object
	private void setPropertyType() {
		String type = uOntology.getJenaModel().GetClass(this.getResource(), false);
		if (type != null)
			this.setPropertyType(type);
	}

	static public Set<String> getPropertyType(IOntoProperty.OntProperty ptype) {
		Set<String> uriList = new HashSet<String>();
		switch (ptype) {
		case ALL:
			uriList.add(IOntoProperty.OBJECT_URI);
			uriList.add(IOntoProperty.DATA_URI);
			break;
		case SCRIBE:
			uriList.add(IOntoProperty.SCRIBE_URI);
			break;
		case SOBJECT:
			uriList.add(IOntoProperty.SOBJECT_URI);
			break;
		case SDATA:
			uriList.add(IOntoProperty.SDATA_URI);
			break;
		case SAGGREGATE:
			uriList.add(IOntoProperty.SAGGREGATE1_URI);
			uriList.add(IOntoProperty.SAGGREGATE2_URI);
			break;
		case SASSOCIATED:
			uriList.add(IOntoProperty.SASSOCIATED_URI);
			break;
		case SATTRIBUTE:
			uriList.add(IOntoProperty.SATTRIBUTE1_URI);
			uriList.add(IOntoProperty.SATTRIBUTE2_URI);
			break;
		case OBJECT:
			uriList.add(IOntoProperty.OBJECT_URI);
			break;
		case DATA:
			uriList.add(IOntoProperty.DATA_URI);
			break;
		case ANNOTATION:
			uriList.add(IOntoProperty.ANNOTATION_URI);
			break;
		case OTHER:
			break;
		}
		return uriList;
	}

	public void setPropertyType(String uri) {
		if (uri.equals(ALL_URI))
			this.ptype = OntProperty.ALL;
		else if (uri.equals(SCRIBE_URI))
			this.ptype = OntProperty.SCRIBE;
		else if (uri.equals(SOBJECT_URI))
			this.ptype = OntProperty.SOBJECT;
		else if (uri.equals(SDATA_URI))
			this.ptype = OntProperty.SDATA;
		else if (uri.equals(SAGGREGATE1_URI))
			this.ptype = OntProperty.SAGGREGATE;
		else if (uri.equals(SAGGREGATE2_URI))
			this.ptype = OntProperty.SAGGREGATE;
		else if (uri.equals(SASSOCIATED_URI))
			this.ptype = OntProperty.SASSOCIATED;
		else if (uri.equals(SATTRIBUTE1_URI))
			this.ptype = OntProperty.SATTRIBUTE;
		else if (uri.equals(SATTRIBUTE2_URI))
			this.ptype = OntProperty.SATTRIBUTE;
		else if (uri.equals(DATA_URI))
			this.ptype = OntProperty.DATA;
		else if (uri.equals(OBJECT_URI) || uri.equals(TRANSITIVE_URI))
			this.ptype = OntProperty.OBJECT;
		else if (uri.equals(ANNOTATION_URI))
			this.ptype = OntProperty.ANNOTATION;
		else
			this.ptype = OntProperty.OTHER;
	}

	public OntPropertyGeneral getGeneralPropertyType() {
		if (this.getPropertyType() == null) {
			return OntPropertyGeneral.OBJECT;
		} else {
			switch (this.getPropertyType()) {
			case SOBJECT:
			case SAGGREGATE:
			case SASSOCIATED:
			case SATTRIBUTE:
			case OBJECT:
				return OntPropertyGeneral.OBJECT;
			case SDATA:
			case DATA:
				return OntPropertyGeneral.DATA;
			default:
				return OntPropertyGeneral.OTHER;
			}
		}

	}

	// if (p != null && (type.getLocalName().equals("ObjectProperty")||
	// type.getLocalName().equals("TransitiveProperty")))
	// return IOntoBase.OntType.OPROPERTY;
	// if (p != null && type.getLocalName().equals("DatatypeProperty"))
	// return IOntoBase.OntType.DPROPERTY;
	// if (p != null && type.getLocalName().equals("AnnotationProperty"))
	// return IOntoBase.OntType.APROPERTY;
	// if (p != null && (type.getLocalName().equals("FunctionalProperty") ||
	// type.getLocalName().equals("Property")))
	// return IOntoBase.OntType.PROPERTY;
	// if (p != null && (type.getLocalName().contains("Property") ||
	// type.getLocalName().equals("inverseOf") ||
	// type.getLocalName().equals("InverseFunctionalProperty")))
	// return IOntoBase.OntType.OPROPERTY;

}
