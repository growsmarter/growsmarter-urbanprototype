package smartcities.semanticweb.ontologycommons.services;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bbn.parliament.jena.joseki.client.RemoteModel;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
//import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.base.file.Location;


public class ParliamentOntology {
	
	RemoteModel parModel = null;
	//ArrayList<JenaOntology> includedModels = new ArrayList<JenaOntology>(); //list of Jena models included in parliament
	
	//public String graph=null;
	public String SPARQL_PARLIAMENT = "/sparql";
	public String BULK_PARLIAMENT = "/bulk";
	public String TRACKER_PARLIAMENT = "/tracker";
	public String parliamentlocation = "http://growsmarter.bsc.es:8080/parliament";
	//public String SPARQL_PARLIAMENT = "http://localhost:8080/parliament/sparql";
	//public String BULK_PARLIAMENT = "http://localhost:8080/parliament/bulk";
		

	public RemoteModel getParModel() {
		return parModel;
	}
	
	public ParliamentOntology(String parliamentlocation){
		this.parliamentlocation = parliamentlocation;
		int count =0;
		int maxAttempts = 3;
		boolean success = false;
		do{
			count++;
			try{
				this.parModel = new RemoteModel(parliamentlocation+SPARQL_PARLIAMENT, parliamentlocation+BULK_PARLIAMENT);
				success = true;
			}catch(Exception e){
				if (count >= maxAttempts)
		        	throw e;
				else
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		} while (!success);
	}

	
	public void insertStatements(String graphURL, Model ontology) throws IOException{
		this.parModel.insertStatements(ontology,graphURL);
	}
	
	public void deleteGraph(String graphURL) throws IOException{
		this.parModel.dropNamedGraph(graphURL);
		
	}
		
	private Set<String> getQueriesId(String trackContents, String idSession) throws ParseException, UnknownHostException, SocketException{
		//http://redrock.bsc.es:8080/parliament/tracker
		//[
//		{
//			"id":12374843,"cancellable":true,"status":"RUNNING","display":"SELECT  *\n  }\n","currentTime":1445964622718,"created":1445964600569,"started":1445964600569,"creator":"84.88.50.18"}
//			]

		Set<String> ipAddresses = new HashSet<String>();
		Set<String> identifiers = new HashSet<String>();
		Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
		while (en.hasMoreElements()){
			NetworkInterface ni = en.nextElement();
			Enumeration<InetAddress> enAddress = ni.getInetAddresses();
			while (enAddress.hasMoreElements()){
				InetAddress ia = enAddress.nextElement();
				ipAddresses.add(ia.getHostAddress());
			}
		}
		JSONArray json = new JSONArray(trackContents);
		for (int i=0;i<json.length();i++){
			JSONObject item = json.getJSONObject(i);
			String itemIp = item.getString("creator");
			if (ipAddresses.contains(itemIp)){
				String isCancellable = item.getString("cancellable");
				if (isCancellable.equals("true")){
					String query = item.getString("display");
				    Pattern pattern = Pattern.compile("BIND\\(\"(?<idSession>.*)\" AS \\?idSession\\)");
				    Matcher matcher = pattern.matcher(query);
				    if (matcher.find()){
				    	String id = matcher.group("idSession");
				    	if(id != null && idSession.equals(id)){
				    		String itemId = item.getString("id");
				    		identifiers.add(itemId);
				    	}
					}
				}
			}
		}
		return identifiers;
	}
	
	private String getJSON(String url, int timeout) throws IOException {
	    HttpURLConnection c = null;
	    int attempt = 0;
	    String json = null;
	    boolean success = false;
	    do{
		    try {
		    	attempt++;
		        URL u = new URL(url);
		        c = (HttpURLConnection) u.openConnection();
		        c.setRequestMethod("GET");
		        c.setRequestProperty("Content-length", "0");
		        c.setUseCaches(false);
		        c.setAllowUserInteraction(false);
		        c.setConnectTimeout(timeout);
		        c.setReadTimeout(timeout);
		        c.connect();
		        int status = c.getResponseCode();
	
		        switch (status) {
		            case 200:
		            case 201:
		                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
		                StringBuilder sb = new StringBuilder();
		                String line;
		                while ((line = br.readLine()) != null) {
		                    sb.append(line+"\n");
		                }
		                br.close();
		                json = sb.toString();
		                success = true;
		        }
	
		    } catch (IOException ex) {
		    	if (attempt >= 3){
		    		if (c != null) 
		  	              c.disconnect();
		    		throw(ex);
		    	}
		       // Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		       }
	    } while (!success && attempt <3);
	    c.disconnect();
	    return json;
	}
		
	public boolean CancelActiveQuery(String idSession) throws IOException, ParseException{
		HttpURLConnection con =null;
		boolean success = false;
		String trackers = getJSON(TRACKER_PARLIAMENT,10000);
		if (trackers != null && !trackers.isEmpty()){
			Set<String> identifiers = getQueriesId(trackers, idSession);
			if (identifiers.isEmpty())
				return true;
			int attempt = 0;			
			do{
				attempt++;
				for (String id:identifiers){
					try{
						URL url = new URL(TRACKER_PARLIAMENT);
						con = (HttpURLConnection)url.openConnection();
						con.setDoOutput(true);
						con.setDoInput(true);
				    
					    String urlParameters  = "id="+id;
					    byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
					    con.setRequestMethod("POST");
					    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
					    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
					    wr.write(postData);
					    wr.close();
				
					    int responseCode = con.getResponseCode();
						System.out.println("Parliament StopQuery response code: "+responseCode);
						success = true;
					}catch(IOException ex){
				    	if (attempt >= 3){
				    		if (con != null) 
				  	              con.disconnect();
				    		throw(ex);
				    	}
					}
				}
			}while (!success && attempt<3);
			
		} else if (trackers.isEmpty())
			success = true;
		con.disconnect();
		return success;
	}
	
	//Insert OntModel in urlGraph
	public void InsertModel(OntModel jenaOnt, String urlGraph) throws IOException{
			parModel.insertStatements(jenaOnt,urlGraph);
			//includedModels.add(jenaOnt);
	}
	
	//Insert Model in Default Graph
	public void InsertModelDefault(OntModel jenaOnt) throws IOException{
			parModel.insertStatements(jenaOnt);
			//includedModels.add(jenaOnt);
	}
	
	
	public QueryResults ResultSetQueryOntology(String query) {
		QueryResults qr;
		try{
			ResultSet rs = parModel.selectQuery(query);
			qr = new QueryResults(rs);
		}catch(Exception e){
			qr = null;
		}
		return qr;  
	}
	
	public String XMLQueryOntology(String query) throws IOException{
		String output=null;
		ResultSet rs = parModel.selectQuery(query);
		output = ResultSetFormatter.asXMLString(rs);
		return output;
	}
	
	public Model RDFQueryOntology(String query) throws IOException{
		ResultSet rs = parModel.selectQuery(query);
		Model rModel= ModelFactory.createDefaultModel();
		ResultSetFormatter.asRDF(rModel, rs);
		return rModel;
	}

	//Predefined prefixes in Parliament
	public Map<String, String> getMapPrefixes(){
		Map<String, String> prefixMap = new HashMap<String,String>();
		prefixMap.put("afn", "http://jena.hpl.hp.com/ARQ/function#");
		prefixMap.put("fn", "http://www.w3.org/2005/xpath-functions#");
		prefixMap.put("geo", "http://www.opengis.net/ont/geosparql#");
		prefixMap.put("geof", "http://www.opengis.net/def/function/geosparql/"); 
		prefixMap.put("gml", "http://www.opengis.net/ont/gml#");
		prefixMap.put("owl", "http://www.w3.org/2002/07/owl#");
		prefixMap.put("par", "http://parliament.semwebcentral.org/parliament#");
		prefixMap.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		prefixMap.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		prefixMap.put("sf", "http://www.opengis.net/ont/sf#");
		prefixMap.put("time", "http://www.w3.org/2006/time#");
		prefixMap.put("units", "http://www.opengis.net/def/uom/OGC/1.0/");
		prefixMap.put("xsd", "http://www.w3.org/2001/XMLSchema#");
		return prefixMap;
		}


public boolean existGraph(String graphName) throws IOException{
	for (Iterator<String> iterator = this.parModel.getAvailableNamedGraphs(); iterator.hasNext();) {
		String graph = (String) iterator.next();
		if (graph.equals(graphName))
			return true;
	}
	return false;
}



public String Query_WithinPolygonURI(String URIclass, String wktPolygon, String graph){ //more efficient with intersect than with within
	return("SELECT DISTINCT ?f1 WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . FILTER (geof:sfIntersects(?wkt,\""+wktPolygon+"\"^^sf:wktLiteral)) . }}");
	//return("SELECT DISTINCT ?f1 WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . FILTER (geof:sfWithin(?wkt,\""+wktPolygon+"\"^^sf:wktLiteral)) . }}");
}

public String Query_WithinPolygon(String URIclass, String wktPolygon, String graph){ //more efficient with intersect than with within
	return("SELECT DISTINCT ?f1 ?wkt ?property ?info ?p2 ?info2 WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info . FILTER (geof:sfIntersects(?wkt,\""+wktPolygon+"\"^^sf:wktLiteral)) . OPTIONAL {?info ?p2 ?info2} }}");
	//return("SELECT DISTINCT ?f1 ?wkt ?property ?info ?p2 ?info2 WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info . FILTER (geof:sfWithin(?wkt,\""+wktPolygon+"\"^^sf:wktLiteral)) . OPTIONAL {?info ?p2 ?info2} }}");
}

public String Query_WithinPolygonOnlyAttributes(String URIclass, String wktPolygon, String graph){ //more efficient with intersect than with within
	return("SELECT DISTINCT ?f1 ?wkt ?property ?info WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info . FILTER (geof:sfIntersects(?wkt,\""+wktPolygon+"\"^^sf:wktLiteral))}}");
	//return("SELECT DISTINCT ?f1 ?wkt ?property ?info ?p2 ?info2 WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info . FILTER (geof:sfWithin(?wkt,\""+wktPolygon+"\"^^sf:wktLiteral)) . OPTIONAL {?info ?p2 ?info2} }}");
}

public void indexGraph(String graph){
	try {
		
		URL url = new URL(parliamentlocation+SPARQL_PARLIAMENT);
	    HttpURLConnection con = (HttpURLConnection)url.openConnection();
	    con.setDoOutput(true);
	    con.setDoInput(true);
	    con.setRequestMethod("POST");
	    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	    wr.writeBytes("update=INSERT {} WHERE {<"+graph+"> <http://parliament.semwebcentral.org/pfunction#enableIndexing> \"true\"^^<http://www.w3.org/2001/XMLSchema#boolean> }");
	    wr.close();

	    int responseCode = con.getResponseCode();
		System.out.println("Parliament Index response code: "+responseCode);
	} 
	catch (MalformedURLException e) { 
		System.out.println("Error: "+e.getMessage());
	    // new URL() failed
	    // ...
	} 
	catch (IOException e) {   
		System.out.println("Error: "+e.getMessage());
	    // openConnection() failed
	    // ...
	}
	
}



//public String Query_Wkt(String URIclass, String graph){
//	//return("SELECT DISTINCT ?f1 ?wkt ?property ?info WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info . }}");
//	//return("SELECT DISTINCT ?f1 ?property ?info WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info . }}");
//	return("SELECT DISTINCT ?f1 ?property ?info ?p2 ?info2 WHERE { GRAPH <"+graph+"> {?f1 rdf:type <"+URIclass+"> . ?f1 geo:hasGeometry ?g1 . ?g1 geo:asWKT ?wkt . ?f1 ?property ?info .}}");
//}

}
