package smartcities.semanticweb.ontologycommons.services;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ontology_commons.tests.serviceInterface.library.PrintMethods;

import com.hp.hpl.jena.datatypes.TypeMapper;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.XSD;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea.OntArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase.OntType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction.OntFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty.OntProperty;

class OntoConcept implements IOntoBase {
	UrbanOntology uOntology;
	Resource resource;

	public Resource getResource() {
		return resource;
	}

	// IOntoBase
	String URI;
	OntType type;
	String label;
	int id = 0; // identifier for multiple purporses
	String prefix;

	// es el constructor que consume menos tiempo
	// used for internal operations
	public OntoConcept(Resource resource, UrbanOntology uOntology, String label, OntType type) {
		this.uOntology = uOntology;
		this.URI = resource.getURI();
		this.label = label;
		this.resource = resource;
		this.type = type;
		setPrefix();
	}

	public OntoConcept(IOntoBase ob, UrbanOntology uOntology) {
		this.uOntology = uOntology;
		this.URI = ob.getURI();
		this.label = ob.getLabel();
		this.resource = ob.getResource();
		this.prefix = ob.getPrefix();
		this.type = ob.getType();
	}

	public OntoConcept(Resource resource, UrbanOntology uOntology) {
		this.uOntology = uOntology;
		this.URI = resource.getURI();
		this.resource = resource;

		FileWriter fwOC;
		long init[] = new long[3], end[] = new long[3];
		try {
			fwOC = new FileWriter("/tmp/GS_OntoConcepts_in.csv", true);
			init[0] = System.currentTimeMillis();
			setPrefix();
			end[0] = System.currentTimeMillis();

			init[1] = System.currentTimeMillis();
			setLabel();
			end[1] = System.currentTimeMillis();

			init[2] = System.currentTimeMillis();
			setType();
			end[2] = System.currentTimeMillis();

			fwOC.write((end[0] - init[0]) + "," + (end[1] - init[1]) + "," + (end[2] - init[2]) + "\n");
			fwOC.flush();
			fwOC.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public OntoConcept(Resource resource, int extraId, UrbanOntology uOntology) {
		this.uOntology = uOntology;
		this.URI = resource.getURI();
		this.resource = resource;
		this.id = extraId;
		setPrefix();
		setLabel();
		setType();
	}

	public OntoConcept(String uriResource, int extraId, UrbanOntology uOntology) {
		this(uOntology.getJenaModel().getResource(uriResource, false), extraId, uOntology);
	}

	public OntoConcept(String uriResource, UrbanOntology uOntology) {
		this(uOntology.getJenaModel().getResource(uriResource, false), uOntology);
	}

	public OntoConcept(String uriResource, UrbanOntology uOntology, String label, OntType type) {
		this(uOntology.getJenaModel().getResource(uriResource, false), uOntology, label, type);
	}

	private void setPrefix() {

		String namespace = this.getResource().getNameSpace();
		this.prefix = this.uOntology.getJenaModel().getNsURIPrefix(namespace);
		// @deprecated
		// this.prefix = uOntology.getJenaModel().getPrefix(namespace);
	}

	public void setSuperConcepts(List<IOntoBase> superConcepts) {
		// this.superConcepts = superConcepts;
	}

	public void setSubConcepts(List<IOntoBase> subConcepts) {
		// this.subConcepts = subConcepts;
	}

	public void setComment(String comment) {
		// this.comment = comment;
	}

	public List<IOntoBase> getSuperConcepts() {
		return getSuperConcepts(false);
	}

	@Override
	public List<IOntoBase> getSuperConcepts(boolean inference) {
		Set<Resource> supers = uOntology.getJenaModel().GetSuperHierarchy(this.getResource(), inference);
		ArrayList<IOntoBase> ontoSupers = new ArrayList<IOntoBase>();
		for (Resource res : supers) {
			if (!UrbanOntology.IsGeneralConcept(res.getURI()) && !res.getURI().equals(this.getURI()))
				ontoSupers.add(new OntoConcept(res, uOntology));
		}
		return ontoSupers;
		// return superConcepts;
	}

	public List<IOntoBase> getSubConcepts() {
		return getSubConcepts(false);
	}

	@Override
	public List<IOntoBase> getSubConcepts(boolean inference) {
		Set<Resource> subs = uOntology.getJenaModel().GetSubHierarchy(this.getResource(), inference, false);
		ArrayList<IOntoBase> ontoSubs = new ArrayList<IOntoBase>();
		for (Resource res : subs) {
			if (!UrbanOntology.IsGeneralConcept(res.getURI()) && !res.getURI().equals(this.getURI()))
				ontoSubs.add(new OntoConcept(res, uOntology));
		}

		return ontoSubs;
		// return subConcepts;
	}

	@Override
	public String getURI() {
		return URI;
	}

	private OntType GetOntoType(Resource res, boolean inferred) {
		if (!res.isURIResource())
			return null;

		Set<Resource> types = uOntology.getJenaModel().GetDirectType(res);
		if (types != null) {
			for (Resource type : types) {
				if (type != null) {
					if (!type.isURIResource())
						return IOntoBase.OntType.OTHER;
					if (type.getLocalName().equals("Class") || type.getLocalName().equals("Resource")) {
						// Map<String,String> prefixes =
						// uOntology.getJenaModel().getMapPrefixes(false);
						// for (String namespace: prefixes.values()){
						// RDFNode obj =
						// uOntology.getJenaModel().GetObject(uOntology.getJenaModel().getResource(namespace+res.getLocalName()+"_INST",false),
						// RDF.type, false);
						// if (obj != null && obj.isURIResource() &&
						// obj.asResource().getURI().equals(res.getURI()))
						// return IOntoBase.OntType.ECLASS;

						if (uOntology.getEnumTypeMap().containsKey(res.getURI()))
							return IOntoBase.OntType.ECLASS;
						else
							return IOntoBase.OntType.CLASS;
					}
					Property p = uOntology.getJenaModel().getProperty(res.getURI(), true);
					if (p != null && type.getLocalName().contains("Property"))
						return IOntoBase.OntType.PROPERTY;
					// if (p != null &&
					// (type.getLocalName().equals("ObjectProperty")||
					// type.getLocalName().equals("TransitiveProperty")))
					// return IOntoBase.OntType.OPROPERTY;
					// if (p != null &&
					// type.getLocalName().equals("DatatypeProperty"))
					// return IOntoBase.OntType.DPROPERTY;
					// if (p != null &&
					// type.getLocalName().equals("AnnotationProperty"))
					// return IOntoBase.OntType.APROPERTY;
					Set<Resource> types2 = uOntology.getJenaModel().GetDirectType(type);
					for (Resource type2 : types2) {
						if (type2 != null && type2.getLocalName().equals("Class")) {
							if (res.getLocalName().contains("_INST")
									&& type.getLocalName().equals(res.getLocalName().replaceFirst("_INST", "")))
								return IOntoBase.OntType.EINDIVIDUAL;
							// if (res.getNameSpace().equals(bcninst+"#") ||
							// (res.getNameSpace().equals(BCNII+"#")))
							// if (!IsGeneralConcept(type.getURI()))
							String localName = type.getLocalName();
							if (!localName.contains(RDF.getURI()) && !localName.contains(OWL.getURI())
									&& !localName.contains(XSD.getURI()) && !localName.contains(RDFS.getURI()))
								return IOntoBase.OntType.DINDIVIDUAL;
							return IOntoBase.OntType.CLASS;
						}
					}
					// if (p != null &&
					// (type.getLocalName().equals("FunctionalProperty") ||
					// type.getLocalName().equals("Property")))
					// return IOntoBase.OntType.PROPERTY;
					// if (p != null &&
					// (type.getLocalName().contains("Property") ||
					// type.getLocalName().equals("inverseOf") ||
					// type.getLocalName().equals("InverseFunctionalProperty")))
					// return IOntoBase.OntType.OPROPERTY;
				}
			}
		}
		// Ask if it is a datatype
		if (res.getLocalName().equals("Datatype") || res.getURI().equals("http://www.opengis.net/ont/sf#wktLiteral")
				|| TypeMapper.getInstance().getTypeByName(res.getURI()) != null)
			return IOntoBase.OntType.DATATYPE;
		for (String function_uri : new String[] { IOntoFunction.BUFFER_URI, IOntoFunction.DISTANCE_URI,
				IOntoFunction.INTERSECTION_URI, IOntoFunction.WITHIN_URI })
			if (res.getURI().equals(function_uri))
				return IOntoBase.OntType.FUNCTION;
		return IOntoBase.OntType.OTHER;
	}

	@Override
	public OntType getType() {
		return this.type;
	}

	private void setType() {
		IOntoBase.OntType result = GetOntoType(this.getResource(), false);
		if (result == IOntoBase.OntType.OTHER)
			result = GetOntoType(this.getResource(), true);
		this.type = result;
		// return type;
	}

	public void setType(OntType type) {
		// this.type = type;
	}

	private void setLabel() {
		this.label = uOntology.getJenaModel().GetLabel(this.getResource());
		if (this.label == null) {
			this.label = this.resource.getLocalName() + " (local_name)";
		} else {
			// do nothing
		}

	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getComment() {
		// return comment;
		RDFNode comment = uOntology.getJenaModel().GetObject(this.getResource(), RDFS.comment, true);
		if (comment != null)
			return comment.asLiteral().getValue().toString();
		return "";
	}

	@Override
	public int getId() {
		return this.id;
	}

	public void setId(int identifier) {
		this.id = identifier;
	}

	public boolean isInstance() {
		IOntoBase.OntType type = this.getType();
		if (type == IOntoBase.OntType.DINDIVIDUAL || type == IOntoBase.OntType.EINDIVIDUAL)
			return true;
		return false;
	}

	public boolean isEnumType() {
		IOntoBase.OntType type = this.getType();
		if (type == IOntoBase.OntType.EINDIVIDUAL || type == IOntoBase.OntType.ECLASS)
			return true;
		return false;
	}

	public boolean isFunction() {
		IOntoBase.OntType type = this.getType();
		if (type != IOntoBase.OntType.FUNCTION)
			return false;
		return true;
	}

	public boolean isProperty() {
		IOntoBase.OntType type = this.getType();
		if (type != IOntoBase.OntType.PROPERTY)
			return false;
		return true;
	}

	public boolean isIndicatorClass() {
		Set<Statement> st = uOntology.getJenaModel().GetStatements(this.getResource(), RDFS.subClassOf,
				uOntology.getJenaModel().getResource(UrbanOntology.core + "#Indicator", false), true);
		if (st != null && st.size() > 0)
			return true;
		return false;
	}

	public boolean isIndicatorDescriptorClass() {

		Set<Statement> st = uOntology.getJenaModel().GetStatements(this.getResource(), RDFS.subClassOf,
				uOntology.getJenaModel().getResource(UrbanOntology.core + "#IndicatorType", false), true);
		if (st != null && st.size() > 0)
			return true;
		return false;
	}

	public boolean isIndicator() {
		Set<Statement> st = uOntology.getJenaModel().GetStatements(this.getResource(), RDF.type,
				uOntology.getJenaModel().getResource(UrbanOntology.core + "#Indicator", false), false);
		if (st != null && st.size() > 0)
			return true;
		return false;
	}

	public boolean isIndicatorDescriptor() {

		Set<Statement> st = uOntology.getJenaModel().GetStatements(this.getResource(), RDF.type,
				uOntology.getJenaModel().getResource(UrbanOntology.core + "#IndicatorType", false), true);
		if (st != null && st.size() > 0)
			return true;
		return false;

		//
		// Set<RDFNode> indtypes =
		// uOntology.getJenaModel().GetObjects(this.getResource(), RDF.type,
		// true);
		//
		// String indClassPrefix = UrbanOntology.core;
		// String indClassLocalName = "IndicatorType";
		// Resource indTypeClass =
		// uOntology.getJenaModel().getResource(indClassPrefix+"#"+indClassLocalName,
		// false);
		// Set<Resource> indicators = new HashSet<Resource>();
		// for (Resource res:
		// uOntology.getJenaModel().GetSubjects(indTypeClass,RDFS.subClassOf,
		// false)){
		// indicators.addAll(uOntology.getJenaModel().GetSubHierarchy(res, true,
		// false));
		// }
		// Set<IIndicatorDescriptor> resultingInd = new
		// HashSet<IIndicatorDescriptor>();
		// for (Resource resInd: indicators){
		// for (Resource
		// indDesc:uOntology.getJenaModel().GetSubjects(resInd,RDF.type,
		// false)){
		// IIndicatorDescriptor oc = new IndicatorDescriptor(new
		// OntoConcept(indDesc,uOntology),uOntology);
		// resultingInd.add(oc);
		// }
		// }
		// return resultingInd;

	}

	public boolean isGeoConcept() {
		IOntoBase cl = null;
		if (this.getType() == IOntoBase.OntType.DINDIVIDUAL || this.getType() == IOntoBase.OntType.EINDIVIDUAL) {
			cl = new OntoConcept(uOntology.getJenaModel().GetClass(this.getResource(), false), uOntology, "",
					OntType.CLASS);
		} else if (this.getType() == IOntoBase.OntType.CLASS)
			cl = this;
		Resource feature = uOntology.getJenaModel().getResource(IOntoArea.SPATIAL_OBJECT, false);
		if (cl != null) {
			Set<Resource> subclass = uOntology.getJenaModel().GetSubHierarchy(cl.getResource(), true, false);
			for (Resource s : subclass) {
				if (uOntology.getJenaModel().getSuperconcept(s, feature) != null)
					return true;
			}
			return false;
		}
		return false;
	}

	public boolean isAreaInstance() {
		if (this.getType() == IOntoBase.OntType.DINDIVIDUAL || this.getType() == IOntoBase.OntType.EINDIVIDUAL) {
			Property property = uOntology.getJenaModel().getProperty(IOntoArea.GEOMETRY_PROPERTY_URI, false);
			RDFNode geometry = uOntology.getJenaModel().GetObject(this.getResource(), property, false);
			RDFNode wkt = null;
			if (geometry != null)
				wkt = uOntology.getJenaModel().GetObject(geometry.asResource(),
						uOntology.getJenaModel().getProperty(IOntoArea.WKT_PROPERTY_URI, false), false);
			else {
				geometry = uOntology.getJenaModel().GetObject(this.getResource(), RDF.type, false);
				wkt = uOntology.getJenaModel().GetObject(this.getResource(),
						uOntology.getJenaModel().getProperty(IOntoArea.WKT_PROPERTY_URI, false), false);
			}
			if (wkt != null)
				return true;
		}
		return false;
	}

	@Override
	public void setLabel(String label) {
		this.label = label;

	}

	public String toString() {
		return this.getURI();
	}

	@Override
	public String getPrefix() {
		return this.prefix;
	}

	public Set<IOntoBase> getInstances() {
		return getInstances(false);
	}

	public Set<IOntoBase> getInstances(boolean inference) {
		Set<IOntoBase> results = new HashSet<IOntoBase>();
		Set<Resource> instances = uOntology.getJenaModel().GetInstances(this.getResource(), inference);
		for (Resource inst : instances) {
			results.add(new OntoConcept(inst, uOntology));
		}
		return results;
	}

	public Set<IOntoProperty> getObjectProperties() {
		Set<IOntoProperty> plist = new HashSet<IOntoProperty>();
		List<IOntoLink> links;
		try {
			links = uOntology.GetDirectLinks(IOntoProperty.OntProperty.OBJECT, this, null, true);
			for (IOntoLink l : links) {
				if (!UrbanOntology.inList(l.getRelationship().getResource(), plist))
					plist.add((IOntoProperty) l.getRelationship());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return plist;
	}

	public Set<IOntoProperty> getDataProperties() {
		Set<IOntoProperty> plist = new HashSet<IOntoProperty>();
		List<IOntoLink> links;
		try {
			// links = uOntology.GetDirectLinks(IOntoProperty.OntProperty.DATA,
			// this, new OntoConcept(UrbanOntology.rdfs+"#Datatype",
			// uOntology,"",IOntoBase.OntType.DATATYPE), true);
			links = uOntology.GetDirectLinks(IOntoProperty.OntProperty.DATA, this, null, true);
			for (IOntoLink l : links) {
				if (!UrbanOntology.inList(l.getRelationship().getResource(), plist))
					plist.add((IOntoProperty) l.getRelationship());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return plist;
	}

	public Set<IOntoBase> getClassConcept(boolean inference) {
		Set<IOntoBase> results = new HashSet<IOntoBase>();
		Set<Resource> cls = uOntology.getJenaModel().GetDirectType(this.getResource());
		if (cls != null) {
			for (Resource res : cls) {
				IOntoBase obcl = new OntoConcept(res, uOntology);
				results.add(obcl);
				if (inference)
					results.addAll(obcl.getSuperConcepts());
			}
		}
		return results;
	}

	public boolean hasDataInstances() {
		return uOntology.getMapPopulated().exists(this.getURI());
	}

	public String getOntologyIdentifier() {
		Property prop = uOntology.getJenaModel().getProperty(UrbanOntology.core + "#identifier", false);
		Set<Resource> subproperties = uOntology.getJenaModel().GetSubHierarchy(prop, true, false);
		for (Resource sp : subproperties) {
			RDFNode id = uOntology.getJenaModel().GetObject(this.getResource(),
					uOntology.getJenaModel().getProperty(sp.getURI(), false), false);
			if (id != null && id.isLiteral())
				return (id.asLiteral().getValue().toString());
		}
		return null;
	}

	public Set<String> getData(IOntoProperty property) {
		Set<Resource> subproperties = uOntology.getJenaModel().GetSubHierarchy(property.getResource(), true, false);
		Set<String> values = new HashSet<String>();
		for (Resource subp : subproperties) {
			Property p = uOntology.getJenaModel().getProperty(subp.getURI(), false);
			for (RDFNode n : uOntology.getJenaModel().GetObjects(this.getResource(), p, false)) {
				if (n.isLiteral())
					values.add(n.asLiteral().getLexicalForm());
			}
		}
		return values;
	}

	public Set<Resource> getObjects(IOntoProperty property) {
		Set<Resource> subproperties = uOntology.getJenaModel().GetSubHierarchy(property.getResource(), true, false);
		Set<Resource> values = new HashSet<Resource>();
		for (Resource subp : subproperties) {
			Property p = uOntology.getJenaModel().getProperty(subp.getURI(), false);
			for (RDFNode n : uOntology.getJenaModel().GetObjects(this.getResource(), p, false)) {
				if (n.isURIResource())
					values.add(n.asResource());
			}
		}
		return values;
	}

}
