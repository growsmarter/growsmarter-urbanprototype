package smartcities.semanticweb.ontologycommons.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.io.File;
import java.io.IOException;

import se.Index;
import se.IndexStructure;
import se.Tokenizer;
import se.resources.MyTokenizer;
import se.resources.MyStemmer;
import se.resources.MyFilter;
import se.resources.MyNormalizer;
import se.TermProcessor;
import se.resources.ConceptInfo;

class SearchEngine {
	static final String FILE = "index"; //_[var]
	private Map<String,IndexStructure> indexes = new HashMap<String,IndexStructure>(); //var-index
	private ArrayList<TermProcessor> processors = null;
	private Tokenizer tokenizer = null;
	
	public Tokenizer getTokenizer() {
		return tokenizer;
	}

	public ArrayList<TermProcessor> getProcessors() {
		return processors;
	}
	
	public Set<String> getVars() {
		return indexes.keySet();
	}

	public IndexStructure getIndex(String var) {
		return indexes.get(var);
	}
	
	public void setIndex(String var, IndexStructure index){
		indexes.put(var, index);
	}
	
	public void setProcessors(String stopwords) throws IOException{
		this.processors = new ArrayList<TermProcessor>();
		processors.add(new MyNormalizer());
		processors.add(new MyFilter(new File(stopwords)));
		processors.add(new MyStemmer());
		this.tokenizer = new MyTokenizer();
	}

	public void setIndex(Set<ConceptInfo> ontConcepts, String indexPath, String var) throws IOException {
		Index indexer = new Index();
		IndexStructure index = indexer.indexCollection(ontConcepts, tokenizer, processors,var);
		index.setWeight(getMinWeights(ontConcepts,var));
		this.setIndex(var, index);
		IndexStructure.saveIndex(new File(indexPath+"/index_"+var),index);
	}
	
	public IndexStructure readIndex(String indexPath, String var){
		return IndexStructure.readIndex(new File(indexPath+"/index_"+var));
	}
	
	private Set<String> getVars(Set<ConceptInfo> ontConcepts){
		Set<String> vars = new HashSet<String>();
		for (ConceptInfo ci: ontConcepts){
			vars.addAll(ci.getVars());
		}
		return vars;
	}
	
	private Double getMinWeights(Set<ConceptInfo> ontConcepts, String var){
		Double minWeigths = Double.MAX_VALUE;
		for (ConceptInfo ci: ontConcepts){
			if (ci.getVars().contains(var)){
				Double weight = ci.getWeight(var);
				if (weight<minWeigths)
					minWeigths= weight;
				}
			}
		return minWeigths;
	}
	
	public SearchEngine(Set<ConceptInfo> ontConcepts, String stopwords, String indexPath) throws IOException{
		setProcessors(stopwords);
		Set<String> vars = getVars(ontConcepts);
		for (String var:vars)
			setIndex(ontConcepts, indexPath, var);
	}
	
	public SearchEngine(String stopwords, String indexPath) throws IOException{
		setProcessors(stopwords);
		File[] resources = new File(indexPath).listFiles();
		for(File res: resources){
			if(res.isFile()){
				String[] tok = res.getName().split("_");
		    	if (tok.length > 1){
		    		String var = tok[tok.length-1];
		    		this.setIndex(var, readIndex(indexPath,var));
		    	}
		    }
		 }
	}
	
	public Map.Entry<String, Double>[] Query(String id, String query, String var){
		return se.Query.runQuery(id, query,this.getIndex(var) , tokenizer, this.processors);
	}
	
	public ConceptInfo getConceptInfo(String uri){
		ConceptInfo ci = new ConceptInfo(uri);
		for (String var:this.getVars()){
			ConceptInfo partialCi = this.getIndex(var).getConceptInfo(uri);
			if (partialCi != null)
				for (String ci_uri: partialCi.getContent(var).keySet()){
					ci.addVar(var, ci_uri, partialCi.getWeight(var), partialCi.getContent(var).get(ci_uri));
				}
		}
		return ci;
	}
	
	public Map<String,Map<String,String>> GetSnippet(String uri, String query, String openlabel, String closelabel){
		ConceptInfo ci = this.getConceptInfo(uri);
		if (ci != null)
			return ci.getSnippet(query, this.getTokenizer(), this.getProcessors(),openlabel,closelabel);
		return null;
	}

}
