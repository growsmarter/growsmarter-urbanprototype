package smartcities.semanticweb.ontologycommons.services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoRelationship;

public class ModelMap implements Serializable {

	static public int MAX_DEEP = 5; //numer of intermediate arcs (0 is the same node, 1 means they are connected directly...)
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//transient UrbanOntology uOntology = null; //source
	Map<String, HashMap<String, Set<String>>> map = new HashMap<String, HashMap<String, Set<String>>>();
	Map<String, HashMap<String, Set<String>>> inverseMap = new HashMap<String, HashMap<String, Set<String>>>();
	//Map<String,BitSet> connectionMap = new HashMap<String,BitSet>(); //conexiones entre cada par de nodos de map/inverseMap (no dirigidas)
	Map<String,Integer> indexConnectionMap = new HashMap<String, Integer>(); //relationship between BitSet index and concept in each connectionMap
	Map<Integer, Map<String,BitSet>> connectionMaps = new HashMap<Integer,Map<String,BitSet>>();
	
//	Map<String,Set<String>> enumTypeMap = new HashMap<String,Set<String>>(); //enum - main classes
//	
//	public Map<String,Set<String>> getEnumTypeMap(){
//		return enumTypeMap;
//	}
	
	public Map<String, HashMap<String, Set<String>>> getMap() {
		return map; // Relation of pair of concepts connected directly, and the properties that connect them. According to domain and ranges of properties. Not inferred
	}

	private void setMap(Map<String, HashMap<String, Set<String>>> map) {
		this.map = map;
	}

	public Map<String, HashMap<String, Set<String>>> getInverseMap() {
		return inverseMap;
	}
	
	public Map<Integer,Map<String,BitSet>> getConectionMaps() {
		return connectionMaps;
	}
	
	public Map<String,BitSet> getConectionMaps(int deep) {
		return connectionMaps.get(deep);
	}

	
	public Map<String,Integer> getIndexConnectionMap(){
		return indexConnectionMap;
	}
	
//	public String getCategory(String enumTypeUri){
//		if (this.getEnumTypeMap().containsKey(enumTypeUri) && map.containsKey(enumTypeUri)){
//			Set<String> rangesEnum = this.map.get(enumTypeUri).keySet();
//			Set<String> domainsEnum = new HashSet<String>();
//			if (this.inverseMap.containsKey(enumTypeUri))
//				domainsEnum = this.inverseMap.get(enumTypeUri).keySet();
//			for (String res: this.getEnumTypeMap().get(enumTypeUri)){
//				Set<String> ranges = new HashSet<String>();
//				if (this.map.containsKey(res))
//					ranges = this.map.get(res).keySet();
//				Set<String> domains = new HashSet<String>();
//				if (this.inverseMap.containsKey(res))
//					domains = this.inverseMap.get(res).keySet();
//				if (ranges.containsAll(rangesEnum) && domains.containsAll(domainsEnum))
//					return res;
////				else
////					System.out.println(enumTypeUri+" cannot be replaced by "+res);
//			}
//		}
//		return enumTypeUri;
//	}
//	
//	public Set<String> filterByCategory(Set<String> concepts){
//		Set<String> results = new HashSet<String>();
//		for (String uri:concepts){
//			results.add(this.getCategory(uri));
//		}
//		return results;
//	}
//	
//	public String filterByCategory(String concept){
//		Set<String> results = new HashSet<String>();
//		return this.getCategory(concept);
//	}
//
	
	private Map<String,BitSet> setConnectionMap(Map<String,Integer> indexConnectionMap, OntoFilter propfilter){
		Map<String,BitSet> connectionMap = new HashMap<String, BitSet>();
		for (String domain: indexConnectionMap.keySet()){
//			System.out.println("Processing "+domain);
//			if (domain.contains("http://www.ibm.com/ICP/Scribe/CoreV2#Building"))
//				System.out.println("Processing "+domain);
			BitSet bs = new BitSet(indexConnectionMap.keySet().size());
			connectionMap.put(domain, bs);
			if (map.containsKey(domain) && map.get(domain)!= null){
				for (String range: map.get(domain).keySet()){
					//System.out.println("Direct "+domain+" - "+range);
					
					if (indexConnectionMap.containsKey(range)){
//						if (domain.equals("http://www.ibm.com/ICP/Scribe/CoreV2#Building") && range.equals("http://www.ibm.com/ICP/Scribe/CoreV2#BuiltStructureOccupiedSurfaceMeasurement"))
//							domain = domain;
//						if (range.contains("LandLot"))
//							System.out.println("Processing "+range);
//						if (domain.contains("Building") && range.contains("Measure"))
//							System.out.println(domain+" - "+range);
						for (String prop: map.get(domain).get(range)){
							if (propfilter.IsEmpty() || !propfilter.IsValid(prop)){
								int rangeIndex = indexConnectionMap.get(range);
								bs.set(rangeIndex);
								break;
							}
						}
					}
				}
			}
			if (inverseMap.containsKey(domain) && inverseMap.get(domain)!= null){
				for (String range: inverseMap.get(domain).keySet()){
					//System.out.println("Inverse "+domain+" - "+range);
					if (indexConnectionMap.containsKey(range)){
						for (String prop: inverseMap.get(domain).get(range)){
							if (propfilter.IsEmpty() || !propfilter.IsValid(prop)){
								int rangeIndex = indexConnectionMap.get(range);
								bs.set(rangeIndex);
								break;
							}
						}
					}
				}
			}
		}
		return connectionMap;
	}

	private void setConnectionMap(UrbanOntology uOntology){
		int index = 0;
		for (String domain: map.keySet())
			if (!domain.equals(UrbanOntology.rdfs+"#Datatype"))
				indexConnectionMap.put(domain, index++);
		for (String domain: inverseMap.keySet())
			if (!indexConnectionMap.containsKey(domain))
				if (!domain.equals(UrbanOntology.rdfs+"#Datatype"))
					indexConnectionMap.put(domain, index++);

		//without attribute properties to calculate deeper connections
		Map<String,BitSet> connectionMapNoAtt = setConnectionMap(indexConnectionMap, uOntology.getFilterAttributeProperties());
		Map<String,BitSet> connectionMapAtt = setConnectionMap(indexConnectionMap, uOntology.getEmptyFilter());
		
		//this.connectionMaps.put(0, connectionMap);
		
		
		Map<String, BitSet> lastConnMap = connectionMapNoAtt;
		for (int i=2;i<=MAX_DEEP;i++){
			Map<String, BitSet> connMap = IntersectConnectionMap(lastConnMap, connectionMapAtt);
			this.connectionMaps.put(i,connMap);
			lastConnMap = IntersectConnectionMap(lastConnMap, connectionMapNoAtt);
		}
		
		//with attribute properties
		//connectionMap = setConnectionMap(indexConnectionMap, uOntology.getEmptyFilter());
		this.connectionMaps.put(0, connectionMapAtt);
		this.connectionMaps.put(1, connectionMapAtt);
	}
	
	private Map<String, BitSet> IntersectConnectionMap(Map<String,BitSet> map1, Map<String,BitSet> map2){
		Map<String, BitSet> result = new HashMap<String, BitSet>();
		for (String concept1: indexConnectionMap.keySet()){
//			if (concept1.contains("Building"))
//				System.out.println(concept1);
			result.put(concept1, (BitSet) map1.get(concept1).clone());
			BitSet bs1 = result.get(concept1);
			for (String concept2: indexConnectionMap.keySet()){
				BitSet bs2 = map2.get(concept2);
				if (bs1.intersects(bs2)){
					result.get(concept1).set(indexConnectionMap.get(concept2));
				}
//				else
//					System.out.println("Not connected "+concept1+" "+concept2);
			}
		}
		return result;
	}
	
	public boolean isConnected(Map<String, BitSet> connectionMap, String concept1, String concept2){
		if (!connectionMap.containsKey(concept1) || !connectionMap.containsKey(concept2))
			return false;
		return connectionMap.get(concept1).get(this.indexConnectionMap.get(concept2));
	}

	private void setInverseMap(){
		for (String firstKey: getMap().keySet()){
			HashMap<String, Set<String>> innerMap = getMap().get(firstKey);
			for (String secondKey: innerMap.keySet()){
				HashMap<String, Set<String>> innerInverseMap;
				if (this.inverseMap.containsKey(secondKey)){
					innerInverseMap = this.inverseMap.get(secondKey);
				} else {
					innerInverseMap = new HashMap<String, Set<String>>();
					this.inverseMap.put(secondKey, innerInverseMap);
				}
				innerInverseMap.put(firstKey, innerMap.get(secondKey));
			}
		}
	}
	
//	private void setEnumTypeMap(UrbanOntology uOntology){
//		for (String uri: this.getMap().keySet()){
//			IOntoBase ob = uOntology.GetOntoBase(uri);
//			if (ob.getType() == IOntoBase.OntType.ECLASS){
//				Set<String> categories = new HashSet<String>();
//				this.enumTypeMap.put(uri, categories);
//				boolean categoryFound = false;
//				List<Resource> parents = new ArrayList<Resource>();
//				parents.add(ob.getResource());
//				while (!categoryFound && parents.size() > 0){
//					Resource actual = parents.remove(parents.size()-1);
//					Set<Resource> par = uOntology.getJenaModel().GetSuperHierarchy(actual, false);
//					for (Resource res: par){
//						if (res != actual){
//							IOntoBase resOb = uOntology.GetOntoBase(res);
//							if (resOb.getType() == IOntoBase.OntType.ECLASS){
//								if (!parents.contains(resOb.getResource()))
//									parents.add(resOb.getResource());
//							} else if (resOb.getType() == IOntoBase.OntType.CLASS){
//								categories.add(resOb.getURI());
//								categoryFound = true;
//							}
//						}
//					}
//				} 
//			}
//		}
//	}
		
	//returns true if there has been changes
	public boolean UpdateAddNewLinks(Set<IOntoLink> links, UrbanOntology uOntology){
		boolean changes = false;
		for (IOntoLink link:links){
			HashMap<String,Set<String>> innerMap = null;
			if (this.getMap().containsKey(link.getDomain().getURI()))
				innerMap = this.getMap().get(link.getDomain().getURI());
			else {
				innerMap = new HashMap<String,Set<String>>();
				this.getMap().put(link.getDomain().getURI(), innerMap);
				changes = true;
			}
			Set<String> properties = null;
			if (innerMap.containsKey(link.getRange().getURI()))
				properties = innerMap.get(link.getRange().getURI());
			else {
				properties = new HashSet<String>();
				innerMap.put(link.getRange().getURI(), properties);
				changes = true;
			}
			if (!properties.contains(link.getRelationship().getURI())){
				properties.add(link.getRelationship().getURI());
				changes = true;
			}
			
		}
		if (changes){
			this.setInverseMap();
			this.setConnectionMap(uOntology);
		}
		return changes;
	}
	
	//returns true if there has been changes
	public boolean UpdateRemoveLinks(Set<IOntoLink> links, UrbanOntology uOntology){
		boolean changes = false;
		for (IOntoLink link:links){
			HashMap<String,Set<String>> innerMap = null;
			if (this.getMap().containsKey(link.getDomain().getURI())){
				innerMap = this.getMap().get(link.getDomain().getURI());
				if (innerMap.containsKey(link.getRange().getURI())){
					Set<String> properties = innerMap.get(link.getRange().getURI());
					if (!this.existsNoMap(link, uOntology)){
						changes = true;
						properties.remove(link.getRelationship().getURI());
						if (properties.isEmpty()){
							innerMap.remove(link.getRange().getURI());
							if (innerMap.isEmpty()){
								this.getMap().remove(link.getDomain().getURI());
							}
						}
					}
				}
			}
		}
		if (changes){
			this.setInverseMap();
			this.setConnectionMap(uOntology);
		}
		return changes;
	}
	
	public ModelMap(Map<String, HashMap<String, Set<String>>> map, UrbanOntology uOntology){
		this.setMap(map);
		this.setInverseMap();
		this.setConnectionMap(uOntology); 
		//this.setEnumTypeMap(uOntology);
	}
	
	
	
	public ModelMap(UrbanOntology uOntology, boolean withInstances){
			Set<IOntoProperty> allProperties = OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.ALL, true, false,uOntology);
			
			for (Resource res: uOntology.getJenaModel().listSubjects(false)) {
				//if (res.isURIResource() && ((res.getURI().equals(UrbanOntology.core+"#BuildingUseUnit") || res.getURI().equals(UrbanOntology.core+"#BuiltStructureOccupiedSurfaceMeasurement")) || res.getURI().equals(UrbanOntology.core+"#AreaMeasurement"))){
				//if (res.isURIResource() && (res.getURI().equals(UrbanOntology.core+"#LandLot") && withInstances)){ //TODO:REMOVE
			//for (Resource res: list){
				boolean withInst = withInstances;
				if (res.isURIResource() && !UrbanOntology.IsGeneralConcept(res.getURI())){
					String uridomain = res.getURI();
					if (uridomain.equals(UrbanOntology.geo+"#Geometry")) //any new geometry can be created while the user is using the app
						withInst = false;
					IOntoBase domain = new OntoConcept(res,uOntology);
					if (domain.getType() == IOntoBase.OntType.CLASS || domain.getType() == IOntoBase.OntType.ECLASS){
						if (!withInst || (withInst && existsNoMap(domain, uOntology))){
							//System.out.println("Processing: "+res.getURI());
							HashMap<String, Set<String>> innerMap;
							if (map.containsKey(uridomain))
								innerMap = map.get(uridomain);
							else {
								innerMap = new HashMap<String, Set<String>>();
								map.put(uridomain, innerMap);
							}
							
							List<IOntoLink> links = GetDirectLinksNoMap(allProperties, domain, null, withInst, uOntology);
							for (IOntoLink l:links){
								//if (l.getRelationship().getURI().contains("numberOfResidents") && l.getDomain().getURI().contains("LandLot")) //TODO:REMOVE
									//l=l; //TODO:REMOVE
								//System.out.println(l.getRange().getURI()+" "+l.getRange().getType().toString());
								String urirange;
								if (l.getRange().getType() == IOntoBase.OntType.DATATYPE) //TODO: check this out!
									urirange = UrbanOntology.rdfs+"#Datatype";
								else
									 urirange = l.getRange().getURI();
								//String uriproperty = l.getRelationship().getURI();
								Set<String> propertyList;
								if (innerMap.containsKey(urirange))
									propertyList = innerMap.get(urirange);
								else {
									propertyList = new HashSet<String>();
									innerMap.put(urirange, propertyList);
								}
								//propertyList.add(uriproperty);
								for (Resource superproperty:uOntology.getJenaModel().GetSuperHierarchy(l.getRelationship().getResource(), true))
									propertyList.add(superproperty.getURI());
								
								
							
							}
						}
					}
				}
				withInst = withInstances;
			//}//TODO:REMOVE 
			}
			//this.uOntology = uOntology;
			//new ModelMap(map);
			this.setInverseMap();
			this.setConnectionMap(uOntology);
			//this.setEnumTypeMap(uOntology);
//			this.getMap();
//			this.getInverseMap();
		}
	
	private HashMap<String,Set<String>> AddPath(Integer actualDeep, String range, String[] actualResult, ArrayList<Object[]> lastNodes, Boolean lastPropDir, Boolean lastPropAtt, HashMap<String,Set<String>> connected){
		boolean inpath = false;
		for (int i=0;i<=actualDeep;i++)
			if (actualResult[i].equals(range)){
				inpath = true;
				break;
			}
		if (!inpath){ //to avoid cycles 
			Object[] newItem = new Object[4];
			newItem[0] = range;
			newItem[1] = actualDeep+1;
			newItem[2] = lastPropDir;
			newItem[3] = lastPropAtt;
			lastNodes.add(newItem);
		}
		for (int i=0;i<=actualDeep;i++){
			for (int j=i+2;j<=actualDeep;j++){
				Set<String> innerList;
				if (connected.containsKey(actualResult[i]))
					innerList = connected.get(actualResult[i]);
				else {
					innerList = new HashSet<String>();
					connected.put(actualResult[i], innerList);
				}
				innerList.add(actualResult[j]);
			}
				
		}
		return connected;
	}
	
	
	public Map<Integer, Set<String[]>> GetPaths(String origin, String end, int maxDeep, OntoFilter filterAttributeProperties, OntoFilter filterPlanningPerspective, boolean onlyFirst, HashMap<String,Set<String>> connected, UrbanOntology uOntology){
		origin = uOntology.filterByCategory(origin);
		end = uOntology.filterByCategory(end);
		Map<Integer,Set<String[]>> results = new HashMap<Integer,Set<String[]>>(); //pairs deep-set of paths 
		String[] actualResult = new String[maxDeep+1];
		ArrayList<Object[]> lastNodes = new ArrayList<Object[]>();
		Object[] item = new Object[4]; //uri deep dirlastprop (true:from origin to end) ifhasAttproperty(true:hasAttribute)
		item[0]= origin;
		item[1] = 0;
		item[2] = true;
		item[3] = false;
		lastNodes.add(item);
		
		do{
			int lastIndex = lastNodes.size()-1; //Deep search
			//int lastIndex = 0; //Broad search
			Object[] itemToProcess = lastNodes.get(lastIndex);
			String actualDomain = (String)itemToProcess[0];
			Integer actualDeep = (Integer)itemToProcess[1];
			Boolean lastPropDir = (Boolean)itemToProcess[2];
			Boolean lastPropAtt = (Boolean)itemToProcess[3];
			actualResult[actualDeep]= actualDomain;
			lastNodes.remove(lastIndex);
			if (actualDomain.equals(end)){
				for (int i=actualDeep+1;i<=maxDeep;i++)
					actualResult[i]=null;
				Set<String[]> previousResults;
				if (results.containsKey(actualDeep))
					previousResults = results.get(actualDeep);
				else{
					previousResults = new HashSet<String[]>();
				}
				previousResults.add(actualResult.clone());
				results.put(actualDeep, previousResults);
				if (onlyFirst)
					return results;
			} else if (actualDeep < maxDeep && (!lastPropAtt) && !UrbanOntology.IsGeneralConcept(actualDomain) && filterPlanningPerspective.IsValid(actualDomain)){
			//} else if (actualDeep < maxDeep && (!lastPropAtt) && !UrbanOntology.IsGeneralConcept(actualDomain)){
				Map<String,Set<String>> m1 = this.map.get(actualDomain);
				if (m1 != null){
					for (String concept: uOntology.filterByCategory(m1.keySet())){
						boolean path = false;
//						if (concept.contains("BuildUnitAreaMeasurement"))
//							concept = concept;
						//if (m1.containsKey(concept)){
							for (String prop: m1.get(concept)){
								if (filterAttributeProperties.IsEmpty() || !filterAttributeProperties.IsValid(prop)){
									connected = AddPath(actualDeep, concept, actualResult, lastNodes, true, false, connected);
									path = true;
									break;
									}
							}
						//} else System.out.println("Domain:"+actualDomain+" Range:"+concept+" not found");
						
						if (!path){
							connected = AddPath(actualDeep, concept, actualResult, lastNodes, true, true, connected);
						}
					}
				}
				//si todas las propiedades tienen la inversa, esto es redundante
//				Map<String,Set<String>> m2 = this.inverseMap.get(actualDomain);
//				if (m2!=null){
//					boolean path = false;
//					for (String concept: m2.keySet()){
//						for (String prop: m2.get(concept))
//							if (!filterAttributeProperties.IsValid(prop)){
//							//if (true){
//								//ranges.add(concept);
//								AddPath(actualDeep, concept, actualResult, lastNodes, false, false);
//								path = true;
//								break;
//							} 
//						if (!path){
//							AddPath(actualDeep, concept, actualResult, lastNodes, false, true);
//							break;
//						}
//					}
//				}
			}
		}while (lastNodes.size()>0);
		return results;
	}
	
//	public boolean GetPaths(String origin, String end, int maxDeep, OntoFilter filterAttributeProperties, OntoFilter filterPlanningPerspective, HashMap<String,Set<String>> connected){
//		String[] actualResult = new String[maxDeep+1];
//		ArrayList<Object[]> lastNodes = new ArrayList<Object[]>();
//		Object[] item = new Object[4]; //uri deep dirlastprop (true:from origin to end) ifhasAttproperty(true:hasAttribute)
//		item[0]= origin;
//		item[1] = 0;
//		item[2] = true;
//		item[3] = false;
//		lastNodes.add(item);
//		
//		do{
//			int lastIndex = lastNodes.size()-1; //Deep search
//			//int lastIndex = 0; //Broad search
//			Object[] itemToProcess = lastNodes.get(lastIndex);
//			String actualDomain = (String)itemToProcess[0];
//			Integer actualDeep = (Integer)itemToProcess[1];
//			Boolean lastPropDir = (Boolean)itemToProcess[2];
//			Boolean lastPropAtt = (Boolean)itemToProcess[3];
//			actualResult[actualDeep]= actualDomain;
//			lastNodes.remove(lastIndex);
//			if (actualDomain.equals(end)){
//				return true;
//			} else if (actualDeep < maxDeep && (!lastPropAtt) && !UrbanOntology.IsGeneralConcept(actualDomain) && filterPlanningPerspective.IsValid(actualDomain)){
//			//} else if (actualDeep < maxDeep && (!lastPropAtt) && !UrbanOntology.IsGeneralConcept(actualDomain)){
//				Map<String,Set<String>> m1 = this.map.get(actualDomain);
//				if (m1 != null){
//					boolean path = false;
//					for (String concept: m1.keySet()){
//						for (String prop: m1.get(concept)){
//							if (!filterAttributeProperties.IsValid(prop)){
//								connected = AddPath(actualDeep, concept, actualResult, lastNodes, true, false, connected);
//								if (concept.equals(end))
//									return true;
//								path = true;
//								break;
//								}
//						}
//						if (!path){
//							connected = AddPath(actualDeep, concept, actualResult, lastNodes, true, true, connected);
//							if (concept.equals(end))
//								return true;
//							
//						}
//					}
//				}
//			}
//		}while (lastNodes.size()>0);
//		return false;
//	}
//
	
	private boolean existsNoMap(IOntoBase concept, UrbanOntology uOntology){
		
		if (concept == null)
			return false;
		if (concept.getType() == IOntoBase.OntType.ECLASS)
			return true;
		if (concept.getType() == IOntoBase.OntType.DINDIVIDUAL || concept.getType() == IOntoBase.OntType.EINDIVIDUAL)
			return true;
		if (concept.getType() == IOntoBase.OntType.DATATYPE) //Assumption: every datatype in the ontology is populated
			return true;
		//Model ontology =uOntology.getJenaModel().getModel();
		//Dataset ds = uOntology.getJenaModel().getDbModel();
		Property ppopulated = uOntology.getJenaModel().getProperty(UrbanOntology.core+"#currentlyPopulated",false);
		RDFNode numberInst = uOntology.getJenaModel().GetObject(concept.getResource(), ppopulated, false); //Assumption about the existence of this attribute is not made
		if (numberInst != null)
			return true;
		if (concept.getType() == IOntoBase.OntType.CLASS){
			if (uOntology.getJenaModel().PopulatedConcept(concept))
				return true;
//			Set<Resource> instances = uOntology.getJenaModel().GetDirectConceptsOfType(concept.getResource());
//			if (instances != null && instances.size() > 0)
//				return true;
			}
		if (concept.getType() == IOntoBase.OntType.PROPERTY){
			Property p = uOntology.getJenaModel().getProperty(concept.getURI(),false);
			if (p == null)
				return false;
			Set<Resource> instances = uOntology.getJenaModel().GetSubjects(null, p, false);
			for (Resource i:instances){
				IOntoBase ob = new OntoConcept(i,uOntology);
				if (ob.getType() == IOntoBase.OntType.DINDIVIDUAL || ob.getType() == IOntoBase.OntType.EINDIVIDUAL)
					return true;
			}
		}
		return false;
	}
	
	private boolean existsNoMap(String uri , UrbanOntology uOntology){
		Resource concept = uOntology.getJenaModel().getResource(uri, false);
		return existsNoMap(new OntoConcept(concept,uOntology) , uOntology);
	}

//"ASK {?ind1 <"+RDF.type+"> <"+link.getDomain().getURI()+"> . ?ind2 <"+RDF.type+"> <"+link.getRange().getURI()+"> . 
//	?ind1 <"+link.getRelationship().getURI()+"> ?ind2}";
private boolean Ask1(IOntoBase domain, IOntoBase range, IOntoBase property, UrbanOntology uOntology){
	return uOntology.getJenaModel().PopulatedLink(domain, property, range);
}

//"ASK {?ind1 <"+RDF.type+"> <"+link.getDomain().getURI()+"> . ?ind1 <"+link.getRelationship().getURI()+"> ?value}";
private boolean Ask2(IOntoBase domain, IOntoBase property, UrbanOntology uOntology){
	return uOntology.getJenaModel().PopulatedDomainProp(domain, property);
}

//"ASK {?ind2 <"+RDF.type+"> <"+link.getRange().getURI()+"> . ?any <"+link.getRelationship().getURI()+"> ?ind2}";
private boolean Ask3(IOntoBase range, IOntoBase property, UrbanOntology uOntology){
	return uOntology.getJenaModel().PopulatedRangeProp(range, property);
}



private boolean existsNoMap(IOntoLink link , UrbanOntology uOntology){
		
		if (link.getDomain() == null && (link.getRange() == null || link.getRange().getType() == IOntoBase.OntType.DATATYPE))
			return existsNoMap(link.getRelationship(), uOntology);

		//String query;
		if (link.getDomain() != null && link.getRange() != null){
			if (!link.getRange().getResource().getLocalName().equals("Literal") &&  link.getRange().getType() != IOntoBase.OntType.DATATYPE){
				//query = "ASK {?ind1 <"+RDF.type+"> <"+link.getDomain().getURI()+"> . ?ind2 <"+RDF.type+"> <"+link.getRange().getURI()+"> . ?ind1 <"+link.getRelationship().getURI()+"> ?ind2}";
				return Ask1(link.getDomain(), link.getRange(), link.getRelationship(), uOntology);
			} else { 
				//query = "ASK {?ind1 <"+RDF.type+"> <"+link.getDomain().getURI()+"> . ?ind1 <"+link.getRelationship().getURI()+"> ?value}";
				return Ask2(link.getDomain(),link.getRelationship(),uOntology);
			}
			
		} else if (link.getRange() == null){
			//query = "ASK {?ind1 <"+RDF.type+"> <"+link.getDomain().getURI()+"> . ?ind1 <"+link.getRelationship().getURI()+"> ?any}";
			return Ask2(link.getDomain(),link.getRelationship(),uOntology);
		} else { //domain = null && range != datatype
			//query = "ASK {?ind2 <"+RDF.type+"> <"+link.getRange().getURI()+"> . ?any <"+link.getRelationship().getURI()+"> ?ind2}";
			return Ask3(link.getRange(), link.getRelationship(),uOntology);
		}
		//return uOntology.getJenaModel().AskQueryOntology(query, false);
	}

	
	private List<IOntoLink> GetDirectLinksNoMap(Set<IOntoProperty> properties, IOntoBase classDomain, IOntoBase classRange, boolean withInstances , UrbanOntology uOntology){
		List<IOntoLink> results = new ArrayList<IOntoLink>();
		
		for (IOntoProperty p:properties){
			if (classDomain!= null && classRange != null){
				if (OntoProperty.IsValidProperty(p, classDomain.getResource(), classRange.getResource()))
					results.add(new OntoLink(p,classDomain, classRange,uOntology));
			}else if (classDomain != null){
				if (OntoProperty.IsValidProperty(p, classDomain.getResource(), null))
					for (Resource range:p.getRange()){
						if (!UrbanOntology.existLink(p.getResource(),classDomain.getResource(),range, results))
							results.add(new OntoLink(p,classDomain, new OntoConcept(range,uOntology),uOntology));
					}
			} else if (classRange != null){
				if (OntoProperty.IsValidProperty(p, null,classRange.getResource()))
					for (Resource domain:p.getDomain()){
						if (!UrbanOntology.existLink(p.getResource(),domain,classRange.getResource(), results))
							results.add(new OntoLink(p,new OntoConcept(domain,uOntology),classRange,uOntology));
					}
			} else { //both null
				if (OntoProperty.IsValidProperty(p, null,null))
					for (Resource domain:p.getDomain())
						for (Resource range: p.getRange())
							if (!UrbanOntology.existLink(p.getResource(),domain,range, results))
								results.add(new OntoLink(p,new OntoConcept(domain,uOntology),new OntoConcept(range,uOntology),uOntology));
				
			}
		}
		List<IOntoLink> toremove = new ArrayList<IOntoLink>();
		
		//filter with instances
		if (withInstances)
			for (IOntoLink link:results){
				if (!existsNoMap(link, uOntology))
						toremove.add(link);
			}
		results.removeAll(toremove);
		
		//add superproperties with same domain/range. TARDA DEMASIADO
//		Set<IOntoLink> toadd = new HashSet<IOntoLink>();
//		for (IOntoLink result: results){
//			Resource prop = result.getRelationship().getResource(); //en este punto no hay funciones, sólo properties
//			Set<Resource> superproperties = uOntology.getJenaModel().GetSuperHierarchy(prop, true);
//			for (Resource supp:superproperties){
//				toadd.add(new OntoLink(uOntology.GetOntoProperty(uOntology.GetOntoBase(supp)), result.getDomain(),result.getRange()));
//			}
//		}
		
		
		return results;
	}

	
	public static ModelMap DeserializeObject(String filePath) throws IOException, ClassNotFoundException{
        FileInputStream fileIn = new FileInputStream(filePath);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        ModelMap obj = (ModelMap) in.readObject();
        in.close();
        fileIn.close();
        return obj;
	}
	
	public static void SerializeObject(ModelMap obj, String filePath) throws IOException{
	     FileOutputStream fileOut = new FileOutputStream(filePath);
	     ObjectOutputStream out = new ObjectOutputStream(fileOut);
	     out.writeObject(obj);
	     out.close();
	     fileOut.close();
	}

	public boolean exists(IOntoLink link){
		//if link.getDomain is null or link.getRange is null, then we assume that whatever range/domain is valid
		IOntoBase domain = link.getDomain();
		IOntoBase range = link.getRange();
		IOntoRelationship rel = link.getRelationship();
		if (domain == null && rel == null && range != null)
			return exists(range);
		if (domain == null && rel != null && range == null)
			return exists(rel);
		if (domain != null && rel == null && range == null)
			return exists(domain);
		if (domain != null && rel != null && range != null){
			HashMap<String,Set<String>> innerMap = map.get(domain.getURI());
			if (innerMap != null){
				Set<String> innerProperties = innerMap.get(range.getURI());
				if (innerProperties != null){
					for (String innerP:innerProperties){
						if (innerP.equals(rel.getURI()))
							return true;
						}
				}
			}
			return false;
		}
		if (domain == null && rel != null && range != null){
			HashMap<String,Set<String>> innerMap = this.inverseMap.get(range.getURI());
			if (innerMap != null){
				Set<String> innerProperties = new HashSet<String>(); 
				for (String d: innerMap.keySet())
					innerProperties.addAll(innerMap.get(d));
				for (String innerP:innerProperties){
					if (innerP.equals(rel.getURI()))
						return true;
					}
				
			}
			return false;
		}
		if (domain != null && rel == null && range != null){
			HashMap<String,Set<String>> innerMap = map.get(domain.getURI());
			if (innerMap != null){
				Set<String> innerProperties = innerMap.get(range.getURI());
				if (innerProperties != null)
					return true;
			}
			return false;
		}
		if (domain != null && rel != null && range == null){ //TODO: check
			HashMap<String,Set<String>> innerMap = map.get(domain.getURI());
			if (innerMap != null){
				Set<String> innerProperties = new HashSet<String>(); 
				for (String r: innerMap.keySet())
					innerProperties.addAll(innerMap.get(r));
				for (String innerP:innerProperties){
					if (innerP.equals(rel.getURI()))
						return true;
					}
			}
			return false;
		}
		return false; //all of them null
	}
	
	public Set<String> getProperties(String domain, String range){
		Set<String> properties = new HashSet<String>();
		HashMap<String, Set<String>> innerMap = this.getMap().get(domain);
		if (innerMap != null){
			Set<String>  innerMapProp = innerMap.get(range);
			if (innerMapProp != null){
				properties.addAll(innerMapProp);
			}
		}
		return properties;
	}
	
	public Set<String> getRangesFromDomain(String uri){
		Set<String> ranges = new HashSet<String>();
		HashMap<String, Set<String>> innerMap = this.getMap().get(uri);
		if (innerMap != null && innerMap.size() > 0)
				ranges.addAll(innerMap.keySet());
		return ranges;
	}
	
	public Set<String> getRangesFromDomain(IOntoBase concept){
		return getRangesFromDomain(concept.getURI());
	}
	
	public Set<String> getDomainsFromRange(String uri){
		Set<String> domains = new HashSet<String>();
		for (String domain:this.getMap().keySet()){
			HashMap<String, Set<String>> innerMap = this.getMap().get(domain);
			if (innerMap != null && innerMap.size() > 0)
				for (String range:innerMap.keySet()){
					if (range.equals(uri)){
						domains.add(domain);
					}
				}
			}
		return domains;
	}
	
	public Set<String> getDomainsFromRange(IOntoBase concept){
		return getDomainsFromRange(concept.getURI());
	}
	
	public boolean exists(IOntoBase concept){
		return exists(concept.getURI());
	}

	public boolean exists(String uri){
		if (this.getMap().keySet().contains(uri))
			return true;
//		for (String uriclass:this.getMap().keySet()){
//			if (uri.equals(uriclass)){
//				//HashMap<String, Set<String>> innerMap = this.getMap().get(uriclass);
//				//if (innerMap != null)
//					return true;
//			}
//		}
		return false;
	}
	
	
	public List<IOntoLink> GetDirectLinks(Set<IOntoProperty> properties, IOntoBase classDomain, IOntoBase classRange, UrbanOntology uOntology, OntoFilter filter){
		List<IOntoLink> results = new ArrayList<IOntoLink>();
		Map<String, HashMap<String,Set<String>>> map = this.getMap();
		if (classDomain == null && classRange == null) {
			for (String domain: uOntology.filterByCategory(map.keySet())){
				HashMap<String,Set<String>> innerMap = map.get(domain);
				for (String range:uOntology.filterByCategory(innerMap.keySet())){
					if (filter.IsEmpty() || (filter.IsValid(domain) && (range.equals(UrbanOntology.rdfs+"#Datatype") || filter.IsValid(range)))){
						Set<String> innerProperties = innerMap.get(range);
						for (String innerP:innerProperties){
							for (IOntoProperty op:properties){
								if (innerP.equals(op.getURI()))
									results.add(new OntoLink(op,new OntoConcept(domain,uOntology),new OntoConcept(range,uOntology),uOntology));
							}
						}
					}
				}
			}
		} else if (classDomain != null && classRange == null){
			HashMap<String,Set<String>> innerMap = map.get(classDomain.getURI());
			if (innerMap != null){
				for (String range:uOntology.filterByCategory(innerMap.keySet())){
					if (filter.IsEmpty() || (filter.IsValid(classDomain.getURI()) && (range.equals(UrbanOntology.rdfs+"#Datatype") || filter.IsValid(range)))){
						Set<String> innerProperties = innerMap.get(range);
						for (String innerP:innerProperties){
							for (IOntoProperty op:properties){
								if (innerP.equals(op.getURI()))
									results.add(new OntoLink(op,classDomain,new OntoConcept(range,uOntology),uOntology));
							}
						}
					}
				}
			}
		} else if (classDomain == null && classRange != null){
			for (String domain: uOntology.filterByCategory(map.keySet())){
				HashMap<String,Set<String>> innerMap = map.get(domain);
				for (String range:innerMap.keySet()){
					if (filter.IsEmpty() || (filter.IsValid(domain) && (classRange.getURI().equals(UrbanOntology.rdfs+"#Datatype") || filter.IsValid(classRange.getURI())))){
						if (range.equals(classRange.getURI())){
							Set<String> innerProperties = innerMap.get(range);
							for (String innerP:innerProperties){
								for (IOntoProperty op:properties){
									if (innerP.equals(op.getURI()))
										results.add(new OntoLink(op,new OntoConcept(domain,uOntology),classRange,uOntology));
								}
							}
						}
					}
				}
			}
		} else { //classDomain != null && classRange != null
			HashMap<String,Set<String>> innerMap = map.get(classDomain.getURI());
			if (innerMap != null){
				Set<String> innerProperties = innerMap.get(classRange.getURI());
				if (filter.IsEmpty() || (filter.IsValid(classDomain.getURI()) && (classRange.getURI().equals(UrbanOntology.rdfs+"#Datatype") || filter.IsValid(classRange.getURI())))){
					if (innerProperties != null){
						for (String innerP:innerProperties){
							for (IOntoProperty op:properties){
								if (innerP.equals(op.getURI()))
									results.add(new OntoLink(op,classDomain,classRange,uOntology));
							}
						}
					}
				}
			}
			
			
		}
		return results;
	}

	


	

}
