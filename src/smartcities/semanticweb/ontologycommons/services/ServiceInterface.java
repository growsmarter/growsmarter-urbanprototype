package smartcities.semanticweb.ontologycommons.services;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.*;

import smartcities.semanticweb.ontologycommons.data.DataGeneration;
import se.resources.ConceptInfo;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoRelationship;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;

public class ServiceInterface {

	final String NO_ONT = "No ontology loaded";

	static UrbanOntology ontology = null;
	static boolean loadedOntology = false;

	public static List<IOntoLink> FilterLinks(List<IOntoLink> links) {
		if (loadedOntology)
			return ontology.FilterLinks(links);
		return links;
	}

	public static Properties getProperties(String configfile) throws IOException {
		FileReader fr = null;
		Properties prop = null;
		try {
			prop = new Properties();
			fr = new FileReader(configfile);
			// is = getClass().getClassLoader().getResourceAsStream(configfile);
			prop.load(fr);
		} catch (Exception e) {
			System.err.println("Exception: " + e);
		} finally {
			fr.close();
		}
		return prop;
	}

	public static UrbanOntology GenerateResources(String configfile) throws Exception {
		Properties properties = getProperties(configfile);
		return DataGeneration.GenerateData(properties);
	}

	// start app from existing UrbanOntology
	public static void CreateOntBroker(UrbanOntology uOntology) throws Exception {
		ontology = uOntology;
		if (ontology != null) {
			loadedOntology = true;
		} else
			throw (new Exception("FATAL ERROR: urban ontology not created"));
	}

	// start app from existing resources
	public static void CreateOntBroker(String configfile) throws Exception {
		try {
			Properties properties = getProperties(configfile);
			ontology = new smartcities.semanticweb.ontologycommons.services.UrbanOntology(properties);
			if (ontology != null) {
				loadedOntology = true;
			}
			// ontBroker = new services.OntologyBroker(name, ontUri, ontFile,
			// policyFile, appPath, pathGeoFiles, pathIndFiles, modelMapsPath,
			// searchPath);
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("FATAL ERROR: urban ontology not created");
			throw (e);
		}
	}

	public static String getParliamentGraph() {
		if (OntologyLoaded()) {
			return ontology.parliamentmodel;
		}
		return null;
	}

	public static boolean OntologyLoaded() {
		return loadedOntology;
	}

	// public static String GetOntBrokerName(){
	// if (ontBroker != null)
	// return ontBroker.getName();
	// return null;
	// }

	// public static String GetOntUri(){
	// if (ontBroker != null)
	// return ontBroker.getOntURI();
	// return null;
	// }

	public static String PrefixBlock() {
		if (loadedOntology)
			return ontology.getStringPrefixes(ontology.getAllPrefixes(true));
		return null;
	}

	public static QueryResults Query(String sparqlQ, boolean addPrefixes, String idSession) {
		if (loadedOntology)
			return ontology.GeneralQuery(sparqlQ, addPrefixes, idSession);
		return null;
	}

	// TODO:DEPRECATED
	public static HashMap<String, ArrayList<String>> GetUrbanPlanningPerspectiveConcepts() {
		if (loadedOntology)
			return ontology.GetUrbanPlanningPerspectiveConcepts();
		return null;
	}

	// TODO:DEPRECATED
	public static QueryResults GetInstancesIntersectPolygon(String URIClass, String wktPolygon) throws IOException {
		if (loadedOntology)
			return ontology.GetInstancesWithinPolygon(URIClass, wktPolygon);
		return null;
	}

	// TODO:DEPRECATED
	public static QueryResults GetWKTSelectionGeometries(String type) throws IOException {
		if (loadedOntology)
			return ontology.GetWkt(type);
		return null;
	}

	// public static Set<IOntoBase> GetInstances(String uriClass, boolean
	// inference){
	// if (ontBroker !=null)
	// return (ontBroker.GetInstances(uriClass, inference));
	// return null;
	// }

	// TODO: DEPRECATED
	public static String GetLabel(String uri) throws IOException {
		if (loadedOntology) {
			IOntoBase ob = new OntoConcept(uri, ontology);
			if (ob != null)
				return ob.getLabel();
			// return ontology.GetOntoBase(uri).getLabel();
			// return ontology.GetLabel(uri);
		}
		return null;
	}

	// TODO: DEPRECATED
	public static QueryResults GetMatchProperties(String inputProperty, String idSession) throws IOException {
		if (loadedOntology)
			return ontology.GetMatchProperties(inputProperty, idSession);
		return null;
	}

	// TODO: DEPRECATED
	public static QueryResults GetRelatedConcepts(String ptype, String uriclass, boolean populated) throws IOException {
		if (loadedOntology)
			return ontology.GetRelatedConcepts(ptype, uriclass, populated);
		return null;
	}

	// TODO: DEPRECATED
	public static String GetClassOfIndividual(String uriIndividual) throws IOException {
		if (loadedOntology) {
			IOntoBase ob = GetConcept(uriIndividual);
			Set<IOntoBase> results = ob.getClassConcept(false);
			for (IOntoBase res : results)
				return res.getURI();
		}
		return null;

		// if (ontBroker !=null)
		// return (ontBroker.GetClassOfIndividual(uriIndividual));
		// return null;
	}

	// TODO: DEPRECATED
	public static QueryResults GetMatchConcepts(String keyword, String idSession) {
		if (loadedOntology)
			return ontology.GetMatchConcepts(keyword, idSession);
		return null;
	}

	public static boolean IsGeneralConcept(String uri) {
		return UrbanOntology.IsGeneralConcept(uri);
	}

	public static List<String> GetAllUris() throws IOException {
		if (loadedOntology)
			return (ontology.GetAllUris());
		return null;
	}

	public static ConceptInfo GetIndexedConcept(String uri) {
		if (loadedOntology)
			return (ontology.getSearchEngine().getConceptInfo(uri));
		return null;
	}

	public static void Finalize() throws IOException {
		if (loadedOntology)
			ontology.Finalize();
	}

	public static List<ISearchResult> SearchConcepts(String id, String query, boolean withInstances)
			throws IOException {
		if (loadedOntology)
			return ontology.SearchConcepts(id, query, withInstances);
		return null;
	}

	public static Map<String, Map<String, String>> GetSnippet(String uri, String query, String openlabel,
			String closelabel) {
		if (loadedOntology)
			return ontology.getSearchEngine().GetSnippet(uri, query, openlabel, closelabel);
		return null;
	}

	public static Map<IOntoBase, Set<IOntoBase>> GroupByHierarchy(Set<? extends IOntoBase> concepts) {
		if (loadedOntology)
			return ontology.GroupByHierarchy(concepts);
		return null;
	}

	public static ISearchResult GetSearchConcept(IOntoBase ob, double similarity) throws IOException {
		if (loadedOntology) {
			// ISearchResult isr = ontology.GetSearchConcept(ob);
			// SearchResult sr = new SearchResult(isr,similarity);
			SearchResult sr = new SearchResult(ob, similarity, ontology);
			return sr;
		}
		return null;
	}

	public static ISearchResult GetSearchConcept(ISearchResult sr, double similarity) throws IOException {
		if (loadedOntology) {
			// return new SearchResult(sr,similarity);
			return new SearchResult(sr.getOntoConcept(), similarity, ontology);
		}
		return null;
	}

	public static List<IIndicatorType> GetIndicatorTypes() throws IOException {
		if (loadedOntology)
			return (IndicatorType.GetIndicatorTypes(ontology));
		return null;
	}

	public static IIndicatorType GetIndicatorTypes(IOntoBase ind) throws IOException {
		if (loadedOntology)
			return (new IndicatorType(ind, ontology));
		return null;
	}

	public static IIndicator GetIndicator(IOntoBase ind) throws IOException {
		if (loadedOntology)
			return (new Indicator(ind, ontology));
		return null;
	}

	public static IIndicatorDescriptor GetIndicatorDescriptor(IOntoBase indDescriptor) throws IOException {
		if (loadedOntology)
			return (new IndicatorDescriptor(indDescriptor, ontology));
		return null;
	}

	public static IOntoBase GetConcept(String URI) throws IOException {
		if (loadedOntology)
			return (new OntoConcept(URI, ontology));
		return null;
	}

	// TODO: DEPRECATED
	public static IOntoBase GetSub(IOntoBase ob) {
		return ob;
	}

	// TODO: DEPRECATED
	public static IOntoBase GetSuper(IOntoBase ob) {
		return ob;
	}

	public static IOntoBase GetUrbanDimensionConcept() {
		if (loadedOntology)
			return (ontology.GetUrbanDimensionConcept());
		return null;
	}

	public static IOntoBase GetUrbanPlanningPerspective() {
		if (loadedOntology)
			return (ontology.GetUrbanPlanningPerspective());
		return null;

	}

	public static List<IIndicator> GetIndicatorConcepts() throws IOException {
		if (loadedOntology)
			return (Indicator.GetIndicators(ontology));
		return null;
	}

	public static Set<IIndicatorDescriptor> GetIndicatorDescriptors() throws IOException {
		if (loadedOntology)
			return (IndicatorDescriptor.GetIndicatorDescriptors(ontology));
		return null;
	}

	public static IOntoProperty GetProperty(IOntoBase concept) throws IOException {
		if (loadedOntology)
			return (new OntoProperty(concept, ontology));
		return null;
	}

	public static IOntoFunction GetFunction(IOntoBase concept, List<String> params) throws Exception {
		if (loadedOntology)
			return (new OntoFunction(concept, params, ontology));
		return null;
	}

	public static List<IOntoLink> GetDirectLinks(IOntoProperty property, IOntoBase classDomain, IOntoBase classRange,
			boolean withInstances) throws IOException {
		if (loadedOntology)
			return (ontology.GetDirectLinks(property, classDomain, classRange, withInstances));
		return null;
	}

	public static List<IOntoLink> GetDirectLinks(IOntoProperty.OntProperty ptype, IOntoBase classDomain,
			IOntoBase classRange, boolean withInstances) throws Exception {
		if (loadedOntology) {
			long t1 = System.currentTimeMillis();
			List<IOntoLink> result = ontology.GetDirectLinks(ptype, classDomain, classRange, withInstances);
			long t2 = System.currentTimeMillis();
			System.out.println("GetDirectLinks:" + (t2 - t1) / 1000);
			result = ontology.FilterLinks(result);
			return result;
		}
		return null;
	}

	public static Integer GetGraphResult(List<IOntoLink> graph, String idSession) throws IOException {
		if (loadedOntology) {
			long t1 = System.currentTimeMillis();
			Integer result = ontology.GetGraphResult(graph, idSession);
			long t2 = System.currentTimeMillis();
			System.out.println("GetGraphResult:" + (t2 - t1) / 1000);
			return result;
		}
		return null;
	}

	public static Integer GetGraphResult(IOntoBase node, String idSession) throws IOException {
		if (loadedOntology) {
			long t1 = System.currentTimeMillis();
			Integer result = ontology.GetGraphResult(node, idSession);
			long t2 = System.currentTimeMillis();
			System.out.println("GetGraphResult:" + (t2 - t1) / 1000);
			return result;
		}
		return null;
	}

	public static Integer GetGraphResult(Set<IOntoBase> nodes, List<IOntoLink> links, String idSession)
			throws IOException {
		if (loadedOntology) {
			long t1 = System.currentTimeMillis();
			Integer result = ontology.GetGraphResult(nodes, links, idSession);
			long t2 = System.currentTimeMillis();
			System.out.println("GetGraphResult:" + (t2 - t1) / 1000);
			return result;
		}
		return null;
	}

	public static void RemoveGraph(Integer idGraph) {
		if (loadedOntology)
			ontology.removeGraph(idGraph);
	}

	public static List<List<IOntoLink>> getSubgraphs(Integer idGraph) {
		if (loadedOntology)
			return (ontology.getSubgraphs(idGraph));
		return null;
	}

	public static Integer getRowsNumber(int idSubgraph, Integer idGraph) {
		if (loadedOntology)
			return (ontology.getRowsNumber(idSubgraph, idGraph));
		return null;
	}

	public static String getSparql(int idSubgraph, Integer idGraph) {
		if (loadedOntology)
			return (ontology.getSparql(idSubgraph, idGraph));
		return null;
	}

	public static Integer getSubgraphId(int idNode, Integer idGraph) {
		if (loadedOntology)
			return (ontology.getSubgraphId(idNode, idGraph));
		return null;
	}

	public static List<IOntoBase> GetGraphNodesToShow(List<IOntoLink> graph) {
		return (ontology.GetGraphNodesToShow(graph));
	}

	public static List<List<IOntoLink>> GetConnectedGraphs(List<IOntoLink> graph, Integer idGraph) {
		if (loadedOntology)
			return (ontology.GetConnectedGraphs(graph, idGraph));
		return null;
	}

	public static List<String> GetGraphDataURIS(int nodeId, int initRow, int endRow, Integer idGraph) {
		if (loadedOntology)
			return (ontology.GetGraphDataURIS(nodeId, initRow, endRow, idGraph));
		return null;
	}

	public static List<Integer> GetGraphDataRow(int nodeId, String uri, Integer idGraph) {
		if (loadedOntology)
			return (ontology.GetGraphDataRow(nodeId, uri, idGraph));
		return null;
	}

	public static List<Set<String>> GetGraphDataValues(int nodeId, String propertyUri, int initRow, int endRow,
			Integer idGraph) throws IOException {
		if (loadedOntology) {
			return ontology.GetGraphDataValues(nodeId, propertyUri, initRow, endRow, idGraph);
		}
		return null;
	}

	public static List<HashMap<String, Set<String>>> GetGraphDataValues(int nodeId, int initRow, int endRow,
			Integer idGraph) throws IOException {
		if (loadedOntology) {
			return ontology.GetGraphDataValues(nodeId, initRow, endRow, idGraph);
		}
		return null;
	}

	// TODO: DEPRECATED
	public static Set<IOntoProperty> GetDataProperties(IOntoBase concept) {
		return concept.getDataProperties();
	}

	// TODO: DEPRECATED
	public static Set<IOntoProperty> GetObjectProperties(IOntoBase concept) {
		return concept.getObjectProperties();
	}

	public static IOntoArea CreateArea(IOntoArea.OntArea atype, String wkt, String label) throws IOException {
		if (loadedOntology) {
			return OntoArea.CreateArea(atype, wkt, label, ontology);
		}
		return null;
	}

	// If ob has not any area associated, then the method returns null
	public static IOntoArea GetArea(IOntoBase ob) throws IOException {
		if (loadedOntology) {
			return new OntoArea(ob, ontology);
		}
		return null;
	}

	// TODO: DEPRECATED
	public static boolean isGeoConcept(IOntoBase ob) {
		return ob.isGeoConcept();
		// if (ontBroker !=null){
		// return ontBroker.isGeoConcept(ob);
		// }
		// return false;
	}

	public static IOntoLink CreateLink(IOntoBase domain, IOntoRelationship relationship, IOntoBase range) {
		if (loadedOntology)
			return new OntoLink(relationship, domain, range, ontology);
		return null;
	}

	public static IOntoLink CreateGeospatialLink(IOntoBase domain, IOntoFunction.OntFunction ftype, List<String> fargs,
			IOntoBase range) throws Exception {
		if (loadedOntology)
			return new OntoLink(ftype, fargs, domain, range, ontology);
		return null;
	}

	public static Map<Integer, Set<String[]>> GetPaths(IOntoBase domain, IOntoBase range, int maxDeep,
			boolean populated) {
		if (loadedOntology)
			return ontology.GetPaths(domain, range, maxDeep, populated);
		return null;
	}

	public static boolean existsPath(IOntoBase domain, IOntoBase range, int deep, boolean populated) {
		if (loadedOntology)
			return ontology.existsPath(domain, range, deep, populated);
		return false;
	}

	public static String GetCategory(String enumTypeUri) {
		if (loadedOntology)
			return ontology.getCategory(enumTypeUri);
		return null;
	}

	public static IIndicator CreateIndicator(IIndicator.ANCHOR_AREAS anchorArea, IIndicatorDescriptor indDesc,
			String areaId, Float value) {
		if (loadedOntology)
			return Indicator.CreateIndicator(anchorArea, value, indDesc, areaId, ontology);
		return null;
	}

	public static void RemoveIndicator(IIndicator indInstance) {
		if (loadedOntology)
			Indicator.RemoveIndicator(indInstance, ontology);
	}

	public static boolean CancelQuery(String idSession) throws ParseException, IOException {
		if (loadedOntology)
			return ontology.CancelQuery(idSession);
		return false;
	}

	public static boolean OnQuery(String idSession) {
		if (loadedOntology)
			return ontology.OnQuery(idSession);
		return false;
	}

	public static String OntologyReport(String separator, PrintStream out) {
		if (loadedOntology) {
			Set<Object[]> results = ontology.OntologyInfo();
			for (Object[] res : results) {
				for (int i = 0; i < res.length - 1; i++) {
					out.print(res[i].toString().replaceAll("http.*#", "") + separator);
				}
				for (int i = res.length - 1; i < res.length; i++) {
					out.print(res[i].toString().replaceAll("http.*#", ""));
				}
				out.println();
			}
		}
		return "";
	}

	// DEBUGGING

	public static void printConnectionData() {
		ontology.printConnectionData();
	}

	// MAXTIME TO OBTAIN RESULTS FROM THE QUERY. IN MILLISECONDS
	// public static Object[] QueryModel(String query, boolean onlyParliament,
	// int maxTime, String idSession){
	// try{
	//// long t1 = System.currentTimeMillis();
	// return ontology.QueryModel(query, onlyParliament, maxTime, idSession);
	//// long t2 = System.currentTimeMillis();
	//// System.out.println("Time (millis):"+(t2-t1));
	// //return (t2-t1);
	// }catch (Exception e){
	// System.out.println("Error:"+e.getMessage());
	// return new Object[]{-1,-1,-1};
	// }
	// }

	public static void createFilterViewPlanningPerspective() {
		ontology.createFilterViewPlanningPerspective();
	}

	public static void ProcessPairs() {
		ontology.ProcessPairs();
	}

	public static void TestCreateView() {
		ontology.TestCreateView();
	}

}
