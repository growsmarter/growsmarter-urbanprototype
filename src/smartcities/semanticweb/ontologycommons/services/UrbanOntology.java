package smartcities.semanticweb.ontologycommons.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import ontology_commons.tests.serviceInterface.library.PrintMethods;
import se.resources.ConceptInfo;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;
import smartcities.semanticweb.ontologycommons.services.SearchEngine;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.data.Mapping;

class OntoBaseComparator implements Comparator<IOntoBase> {

	public int compare(IOntoBase o1, IOntoBase o2) {
		return (o1.getId() - o2.getId());
	}
}

class URL_pairs_Comparator implements Comparator<Object[]> {

	public int compare(Object[] o1, Object[] o2) {
		return ((int) o1[1] - (int) o2[1]);
	}
}

class SearchResultComparator implements Comparator<ISearchResult> {
	public int compare(ISearchResult o1, ISearchResult o2) {
		return -(o1.getSimilarity().compareTo(o2.getSimilarity()));
	}
}

class HashKeyComparator implements Comparator<Map.Entry<String, List<String>>> {
	public int compare(Map.Entry<String, List<String>> o1, Map.Entry<String, List<String>> o2) {
		return (o2.getValue().size() - (o1.getValue().size()));
	}
}

public class UrbanOntology {
	JenaOntology jenaModel = null; // baseModel managed by Jena
	ParliamentOntology parliamentModel = null; // instance model managed by
												// Parliament
	String modelMapPopulatedPath = null;
	ModelMap map = null;
	ModelMap populatedMap = null;
	protected Map<Integer, QueryGraph> qGraphs = new HashMap<Integer, QueryGraph>();
	protected Integer nGraphs = 0;
	protected static SearchEngine sengine = null;
	OntoFilter filterPlanningPerspective = null;
	OntoFilter filterConceptDimension = null;
	OntoFilter filterAttributeProperties = null;

	final static String bcninst = "http://www.ibm.com/BCNEcologyINST";
	final static String energy = "http://www.bsc.es/Energy";
	final static String bcn = "http://www.ibm.com/BCNEcology";
	final static String core = "http://www.ibm.com/ICP/Scribe/CoreV2";
	final static String coregeo = "http://www.ibm.com/ICP/Scribe/CoreGeoV2";
	final static String coreweather = "http://www.ibm.com/ICP/Scribe/CoreWeatherV2";
	final static String businessact = "http://www.businessactivities.com/BusAc";
	final static String geoWkt = "http://www.opengis.net/ont/geosparql#asWKT";
	final static String BCNII = "http://www.ibm.com/BCNIndicatorInstances";
	final static String rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns";
	final static String geo = "http://www.opengis.net/ont/geosparql";
	final static String rdfs = "http://www.w3.org/2000/01/rdf-schema";
	final static String sf = "http://www.opengis.net/ont/sf";

	static String CONCEPT_DIMENSION = core + "#UrbanConceptDimension";
	static String PLANNING_PERSPECTIVE = core + "#UrbanPlanningPerspective";
	static String CITY_PHYSICAL_ENTITY = core + "#CityPhysicalEntity";

	String parliamentmodel;
	String querytype;

	static int tempInstCounter = 0; // TODO: eliminar instancias temporales

	Map<String, Set<String>> enumTypeMap = new HashMap<String, Set<String>>(); // enum
																				// -
																				// main
																				// classes

	public List<IOntoLink> FilterLinks(List<IOntoLink> links) {
		Set<IOntoLink> toRemove = new HashSet<IOntoLink>();
		for (IOntoLink l : links) {
			for (IOntoLink l2 : links) {
				if (l != l2 && !toRemove.contains(l2) && l.getDomain().getURI().equals(l2.getDomain().getURI())
						&& l.getRange().getURI().equals(l2.getRange().getURI())) {
					Set<Statement> st = this.getJenaModel().GetStatements(l.getRelationship().getResource(),
							RDFS.subPropertyOf, l2.getRelationship().getResource(), true);
					if (!st.isEmpty()) {
						toRemove.add(l2);
					}
				}
			}
		}
		links.removeAll(toRemove);
		return links;
	}

	public Map<String, Set<String>> getEnumTypeMap() {
		return enumTypeMap;
	}

	public String getModelMapPopulatedPath() {
		return this.modelMapPopulatedPath;
	}

	public OntoFilter getFilterAttributeProperties() {
		return filterAttributeProperties;
	}

	public OntoFilter getEmptyFilter() {
		return new OntoFilter(new HashSet<IOntoBase>(), new HashSet<IOntoBase>());
	}

	public OntoFilter getFilterPlanningPerspective() {
		return filterPlanningPerspective;
	}

	public OntoFilter getFilterConceptDimension() {
		return filterConceptDimension;
	}

	public SearchEngine getSearchEngine() {
		return sengine;
	}

	public void setFilterAttributeProperties() {
		IOntoProperty meas1 = new OntoProperty(new OntoConcept(IOntoProperty.SATTRIBUTE1_URI, this), this);
		IOntoProperty meas2 = new OntoProperty(new OntoConcept(IOntoProperty.SATTRIBUTE2_URI, this), this);
		Set<Resource> prop = this.getJenaModel().GetSubHierarchy(meas1.getResource(), true, false);
		prop.addAll(this.getJenaModel().GetSubHierarchy(meas2.getResource(), true, false));
		Set<IOntoBase> concepts = new HashSet<IOntoBase>();
		for (Resource r : prop)
			concepts.add(new OntoConcept(r, this));
		this.filterAttributeProperties = new OntoFilter(concepts, new HashSet<IOntoBase>());
	}

	public void setFilterConceptDimension() {
		Set<Resource> res = this.getJenaModel().GetSubHierarchy(
				this.getJenaModel().getResource(CONCEPT_DIMENSION, false), true, false);
		Set<IOntoBase> concepts = new HashSet<IOntoBase>();
		for (Resource r : res)
			concepts.add(new OntoConcept(r, this));
		this.filterConceptDimension = new OntoFilter(concepts, new HashSet<IOntoBase>());
	}

	public void setFilterPlannPerspective() {
		Set<Resource> res = this.getJenaModel().GetSubHierarchy(
				this.getJenaModel().getResource(PLANNING_PERSPECTIVE, false), true, false);
		Set<IOntoBase> concepts = new HashSet<IOntoBase>();
		for (Resource r : res)
			concepts.add(new OntoConcept(r, this));
		this.filterPlanningPerspective = new OntoFilter(concepts, new HashSet<IOntoBase>());
	}

	public ModelMap getMap() {
		return map;
	}

	private void setMap(ModelMap modelMap) {
		this.map = modelMap;
	}

	public ModelMap getMapPopulated() {
		return populatedMap;
	}

	private void setMapPopulated(ModelMap modelMapPopulated) {
		this.populatedMap = modelMapPopulated;
	}

	public QueryGraph getqGraph(Integer id) {
		return qGraphs.get(id);
	}

	private Integer setqGraph(QueryGraph qGraph) {
		this.qGraphs.put(++nGraphs, qGraph);
		return nGraphs;
	}

	public void removeGraph(Integer idGraph) {
		this.qGraphs.remove(idGraph);
	}

	// final String WORKING_PARL_GRAPH = "http://BCNEcologyINST";

	// TODO: MODIFIED TO TEST SCALABILITY
	// public static final String WORKING_PARL_GRAPH = "http://TestingModel";
	// public String WORKING_PARL_GRAPH = "http://TestingModel1";
	//
	// public boolean QUERYJENA = false;
	// ENDTODO

	public JenaOntology getJenaModel() {
		return jenaModel;
	}

	public ParliamentOntology getParliamentModel() {
		return parliamentModel;
	}

	public boolean OnQuery(String idSession) {
		return this.getJenaModel().OnQuery(idSession);
	}

	public boolean CancelQuery(String idSession) throws ParseException, IOException {
		boolean result;
		ParliamentOntology pOnt = this.getParliamentModel();
		result = pOnt.CancelActiveQuery(idSession);
		JenaOntology jOnt = this.getJenaModel();
		return (result && jOnt.CancelActiveQuery(idSession));
	}

	// generate new resources
	public UrbanOntology(Properties properties, Dataset mainmodel, Model tbox) throws IOException {
		String mapsPath = properties.getProperty("mapsPath").trim();
		String searchindexPath = properties.getProperty("searchindexPath").trim();
		String stopwords = properties.getProperty("stopwords").trim();
		String parliamentlocation = properties.getProperty("parliamentlocation").trim();
		parliamentmodel = properties.getProperty("parliamentmodel").trim();
		querytype = properties.getProperty("querytype").trim();

		System.out.println("Creating jenaModel...");
		jenaModel = new JenaOntology(properties, mainmodel, tbox);
		System.out.println("Created jenaModel");

		System.out.println("Connecting to Parliamentl at " + parliamentlocation + " ...");
		parliamentModel = new ParliamentOntology(parliamentlocation);

		if (parliamentModel.existGraph(parliamentmodel))
			parliamentModel.deleteGraph(parliamentmodel);
		// insert and index jena.model in parliament
		this.getJenaModel().beginRead();
		parliamentModel.insertStatements(parliamentmodel, this.getJenaModel().getModel());
		this.getJenaModel().commitEnd();
		parliamentModel.indexGraph(parliamentmodel);

		System.out.println("Connected to " + parliamentlocation);

		this.setFilterConceptDimension();
		this.setFilterPlannPerspective();
		this.setFilterAttributeProperties();

		Path mappath = Paths.get(mapsPath);
		if (Files.notExists(mappath))
			Files.createDirectories(mappath);

		System.out.println("Creating Model Maps...");
		setMap(new ModelMap(this, false)); // use filters
		ModelMap.SerializeObject(this.getMap(), mapsPath + "/modelMap");
		System.out.println("Map No Populated Created");

		setMapPopulated(new ModelMap(this, true)); // use filters
		ModelMap.SerializeObject(this.getMapPopulated(), mapsPath + "/modelMapPopulated");
		modelMapPopulatedPath = mapsPath + "/modelMapPopulated";
		System.out.println("Map populated Created");

		System.out.println("Creating Search Engine...");
		System.out.println("Creating Search Engine... [SE path:" + searchindexPath + " ]...");

		Path searchpath = Paths.get(searchindexPath);
		if (Files.notExists(searchpath))
			Files.createDirectories(searchpath);
		CreateSearchEngine(GetConceptInfo(), stopwords, searchindexPath); // use
																			// Maps
		System.out.println("Search Engine Created");

		setEnumTypeMap();
	}

	// read existing resources
	public UrbanOntology(Properties properties) throws Exception {
		String mapsPath = properties.getProperty("mapsPath").trim();
		String searchindexPath = properties.getProperty("searchindexPath").trim();
		String stopwords = properties.getProperty("stopwords").trim();
		String parliamentlocation = properties.getProperty("parliamentlocation").trim();
		parliamentmodel = properties.getProperty("parliamentmodel").trim();
		querytype = properties.getProperty("querytype").trim();

		System.out.println("Reading JenaOntology...");
		jenaModel = new JenaOntology(properties);
		System.out.println("Jena Ontology read");
		System.out.println("Connecting to Parliamentl at " + parliamentlocation + " ...");
		parliamentModel = new ParliamentOntology(parliamentlocation);
		System.out.println("Connected to " + parliamentlocation);

		this.setFilterConceptDimension();
		this.setFilterPlannPerspective();
		this.setFilterAttributeProperties();
		if (!parliamentModel.existGraph(parliamentmodel))
			throw (new Exception("Graph " + parliamentmodel + " does not exist in " + parliamentlocation));

		System.out.println("Reading Maps...");
		setMap(ModelMap.DeserializeObject(mapsPath + "/modelMap"));
		System.out.println("Map-No-Populated Loaded");

		setMapPopulated(ModelMap.DeserializeObject(mapsPath + "/modelMapPopulated"));
		System.out.println("Map-Populated Loaded");

		System.out.println("Loading Search Engine [SearchIndexPath: " + searchindexPath + "]...");
		LoadSearchEngine(stopwords, searchindexPath);
		System.out.println("Search Engine Loaded");

		modelMapPopulatedPath = mapsPath + "/modelMapPopulated";
		setEnumTypeMap();

		// DEBUG
		// System.out.println("Creating Search Engine...");
		// CreateSearchEngine(GetConceptInfo(), appPath, searchPath);
		// System.out.println("Search Engine Created");

		// IOntoBase ob =
		// this.GetOntoBase("http://www.opengis.net/ont/sf#wktLiteral");
		// System.out.println(ob.getType());
		// for (String range:
		// this.map.map.get(core+"#PedestrianStreet").keySet()){
		// System.out.println(range);
		// }
		// Property p = null;
		// Set<RDFNode> results =
		// this.getJenaModel().GetObjects(this.GetOntoBase(core+"#PedestrianStreet").getResource(),
		// p, true);
		// for (RDFNode r:results){
		// System.out.println(r.asResource().getURI());
		// }
	}

	private void setEnumTypeMap() {
		// for (String uri: this.map.getMap().keySet()){
		Resource scb = this.getJenaModel().getResource(core + "#ScribeCoreBase", false);

		Set<Resource> subclasses = this.getJenaModel().GetSubjects(scb, RDFS.subClassOf, true);
		for (Resource scbsub : subclasses) {
			String uri = scbsub.getURI();
			IOntoBase ob = new OntoConcept(uri, this);
			Resource instance = this.getJenaModel().GetSubject(ob.getResource(), RDF.type, false);
			if (instance != null && instance.getURI().endsWith("_INST")
					&& instance.getLocalName().replace("_INST", "").equals(scbsub.getLocalName())) {
				Set<String> categories = new HashSet<String>();
				this.enumTypeMap.put(uri, categories);
				boolean categoryFound = false;
				List<Resource> parents = new ArrayList<Resource>();
				parents.add(ob.getResource());
				while (!categoryFound && parents.size() > 0) {
					Resource actual = parents.remove(parents.size() - 1);
					Set<Resource> par = getJenaModel().GetSuperHierarchy(actual, false);
					for (Resource res : par) {
						if (res != actual) {
							IOntoBase resOb = new OntoConcept(res, this);
							Resource inst = this.getJenaModel().GetSubject(resOb.getResource(), RDF.type, false);
							if (inst != null
									&& inst.getURI().endsWith("_INST")
									&& inst.getLocalName().replace("_INST", "")
											.equals(resOb.getResource().getLocalName())) {
								if (!parents.contains(resOb.getResource()))
									parents.add(resOb.getResource());
							} else if (resOb.getType() == IOntoBase.OntType.CLASS) {
								categories.add(resOb.getURI());
								categoryFound = true;
							}
						}
					}
				}
			}
		}
		Set<String> toAdd = new HashSet<String>();
		;
		for (Set<String> values : this.enumTypeMap.values())
			// categories al
			// also included
			// as enums with
			// self as
			// category
			for (String v : values)
				toAdd.add(v);
		for (String v : toAdd) {
			HashSet<String> category = new HashSet<String>();
			category.add(v);
			if (!this.enumTypeMap.containsKey(v))
				this.enumTypeMap.put(v, category);
		}

		// DEBUG
		for (String entype : this.enumTypeMap.keySet()) {
			String category = this.getCategory(entype);
			if (category == null || category == "")
				System.out.println();
			System.out.println(entype + "        " + category);
		}
	}

	// public String getCategory(String enumTypeUri){
	// if (this.getEnumTypeMap().containsKey(enumTypeUri)) {
	// for (String res: this.getEnumTypeMap().get(enumTypeUri)){
	// return res;
	// // else
	// // System.out.println(enumTypeUri+" cannot be replaced by "+res);
	// }
	// }
	// return enumTypeUri;
	// }

	public String getCategory(String enumTypeUri) {
		if (this.getEnumTypeMap().containsKey(enumTypeUri) && this.map.getMap().containsKey(enumTypeUri)) {
			Set<String> rangesEnum = this.map.getMap().get(enumTypeUri).keySet();
			Set<String> domainsEnum = new HashSet<String>();
			if (this.map.getInverseMap().containsKey(enumTypeUri))
				domainsEnum = this.map.getInverseMap().get(enumTypeUri).keySet();
			for (String res : this.getEnumTypeMap().get(enumTypeUri)) {
				Set<String> ranges = new HashSet<String>();
				if (this.map.getMap().containsKey(res))
					ranges = this.map.getMap().get(res).keySet();
				Set<String> domains = new HashSet<String>();
				if (this.map.getInverseMap().containsKey(res))
					domains = this.map.getInverseMap().get(res).keySet();
				if (ranges.containsAll(rangesEnum) && domains.containsAll(domainsEnum))
					return res;
				// else
				// System.out.println(enumTypeUri+" cannot be replaced by
				// "+res);
			}
		}
		return enumTypeUri;
	}

	public Set<String> filterByCategory(Set<String> concepts) {
		Set<String> results = new HashSet<String>();
		for (String uri : concepts) {
			results.add(this.getCategory(uri));
		}
		return results;
	}

	public String filterByCategory(String concept) {
		return this.getCategory(concept);
	}

	private void CreateSearchEngine(Set<ConceptInfo> ontConcepts, String stopwordsPath, String searchPath)
			throws IOException {
		try {
			sengine = new SearchEngine(ontConcepts, stopwordsPath, searchPath);
		} catch (IOException e) {
			System.out.println(e);
			System.out.println("FATAL ERROR: search engine not created");
			sengine = null;
			throw (e);
		}
	}

	private void LoadSearchEngine(String stopwords, String searchPath) throws IOException {
		try {
			sengine = new SearchEngine(stopwords, searchPath);
		} catch (IOException e) {
			System.out.println(e);
			System.out.println("FATAL ERROR: search engine not created");
			sengine = null;
			throw (e);
		}
	}

	// public Map<String,Map<String,String>> GetSnippet(String uri, String
	// query, String openlabel, String closelabel){
	// ConceptInfo ci = this.getSearchEngine().getConceptInfo(uri);
	// if (ci != null)
	// return ci.getSnippet(query, this.getSearchEngine().getTokenizer(),
	// this.getSearchEngine().getProcessors(),openlabel,closelabel);
	// return null;
	// }

	public Set<ConceptInfo> GetConceptInfo() {
		double labelW = 5;
		double commentW = 1;
		double relatedW = 1;
		double propertyW = 3;
		Set<ConceptInfo> concepts = new HashSet<ConceptInfo>();
		for (Resource res : this.getJenaModel().listSubjects(false)) {
			if (res.isURIResource() && !UrbanOntology.IsGeneralConcept(res.getURI())) {
				IOntoBase ob = new OntoConcept(res, this);
				if (ob.getType() == IOntoBase.OntType.CLASS || ob.getType() == IOntoBase.OntType.ECLASS) {
					// if (ob.getURI().equals(core+"#BuildingUseUnit"))
					// System.out.println("Stop");
					ConceptInfo ci = new ConceptInfo(res.getURI());
					String label = this.getJenaModel().GetLabel(res);
					if (label != null)
						ci.addVar("label", res.getURI(), labelW, label);
					else
						ci.addVar("label", res.getURI(), labelW, res.getLocalName());
					String comment = this.getJenaModel().GetComment(res);
					if (comment != null)
						ci.addVar("comment", res.getURI(), commentW, comment);

					Set<String> properties = new HashSet<String>();
					// Property property =
					// this.getJenaModel().getProperty(IOntoProperty.SCRIBE_URI,
					// false); if we ask with this property, the property of the
					// resulting statements is just the same
					for (String range : this.map.getRangesFromDomain(ob)) {
						String conc = this.getJenaModel().GetLabel(range, false);
						if (conc != null)
							ci.addVar("related", range, relatedW, conc);
						else
							ci.addVar("related", range, relatedW, this.getJenaModel().getResource(range, false)
									.getLocalName());
						properties.addAll(this.map.getProperties(ob.getURI(), range));
					}
					for (String p : properties) {
						String prop = this.getJenaModel().GetLabel(p, false);
						if (prop != null)
							ci.addVar("property", p, propertyW, prop);
						else
							ci.addVar("property", p, propertyW, this.getJenaModel().getResource(p, false)
									.getLocalName());

					}
					concepts.add(ci);
				}
			}
		}
		return concepts;
	}

	private List<ISearchResult> FilterSearchResults(Collection<SearchResult> listResults, boolean withInstances) {
		List<ISearchResult> results = new ArrayList<ISearchResult>();
		for (SearchResult sr : listResults) {
			// if (sr.getOntoConcept().getType() == IOntoBase.OntType.CLASS &&
			// getFilterPlanningPerspective().IsValid(sr.getOntoConcept()))
			if (!withInstances || (withInstances && sr.getOntoConcept().hasDataInstances()))
				results.add(sr);
		}
		return results;
	}

	public List<ISearchResult> SearchConcepts(String id, String query, boolean withInstances) throws IOException {
		HashMap<String, SearchResult> map = new HashMap<String, SearchResult>(); // map
																					// searchResult
		FileWriter fw2 = null;
		long init, end;
		fw2 = new FileWriter("/tmp/GS_SI.SearchConcepts.log");
		FileWriter fw3 = new FileWriter("/tmp/GS_OntoConcept.log");
		FileWriter fw4 = new FileWriter("/tmp/GS_SearchResult.log");
		File fwOC = new File("/tmp/GS_OntoConcepts_setPrefix.csv");
		fwOC.delete();

		if (sengine != null) {
			for (String var : sengine.getVars()) {
				init = System.currentTimeMillis();
				Map.Entry<String, Double>[] results = sengine.Query(id, query, var);
				end = System.currentTimeMillis();
				PrintMethods.printFile(fw2, init, end, "sengine.Query()");

				long initFOR = System.currentTimeMillis();

				// for (Map.Entry<String, Double> res : results) {
				JenaOntology jenaModel = this.getJenaModel();

				Map.Entry<String, Double> res = null;
				for (int i = 0; i < results.length; ++i) {
					res = results[i];
					init = System.currentTimeMillis();
					Double similarity = res.getValue() * sengine.getIndex(var).getWeight();
					end = System.currentTimeMillis();
					PrintMethods.printFile(fw2, init, end, "sengine.getIndex");
					init = System.currentTimeMillis();
					// IOntoBase ob = new OntoConcept(res.getKey(), this);
					init = System.currentTimeMillis();
					Resource resource = jenaModel.getResource(res.getKey(), false);
					end = System.currentTimeMillis();
					PrintMethods.printFile(fw2, init, end, "jenaModel.getResource()");
					init = System.currentTimeMillis();
					IOntoBase ob = new OntoConcept(resource, this);
					end = System.currentTimeMillis();
					PrintMethods.printFile(fw3, init, end, "new OntoConcept");
					init = System.currentTimeMillis();
					SearchResult os = new SearchResult(ob, similarity, this);
					end = System.currentTimeMillis();
					PrintMethods.printFile(fw4, init, end, "new SearchResult");
					// GetSearchConcept(ob);
					init = System.currentTimeMillis();
					os.setSearchSource(var); // the var source of these search
					end = System.currentTimeMillis();
					PrintMethods.printFile(fw2, init, end, "os.setSearchSource()");

					init = System.currentTimeMillis();
					if (!map.containsKey(os.getOntoConcept().getURI()))
						map.put(os.getOntoConcept().getURI(), os);
					else if (map.get(os.getOntoConcept().getURI()).getSimilarity() < similarity) {
						map.put(os.getOntoConcept().getURI(), os);
					}
				}
				long endFOR = System.currentTimeMillis();
				PrintMethods.printFile(fw2, initFOR, endFOR, "for(Map.Entry<>)");

			}
		}
		init = System.currentTimeMillis();
		List<ISearchResult> results = FilterSearchResults(map.values(), withInstances);
		end = System.currentTimeMillis();
		PrintMethods.printFile(fw2, init, end, "FilterSearchResults()");

		init = System.currentTimeMillis();
		java.util.Collections.sort(results, new SearchResultComparator());
		end = System.currentTimeMillis();
		PrintMethods.printFile(fw2, init, end, "SearchResultComparator()");
		fw2.close();
		return results;
	}

	public static boolean IsGeneralConcept(String uri) {
		String coreClass = core + "#ScribeCoreBase";
		String coreBaseClass = core + "#ScribeBase";
		String[] tArray = { coreClass, coreBaseClass };
		for (String prefix : tArray) {
			if (uri.contains(prefix))
				return false;
		}

		String[] fArray = { BCNII, bcninst, energy, bcn, core, coregeo, coreweather, businessact,
				IOntoArea.GEOMETRY_PROPERTY_URI, IOntoArea.WKT_PROPERTY_URI, sf };

		for (String prefix : fArray) {
			if (uri.contains(prefix))
				return false;
		}
		return true;
	}

	public static boolean inList(Resource ob, Collection<? extends IOntoBase> obList) {
		if (obList != null) {
			for (IOntoBase o : obList)
				if (o.getURI().equals(ob.getURI()))
					return true;
		}
		return false;
	}

	public List<String> GetAllUris() {
		List<String> results = new ArrayList<String>();
		for (Resource r : this.jenaModel.GetURIs(true))
			if (r.isURIResource())
				results.add(r.getURI());
		return results;
	}

	// If imported closure is true, the prefixes of imported ontologies in
	// baseModel are added (in parliament models imported closure is not done)
	public Map<String, String> getAllPrefixes(boolean importedClosure) {
		Map<String, String> allPrefixes, parlPrefixes;
		allPrefixes = this.getJenaModel().getMapPrefixes(true);
		parlPrefixes = this.getParliamentModel().getMapPrefixes();
		for (String prefix : parlPrefixes.keySet()) {
			if (!allPrefixes.containsKey(prefix))
				allPrefixes.put(prefix, parlPrefixes.get(prefix));
		}
		return allPrefixes;
	}

	public String getStringPrefixes(Map<String, String> prefixMap) {
		String prefixBlock = "";
		for (String key : prefixMap.keySet()) {
			prefixBlock += "PREFIX  " + key + ":<" + prefixMap.get(key) + ">\n";
		}
		// System.out.println(prefixBlock);

		return prefixBlock;

	}

	public HashMap<String, ArrayList<String>> GetUrbanPlanningPerspectiveConcepts() {
		return this.getJenaModel().GetClassHierarchy(CONCEPT_DIMENSION, this.getJenaModel().model);
	}

	// By default, the complete and inferred jena model is queried
	public QueryResults GeneralQuery(String sparqlQuery, boolean addPrefixes, String idSession) {
		JenaOntology bmodel = this.getJenaModel();
		if (addPrefixes) {
			String prefixes = getStringPrefixes(bmodel.getMapPrefixes(true));
			sparqlQuery = prefixes + "\n" + sparqlQuery;
		}
		return (this.getJenaModel().ResultSetQueryOntology(sparqlQuery, true, idSession));
	}

	public QueryResults GetMatchConcepts(String keyword, String idSession) {
		JenaOntology jOnt = this.getJenaModel();
		String query = jOnt.Query_GetMatchConcept(keyword);
		String prefixes = getStringPrefixes(getAllPrefixes(true));
		return (jOnt.ResultSetQueryOntology(prefixes + "\n" + query, false, idSession));
	}

	public QueryResults GetInstancesWithinPolygon(String URIclass, String wktPolygon) throws IOException {
		ParliamentOntology pOnt = this.getParliamentModel();
		JenaOntology jOnt = this.getJenaModel();
		String prefixes = getStringPrefixes(getAllPrefixes(true));
		String query = pOnt.Query_WithinPolygonURI(URIclass, wktPolygon, parliamentmodel);
		QueryResults qrInstances = pOnt.ResultSetQueryOntology(prefixes + "\n" + query);
		ArrayList<Resource> validInstances = new ArrayList<Resource>();
		for (HashMap<String, String> row : qrInstances.getResults()) {
			// validInstances.add(row.get("f1"));
			validInstances.add(this.jenaModel.getResource(row.get("f1"), false));
		}
		QueryResults qrProperties = jOnt.GetInstanceProperties(validInstances, true);
		return qrProperties;
	}

	public QueryResults GetWkt(String type) throws IOException {
		String classURI = null;
		if (type.equals("Blocks"))
			classURI = core + "#UrbanBlock";
		else if (type.equals("Landlots"))
			classURI = core + "#LandLot";
		if (classURI != null) {
			return (this.jenaModel.GetInstanceProperties(classURI, true));
		}
		return null;
	}

	public QueryResults GetMatchProperties(String inputProperty, String idSession) {
		JenaOntology jOnt = this.getJenaModel();
		String prefixes = getStringPrefixes(getAllPrefixes(true));
		String query = jOnt.Query_GetMatchProperties(inputProperty, CONCEPT_DIMENSION);
		// return (jOnt.XMLQueryOntology(prefixes +"\n"+query,
		// jOnt.getInferredModel()));
		return (jOnt.ResultSetQueryOntology(prefixes + "\n" + query, true, idSession));
	}

	public QueryResults GetRelatedConcepts(String ptype, String uriclass, boolean populated) { // TODO:
																								// add
																								// param
																								// populated
																								// in
																								// webapp
		Set<IOntoProperty> properties = new HashSet<IOntoProperty>();
		switch (ptype) {
		case "all":
			properties.addAll(OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.ALL, true, false, this));
			break;
		case "iagg":
			properties.addAll(OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.SAGGREGATE, true, false, this));
			break; // TODO: remove one of this categories iagg,dagg
		case "ass":
			properties.addAll(OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.SASSOCIATED, true, false, this));
			break;
		case "iatt":
			properties.addAll(OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.SATTRIBUTE, true, false, this));
			break;
		case "dagg":
			properties.addAll(OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.SAGGREGATE, true, false, this));
			break;
		case "datt":
			properties.addAll(OntoProperty.GetOntoProperty(IOntoProperty.OntProperty.SATTRIBUTE, true, false, this));
			break;
		}
		List<String> vars = new ArrayList<String>();
		vars.add("relclass");
		vars.add("relprop");
		vars.add("relclassIsDomain"); // TODO: use it in webapp if needed: value
										// 0 indicate false, value 1 indicate
										// true
		List<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();
		IOntoBase concept = new OntoConcept(uriclass, this);

		List<IOntoLink> linksConceptAsDomain = GetDirectLinks(properties, concept, null, populated,
				this.filterPlanningPerspective);
		List<IOntoLink> linksConceptAsRange = GetDirectLinks(properties, null, concept, populated,
				this.filterPlanningPerspective);
		for (IOntoLink link : linksConceptAsDomain) {
			HashMap<String, String> newItem = new HashMap<String, String>();
			newItem.put("relclass", link.getRange().getURI());
			newItem.put("relprop", link.getRelationship().getURI());
			newItem.put("relclassIsDomain", "0");
			results.add(newItem);
		}
		for (IOntoLink link : linksConceptAsRange) {
			HashMap<String, String> newItem = new HashMap<String, String>();
			newItem.put("relclass", link.getDomain().getURI());
			newItem.put("relprop", link.getRelationship().getURI());
			newItem.put("relclassIsDomain", "1");
			results.add(newItem);
		}
		QueryResults qr = new QueryResults(vars, results);
		return qr;
	}

	// public QueryResults GetIndicators(){
	// final String hasDescriptor = core+"#hasIndicatorDescriptor";
	// final String hasMeasurement = core+"#measurementFloatValue";
	// return this.jenaModel.GetIndicators(hasDescriptor, hasMeasurement);
	// }
	//
	// public QueryResults GetIndDescriptorMeasurements(String uriDescriptor){
	// final String relatedMeasurementsProp = core+"#hasMeasurementDescriptor";
	// final String relatedMeasurementsValue = core+"#indicatorValueConstraint";
	// return this.jenaModel.GetIndDescriptorMeasurements(uriDescriptor,
	// relatedMeasurementsProp, relatedMeasurementsValue);
	// }
	//
	// public QueryResults GetIndDescriptorRelatedClasses(String uriDescriptor){
	// final String relatedClassesProp = core+"#indicatorAssociatedToEntity";
	// return this.jenaModel.GetIndDescriptorClassesAffected(uriDescriptor,
	// relatedClassesProp);
	// }
	//
	// public String GetIndDescriptorComment(String uriDescriptor){
	// return this.jenaModel.GetIndDescriptorComment(uriDescriptor);
	// }

	// NEW METHODS

	public IOntoBase GetUrbanDimensionConcept() {
		return new OntoConcept(CONCEPT_DIMENSION, this);
		// return GetSub(ob);
	}

	public IOntoBase GetUrbanPlanningPerspective() {
		return new OntoConcept(PLANNING_PERSPECTIVE, this);
		// return GetSub(ob);
	}

	public boolean SameConcept(IOntoBase c1, IOntoBase c2) {
		if (c1.getURI().equals(c2.getURI()))
			if (c1.getId() == c2.getId())
				return true;
		return false;
	}

	static public List<IOntoBase> OrderConceptsById(List<IOntoBase> conceptList) {
		java.util.Collections.sort(conceptList, new OntoBaseComparator());
		return conceptList;
	}

	public static boolean existLink(Resource p, Resource cDomain, Resource cRange, List<IOntoLink> links) {
		String pUri = p.getURI();
		String cdUri = cDomain.getURI();
		String crUri = cRange.getURI();
		for (IOntoLink l : links) {
			if (l.getRelationship().getURI().equals(pUri) && l.getDomain().getURI().equals(cdUri)
					&& l.getRange().getURI().equals(crUri))
				return true;
		}
		return false;
	}

	public void Finalize() {
		this.getJenaModel().finalize();
	}

	private List<IOntoLink> GetDirectLinks(Set<IOntoProperty> properties, IOntoBase classDomain, IOntoBase classRange,
			boolean withInstances, OntoFilter filter) {
		if (withInstances)
			return this.getMapPopulated().GetDirectLinks(properties, classDomain, classRange, this, filter);
		else
			return this.getMap().GetDirectLinks(properties, classDomain, classRange, this, filter);
	}

	public List<IOntoLink> GetDirectLinks(IOntoProperty.OntProperty ptype, IOntoBase classDomain, IOntoBase classRange,
			boolean withInstances) throws Exception {
		//
		// Property p =null;
		// RDFNode n = null;
		// Set<Statement> sts =
		// this.getJenaModel().GetStatements(this.getJenaModel().getResource("http://www.ibm.com/BCNEcologyINST#bcnInstance49564",
		// false), p, n, true);
		// for (Statement st:sts){
		// System.out.println(st);
		// }

		List<IOntoLink> results = new ArrayList<IOntoLink>();
		Set<IOntoProperty> ontoProps = OntoProperty.GetOntoProperty(ptype, true, false, this);
		if (classDomain != null && classRange != null) {
			// if any of them is an area, add geo functions as possible link
			if (classRange.isGeoConcept() && classDomain.isGeoConcept()) {
				results.add(new OntoLink(IOntoFunction.OntFunction.INTERSECTION, new ArrayList<String>(), classDomain,
						classRange, this));
				results.add(new OntoLink(IOntoFunction.OntFunction.WITHIN, new ArrayList<String>(), classDomain,
						classRange, this));
				results.add(new OntoLink(IOntoFunction.OntFunction.DISTANCE, new ArrayList<String>(), classDomain,
						classRange, this));
			}
		}
		results.addAll(GetDirectLinks(ontoProps, classDomain, classRange, withInstances, this.getEmptyFilter()));
		return results;
	}

	public List<IOntoLink> GetDirectLinks(IOntoProperty property, IOntoBase classDomain, IOntoBase classRange,
			boolean withInstances) {
		Set<Resource> pRes = this.getJenaModel().GetSubHierarchy(property.getResource(), true, false);
		Set<IOntoProperty> pOnto = new HashSet<IOntoProperty>();
		for (Resource p : pRes) {
			if (!IsGeneralConcept(p.getURI())) {
				OntoConcept pOc = (OntoConcept) new OntoConcept(p, this);
				OntoProperty oProp = new OntoProperty(pOc, this);
				pOnto.add(oProp);
			}
		}
		return GetDirectLinks(pOnto, classDomain, classRange, withInstances, this.getEmptyFilter());
	}

	// GRAPH

	public Integer GetGraphResult(List<IOntoLink> graph, String idSession) {
		try {
			Integer count = 0;
			for (IOntoLink l : graph) {
				Integer relId = Integer.parseInt(count.toString() + l.getDomain().getId() + l.getRange().getId());
				l.getRelationship().setId(relId);
				count++;
			}
			QueryGraph qGraph = new QueryGraph(graph, this, idSession, querytype, parliamentmodel);
			Integer id = this.setqGraph(qGraph);
			return id;
		} catch (Exception e) {
			System.out.println("Query Graph no created");
			// this.setqGraph(null);
			return null;
		}
	}

	public Integer GetGraphResult(IOntoBase node, String idSession) {
		try {
			QueryGraph qGraph = new QueryGraph(node, this, idSession, querytype, parliamentmodel);
			Integer id = this.setqGraph(qGraph);
			return id;
		} catch (Exception e) {
			System.out.println("Query Graph no created");
			// this.setqGraph(null);
			return null;
		}
	}

	public Integer GetGraphResult(Set<IOntoBase> nodes, List<IOntoLink> links, String idSession) {
		try {
			QueryGraph qGraph = new QueryGraph(nodes, links, this, idSession, querytype, parliamentmodel);
			Integer id = this.setqGraph(qGraph);
			return id;
		} catch (Exception e) {
			System.out.println("Query Graph no created");
			// this.setqGraph(null);
			return null;
		}
	}

	public List<List<IOntoLink>> GetConnectedGraphs(List<IOntoLink> graph, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.getSubgraphs();
		return null;
	}

	public List<String> GetGraphDataURIS(int nodeId, int initRow, int endRow, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.GetGraphDataURIS(nodeId, initRow, endRow);
		return null;
	}

	public List<Integer> GetGraphDataRow(int nodeId, String uri, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.GetGraphDataRow(nodeId, uri);
		return null;
	}

	public List<Set<String>> GetGraphDataValues(int nodeId, String propertyUri, int initRow, int endRow, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.GetGraphDataValues(nodeId, propertyUri, initRow, endRow);
		return null;
	}

	public List<HashMap<String, Set<String>>> GetGraphDataValues(int nodeId, int initRow, int endRow, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.GetGraphDataValues(nodeId, initRow, endRow);
		return null;
	}

	public static List<IOntoBase> GetGraphNodesToShow(List<IOntoLink> graph) {
		return QueryGraph.GetGraphNodesToShow(graph);
	}

	public List<List<IOntoLink>> getSubgraphs(Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.getSubgraphs();
		return null;
	}

	public int getRowsNumber(int idSubgraph, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.getRowsNumber(idSubgraph);
		return -1;
	}

	public String getSparql(int idSubgraph, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.getSparql(idSubgraph);
		return null;
	}

	public int getSubgraphId(int idNode, Integer idGraph) {
		QueryGraph qGraph = this.getqGraph(idGraph);
		if (qGraph != null)
			return qGraph.getSubgraphId(idNode);
		return -1;
	}

	private Map<Integer, Set<String[]>> GetPaths(String origin, String end, int maxDeep, boolean populated,
			OntoFilter filterProperties, OntoFilter filterConcepts, boolean onlyFirstResult) {
		HashMap<String, Set<String>> newConnections = new HashMap<String, Set<String>>();
		Map<Integer, Set<String[]>> results;
		if (populated)
			results = this.populatedMap.GetPaths(origin, end, maxDeep, filterProperties, filterConcepts,
					onlyFirstResult, newConnections, this);
		else
			results = this.map.GetPaths(origin, end, maxDeep, filterProperties, filterConcepts, onlyFirstResult,
					newConnections, this);
		return results;
	}

	public boolean existsPath(IOntoBase domain, IOntoBase range, int deep, boolean populated) {
		ModelMap map;
		if (populated)
			map = this.getMapPopulated();
		else
			map = this.getMap();
		Map<String, BitSet> connmap = map.getConectionMaps(deep);
		if (connmap != null && map.isConnected(connmap, domain.getURI(), range.getURI()))
			return true;
		return false;
	}

	public Map<Integer, Set<String[]>> GetPaths(IOntoBase domain, IOntoBase range, int maxDeep, boolean populated) {
		Map<Integer, Set<String[]>> paths = new HashMap<Integer, Set<String[]>>();
		boolean connected = false;
		for (int i = 0; i <= maxDeep; i++) {
			if (existsPath(domain, range, i, populated)) {
				connected = true;
				break;
			}
		}
		if (!connected)
			return GroupPathByHierarchy(paths);
		paths = this.GetPaths(domain.getURI(), range.getURI(), maxDeep, populated, this.getFilterAttributeProperties(),
				this.getEmptyFilter(), false);
		return GroupPathByHierarchy(paths);
	}

	private Map<Integer, Set<String[]>> GroupPathByHierarchy(Map<Integer, Set<String[]>> map) {
		Set<String[]> modifiedPaths = null;
		Set<String[]> toRemove = null;
		do {
			modifiedPaths = new HashSet<String[]>();
			toRemove = new HashSet<String[]>();
			Map<String, List<String[]>> auxMap = new HashMap<String, List<String[]>>();
			Map<String, List<String>> auxMap2 = new HashMap<String, List<String>>();
			for (Integer deep : map.keySet()) {
				Set<String[]> innerMap = map.get(deep);
				for (String[] path : innerMap) {
					List<String> pathAux = new ArrayList<String>(Arrays.asList(path));
					for (int i = 0; i < pathAux.size() - 2; i += 2) {
						String left = pathAux.get(i);
						String middle = pathAux.get(i + 1);
						String right = pathAux.get(i + 2);
						if (middle != null) {
							String key = left + right;
							List<String[]> innerAux;
							List<String> innerAux2;
							if (!auxMap.containsKey(key)) {
								innerAux = new ArrayList<String[]>();
								innerAux2 = new ArrayList<String>();
								auxMap.put(key, innerAux);
								auxMap2.put(key, innerAux2);
							} else {
								innerAux = auxMap.get(key);
								innerAux2 = auxMap2.get(key);
							}
							innerAux.add(path);
							innerAux2.add(middle);
						}
					}
				}
			}
			List<Map.Entry<String, List<String>>> mapList = new ArrayList<Map.Entry<String, List<String>>>(
					auxMap2.entrySet());
			java.util.Collections.sort(mapList, new HashKeyComparator());
			for (Map.Entry<String, List<String>> entry : mapList) {
				System.out.println(entry.getKey() + " " + entry.getValue().size());
				String key = entry.getKey();
				// System.out.print(key+":");
				Set<IOntoBase> middleSet = new HashSet<IOntoBase>();
				for (String middle : auxMap2.get(key)) {
					IOntoBase ob = new OntoConcept(middle, this);
					middleSet.add(ob);
					// System.out.print(middle+" ");
				}
				// System.out.println();
				Map<IOntoBase, Set<IOntoBase>> group = GroupByHierarchy(middleSet);
				Map<String, String> mapGroup = new HashMap<String, String>();
				for (IOntoBase concept : group.keySet()) {
					Set<IOntoBase> superSet = group.get(concept);
					IOntoBase sup = concept;
					while (superSet != null & superSet.size() > 0) {
						sup = superSet.iterator().next();
						superSet = group.get(sup);
					}
					mapGroup.put(concept.getURI(), sup.getURI());
				}

				for (int i = 0; i < auxMap.get(key).size(); i++) {
					String[] path = auxMap.get(key).get(i);
					String middle = auxMap2.get(key).get(i);
					String replace = mapGroup.get(middle);
					if (!middle.equals(replace)) {
						for (int j = 0; j < path.length; j++)
							if (path[j].equals(middle)) {
								path[j] = replace;
								break;
							}
						modifiedPaths.add(path);
					}
				}

				for (String[] path1 : modifiedPaths) {
					if (!toRemove.contains(path1)) {
						for (String[] path2 : auxMap.get(key)) {
							if (path1 != path2) {
								boolean equal = true;
								for (int i = 0; i < path1.length; i++)
									if (path1[i] == null || path2[i] == null) {
										if (path1[i] == null && path2[i] != null) {
											equal = false;
											break;
										} else if (path1[i] != null && path2[i] == null) {
											equal = false;
											break;
										}
									} else if (!(path1[i].equals(path2[i]))) {
										equal = false;
										break;
									}
								if (equal)
									toRemove.add(path2);

							}
						}
					}
				}
				if (modifiedPaths.size() > 0) {
					System.out.println("Modified");
					break;
				} else
					System.out.println("NOT Modified");
			}
			int total = 0;
			for (Integer deep : map.keySet())
				total += map.get(deep).size();
			for (String[] torem : toRemove)
				for (Integer deep : map.keySet()) {
					map.get(deep).remove(torem);
				}
			System.out.println(toRemove.size() + " removed out of " + total);
		} while (modifiedPaths.size() > 0);
		return map;
	}

	private Map<IOntoBase, Set<IOntoBase>> mapSuperConcepts(List<IOntoBase> conceptList) {
		Map<IOntoBase, Set<IOntoBase>> map = new HashMap<IOntoBase, Set<IOntoBase>>();
		for (IOntoBase concept : conceptList) {
			map.put(concept, new HashSet<IOntoBase>());
		}
		for (int i = 0; i < conceptList.size(); i++) {
			for (int j = i + 1; j < conceptList.size(); j++) {
				IOntoBase c1 = conceptList.get(i);
				IOntoBase c2 = conceptList.get(j);
				Resource superCon = null;
				if (c1.getType() == IOntoBase.OntType.PROPERTY)
					superCon = this.getJenaModel().getSuperProperty(c1.getResource(), c2.getResource());
				else
					superCon = this.getJenaModel().getSuperClass(c1.getResource(), c2.getResource());
				if (superCon != null) {
					if (c1.getResource() == superCon)
						map.get(c2).add(c1);
					else
						map.get(c1).add(c2);
				}
			}
		}
		return map;
	}

	private Set<IOntoBase> RemoveSuper(Set<IOntoBase> toprocess, IOntoBase key, Set<IOntoBase> listKey) {
		Set<IOntoBase> newListKey = new HashSet<IOntoBase>();
		Set<Resource> processed = new HashSet<Resource>();
		processed.add(key.getResource());
		for (IOntoBase tp : toprocess) {
			boolean keep = false;
			Set<Resource> superConcepts = this.getJenaModel().GetSuperHierarchy(key.getResource(), false);
			while (superConcepts.size() > 0) {
				Resource last = new ArrayList<Resource>(superConcepts).remove(superConcepts.size() - 1);
				processed.add(last);
				if (last == tp.getResource())
					keep = true;
				else if (!inList(last, listKey) && !toprocess.contains(last))
					superConcepts.addAll(this.getJenaModel().GetSuperHierarchy(last, false));
				superConcepts.removeAll(processed);
			}
			if (keep) // if I can reach the concept tp with no help from other
						// concepts in liskKey, I have to keep it
				newListKey.add(tp);
		}
		for (IOntoBase lk : listKey) {
			if (!toprocess.contains(lk))
				newListKey.add(lk);
		}
		return newListKey;
	}

	public Map<IOntoBase, Set<IOntoBase>> GroupByHierarchy(Set<? extends IOntoBase> concepts) {
		List<IOntoBase> conceptList = new ArrayList<IOntoBase>(concepts);
		Map<IOntoBase, Set<IOntoBase>> map = mapSuperConcepts(conceptList);
		for (IOntoBase key : map.keySet()) {
			Set<IOntoBase> toprocess = new HashSet<IOntoBase>();
			Set<IOntoBase> listKey = map.get(key);
			for (IOntoBase x : listKey) {
				Set<IOntoBase> listX = map.get(x);
				for (IOntoBase y : listX) {
					if (listKey.contains(y))
						toprocess.add(y);
				}
			}
			map.put(key, RemoveSuper(toprocess, key, listKey));
		}
		return map;
	}

	public Set<Object[]> OntologyInfo() {

		Set<Object[]> mainconcepts = new HashSet<Object[]>(); // uri concept,
																// uri domain,
																// uri range,
																// size,
																// superclasses,
																// subclasses
		Set<IOntoBase> urbanc = new HashSet<IOntoBase>();
		for (Resource concept : this.getJenaModel().GetSubjects(
				this.getJenaModel().getResource(core + "#ScribeCoreBase", false), RDFS.subClassOf, true)) {
			if (concept.getURI().contains("BuiltStructureFacility"))
				System.out.println(concept.getURI());
			if (!IsGeneralConcept(concept.getURI())) {
				boolean isenumtype = false;
				boolean isenumcategory = false;
				for (String key : this.enumTypeMap.keySet())
					if (concept.getURI().equals(key))
						isenumtype = true;
				for (Set<String> values : this.enumTypeMap.values())
					for (String value : values)
						if (concept.getURI().equals(value))
							isenumcategory = true;
				if (!isenumtype) {
					IOntoBase ob = new OntoConcept(concept, this);
					if (isenumcategory) {
						Set<Resource> instances = this.getJenaModel().GetSubjects(ob.getResource(), RDF.type, false);
						Set<Resource> instancesInf = this.getJenaModel().GetSubjects(ob.getResource(), RDF.type, true);
						if (!instancesInf.isEmpty()) {
							String category = this.getCategory(concept.getURI());
							if (category != null) {
								mainconcepts
										.add(new Object[] { category, "", "", instances.size(), instancesInf.size() });
								urbanc.add(new OntoConcept(category, this));
							}
						}
					} else if (ob.getType() == IOntoBase.OntType.CLASS) {
						Set<Resource> instances = this.getJenaModel().GetSubjects(ob.getResource(), RDF.type, false);
						Set<Resource> instancesInf = this.getJenaModel().GetSubjects(ob.getResource(), RDF.type, true);
						if (!instances.isEmpty()) {
							mainconcepts.add(new Object[] { concept.getURI(), "", "", instances.size(),
									instancesInf.size() });
							urbanc.add(ob);
						}
					}
				}
			}
		}

		Set<Object[]> concepts = new HashSet<Object[]>(); // uri concept, uri
															// domain, uri
															// range, size,
															// superclasses,
															// subclasses
		int total = urbanc.size();
		int ndomain = 0;
		int nrange = 0;
		List<IOntoBase> urbanclist = new ArrayList<IOntoBase>(urbanc);
		for (int i = 0; i < urbanclist.size(); i++) {
			IOntoBase domain = urbanclist.get(i);
			ndomain++;
			nrange = 0;
			boolean domainEnum = false;
			for (Set<String> values : this.enumTypeMap.values())
				for (String value : values)
					if (domain.getURI().equals(value))
						domainEnum = true;
			Set<Resource> subconcepts = this.getJenaModel().GetSubHierarchy(domain.getResource(), true, false);
			for (int j = 0; j < urbanclist.size(); j++) {
				IOntoBase range = urbanclist.get(j);
				nrange++;
				boolean rangeEnum = false;
				for (Set<String> values : this.enumTypeMap.values())
					for (String value : values)
						if (range.getURI().equals(value))
							rangeEnum = true;
				System.out.println("Processing " + ndomain + " * " + nrange + " (total " + total + ")");
				// if (i!=j && !JenaOntology.inList(range.getResource(),
				// subconcepts)){
				String query = "SELECT DISTINCT ?p WHERE {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"
						+ domain.getURI() + "> . " + "?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"
						+ range.getURI() + "> ." + "?s ?p ?o}";
				//
				// String query = "SELECT DISTINCT ?p WHERE {?domain
				// <http://www.w3.org/2000/01/rdf-schema#subClassOf>
				// <"+domain.getURI()+"> . "
				// + "?range <http://www.w3.org/2000/01/rdf-schema#subClassOf>
				// <"+range.getURI()+"> . "
				// + "?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>
				// ?domain . "
				// + "?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>
				// ?range ."
				// + "?s ?p ?o .}";
				QueryResults qr = this.getJenaModel().ResultSetQueryOntology(query, true, "OntologyInfo");
				for (HashMap<String, String> result : qr.getResults()) {
					String property = result.get("p");
					if (!IsGeneralConcept(property)) {
						String querydirect = "SELECT DISTINCT ?p ?s ?o WHERE {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"
								+ domain.getURI()
								+ "> . "
								+ "?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"
								+ range.getURI() + "> ." + "?s <" + property + "> ?o}";
						System.out.println(ndomain + " " + nrange + " " + querydirect);
						QueryResults qrdirect = this.getJenaModel().ResultSetQueryOntology(querydirect, true,
								"OntologyInfo");
						int nresultsdirect = qrdirect.getResults().size();
						String queryinferred;
						if (!domainEnum && !rangeEnum) {
							queryinferred = "SELECT DISTINCT ?p ?s ?o WHERE {?domain <http://www.w3.org/2000/01/rdf-schema#subClassOf> <"
									+ domain.getURI()
									+ "> . "
									+ "?range <http://www.w3.org/2000/01/rdf-schema#subClassOf> <"
									+ range.getURI()
									+ "> .  "
									+ "?p <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> <"
									+ property
									+ "> . "
									+ "?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?domain . "
									+ "?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?range ." + "?s ?p ?o .}";
						} else if (!domainEnum && rangeEnum) {
							queryinferred = "SELECT DISTINCT ?p ?s ?o WHERE {?domain <http://www.w3.org/2000/01/rdf-schema#subClassOf> <"
									+ domain.getURI()
									+ "> . "
									+ "?p <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> <"
									+ property
									+ "> . "
									+ "?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?domain . "
									+ "?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"
									+ range.getURI()
									+ "> ."
									+ "?s ?p ?o .}";
						} else if (domainEnum && !rangeEnum) {
							queryinferred = "SELECT DISTINCT ?p ?s ?o WHERE {?range <http://www.w3.org/2000/01/rdf-schema#subClassOf> <"
									+ range.getURI()
									+ "> .  "
									+ "?p <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> <"
									+ property
									+ "> . "
									+ "?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <"
									+ domain.getURI()
									+ "> . "
									+ "?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?range ."
									+ "?s ?p ?o .}";
						} else
							queryinferred = null;
						if (queryinferred != null) {
							System.out.println(ndomain + " " + nrange + " " + queryinferred);
							QueryResults qrinferred = this.getJenaModel().ResultSetQueryOntology(queryinferred, true,
									"OntologyInfo");
							int nresultsinferred = qrinferred.getResults().size();
							concepts.add(new Object[] { property, domain.getURI(), range.getURI(), nresultsdirect,
									nresultsinferred });
						}
					}
					// }
				}
			}
		}

		concepts.addAll(mainconcepts);

		return concepts;
	}

	// DEBUGGING
	public void printConnectionData() {
		this.getJenaModel().printConnectionData();
	}

	// public Object[] QueryModel(String query, boolean onlyParliament, int
	// maxTime, String idSession) throws IOException{
	// boolean exception;
	// int count =0;
	// int maxAttempts = 3;
	// Object[] result = new Object[]{-1,(long)-1,(long)-1}; //nº results, time,
	// cpu
	// do{
	// try{
	// ParliamentOntology pOnt = this.getParliamentModel();
	// JenaOntology jOnt = this.getJenaModel();
	// String prefixes = getStringPrefixes(getAllPrefixes(true));
	// // ThreadMXBean thbean= ManagementFactory.getThreadMXBean();
	// // if (!thbean.isCurrentThreadCpuTimeSupported())
	// // thbean.setThreadCpuTimeEnabled(true);
	// // jOnt.flushTDB(true);
	//
	//
	// // long cpu1 = thbean.getCurrentThreadCpuTime();
	// // long t1 = System.currentTimeMillis();
	// if (!onlyParliament)
	// result = jOnt.ResultSetQueryOntologyDebug(prefixes+"\n"+query, true,
	// maxTime, this, idSession);
	// else
	// result = pOnt.ResultSetQueryOntologyDebug(prefixes+"\n"+query);
	//
	// //int nresults = qrInstances.getResults().size();
	// // long t2 = System.currentTimeMillis();
	// // long cpu2 = thbean.getCurrentThreadCpuTime();
	//
	// // System.out.println("Total results:"+nresults);
	// // long time = t2-t1;
	// // System.out.println("Time (millis):"+time);
	// // long cpu = (Math.abs(cpu2-cpu1)) / 1000000;
	// // System.out.println("Cpu (millis):"+cpu);
	// exception = false;
	// // result[0] = (Integer) nresults;
	// // result[1] = (Long) time;
	// // result[2] = (Long) cpu;
	// }catch (Exception e){
	// exception = true;
	// if (++count >= maxAttempts){
	// System.out.print(e.getMessage());
	// throw e;
	// }
	// }
	// }while (exception);
	// return result;
	// }

	private Set<String[]> getNotConnectedConcepts(int maxDeep, List<IOntoBase> concepts,
			Map<String, Set<String>> superConcepts, Map<String, Set<String>> subConcepts, OntoFilter filterAttributes,
			OntoFilter filterConcepts) {
		Set<String[]> notConnected = new HashSet<String[]>();
		Set<String[]> connected = new HashSet<String[]>();
		for (int i = 0; i < concepts.size(); i++) {
			String origin = concepts.get(i).getURI();
			// if (i>0) break;
			for (int j = i + 1; j < concepts.size(); j++) {

				// if (j>20)break;
				String end = concepts.get(j).getURI();
				System.out.println(+i + ":" + j + " " + origin + " - " + end);
				String[] pair = { origin, end };
				boolean processPair = true;
				if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(origin, false),
						this.getJenaModel().getResource(end, false)) == null) {
					Set<String[]> toAdd = new HashSet<String[]>(1);
					for (String[] conn : connected) {
						if (superConcepts.get(conn[0]).contains(origin))
							if (superConcepts.get(conn[1]).contains(end)) {
								processPair = false;
								toAdd.add(pair);
								break;
							}
					}
					connected.addAll(toAdd);
					toAdd = new HashSet<String[]>(1);
					for (String[] notconn : notConnected) {
						if (subConcepts.get(notconn[0]).contains(origin))
							if (subConcepts.get(notconn[1]).contains(end)) {
								toAdd.add(pair);
								processPair = false;
								break;
							}
					}
					notConnected.addAll(toAdd);
					if (processPair) {
						Map<Integer, Set<String[]>> paths = this.GetPaths(origin, end, maxDeep, false,
								filterAttributes, filterConcepts, true);
						if (paths == null || paths.keySet().size() == 0) {
							notConnected.add(pair);
							// fw.write(i+" "+j+" "+origin+" "+end);
							System.out.println("NOTCONNECTED:" + i + ":" + j + " " + origin + " - " + end);
						} else {
							connected.add(pair);
						}
					}

				}
			}
		}
		return notConnected;
	}

	// Returns the concepts that should be included in the view and remove the
	// pairs origen-end that connects from the notConnected var
	private Set<String> selectedPairs(Map<String, Set<String[]>> url_pairs, Set<String[]> notConnected) {
		Set<String> view = new HashSet<String>();
		List<Object[]> list_url_pairs = new ArrayList<Object[]>(); // urls-number
																	// of pairs
																	// connected
		for (String url : url_pairs.keySet()) {
			Object[] item = new Object[2];
			item[0] = url;
			item[1] = url_pairs.get(url).size();
			list_url_pairs.add(item);
		}
		java.util.Collections.sort(list_url_pairs, new URL_pairs_Comparator()); // TODO:
																				// check
																				// if
																				// the
																				// order
																				// is
																				// descendant
		for (Object[] item : list_url_pairs) {
			String[] urls = ((String) item[0]).split("-");
			if (notConnected.size() <= 0)
				break;
			for (String url : urls)
				if (url != null && !url.equals(""))
					view.add(url);
			for (Object[] pair : url_pairs.get((String) item[0]))
				notConnected.remove(pair);

		}
		return view;
	}

	public OntoFilter createFilterViewPlanningPerspective() {
		// DEBUG
		OntoFilter filt = this.filterPlanningPerspective;
		// OntoFilter = new OntoFilter(new HashSet<IOntoBase>(), new
		// HashSet<IOntoBase>();
		// Map<Integer, Set<String[]>> pathss =
		// this.GetPaths("http://www.ibm.com/ICP/Scribe/CoreV2#PedestrianStreet",
		// "http://www.ibm.com/ICP/Scribe/CoreV2#RecreationalUse", 3, false,
		// this.filterAttributeProperties, filt);

		Set<IOntoBase> inView = new HashSet<IOntoBase>();
		Set<String> view = new HashSet<String>();
		int maxDeep = 3;
		List<IOntoBase> upconcepts = new ArrayList<IOntoBase>(this.getFilterPlanningPerspective().getValids());
		Map<String, Set<String>> superConcepts = new HashMap<String, Set<String>>();
		Map<String, Set<String>> subConcepts = new HashMap<String, Set<String>>();

		for (IOntoBase concept : upconcepts) {
			if (!superConcepts.containsKey(concept.getURI())) {
				Set<String> sc = new HashSet<String>();
				for (Resource r : this.getJenaModel().GetSuperHierarchy(concept.getResource(), true))
					sc.add(r.getURI());
				superConcepts.put(concept.getURI(), sc);
			}
			if (!subConcepts.containsKey(concept.getURI())) {
				Set<String> sc = new HashSet<String>();
				for (Resource r : this.getJenaModel().GetSubHierarchy(concept.getResource(), true, false))
					sc.add(r.getURI());
				subConcepts.put(concept.getURI(), sc);
			}
		}
		Map<String, Set<String>> connected = new HashMap<String, Set<String>>();
		for (IOntoBase c1 : upconcepts) {
			Set<String> innerRanges = new HashSet<String>();
			if (connected.containsKey(c1.getURI()))
				innerRanges = connected.get(c1.getURI());
			else
				connected.put(c1.getURI(), innerRanges);
			for (IOntoBase c2 : upconcepts) {
				innerRanges.add(c2.getURI());
			}

		}

		// inView.addAll(upconcepts);
		Set<String[]> notConnected = getNotConnectedConcepts(maxDeep, upconcepts, superConcepts, subConcepts,
				this.filterAttributeProperties, this.filterPlanningPerspective);
		for (String[] pair : notConnected) {
			String o = pair[0];
			String e = pair[1];
			if (connected.containsKey(o)) {
				Set<String> innerRanges = connected.get(o);
				innerRanges.remove(e);
				if (innerRanges.size() == 0)
					connected.remove(o);
			}
			if (connected.containsKey(e)) {
				Set<String> innerRanges = connected.get(e);
				innerRanges.remove(o);
				if (innerRanges.size() == 0)
					connected.remove(e);
			}

		}
		Set<String[]> toRemove = new HashSet<String[]>();
		for (String[] notcon : notConnected) {
			String origin = notcon[0];
			String end = notcon[1];
			for (String subOrig : subConcepts.get(origin)) {
				if (connected.containsKey(subOrig)) {
					for (String range : connected.get(subOrig))
						for (String subEnd : subConcepts.get(end))
							if (subEnd.equals(range)) {
								toRemove.add(notcon);
								System.out.println("Connected by subclasses:" + origin + " " + end);
							}
				}
			}
		}
		notConnected.removeAll(toRemove);
		File f = new File("notconnected.txt");
		java.io.FileWriter fw;
		try {
			fw = new java.io.FileWriter(f);
			for (String[] pair : notConnected) {
				fw.write(pair[0] + " " + pair[1] + "\n");
			}
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new OntoFilter(inView, new HashSet<IOntoBase>());
	}

	public void TestCreateView() {
		int maxDeep = 4;
		try {
			OntoFilter nofilter = new OntoFilter(new HashSet<IOntoBase>(), new HashSet<IOntoBase>());
			java.io.FileReader fr = new java.io.FileReader(new java.io.File("notconnected.txt"));
			java.io.BufferedReader br = new java.io.BufferedReader(fr);
			Set<String[]> noconnected = new HashSet<String[]>();
			Set<String[]> connected = new HashSet<String[]>();
			String line = br.readLine();
			int count = 0;
			while (line != null) {
				System.out.println(++count);
				String[] pair = line.split(" ");
				String origin = pair[0];
				String end = pair[1];
				boolean processed = false;
				for (String[] proc : connected) {
					String procOrig = proc[0];
					String procEnd = proc[1];
					for (Resource r : this.getJenaModel().GetSubHierarchy(
							this.getJenaModel().getResource(procOrig, false), true, false)) {
						if (r.getURI().equals(origin)) {
							for (Resource r2 : this.getJenaModel().GetSubHierarchy(
									this.getJenaModel().getResource(procEnd, false), true, false))
								if (r2.getURI().equals(end))
									processed = true;
						}
					}
					for (Resource r : this.getJenaModel().GetSubHierarchy(
							this.getJenaModel().getResource(origin, false), true, false)) {
						if (r.getURI().equals(procOrig)) {
							for (Resource r2 : this.getJenaModel().GetSubHierarchy(
									this.getJenaModel().getResource(end, false), true, false))
								if (r2.getURI().equals(procEnd))
									processed = true;
						}
					}
				}
				if (!processed) {
					Map<Integer, Set<String[]>> paths = this.GetPaths(origin, end, maxDeep, false,
							this.filterAttributeProperties, nofilter, true);
					if (paths != null && paths.keySet().size() > 0) {
						// System.out.println(origin+" - "+end+" Yes");
						// connected.add(new String[] {origin, end});
					} else {
						System.out.println(origin + " - " + end + " NO");
						noconnected.add(new String[] { origin, end });
					}
				}
				line = br.readLine();
			}
			br.close();
			File f = new File("impossibletoconnect.txt");
			java.io.FileWriter fw = new java.io.FileWriter(f);
			for (String[] pair : noconnected) {
				fw.write(pair[0] + " " + pair[1]);
			}
			fw.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void ProcessPairs() {
		try {
			java.io.FileReader fr = new java.io.FileReader(new java.io.File("notconnected.txt"));
			java.io.BufferedReader br = new java.io.BufferedReader(fr);
			File f = new File("notconnected2.txt");
			java.io.FileWriter fw = new java.io.FileWriter(f);
			String line = br.readLine();
			Resource indType = this.getJenaModel().getResource(core + "#IndicatorType", false);
			Resource assetUse = this.getJenaModel().getResource(core + "#AssetUse", false);
			Resource entity = this.getJenaModel().getResource(core + "#Entity", false);
			Resource indDesc = this.getJenaModel().getResource(core + "#IndicatorDescriptor", false);
			Map<String, String> pairs = new HashMap<String, String>();
			int count = 0;
			while (line != null) {
				String[] pair = line.split(" ");
				String origin = pair[0];
				String end = pair[1];
				if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(origin, false),
						this.getJenaModel().getResource(end, false)) == null) {
					String origin2 = origin;
					String end2 = end;
					if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(origin, false), entity) != null)
						origin2 = "[" + entity.getURI() + "]";
					else if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(origin, false),
							assetUse) != null)
						origin2 = "[" + assetUse.getURI() + "]";
					else if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(origin, false),
							indDesc) != null)
						origin2 = "[" + indDesc.getURI() + "]";
					else if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(origin, false),
							indType) != null)
						origin2 = "[" + indType.getURI() + "]";
					if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(end, false), entity) != null)
						end2 = "[" + entity.getURI() + "]";
					else if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(end, false), assetUse) != null)
						end2 = "[" + assetUse.getURI() + "]";
					else if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(end, false), indDesc) != null)
						end2 = "[" + indDesc.getURI() + "]";
					else if (this.getJenaModel().getSuperconcept(this.getJenaModel().getResource(end, false), indType) != null)
						end2 = "[" + indType.getURI() + "]";
					pairs.put(origin2, end2);
				} else
					System.out.println("Why is this here?");

				line = br.readLine();
				count++;
			}
			fr.close();
			for (String origin : pairs.keySet()) {
				fw.write(origin + " " + pairs.get(origin) + "\n");
			}
			fw.close();
			System.out.println("Número de lineas:" + count);
			System.out.println("Número de pares:" + pairs.size());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
