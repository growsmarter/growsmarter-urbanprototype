package smartcities.semanticweb.ontologycommons.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;

class IOntoLinkComparator implements Comparator<IOntoLink> {
	public int compare(IOntoLink o1, IOntoLink o2) {
		boolean o1f =o1.getRelationship().isFunction();
		boolean o2f = o2.getRelationship().isFunction();
		if (o1f && !o2f)
			return -1;
		else if (o2f && !o1f)
			return 1;
		return 0;
	}
}



class QueryGraph {
	Map<Integer, String> sparqlQuery = new HashMap<Integer, String>();
	Map<Integer, IOntoBase> nodeToConcept = new HashMap<Integer, IOntoBase>();
	Map<Integer, smartcities.semanticweb.ontologycommons.services.QueryResults> queryResult = new HashMap<Integer, smartcities.semanticweb.ontologycommons.services.QueryResults>();
	//Map<Integer, List<IOntoLink>> graphId = new HashMap<Integer, List<IOntoLink>>();
	Map<Integer, Integer> nodeToSubgraphId = new HashMap<Integer, Integer>();
	List<List<IOntoLink>> subgraphs = new ArrayList<List<IOntoLink>>(); 
	UrbanOntology uOntology = null;
	int subgraphCount = 0;
	String parliamentmodel;


	public IOntoBase getConcept(int idNode){
		return nodeToConcept.get(idNode);
	}
	
	public List<List<IOntoLink>> getSubgraphs() {
		return subgraphs;
	}

	public int getRowsNumber(int idSubgraph){
		smartcities.semanticweb.ontologycommons.services.QueryResults qr = queryResult.get(idSubgraph);
		if (qr == null)
			return -1;
		return qr.getResults().size();
	}
	
	public String getSparql(int idSubgraph){
		String query = sparqlQuery.get(idSubgraph);
		if (query == null)
			return null;
		return query;
	}
	
	public int getSubgraphId(int idNode){
		Integer subgId = this.nodeToSubgraphId.get(idNode);
		if (subgId == null)
			return -1;
		return subgId;
	}
	
	
	public QueryGraph(Set<IOntoBase> nodes,List<IOntoLink> links, UrbanOntology uOntology, String idSession, String querytype, String parliamentmodel){
		this.uOntology = uOntology;
		this.parliamentmodel = parliamentmodel;
		if (querytype.equals("jena")){
			for (IOntoBase node: nodes){
				QueryGraphNodesJena(node, uOntology, idSession);
			}
			QueryGraphLinksJena(links,uOntology, idSession);
		} else if (querytype.equals("parliament")){
			for (IOntoBase node: nodes){
				QueryGraphNodesParliament(node, uOntology, idSession);
			}
			QueryGraphLinksParliament(links,uOntology, idSession);
		} else System.err.println("Query type"+querytype+" from config.properties not recognized");
			
	}

	
	public QueryGraph(IOntoBase node, UrbanOntology uOntology, String idSession, String querytype, String parliamentmodel){
		this.uOntology = uOntology;
		this.parliamentmodel = parliamentmodel;
		if (querytype.equals("jena")){
			QueryGraphNodesJena(node, uOntology, idSession);
		} else if (querytype.equals("parliament")){
			QueryGraphNodesParliament(node, uOntology, idSession);
		} else System.err.println("Query type"+querytype+" from config.properties not recognized");
	}
	
	public QueryGraph(List<IOntoLink> graph, UrbanOntology uOntology, String idSession, String querytype, String parliamentmodel){
		this.uOntology = uOntology;
		this.parliamentmodel = parliamentmodel;
		if (querytype.equals("jena")){
			QueryGraphLinksJena(graph, uOntology, idSession);
		} else if (querytype.equals("parliament")){
			QueryGraphLinksParliament(graph, uOntology, idSession);
		} else System.err.println("Query type"+querytype+" from config.properties not recognized");
	}


	private void initialize(String sparqlQuery, QueryResults qResult, IOntoBase node){
		int graphId = subgraphCount++;
		this.sparqlQuery.put(graphId, sparqlQuery);
		this.queryResult.put(graphId, qResult);
		this.nodeToSubgraphId.put(node.getId(), graphId);
		this.nodeToConcept.put(node.getId(), node);
	}
	
	private void initialize(String sparqlQuery, QueryResults qResult, List<IOntoLink> graph){
		int graphId = subgraphCount++;
		this.sparqlQuery.put(graphId, sparqlQuery);
		this.queryResult.put(graphId, qResult);
		for (IOntoLink link:graph){
			this.nodeToSubgraphId.put(link.getDomain().getId(), graphId);
			this.nodeToSubgraphId.put(link.getRange().getId(), graphId);
			this.nodeToConcept.put(link.getDomain().getId(), link.getDomain());
			this.nodeToConcept.put(link.getRange().getId(), link.getRange());
		}
	}

	private void QueryGraphNodesJena(IOntoBase node, UrbanOntology uOntology, String idSession){
		StringBuffer sbQuery = new StringBuffer();
		String where = createGraphQueryJena(node, parliamentmodel, idSession);
		sbQuery.append(where);
		QueryResults qResults = uOntology.getJenaModel().ResultSetQueryOntology(sbQuery.toString(), true, idSession);
		if (qResults != null)
			initialize(sbQuery.toString(),qResults, node);
	}
	
	public void QueryGraphLinksJena(List<IOntoLink> graph, UrbanOntology uOntology, String idSession){
		this.subgraphs = getConnectedGraphs(graph);
		for (List<IOntoLink> subgraph: subgraphs){ //TODO: para cada subgrafo, si sólo links funciones, crear query para parliament y lanzarlo en Parliament
			StringBuffer sbQuery = new StringBuffer();
			String where = createGraphQueryJena(subgraph, parliamentmodel, idSession);
			sbQuery.append(where);
			QueryResults qResults = uOntology.getJenaModel().ResultSetQueryOntology(sbQuery.toString(), true, idSession);
			if (qResults != null)
				initialize(sbQuery.toString(),qResults, subgraph);
		}
	}
	
	private void QueryGraphNodesParliament(IOntoBase node, UrbanOntology uOntology, String idSession) {
		StringBuffer sbQuery = new StringBuffer();
		String where = createGraphQueryOnlyParliament(node, parliamentmodel, idSession);
		sbQuery.append(where);
		QueryResults qResults = uOntology.getParliamentModel().ResultSetQueryOntology(sbQuery.toString());
		if (qResults != null)
			initialize(sbQuery.toString(),qResults, node);
	}
	
	public void QueryGraphLinksParliament(List<IOntoLink> graph, UrbanOntology uOntology, String idSession) {
		this.subgraphs = getConnectedGraphs(graph);
		for (List<IOntoLink> subgraph: subgraphs){ //TODO: para cada subgrafo, si sólo links funciones, crear query para parliament y lanzarlo en Parliament
			StringBuffer sbQuery = new StringBuffer();
			String where = createGraphQueryOnlyParliament(subgraph, parliamentmodel, idSession);
			sbQuery.append(where);
			QueryResults qResults = uOntology.getParliamentModel().ResultSetQueryOntology(sbQuery.toString());
			if (qResults != null)
				initialize(sbQuery.toString(),qResults, subgraph);
		}
	}
	

	private List<String> getPolygons(String wkt_multipolygon){
	    Pattern pattern = Pattern.compile("(?<pol>\\(\\([^\\)\\(]*\\)\\))");
	    Matcher matcher = pattern.matcher(wkt_multipolygon);
	    List<String> results = new ArrayList<String>();
	    while (matcher.find()) {
	      results.add("POLYGON "+matcher.group("pol"));
	    }
	    return results;
	}
	
	private String getIndividual(IOntoBase concept){
		if (concept.getType() == IOntoBase.OntType.CLASS || concept.getType() == IOntoBase.OntType.ECLASS)
			return "?ind"+concept.getId();
		return "";
	}
	
	private String getSubclass(IOntoBase concept){
		if (concept.getType() == IOntoBase.OntType.CLASS || concept.getType() == IOntoBase.OntType.ECLASS){
			String classDomain = "?indSub"+concept.getId();
			return (classDomain+ " <"+UrbanOntology.rdfs+"#subClassOf> <"+concept.getURI()+"> . ");
			}
		return "";
	}
	
	private String getType(IOntoBase concept){
		if (concept.getType() != IOntoBase.OntType.DINDIVIDUAL || concept.getType() == IOntoBase.OntType.EINDIVIDUAL){
			String indDomain = "?ind"+concept.getId();
			String classDomain = "?indSub"+concept.getId();
			return(indDomain+ " <"+UrbanOntology.rdf+"#type> "+classDomain+" . ");
			}
		return "";
	}
	
	private String getDirectType(IOntoBase concept){
		if (concept.getType() != IOntoBase.OntType.DINDIVIDUAL || concept.getType() == IOntoBase.OntType.EINDIVIDUAL){
			String indDomain = "?ind"+concept.getId();
			return(indDomain+ " <"+UrbanOntology.rdf+"#type> <"+concept.getURI()+"> . ");
			}
		return "";
	}


	private String getSubproperty(IOntoBase concept){
		if (concept.getType() == IOntoBase.OntType.PROPERTY){
			String classDomain = "?indSub"+concept.getId();
			return (classDomain+ " <"+UrbanOntology.rdfs+"#subPropertyOf> <"+concept.getURI()+"> . ");
			}
		return "";
	}
	
	private String getProperty(IOntoLink link){
		if (!link.getRelationship().isFunction()){
			String indDomain =getIndividual(link.getDomain());
			String indRange= getIndividual(link.getRange());
			if (indDomain.isEmpty())
				indDomain = "<"+link.getDomain().getURI()+">";
			if (indRange.isEmpty())
				indRange = "<"+link.getRange().getURI()+">";
			return (indDomain+" "+"?indSub"+link.getRelationship().getId()+" "+indRange+" . ");
		}
		return "";
	}
	
	private String getDirectProperty(IOntoLink link){
		if (!link.getRelationship().isFunction()){
			String indDomain =getIndividual(link.getDomain());
			String indRange= getIndividual(link.getRange());
			if (indDomain.isEmpty())
				indDomain = "<"+link.getDomain().getURI()+">";
			if (indRange.isEmpty())
				indRange = "<"+link.getRange().getURI()+">";
			return (indDomain+" <"+link.getRelationship().getURI()+"> "+indRange+" . ");
		}
		return "";
	}

	
	private String getFunction (IOntoLink link){
		StringBuffer result = new StringBuffer();
		if (link.getRelationship().isFunction()){
			String wktdomain = null;
			String wktrange = null;
			if (link.getDomain().isAreaInstance()){
				IOntoArea dArea = new OntoArea(link.getDomain(), uOntology);
				wktdomain= dArea.getWKT();
			}
			if (link.getRange().isAreaInstance()){
				IOntoArea rArea = new OntoArea(link.getRange(),uOntology);
				wktrange= rArea.getWKT();
			}
			if (wktdomain != null && wktdomain.startsWith("MULTI")){
				List<String> wkt_list = this.getPolygons(wktdomain);
				for (String wkt:wkt_list){
					result.append("{");
					result.append(GetAreaQueryAlternative(link, wkt, wktrange));
					result.append("} UNION ");
				}
				result.append("{}");
			} else if (wktrange != null && wktrange.startsWith("MULTI")){
				List<String> wkt_list = this.getPolygons(wktrange);
				for (String wkt:wkt_list){
					result.append("{");
					result.append(GetAreaQueryAlternative(link, wktdomain, wkt));
					result.append("} UNION ");
				}
				result.append("{}");
			} else {
				result.append(GetAreaQueryAlternative(link, wktdomain, wktrange));
			}
			return result.toString();
		}
		return "";
	}
	
	private String createGraphQueryOnlyParliament(List<IOntoLink> graph, String parliamentGraph, String idSession){
		Map<Integer,String> individual = new HashMap<Integer,String>();
		Map<Integer,String> subclass = new HashMap<Integer,String>();
		Map<Integer,String> subproperty = new HashMap<Integer,String>();
		Map<Integer,String> function = new HashMap<Integer,String>();
		Map<Integer,String> property = new HashMap<Integer,String>();
		Map<Integer,String> classtype = new HashMap<Integer,String>();
		//Map<Integer,String> propertytype = new HashMap<Integer,String>();
		
		StringBuffer sbWhere = new StringBuffer();
		StringBuffer sbSelect = new StringBuffer();
		sbSelect.append("SELECT DISTINCT ");
		sbWhere.append(" WHERE { GRAPH <"+parliamentGraph+"> {BIND('"+idSession+"' AS ?idSession) . ");

		boolean functions = false;
		for (IOntoLink link:graph){
			if (link.getRelationship().isFunction())
				functions = true;
			Integer idRel = link.getRelationship().getId();
			Integer idDomain = link.getDomain().getId();
			Integer idRange = link.getRange().getId(); 
			if (!individual.containsKey(idDomain)){
				individual.put(idDomain,getIndividual(link.getDomain()));
				//subclass.put(idDomain, getSubclass(link.getDomain()));
				classtype.put(idDomain, getDirectType(link.getDomain()));
			}
			
			if (!individual.containsKey(idRange)){
				individual.put(idRange,getIndividual(link.getRange()));
				//subclass.put(idRange, getSubclass(link.getRange()));
				classtype.put(idRange, getDirectType(link.getRange()));
			}
			
			//subproperty.put(idRel, getSubproperty(link.getRelationship()));
			
			function.put(idRel, getFunction(link));
			property.put(idRel, getDirectProperty(link));
			

			
		}
		for (String ind: individual.values()){
			if (!ind.isEmpty())
				sbSelect.append(ind+" ");
		}

		for (String subp: subproperty.values()){
			if (!subp.isEmpty())
				sbWhere.append(subp+" ");
		}
		
		for (String subc: subclass.values()){
			if (!subc.isEmpty())
				sbWhere.append(subc+" ");
		}
		
		for (String prop: property.values()){
			if (!prop.isEmpty())
				sbWhere.append(prop+" ");
		}

//		for (String tp: propertytype.values()){
//			if (!tp.isEmpty())
//				sbWhere.append(tp+" ");
//		}

		for (String tp: classtype.values()){
			if (!tp.isEmpty())
				sbWhere.append(tp+" ");
		}
		
		if (functions){
			//sbWhere.append(" SERVICE <http://redrock.bsc.es:8080/parliament/sparql> { GRAPH <"+parliamentGraph+"> {BIND('"+idSession+"' AS ?idSession) . ");
			for (String funct: function.values()){
				if (!funct.isEmpty())
					sbWhere.append(funct+" ");
			}
			//sbWhere.append("}");
		}

		sbWhere.append("}}");
		String where = sbWhere.toString().replace("UNION {}", "");
		
		
		return sbSelect.toString()+where;
		
	}

	
	private String createGraphQueryJena(List<IOntoLink> graph, String parliamentGraph, String idSession){
		Map<Integer,String> individual = new HashMap<Integer,String>();
		Map<Integer,String> subclass = new HashMap<Integer,String>();
		Map<Integer,String> subproperty = new HashMap<Integer,String>();
		Map<Integer,String> function = new HashMap<Integer,String>();
		Map<Integer,String> property = new HashMap<Integer,String>();
		Map<Integer,String> classtype = new HashMap<Integer,String>();
		//Map<Integer,String> propertytype = new HashMap<Integer,String>();
		
		StringBuffer sbWhere = new StringBuffer();
		StringBuffer sbSelect = new StringBuffer();
		sbSelect.append("SELECT DISTINCT ");
		sbWhere.append(" WHERE { ");

		boolean functions = false;
		for (IOntoLink link:graph){
			if (link.getRelationship().isFunction())
				functions = true;
			Integer idRel = link.getRelationship().getId();
			Integer idDomain = link.getDomain().getId();
			Integer idRange = link.getRange().getId(); 
			if (!individual.containsKey(idDomain)){
				individual.put(idDomain,getIndividual(link.getDomain()));
				subclass.put(idDomain, getSubclass(link.getDomain()));
				classtype.put(idDomain, getType(link.getDomain()));
			}
			
			if (!individual.containsKey(idRange)){
				individual.put(idRange,getIndividual(link.getRange()));
				subclass.put(idRange, getSubclass(link.getRange()));
				classtype.put(idRange, getType(link.getRange()));
			}
			
			subproperty.put(idRel, getSubproperty(link.getRelationship()));
			//propertytype.put(idRel, getType(link.getRelationship()));
			function.put(idRel, getFunction(link));
			property.put(idRel, getProperty(link));
			

			
		}
		for (String ind: individual.values()){
			if (!ind.isEmpty())
				sbSelect.append(ind+" ");
		}

		for (String subp: subproperty.values()){
			if (!subp.isEmpty())
				sbWhere.append(subp+" ");
		}
		
		for (String subc: subclass.values()){
			if (!subc.isEmpty())
				sbWhere.append(subc+" ");
		}
		
		for (String prop: property.values()){
			if (!prop.isEmpty())
				sbWhere.append(prop+" ");
		}

//		for (String tp: propertytype.values()){
//			if (!tp.isEmpty())
//				sbWhere.append(tp+" ");
//		}

		for (String tp: classtype.values()){
			if (!tp.isEmpty())
				sbWhere.append(tp+" ");
		}
		
		if (functions){
			//deprectated: redrock died
			System.out.println("DEPRECATED: QueryGraph querying http://redrock.bsc.es:8080/parliament/sparql");
			sbWhere.append(" SERVICE <http://redrock.bsc.es:8080/parliament/sparql> { GRAPH <"+parliamentGraph+"> {BIND('"+idSession+"' AS ?idSession) . ");
			for (String funct: function.values()){
				if (!funct.isEmpty())
					sbWhere.append(funct+" ");
			}
			sbWhere.append("}}");
		}

		sbWhere.append("}");
		String where = sbWhere.toString().replace("UNION {}", "");
		
		
		return sbSelect.toString()+where;
		
	}
	
	private String GetAreaQueryAlternative(IOntoLink link, String wktdomain, String wktrange){
		StringBuffer sbWhere = new StringBuffer();
		IOntoFunction function = (IOntoFunction) link.getRelationship();
		String uridomain="";
		String urirange="";
		String inddomain="";
		String indrange="";
		if (wktdomain == null){ //is not an area
			IOntoBase d = link.getDomain();
			uridomain = d.getURI();
			inddomain = "?ind"+d.getId();
			String inddomaingeo = inddomain+"GEO";
			wktdomain = inddomain+"WKT";
			//sbWhere.append(inddomain+ " <"+UrbanOntology.rdf+"#type> <"+uridomain+"> . ");
			sbWhere.append(inddomain+ " <"+IOntoArea.GEOMETRY_PROPERTY_URI+"> " + inddomaingeo +" . ");
			sbWhere.append(inddomaingeo+ " <"+IOntoArea.WKT_PROPERTY_URI+"> " + wktdomain +" . ");
		} else wktdomain = "'"+wktdomain+"'^^<"+IOntoArea.WKT_DATATYPE_URI+">";
		if (wktrange == null){ //is not an area
			IOntoBase r = link.getRange();
			urirange = r.getURI();
			indrange = "?ind"+r.getId();
			String indrangegeo = indrange+"GEO";
			wktrange = indrange+"WKT";
			//sbWhere.append(indrange+ " <"+UrbanOntology.rdf+"#type> <"+urirange+"> . ");
			sbWhere.append(indrange+ " <"+IOntoArea.GEOMETRY_PROPERTY_URI+"> " + indrangegeo +" . ");
			sbWhere.append(indrangegeo+ " <"+IOntoArea.WKT_PROPERTY_URI+"> " + wktrange +" . ");
		} else wktrange = "'"+wktrange+"'^^<"+IOntoArea.WKT_DATATYPE_URI+">";
		if (function.getFunctionType() == IOntoFunction.OntFunction.DISTANCE){
			String distance = function.getParams().get(0);
			//TODO: the specification of the projection system should be configurable. Apparently only the distance function in parliament needs this information
			sbWhere.append("FILTER( <"+function.getURI()+">("+wktdomain+","+wktrange+",<http://www.opengis.net/def/uom/OGC/1.0/metre>)<"+distance+") .");
		} else {
			sbWhere.append("FILTER( <"+function.getURI()+">("+wktdomain+","+wktrange+")) .");
		}
		return sbWhere.toString();
	}
	
	private String createGraphQueryJena(IOntoBase node, String parliamentGraph, String idSession){
		Set<String> individuals = new HashSet<String>();
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("SELECT DISTINCT *");
		sbQuery.append(" WHERE { ");
		String domain = node.getURI();
		String indDomain;
		indDomain = "?ind"+node.getId();
		String classNode = "?indClass"+node.getId();
		//sbQuery.append(indDomain+ " <"+UrbanOntology.rdf+"#type> <"+domain+"> . ");
		sbQuery.append(indDomain+ " <"+UrbanOntology.rdf+"#type> "+classNode+" . ");
		sbQuery.append(classNode+ " <"+UrbanOntology.rdfs+"#subClassOf> <"+domain+"> . ");
		sbQuery.append("}");
		individuals.add(indDomain);
		return sbQuery.toString();
	}
	
	private String createGraphQueryOnlyParliament(IOntoBase node, String parliamentGraph, String idSession){
		Set<String> individuals = new HashSet<String>();
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("SELECT DISTINCT *");
		sbQuery.append(" WHERE { GRAPH <"+parliamentGraph+"> {BIND('"+idSession+"' AS ?idSession) . ");
		String domain = node.getURI();
		String indDomain;
		indDomain = "?ind"+node.getId();
		String classNode = "?indClass"+node.getId();
		//sbQuery.append(indDomain+ " <"+UrbanOntology.rdf+"#type> <"+domain+"> . ");
		sbQuery.append(indDomain+ " <"+UrbanOntology.rdf+"#type> <"+domain+"> . ");
		//sbQuery.append(classNode+ " <"+UrbanOntology.rdfs+"#subClassOf> <"+domain+"> . ");
		sbQuery.append("}}");
		individuals.add(indDomain);
		return sbQuery.toString();
	}
		
//	private String createGraphQuery(List<IOntoLink> graph, String parliamentGraph){
//		Set<String> individuals = new HashSet<String>();
//		StringBuffer sbWhere = new StringBuffer();
//		StringBuffer sbSelect = new StringBuffer();
//		sbWhere.append(" WHERE {");
//		List<Integer> processed = new ArrayList<Integer>();
//		java.util.Collections.sort(graph, new IOntoLinkComparator()); //first parliament query
//		for (IOntoLink link: graph){
//			String domain = link.getDomain().getURI();
//			String range = link.getRange().getURI();
//			String property = link.getRelationship().getURI();
//			//if (!(link.getRelationship().isFunction())){
//				String indDomain;
//				String indRange;
//				StringBuffer typeDomain = new StringBuffer();
//				StringBuffer typeRange = new StringBuffer();
//				//TODO: Si son enum-types, la consulta se genera igual, pero el individuo que aparece en el select es su clase. Se añade la relación enum-type y clase al where
//				if (link.getDomain().getType() == IOntoBase.OntType.CLASS || link.getDomain().getType() == IOntoBase.OntType.ECLASS){
//					indDomain = "?ind"+link.getDomain().getId();
//					String classDomain = "?indClass"+link.getDomain().getId();
//					if (!processed.contains(link.getDomain().getId())){
//						//typeDomain.append(indDomain+ " <"+UrbanOntology.rdf+"#type> <"+domain+"> . ");
//						
//						typeDomain.append(indDomain+ " <"+UrbanOntology.rdf+"#type> "+classDomain+" . ");
//						typeDomain.append(classDomain+ " <"+UrbanOntology.rdfs+"#subClassOf> <"+domain+"> . ");
//						
//						individuals.add(indDomain);
//						processed.add(link.getDomain().getId());
//						
//					}
//				} else {
//					 indDomain = "<"+link.getDomain().getURI()+">";
//				}
//				
//				if (link.getRange().getType() == IOntoBase.OntType.CLASS || link.getRange().getType() == IOntoBase.OntType.ECLASS){
//					String classRange = "?indClass"+link.getRange().getId();
//					indRange = "?ind"+link.getRange().getId();
//					if (!processed.contains(link.getRange().getId())){
//						
//						//typeRange.append(indRange+" <"+UrbanOntology.rdf+"#type> <"+classRange+"> . ");
//						
//						typeRange.append(indRange+" <"+UrbanOntology.rdf+"#type> "+classRange+" . ");
//						typeRange.append(classRange+" <"+UrbanOntology.rdfs+"#subClassOf> <"+range+"> . ");
//						
//						individuals.add(indRange);
//						processed.add(link.getRange().getId());
//						
//					}
//				} else {
//					indRange = "<"+link.getRange().getURI()+">";
//				}
//				
//			//	} 
//			//else {
//			sbWhere.append(typeDomain);
//			sbWhere.append(typeRange);
//
//			if (link.getRelationship().isFunction()){
//				//sbWhere.append(" SERVICE <http://redrock.bsc.es:8080/parliament/sparql> { GRAPH <http://BCNEcologyINST> {");
//				sbWhere.append(" SERVICE <http://redrock.bsc.es:8080/parliament/sparql> { GRAPH <"+parliamentGraph+"> {");
//				String wktdomain = null;
//				String wktrange = null;
//				if (link.getDomain().isAreaInstance()){
//					IOntoArea dArea = new OntoArea(link.getDomain(), uOntology);
//					wktdomain= dArea.getWKT();
//				}
//				if (link.getRange().isAreaInstance()){
//					IOntoArea rArea = new OntoArea(link.getRange(),uOntology);
//					wktrange= rArea.getWKT();
//				}
//				if (wktdomain != null && wktdomain.startsWith("MULTI")){
//					List<String> wkt_list = this.getPolygons(wktdomain);
//					for (String wkt:wkt_list){
//						sbWhere.append("{");
//						sbWhere.append(GetAreaQuery(link,individuals, wkt, wktrange));
//						sbWhere.append("} UNION ");
//					}
//					sbWhere.append("{}");
//				} else if (wktrange != null && wktrange.startsWith("MULTI")){
//					List<String> wkt_list = this.getPolygons(wktrange);
//					for (String wkt:wkt_list){
//						sbWhere.append("{");
//						sbWhere.append(GetAreaQuery(link,individuals, wktdomain, wkt));
//						sbWhere.append("} UNION ");
//					}
//					sbWhere.append("{}");
//				} else {
//					sbWhere.append(GetAreaQuery(link,individuals, wktdomain, wktrange));
//				}
//				
//				sbWhere.append("}}");
//			} else {
//				sbWhere.append(indDomain+" <"+property+"> "+indRange+" . ");
//			}
//		}
//		sbWhere.append("}");
//		
//		sbSelect.append("SELECT DISTINCT ");
//		for (String ind: individuals){
//			sbSelect.append(ind+" ");
//		}
//		sbSelect.append(sbWhere);
//		String result = sbSelect.toString().replace("UNION {}", "");
//		return result;
//	}
//	
//	private String GetAreaQuery(IOntoLink link, Set<String> individuals, String wktdomain, String wktrange){
//		StringBuffer sbWhere = new StringBuffer();
//		IOntoFunction function = (IOntoFunction) link.getRelationship();
//		String uridomain="";
//		String urirange="";
//		String inddomain="";
//		String indrange="";
//		if (wktdomain == null){ //is not an area
//			IOntoBase d = link.getDomain();
//			uridomain = d.getURI();
//			inddomain = "?ind"+d.getId();
//			String inddomaingeo = inddomain+"GEO";
//			wktdomain = inddomain+"WKT";
//			//sbWhere.append(inddomain+ " <"+UrbanOntology.rdf+"#type> <"+uridomain+"> . ");
//			sbWhere.append(inddomain+ " <"+IOntoArea.GEOMETRY_PROPERTY_URI+"> " + inddomaingeo +" . ");
//			sbWhere.append(inddomaingeo+ " <"+IOntoArea.WKT_PROPERTY_URI+"> " + wktdomain +" . ");
//		} else wktdomain = "'"+wktdomain+"'^^<"+IOntoArea.WKT_DATATYPE_URI+">";
//		if (wktrange == null){ //is not an area
//			IOntoBase r = link.getRange();
//			urirange = r.getURI();
//			indrange = "?ind"+r.getId();
//			String indrangegeo = indrange+"GEO";
//			wktrange = indrange+"WKT";
//			//sbWhere.append(indrange+ " <"+UrbanOntology.rdf+"#type> <"+urirange+"> . ");
//			sbWhere.append(indrange+ " <"+IOntoArea.GEOMETRY_PROPERTY_URI+"> " + indrangegeo +" . ");
//			sbWhere.append(indrangegeo+ " <"+IOntoArea.WKT_PROPERTY_URI+"> " + wktrange +" . ");
//		} else wktrange = "'"+wktrange+"'^^<"+IOntoArea.WKT_DATATYPE_URI+">";
//		if (function.getFunctionType() == IOntoFunction.OntFunction.DISTANCE){
//			String distance = function.getParams().get(0);
//			//TODO: the specification of the projection system should be configurable. Apparently only the distance function in parliament needs this information
//			sbWhere.append("FILTER( <"+function.getURI()+">(<http://www.opengis.net/def/crs/EPSG/0/3049> "+wktdomain+",<http://www.opengis.net/def/crs/EPSG/0/3049> "+wktrange+",units:metre)<"+distance+") .");
//		} else {
//			sbWhere.append("FILTER( <"+function.getURI()+">("+wktdomain+","+wktrange+")) .");
//		}
//		individuals.add(inddomain);
//		individuals.add(indrange);
//		return sbWhere.toString();
//	}
		
	//APPARENTLY NOT USED - TO CHECK
	public static List<IOntoBase> GetGraphNodesToShow(List<IOntoLink> graph){
		List<IOntoBase> results = new ArrayList<IOntoBase>();
		Set<IOntoBase> nodes = new HashSet<IOntoBase>();
		for (IOntoLink link: graph){
			IOntoBase domain = link.getDomain();
			IOntoBase range = link.getRange();
			if (domain.getType() == IOntoBase.OntType.CLASS || domain.getType() == IOntoBase.OntType.ECLASS)
				nodes.add(domain);
			if (range.getType() == IOntoBase.OntType.CLASS || range.getType() == IOntoBase.OntType.ECLASS)
				nodes.add(range);
		}
		List<IOntoBase> nodeList = new ArrayList<IOntoBase>(nodes);
		nodeList = UrbanOntology.OrderConceptsById(nodeList);
		int lastId=-1;
		for (IOntoBase node: nodeList){
			if (node.getId() != lastId){
				results.add(node);
				lastId = node.getId();
				}
		}
		return results;
	}
		
	
	//init row from 0
	public List<HashMap<String, Set<String>>> GetGraphDataValues(int idNode, int initRow, int endRow){
		
		List<HashMap<String, Set<String>>> results = new ArrayList<HashMap<String, Set<String>>>();
		QueryResults qResults = null;
		try{
			Integer graphId = this.nodeToSubgraphId.get(idNode);
			qResults = this.queryResult.get(graphId);
			List<HashMap<String,String>> listMap = qResults.getResults();
			if (listMap.size()<= endRow)
				endRow = listMap.size()-1;
			IOntoBase concept = this.getConcept(idNode);
			if (concept.getType() == IOntoBase.OntType.DINDIVIDUAL || concept.getType() == IOntoBase.OntType.EINDIVIDUAL){
				IOntoBase conceptClass = new OntoConcept(uOntology.getJenaModel().GetClass(concept.getResource(), false),uOntology);
				Set<IOntoProperty> plist = conceptClass.getDataProperties();
				HashMap<String, Set<String>> propMap = new HashMap<String, Set<String>>();
				results.add(propMap);
				for (IOntoProperty op:plist){
					propMap.put(op.getURI(), concept.getData(op));
				}

			} else {
				Set<IOntoProperty> plist = concept.getDataProperties();
				for (int i=initRow;i<=endRow;i++){
					String uri = listMap.get(i).get("ind"+idNode);
					IOntoBase ind = new OntoConcept(uri,uOntology);
					HashMap<String, Set<String>> propMap = new HashMap<String, Set<String>>();
					results.add(propMap);
					for (IOntoProperty op:plist){
						propMap.put(op.getURI(), ind.getData(op));
					}
				}
			}
		}catch (Exception e){
			System.out.println("GetGraphDataValues Error");
			return null;
		}
		return results;

	}
	
//	private List<String> getPropertiesData(Resource res, IOntoProperty property){
//		Set<Resource> subproperties = uOntology.getJenaModel().GetSubHierarchy(property.getResource(), true, false);
//		ArrayList<String> values = new ArrayList<String>();
//		for (Resource subp:subproperties){
//			Property p = uOntology.getJenaModel().getProperty(subp.getURI(), false);
//			for (RDFNode n: uOntology.getJenaModel().GetObjects(res, p, false)){
//				if (n.isLiteral())
//					values.add(n.asLiteral().getLexicalForm());
//			}
//		}
//		return values;
//	}
	
	public List<Set<String>> GetGraphDataValues(int nodeId, String propertyUri, int initRow, int endRow){
		List<Set<String>> results = new ArrayList<Set<String>>();
		QueryResults qResults = null;
		try{
			Integer graphId = this.nodeToSubgraphId.get(nodeId);
			qResults = this.queryResult.get(graphId);
			List<HashMap<String,String>> listMap = qResults.getResults();
			if (listMap.size()<= endRow)
				endRow = listMap.size()-1;
			
			if(propertyUri.equals("http://www.bsc.com/OntManaging#URIs")) {
				// hand-made case for view the instance/individual URI
				for (int i= initRow;i<=endRow;i++){
					Set<String> values = new HashSet<String>();
					HashMap<String,String> map = listMap.get(i);
					String uri = map.get("ind"+nodeId);
					values.add(uri);
					results.add(values);
				}
			}else {
				// data-properties defined in the ontologies 
				IOntoProperty property = new OntoProperty(new OntoConcept(propertyUri,uOntology),uOntology);
				IOntoBase concept = this.getConcept(nodeId);
				if (concept.getType() == IOntoBase.OntType.DINDIVIDUAL || concept.getType() == IOntoBase.OntType.EINDIVIDUAL){
					results.add(concept.getData(property));
				} else {
					for (int i= initRow;i<=endRow;i++){
						HashMap<String,String> map = listMap.get(i);
						String uri = map.get("ind"+nodeId);
						Resource uriRes = uOntology.getJenaModel().getResource(uri, false);
						IOntoBase uriOb = new OntoConcept(uriRes, uOntology,"",IOntoBase.OntType.DINDIVIDUAL);
						Set<String> values = new HashSet<String>();
						values = uriOb.getData(property);
						results.add(values);
					}
				}
			}
		}catch (Exception e){
			System.out.println("GetGraphDataValues Error");
			return null;
		}
		return results;
	}

	
	public List<String> GetGraphDataURIS(int nodeId, int initRow, int endRow){
		List<String> results = new ArrayList<String>();
		QueryResults qResults = null;
		Integer graphId = this.nodeToSubgraphId.get(nodeId);
		qResults = this.queryResult.get(graphId);
		if (qResults.getResults().size()<=endRow)
			endRow = qResults.getResults().size()-1;
		List<HashMap<String,String>> listMap = qResults.getResults();
		for (int i= initRow;i<=endRow;i++){
			HashMap<String,String> map = listMap.get(i);
			String uri = map.get("ind"+nodeId);
			results.add(uri);
		}
		return results;
	}
	
	public List<Integer> GetGraphDataRow(int nodeId, String uri){
		QueryResults qResults = null;
		List<Integer> results = new ArrayList<Integer>();
		Integer graphId = this.nodeToSubgraphId.get(nodeId);
		qResults = this.queryResult.get(graphId);
		List<HashMap<String,String>> listMap = qResults.getResults();
		for (int i= 0;i<listMap.size();i++){
			HashMap<String,String> map = listMap.get(i);
			if (uri.equals(map.get("ind"+nodeId)))
					results.add(i);
		}
		return results;
	}
	
	
		
	private List<List<IOntoLink>> getConnectedGraphs(List<IOntoLink> graph){
		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		int count =0;
		for (IOntoLink g:graph){
			Integer id1 = g.getDomain().getId();
			Integer id2 = g.getRange().getId();
			int value1 = count;
			int value2 = count;
			if (map.containsKey(id1)) { //link connected
				value1 = map.get(id1);
			}
			if (map.containsKey(id2)) { //link connected
				value2 = map.get(id2);
			}
			if (value1 != count || value2 != count){ //link connected
				for (int key:map.keySet())
					if (map.get(key) == value1 || map.get(key) == value2)
						map.put(key, count);
			}
			map.put(id1, count);
			map.put(id2, count);
			count++;
		}
		List<List<IOntoLink>> results = new ArrayList<List<IOntoLink>>();
		Set<Integer> difValues = new HashSet<Integer>(map.values());
		for (Integer value:difValues){
			Set<IOntoLink> connectedGraph = new HashSet<IOntoLink>();
			for (IOntoLink link:graph){
				if (map.get(link.getDomain().getId()) == value || map.get(link.getRange().getId()) == value)
					connectedGraph.add(link);
			}
			results.add(new ArrayList<IOntoLink>(connectedGraph));
		}
		return results;
	}




}
