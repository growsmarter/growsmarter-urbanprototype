package smartcities.semanticweb.ontologycommons.services;

import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Resource;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction.OntFunction;

class OntoFunction extends OntoConcept implements IOntoFunction {
	private List<String> functionParams= new ArrayList<String>();
	private OntFunction ftype;

	
	
//	public IOntoFunction GetFunction(IOntoBase ob, List<String> args) throws Exception{
//		OntoConcept oc = new OntoConcept(ob,this);
//		oc.setFunctionType(oc.getURI());
//		oc.setParams(args);
//		if (!CheckFunctionValidity(oc.getFunctionType(),args.size()))
//			throw new Exception("Unknown function or wrong number of params");
//		return (IOntoFunction) oc;
//	}
	

	public OntoFunction(IOntoBase ob, List<String> args, UrbanOntology uOntology) {
		super(ob, uOntology);
		setFunctionType(ob.getURI());
		setParams(args);
	}
	
//	static public IOntoFunction GetFunction(IOntoFunction.OntFunction ftype, List<String> args, UrbanOntology uOntology) {
//		String uriFunction = getFunctionType(ftype);
//		IOntoBase obfunction = new OntoConcept(uriFunction,uOntology);
//		return new OntoFunction(obfunction, args,uOntology);
//	}
	
//	static public IOntoBase GetFunction(IOntoFunction.OntFunction ftype, UrbanOntology uOntology) {
//		String uriFunction = getFunctionType(ftype);
//		IOntoBase obfunction = new OntoConcept(uriFunction,uOntology);
//		return obfunction;
//	}


	
	@Override
	public List<String> getParams() {
		return this.functionParams;
	}
	
	public void setParams(List<String> functionParams) {
		this.functionParams = functionParams;
	}


	@Override
	public OntFunction getFunctionType() {
		return this.ftype;
	}
	
	static public String getFunctionType(IOntoFunction.OntFunction ftype){
		switch(ftype){
		case INTERSECTION: return IOntoFunction.INTERSECTION_URI;
		case DISTANCE: return IOntoFunction.DISTANCE_URI;
		case BUFFER: return IOntoFunction.BUFFER_URI;
		case WITHIN: return IOntoFunction.WITHIN_URI;
		case OTHER: return null;
		}
		return null;
	}
	
	static public boolean CheckFunctionValidity(IOntoFunction.OntFunction ftype,int paramNumber){
		switch (ftype){
			case INTERSECTION: if (paramNumber == 0) return true;
			case WITHIN: if (paramNumber == 0) return true;
			case DISTANCE: if (paramNumber == 1) return true;
			case BUFFER: if (paramNumber == 1) return true;
			case OTHER: return true;
		}
		return false;
	}

	
	public void setFunctionType(String uri) {
		if (uri.equals(INTERSECTION_URI))
			this.ftype = OntFunction.INTERSECTION;
		else if (uri.equals(DISTANCE_URI))
			this.ftype = OntFunction.DISTANCE;
		else if (uri.equals(BUFFER_URI))
			this.ftype = OntFunction.BUFFER;
		else if (uri.equals(WITHIN_URI))
			this.ftype = OntFunction.WITHIN;
		else this.ftype = OntFunction.OTHER;
			
	}
	
	public int getParamNumber(){
		if (this.getURI() == IOntoFunction.DISTANCE_URI || this.getURI() == IOntoFunction.BUFFER_URI)
			return 1;
		return 0;
	}


}
