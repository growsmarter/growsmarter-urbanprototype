package smartcities.semanticweb.ontologycommons.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDFS;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;

class IndicatorType extends OntoConcept implements IIndicatorType {
	private int GUIOrderTag=0;

	public IndicatorType(Resource resource, UrbanOntology uOntology,
			String label, OntType type) {
		super(resource, uOntology, label, type);
		setGUIOrderTag();
	}

	public IndicatorType(IOntoBase ob, UrbanOntology uOntology) {
		super(ob, uOntology);
		setGUIOrderTag();
	}
	
	static 	public List<IIndicatorType> GetIndicatorTypes(UrbanOntology uOntology){
		String indClassPrefix = UrbanOntology.core;
		String indClassLocalName = "IndicatorType";
		Resource indTypeClass = uOntology.getJenaModel().getResource(indClassPrefix+"#"+indClassLocalName, false);
		Set<Resource> indicators =  uOntology.getJenaModel().GetSubjects(indTypeClass,RDFS.subClassOf, false);
		ArrayList<IIndicatorType> resultingInd = new ArrayList<IIndicatorType>();
		for (Resource resInd: indicators){
			IOntoBase ob = new OntoConcept(resInd,uOntology);
			IIndicatorType oc = new IndicatorType(ob,uOntology);
			resultingInd.add(oc);
		}
		return resultingInd;
	}


	private void setGUIOrderTag(){
	Property orderp = uOntology.getJenaModel().getProperty(UrbanOntology.core +"#GUIOrderTag",false);
	RDFNode order = uOntology.getJenaModel().GetObject(this.getResource(), orderp, false);
	int orderValue =0;
	try{
		if (order != null)
			orderValue = Integer.parseInt(order.asLiteral().getValue().toString());
	}catch (Exception e){
		System.out.println("Conversion error in "+this.getURI()+".GUIOrderTag");
	}
	this.GUIOrderTag = orderValue;
	}

	@Override
	public int getGUIOrderTag() {
		return GUIOrderTag;
	}

}
