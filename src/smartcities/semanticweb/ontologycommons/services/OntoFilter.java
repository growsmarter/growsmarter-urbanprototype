package smartcities.semanticweb.ontologycommons.services;

import java.util.HashSet;
import java.util.Set;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;

class OntoFilter {
	
	Set<IOntoBase> valids = new HashSet<IOntoBase>(); //all the items allowed. If empty, only invalids are taken into account
	Set<IOntoBase> invalids = new HashSet<IOntoBase>(); //all the invalid items. If empty, only valids are taken into account
	
	Set<String> validsStr = new HashSet<String>();
	Set<String> invalidsStr = new HashSet<String>();
	
	public Set<IOntoBase> getValids() {
		return valids;
	}

	private void setValids(Set<IOntoBase> valids) {
		this.valids = valids;
		for (IOntoBase v:valids)
			validsStr.add(v.getURI());
	}

	public Set<IOntoBase> getInvalids() {
		return invalids;
	}

	private void setInvalids(Set<IOntoBase> invalids) {
		this.invalids = invalids;
		for (IOntoBase v:invalids)
			invalidsStr.add(v.getURI());
	}

	
	public OntoFilter(Set<IOntoBase> valids, Set<IOntoBase> invalids){
		setValids(valids);
		setInvalids(invalids);
	}
	
	public Set<IOntoBase> Filter(Set<IOntoBase> concepts){
		if (valids.size() == 0 && invalids.size() == 0) //if both are empty, no filtering is done
			return concepts;
		Set<IOntoBase> results = new HashSet<IOntoBase>();
		Set<IOntoBase> results2 = new HashSet<IOntoBase>();
		if (valids.size() > 0){
			for (IOntoBase c:concepts)
				if (validsStr.contains(c.getURI()))
					results.add(c);
		} else 
			results = concepts;
		if (invalids.size() >0)
			for (IOntoBase c:results)
				if (!invalidsStr.contains(c.getURI()))
					results2.add(c);
		return results2;
	}
	
	public boolean IsValid(IOntoBase concept){
		return IsValid(concept.getURI());
	}
	
	public boolean IsEmpty(){
		if (invalids.size() == 0 && valids.size() == 0)
			return true;
		return false;
	}
	
	public boolean IsValid(String conceptURI){
		boolean valid = true;
		if (invalids.size() > 0)
			if (invalidsStr.contains(conceptURI))
					valid = false;
		if (valids.size() > 0)
			if (!validsStr.contains(conceptURI))
				valid = false;
		return valid;
	}

}
