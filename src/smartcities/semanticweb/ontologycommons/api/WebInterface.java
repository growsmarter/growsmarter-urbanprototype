package smartcities.semanticweb.ontologycommons.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import smartcities.semanticweb.ontologycommons.api.jstreemanagement.DataJsTree;
import smartcities.semanticweb.ontologycommons.api.jstreemanagement.DataNodeJSTree;
import smartcities.semanticweb.ontologycommons.api.queries.QueryGNode;
import smartcities.semanticweb.ontologycommons.api.queries.QueryGraphs;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorType;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator.ANCHOR_AREAS;
import smartcities.semanticweb.ontologycommons.services.QueryResults;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;
import ontology_commons.tests.serviceInterface.library.PrintMethods;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.hp.hpl.jena.query.Query;

import es.bsc.casedep.gs.sal.apis.cellnex.CellnexAPI;
import es.bsc.casedep.gs.sal.queryfactory.SparQLParser;
import es.bsc.casedep.gs.sal.queryfactory.results.DataResultsUtils;
import es.bsc.casedep.gs.sal.resources.ResourcesFactory;
import es.bsc.casedep.gs.sal.resources.ResourcesSources;
import es.bsc.casedep.gs.sal.resultsmanager.ResultsCrossing;
import es.bsc.casedep.gs.sal.resultsmanager.ResultsDAG;
import es.bsc.casedep.gs.sal.resultsmanager.ResultsGraph;
import es.bsc.casedep.gs.sal.resultsmanager.ResultsGraphFactory;
import se.resources.MyStemmer;

public class WebInterface {

	final static String SEP_OBJECTPROPERTY = " ";
	static HashMap<String, String> urllabelmap = new HashMap<String, String>();
	public static String sessionID = "temporally_harcoded";

	private static String getMainKey(HashMap<String, ArrayList<String>> hierarchy) {
		Set<String> keys = hierarchy.keySet();
		HashSet<String> values = new HashSet<String>();
		for (ArrayList<String> listVal : hierarchy.values()) {
			for (String value : listVal) {
				if (!values.contains(value))
					values.add(value);
			}
		}
		for (String key : keys) {
			if (!values.contains(key))
				return key;
		}
		return null;
	}

	private static void printHierarchy(int count, StringBuffer out, String key,
			HashMap<String, ArrayList<String>> hierarchy, int level) {
		try {

			ArrayList<String> childs = hierarchy.get(key);
			if (childs.size() > 0) {
				if (level == 1)
					out.append("<ul style='list-style: none;padding-left:1em;display:block'>");
				else
					out.append("<ul style='list-style: none;padding-left:1em;display:none'>");
				for (String eachChild : childs) {
					String localUri = eachChild.replaceAll(".*#", "");
					String identifier = eachChild + count;
					// String identifier = eachChild;
					String textshow = GetLabel(eachChild);

					if (level == 1) {
						out.append("<li class='noselected' name='"
								+ localUri
								+ "'><input type='checkbox' name='"
								+ eachChild
								+ "' id='"
								+ identifier
								+ "' onclick=\"clickConcepts('"
								+ identifier
								+ "', document.getElementById('"
								+ identifier
								+ "').checked)\" ><div class='tab1mainconcepts' onclick=\"clickTab1MainConcept(this);\">"
								+ textshow + "</div></input>");
					} else {
						out.append("<li class='noselected' name='" + localUri + "'><input type='checkbox' name='"
								+ eachChild + "' id='" + identifier + "' onclick=\"clickConcepts('" + identifier
								+ "', document.getElementById('" + identifier + "').checked)\" >" + textshow
								+ "</input>");
					}

					count++;
					printHierarchy(count, out, eachChild, hierarchy, level + 1);
					out.append("</li>");
				}
				out.append("</ul>");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static String GenerateGraphExampleWithRecalculus(String params) {
		String output = null;
		JSONObject jsonOut = new JSONObject();
		JSONObject jsonIn = new JSONObject(params);
		// query trimmed & lowercased
		String query = jsonIn.getString("query").trim().toLowerCase();

		ArrayList<JSONObject> conceptsAL = new ArrayList<>();
		// Move concepts JSONArray to ArrayList
		ArrayList<String> queryTokenized = new ArrayList<>(Arrays.asList(query.split(" ")));
		JSONArray graphNodesURI = WebInterfaceUtils.recalculusRecursive(queryTokenized);
		jsonOut.put("nodes", graphNodesURI);
		output = jsonOut.toString();

		return output;
	}

	public static String GenerateGraphExample(String params) {
		String output = null;
		JSONObject jsonOut = new JSONObject();
		JSONObject jsonIn = new JSONObject(params);
		// query trimmed & lowercased
		String query = jsonIn.getString("query").trim().toLowerCase();

		ArrayList<JSONObject> conceptsAL = new ArrayList<>();
		// 1)Seleccionar los nodos que se adecuan mas

		// Move concepts JSONArray to ArrayList
		JSONObject jConcepts = jsonIn.getJSONObject("concepts");
		Iterator conceptKeys = jConcepts.keys();
		while (conceptKeys.hasNext()) {
			String key = (String) conceptKeys.next();
			JSONObject concept = jConcepts.getJSONObject(key);
			conceptsAL.add(concept);
		}
		// Sort the ArrayList by concept Similarity
		Collections.sort(conceptsAL, new Comparator<JSONObject>() {
			public int compare(JSONObject a, JSONObject b) {
				// Write your logic here.
				Double aSimilarity = a.getDouble("similarity");
				Double bSimilarity = b.getDouble("similarity");
				if (aSimilarity == bSimilarity) {
					return 0;
				} else if (aSimilarity < bSimilarity) {
					return 1;
				} else {
					return -1;
				}
			}
		});

		ArrayList<String> queryTokenized = new ArrayList<>(Arrays.asList(query.split(" ")));
		JSONArray graphNodesURI = new JSONArray();
		Double minQuerySize = query.length() * 0.2;
		// Select the most adequate concepts
		int nodes = 0;
		int maxNodes = (int) (queryTokenized.size() * 0.9);
		for (JSONObject c : conceptsAL) {
			if ((c.getDouble("similarity")) > 2 && (nodes <= maxNodes)) {
				// if (query.length() > minQuerySize) {
				// get the label timmed
				// String label = c.getString("label").trim().toLowerCase();
				// TODO: get el snippet
				String reason = null;
				JSONObject snippet = c.getJSONObject("snippet");

				if (snippet.has("label")) {
					Iterator relatedKeys = snippet.getJSONObject("label").keys();
					while (relatedKeys.hasNext()) {
						String key = (String) relatedKeys.next();
						reason = snippet.getJSONObject("label").getString(key);
					}
				} else if (snippet.has("related")) {
					Iterator relatedKeys = snippet.getJSONObject("related").keys();
					while (relatedKeys.hasNext()) {
						String key = (String) relatedKeys.next();
						reason = snippet.getJSONObject("related").getString(key);
					}
				} else if (snippet.has("property")) {
					Iterator relatedKeys = snippet.getJSONObject("property").keys();
					while (relatedKeys.hasNext()) {
						String key = (String) relatedKeys.next();
						reason = snippet.getJSONObject("property").getString(key);
					}
				} else {
					reason = "";
				}

				String opener = "<label>";
				String closer = "</label>";
				reason = reason.trim().toLowerCase();

				if (reason.length() == 0) {
					// If nothing to be replaced, stop adding nodes!
					break;
				}
				// reason = reason.substring(reason.indexOf(opener) +
				// opener.length() + 1, reason.indexOf(closer));
				// Pattern findUrl = Pattern.compile("\\b" + opener +
				// ".*?\\." + closer + "\\b");
				// Pattern patternBetween =
				// Pattern.compile(Pattern.quote(opener) + "(.*?)" +
				// Pattern.quote(closer));
				ArrayList<String> toRemoveFromQueryTokenized = new ArrayList<>();
				Pattern patternBetween = Pattern.compile("\\" + opener + "(.*?)\\" + closer);
				Matcher matcher = patternBetween.matcher(reason);
				while (matcher.find()) {
					String toRemoveFromQuery = matcher.group();
					toRemoveFromQuery = toRemoveFromQuery.replace(opener, "").replace(closer, "");
					toRemoveFromQueryTokenized.add(toRemoveFromQuery);
				}

				boolean addNode = WebInterfaceUtils.removeStemming(queryTokenized, toRemoveFromQueryTokenized);
				if (addNode) {
					graphNodesURI.put(c.get("uri"));
					nodes++;
				} else {
					// do nothing
				}

				// Remove label from query
				// TODO IMPROVE: optimize instead of label, remove the
				// snippet
				// String[] label_split = label.split(" ");
				// remove label from query
				// query = query.replace(reason, "");

			} else {
				// Not enough query
				break; // stop looping the ArrayList
			}
		}
		jsonOut.put("nodes", graphNodesURI);

		// TODO: 2)Generar relaciones entre ellos

		output = jsonOut.toString();

		// URI's from the
		// JSONArray exploredUris= jsonIn.getJSONArray(key);

		return output;
	}

	public static String ExistsPath(String params) {
		JSONObject jsonOut = null;
		JSONObject jsonIn = null;
		String output = null;
		jsonIn = new JSONObject(params);
		String uriDomain = jsonIn.getString("domain");
		String uriRange = jsonIn.getString("range");
		int deep = jsonIn.getInt("deep");
		boolean populated = jsonIn.getBoolean("populated");

		IOntoBase domain = null;
		IOntoBase range = null;
		output = jsonOut.toString();
		return output;
	}

	public static String PrefixBlock() {
		return (ServiceInterface.PrefixBlock());
	}

	public static String HtmlHierarchy(HashMap<String, ArrayList<String>> hierarchy) {
		String mainKey = getMainKey(hierarchy);
		StringBuffer out = new StringBuffer();
		printHierarchy(0, out, mainKey, hierarchy, 1);
		return out.toString();
	}

	public static HashMap<String, List<HashMap<String, String>>> IndexQueryResults(QueryResults qr, String key) {
		HashMap<String, List<HashMap<String, String>>> output = new HashMap<String, List<HashMap<String, String>>>();

		List<HashMap<String, String>> results = qr.getResults();
		for (HashMap<String, String> row : results) {
			if (row.containsKey(key)) {
				String value = row.get(key);
				List<HashMap<String, String>> keyrows;
				if (output.containsKey(value))
					keyrows = output.get(value);
				else {
					keyrows = new ArrayList<HashMap<String, String>>();
					output.put(value, keyrows);
				}
				keyrows.add(row);
			}
		}
		return output;
	}

	public static String JSON_QueryResults(String sparqlQ) {
		JSONArray jsonresults = new JSONArray();
		try {
			QueryResults results = ServiceInterface.Query(sparqlQ, false, WebInterface.sessionID);
			List<String> varnames = results.getVars();
			JSONArray jsonrow = new JSONArray();
			for (int i = 0; i < varnames.size(); i++) {
				jsonrow.put(i, varnames.get(i));
			}
			jsonresults.put(jsonrow);
			for (HashMap<String, String> row : results.getResults()) {
				jsonrow = new JSONArray();
				for (int i = 0; i < varnames.size(); i++) {
					String varn = varnames.get(i);
					if (row.containsKey(varn))
						jsonrow.put(i, row.get(varn));
					else
						jsonrow.put(i, "");
				}
				jsonresults.put(jsonrow);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return jsonresults.toString();
	}

	public static String GetRelatedConcepts(String ptype, String uriclass, String populated) {
		JSONObject jsonresults = new JSONObject();
		Boolean b = Boolean.valueOf(populated);

		try {
			QueryResults results = ServiceInterface.GetRelatedConcepts(ptype, uriclass, b); // property
			// OPTIONAL(domain)
			// OPTIONAL(range)
			HashMap<String, List<HashMap<String, String>>> indexedResults = IndexQueryResults(results, "relclass");
			for (String relclass : indexedResults.keySet()) {
				JSONArray jsonrow = new JSONArray();
				int cont = 0;
				for (HashMap<String, String> row : indexedResults.get(relclass)) {
					if (row.containsKey("relclassIsDomain")) {
						if (row.get("relclassIsDomain") == "0") {
							boolean domain = false;
						} else {
							boolean domain = true;
						}
					}
					if (row.containsKey("relprop"))
						jsonrow.put(cont++, row.get("relprop"));
				}
				jsonresults.put(relclass, jsonrow);
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return jsonresults.toString();
	}

	public static String GetMatchClassProperty(String inputProperty) {
		JSONArray jsonresults = new JSONArray();
		try {
			QueryResults results = ServiceInterface.GetMatchProperties(inputProperty, WebInterface.sessionID);

			int cont = 0;
			for (HashMap<String, String> row : results.getResults()) {
				if (row.containsKey("property")) {
					String property = row.get("property");
					if (!ServiceInterface.IsGeneralConcept(property)) {
						JSONObject tupla = new JSONObject();
						String label = "";
						String domain = "";
						String range = "";
						tupla.put("property", property);
						if (row.containsKey("label"))
							label = row.get("label");
						tupla.put("label", label);
						if (row.containsKey("domain_urban"))
							domain = row.get("domain_urban");
						else
							domain = "N/A";
						tupla.put("domain", domain);
						if (row.containsKey("range_urban"))
							range = row.get("range_urban");
						else
							range = "N/A";
						tupla.put("range", range);
						jsonresults.put(cont++, tupla);
					}
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return jsonresults.toString();
	}

	protected static String Generate_Json(HashMap<String, List<HashMap<String, String>>> indexedResults) {
		JSONArray featuresArray = new JSONArray();
		int cont = 0;
		int contFeatures = 0;
		for (String key : indexedResults.keySet()) {
			JSONObject feature = new JSONObject();
			JSONObject properties = new JSONObject();
			JSONObject geometry = new JSONObject();
			properties.put("URI", key);
			String wkt = "";
			HashMap<String, ArrayList<String>> mapProperties = new HashMap<String, ArrayList<String>>();
			for (HashMap<String, String> row : indexedResults.get(key)) {
				String property1 = "";
				String value1 = "";
				if (row.containsKey("property"))
					property1 = row.get("property");
				if (row.containsKey("info"))
					value1 = row.get("info").replaceFirst("\\^\\^.*", "");
				String property2 = "";
				String value2 = "";
				if (row.containsKey("p2"))
					property2 = row.get("p2");
				if (row.containsKey("info2"))
					value2 = row.get("info2").replaceFirst("\\^\\^.*", "");
				if (property2.equals("http://www.opengis.net/ont/geosparql#asWKT"))
					wkt = value2;
				if (!property1.equals("") && !property2.equals("") && !value2.equals(key)
						&& !ServiceInterface.IsGeneralConcept(property1)
						&& !ServiceInterface.IsGeneralConcept(property2)) {
				} else if (!property1.equals("") && property2.equals("") && !value1.equals(key)
						&& !ServiceInterface.IsGeneralConcept(property1)) {
					if (!mapProperties.containsKey(property1))
						mapProperties.put(property1, new ArrayList<String>());
					ArrayList<String> pvalues = mapProperties.get(property1);
					pvalues.add(value1); // TODO: qué ocurre en las consultas si
											// tenemos más de un valor para una
											// misma propiedad
				}
			}
			for (String prop : mapProperties.keySet()) {
				JSONArray listvalues = new JSONArray();
				for (String value : mapProperties.get(prop))
					listvalues.put(value);
				properties.put(prop, listvalues);
			}
			if (!wkt.equals("")) {
				feature.put("properties", properties);
				geometry.put("wkt", wkt);
				feature.put("geometry", geometry);
				featuresArray.put(contFeatures++, feature);
			} else
				cont++;
		}
		System.out.println(featuresArray.length());
		return featuresArray.toString();

	}

	public static String Json_MatchConcepts(String keyword) {
		QueryResults results = ServiceInterface.GetMatchConcepts(keyword, WebInterface.sessionID);

		JSONArray listMatches = new JSONArray();
		JSONObject match = new JSONObject();
		for (HashMap<String, String> row : results.getResults()) {
			String concept = row.get("concept");
			String type = row.get("type");
			if (!ServiceInterface.IsGeneralConcept(concept)) {
				match.put("uri", concept);
				match.put("type", type);
				listMatches.put(match);
			}

		}
		return listMatches.toString();
	}

	protected static String Json_SelectFeatures(String type) {
		String json = null;
		try {
			QueryResults results = ServiceInterface.GetWKTSelectionGeometries(type);
			HashMap<String, List<HashMap<String, String>>> indexedResults = WebInterface.IndexQueryResults(results, "f1");
			json = WebInterface.Generate_Json(indexedResults);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return json;
	}

	public static String Json_SelectFeatures() {
		JSONObject json = new JSONObject();
		String jsonBlocks = WebInterface.Json_SelectFeatures("Blocks");
		String jsonLandlots = WebInterface.Json_SelectFeatures("Landlots");
		json.put("Blocks", jsonBlocks);
		json.put("Landlots", jsonLandlots);
		return json.toString();
	}

	public static String Json_InstancesIntersectPolygon(String URIclass, String wktPolygon) {
		String json = null;
		try {
			QueryResults results = ServiceInterface.GetInstancesIntersectPolygon(URIclass, wktPolygon);
			HashMap<String, List<HashMap<String, String>>> indexedResults = IndexQueryResults(results, "f1");
			json = WebInterface.Generate_Json(indexedResults);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return json;
	}

	public static String GetUrbanPlanningPerspectiveConcepts() {
		return (HtmlHierarchy(ServiceInterface.GetUrbanPlanningPerspectiveConcepts()));
	}

	public static String GetLabel(String uri) throws IOException {
		if (urllabelmap.containsKey(uri))
			return urllabelmap.get(uri);
		String label = ServiceInterface.GetLabel(uri);
		if (label == null) {
			label = uri.replaceFirst("([^\\s]+://[^\\s]*[\\#/])", "").trim();
		}
		// TOREMOVE:JUST FOR THE DEMO
		if (label.equalsIgnoreCase("Beverage serving activities"))
			label = "Hospital activities";
		// END TOREMOVE

		urllabelmap.put(uri, label);
		return label;
	}

	public static String GetLabelTable() {
		JSONObject result = new JSONObject();
		for (String key : urllabelmap.keySet()) {
			result.put(key, urllabelmap.get(key));
		}
		return result.toString();
	}

	public static String GetIndicators() {
		JSONObject json = new JSONObject();
		// try {
		// List<IIndicator> indicators =
		// ServiceInterface.GetIndicatorConcepts();
		//
		// for (IIndicator row : indicators) {
		// JSONObject indicator = new JSONObject();
		// String uriIndicator = row.getURI();
		// String uriDescriptor = row.getIndDescriptor().getURI();
		// String comment = row.getComment();
		// float measureValue = row.getIndValue();
		// String uriClass =
		// ServiceInterface.GetClassOfIndividual(uriDescriptor);
		// String uriClass = row.getIndDescriptor().getLabel();
		//
		// indicator.put("comment", comment);
		// indicator.put("value", measureValue);
		//
		// indicator.put("class", uriClass);
		// JSONArray arrayClass = new JSONArray();
		// Set<IOntoBase> relClasses =
		// row.getIndDescriptor().getClassConcept(true);
		// for (IOntoBase rowAffClass : relClasses) {
		// JSONObject relClass = new JSONObject();
		// String uriRelClass = rowAffClass.get("relatedClass");
		// relClass.put("class", uriRelClass);
		// arrayClass.put(relClass);
		// }
		// indicator.put("affects", arrayClass);
		// JSONArray arrayMeas = new JSONArray();
		// QueryResults relMeasurements =
		// ServiceInterface.GetIndicatorRelatedMeasurements(uriDescriptor);
		// for (HashMap<String, String> rowRelMeas :
		// relMeasurements.getResults()) {
		// JSONObject relMeas = new JSONObject();
		// String uriRelMeas = rowRelMeas.get("measurement");
		// String refValue = rowRelMeas.get("measurement_value");
		// relMeas.put("measure", uriRelMeas);
		// relMeas.put("value", refValue);
		// arrayMeas.put(relMeas);
		// }
		// indicator.put("references", arrayMeas);
		// json.put(uriIndicator, indicator);
		// }
		// } catch (IOException e) {
		// System.out.println(e.getMessage());
		// e.printStackTrace();
		//
		// }
		return json.toString();
	}

	/**
	 * Will return the related concepts associated with the query parameter
	 * obtained from the user
	 * 
	 * @param query
	 *            String containing the user query
	 * @return json String containing related concepts info
	 */
	public static String GetDataQuery(String params) {
		FileWriter fw = null;
		long init, end;
		try {
			fw = new FileWriter("/tmp/GS_timings.log");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JSONObject jsonOutput = new JSONObject();
		JSONObject json = new JSONObject();

		try {
			JSONObject jsonInput = new JSONObject(params);
			String query = jsonInput.getString("query");
			boolean withInstances = jsonInput.getBoolean("withinstances");
			// JSONArray filtertype = jsonInput.getJSONArray("filtertype");
			int trunkingTo;
			if (jsonInput.has("trunkto")) {
				trunkingTo = jsonInput.getInt("trunkto");
			} else {
				trunkingTo = 50;
			}

			String id = "5"; // id param has no meaning (5, 6 or -inf will
								// return the same
								// List<SearchResult> results =
								// services.ServiceInterface.SearchConcepts(id,
								// query);
			init = System.currentTimeMillis();
			List<ISearchResult> resultsUntrunked = ServiceInterface.SearchConcepts(id, query, withInstances);
			end = System.currentTimeMillis();
			PrintMethods.printFile(fw, init, end, "ServiceInterface.SearchConcepts");

			int i = 0;
			for (ISearchResult isr : resultsUntrunked) {
				String isrURI = isr.getOntoConcept().getURI();
				if (isrURI.toLowerCase().contains("energy")) {
					System.out.println("Contains energy isr[" + i + "]: " + isrURI);
				}
				i++;
			}

			// Sort results by similarity
			WebInterfaceUtils.iSRSortBySimilarity(resultsUntrunked);
			// String filtertype = "CLASS";
			// WebInterfaceUtils.filterByType(resultsUntrunked, filtertype);

			// Trunk results
			List<ISearchResult> resultsTrunked;
			if (resultsUntrunked.size() > trunkingTo) {
				resultsTrunked = resultsUntrunked.subList(0, trunkingTo);
			} else {
				resultsTrunked = resultsUntrunked;
			}

			init = System.currentTimeMillis();
			// Concepts to group By Hierarchy
			Set<IOntoBase> conceptsToGroupByHierarchy = new HashSet<IOntoBase>();

			for (ISearchResult res : resultsTrunked) {
				IOntoBase resIOB = res.getOntoConcept();

				WebInterfaceUtils.checkConceptIOB(resIOB);

				conceptsToGroupByHierarchy.add(resIOB);

				// Data from each result
				String uri = resIOB.getURI();
				String label = resIOB.getLabel();
				String comment = resIOB.getComment();
				String type = resIOB.getType().toString();
				String source = res.getSearchSource(); // .getLabel()
				Double similarity = res.getSimilarity();
				JSONArray jcats = new JSONArray();
				Set<IOntoBase> cats = res.getCategory();
				if (cats != null) {
					for (IOntoBase cat : res.getCategory()) {
						JSONObject jcat = new JSONObject();
						jcat.put("uri", cat.getURI());
						jcat.put("label", cat.getLabel());
						jcats.put(jcat);
					}
					if (jcats.length() == 0) {
						JSONObject jcat = new JSONObject();
						jcat.put("uri", "Other");
						jcat.put("label", "Other");
						jcats.put(jcat);
					}
				} else {
					JSONObject jcat = new JSONObject();
					jcat.put("uri", "Other");
					jcat.put("label", "Other");
					jcats.put(jcat);
				}

				if (comment == null || comment.trim().equals("")) {
					comment = "N/A";
				}

				if (source == null || source.trim().equals("")) {
					comment = "N/A";
				}

				// se añaden subclasses y superclasses a los conceptos
				List<IOntoBase> superc = resIOB.getSuperConcepts();
				List<IOntoBase> subc = resIOB.getSubConcepts();
				JSONArray jsuperc = new JSONArray();
				for (IOntoBase sc : superc) {
					JSONObject jso = new JSONObject();
					jso.put("id", Integer.toString(sc.getId()));
					jso.put("label", sc.getLabel());
					jso.put("uri", sc.getURI());
					jso.put("type", sc.getType().toString());
					jso.put("relation", "(SUP)");
					Set<IOntoBase> supercCats = ServiceInterface.GetSearchConcept(sc, 1).getCategory();
					JSONArray jSupercCats = new JSONArray();
					if (supercCats != null && supercCats.size() > 0) {
						for (IOntoBase cat : supercCats) {
							JSONObject jcat = new JSONObject();
							jcat.put("uri", cat.getURI());
							jcat.put("label", cat.getLabel());
							jcats.put(jcat);
						}
					} else {
						JSONObject jcat = new JSONObject();
						jcat.put("uri", "Other");
						jcat.put("label", "Other");
						jcats.put(jcat);
					}
					jso.put("categories", jSupercCats);
					jsuperc.put(jso);
				}

				JSONArray jsubc = new JSONArray();
				for (IOntoBase sc : subc) {
					JSONObject jso = new JSONObject();
					jso.put("id", Integer.toString(sc.getId()));
					jso.put("label", sc.getLabel());
					jso.put("uri", sc.getURI());
					jso.put("type", sc.getType().toString());
					jso.put("relation", "(sub)");
					Set<IOntoBase> subCats = ServiceInterface.GetSearchConcept(sc, 1).getCategory();
					JSONArray jSubCats = new JSONArray();
					if (subCats != null && subCats.size() > 0) {
						for (IOntoBase cat : subCats) {
							JSONObject jcat = new JSONObject();
							jcat.put("uri", cat.getURI());
							jcat.put("label", cat.getLabel());
							jcats.put(jcat);
						}
					} else {
						JSONObject jcat = new JSONObject();
						jcat.put("uri", "Other");
						jcat.put("label", "Other");
						jcats.put(jcat);

					}
					jso.put("categories", jSubCats);
					jsubc.put(jso);
				}

				String openlabel = "<label>";
				String closelabel = "</label>";
				Map<String, Map<String, String>> snippets = ServiceInterface.GetSnippet(uri, query, openlabel,
						closelabel);
				JSONObject jSnippet = WebInterfaceUtils.snippetToJSON(snippets);

				// EXTRAER PROPIEDADES (ATRIBUTOS) DE UN RESULTADO DE LA
				// BUSQUEDA
				// String key = "properties";
				String key = "property";
				JSONArray jProperties = new JSONArray();

				// TODO: solve when its null!!
				if (snippets.get(key) != null && !snippets.get(key).isEmpty()) {
					for (String snippetsUri : snippets.get(key).keySet()) {
						IOntoProperty property = ServiceInterface.GetProperty(ServiceInterface.GetConcept(snippetsUri));
						IOntoProperty.OntPropertyGeneral ptype = property.getGeneralPropertyType();

						// SI ES 'OBJECT', ES UNA OBJECT PROPERTY
						// SI ES'DATA' ES UN ATRIBUTO
						if (IOntoProperty.OntPropertyGeneral.OBJECT.equals(ptype)) {
							// es una property
						} else if (IOntoProperty.OntPropertyGeneral.DATA.equals(ptype)) {
							// ES UN ATRIBUTO
							JSONObject jProperty = WebInterfaceUtils.toJSONIOntoProperty(property);
							jProperties.put(jProperty);
						} else if (IOntoProperty.OntPropertyGeneral.OTHER.equals(ptype)) {

						}
					}
				} else {
					// do nothing
				}

				// Each result data into a JSONObject
				JSONObject jsonResult = new JSONObject();
				jsonResult.put("uri", uri);
				jsonResult.put("label", label);
				jsonResult.put("comment", comment);
				jsonResult.put("type", type);
				jsonResult.put("similarity", similarity);
				jsonResult.put("searchsource", source);
				jsonResult.put("categories", jcats);
				jsonResult.put("superc", jsuperc);
				jsonResult.put("subc", jsubc);
				jsonResult.put("snippet", jSnippet);
				jsonResult.put("properties", jProperties);

				// Add Each result into the definitive JSONArray
				json.put(uri, jsonResult);
			}
			end = System.currentTimeMillis();
			PrintMethods.printFile(fw, init, end, "group By Hierarchy");

			init = System.currentTimeMillis();

			HashMap<String, Set<IOntoBase>> filters = WebInterfaceUtils.filterByClasses(conceptsToGroupByHierarchy);
			JSONObject conceptsFiltered = new JSONObject();
			for (Entry<String, Set<IOntoBase>> conceptsByTypeEntry : filters.entrySet()) {
				String filter = conceptsByTypeEntry.getKey();
				Set<IOntoBase> conceptsByType = conceptsByTypeEntry.getValue();

				// Get the Hierarchy of the concepts of this filter
				Map<IOntoBase, Set<IOntoBase>> groupByHierarchyFiltered = ServiceInterface
						.GroupByHierarchy(conceptsByType);

				// to URIs mapping
				Map<String, Set<String>> parentMap = new HashMap<>();
				for (Entry<IOntoBase, Set<IOntoBase>> parentEntry : groupByHierarchyFiltered.entrySet()) {
					String conceptURI = parentEntry.getKey().getURI();
					Set<String> parentsURI = new HashSet<>();
					for (IOntoBase parentIOB : parentEntry.getValue()) {
						parentsURI.add(parentIOB.getURI());
					}
					parentMap.put(conceptURI, parentsURI);
				}

				JSONArray jstree = WebInterfaceUtils.jsTreeGeneratorFromParentMap(parentMap);
				WebInterfaceUtils.jstreeRetocator(jstree, json);
				JSONObject jstreeJSON = new JSONObject();
				jstreeJSON.put("jstree", jstree);
				jstreeJSON.put("jstreenodes", conceptsByType.size());
				conceptsFiltered.put(conceptsByTypeEntry.getKey(), jstreeJSON);
			}

			end = System.currentTimeMillis();
			PrintMethods.printFile(fw, init, end, "WebInterfaceUtils.filterByClasses");

			jsonOutput.put("conceptsfiltered", conceptsFiltered);
			jsonOutput.put("valuestrunked", json);
			jsonOutput.put("untrunkedsize", resultsUntrunked.size());
			fw.flush();
			fw.close();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return jsonOutput.toString();
	}

	/**
	 * Returns a List<> of the categories where the indicators will pertain to
	 * 
	 * @return categories that pertain to the indicators
	 */
	public static String GetIndicatorsHierarchy() {

		JSONObject json = new JSONObject();
		try {
			// Get Indicator Hierarchy
			List<IIndicatorType> itypes = ServiceInterface.GetIndicatorTypes();
			for (IIndicatorType ind : itypes) {
				String indLabel = ind.getLabel();
				String indCategory = Integer.valueOf(ind.getGUIOrderTag()).toString();
				System.out.println("Label=" + indLabel);
				System.out.println("indCategory=" + indCategory);
				// Returns an empty list if the indicator type has not any
				// subtypes
				JSONObject jsonCat1 = new JSONObject();
				for (IOntoBase sub : ind.getSubConcepts()) {
					IIndicatorType subType = ServiceInterface.GetIndicatorTypes(sub);
					System.out.println("Sub: " + sub.getLabel());

					IIndicatorType subType2 = ServiceInterface.GetIndicatorTypes(sub);

					JSONObject jsonCat2 = new JSONObject();
					for (IOntoBase sub2 : subType2.getSubConcepts()) {
						// IIndicatorType subType2 =
						// services.ServiceInterface.GetIndicatorTypes(sub2);
						System.out.println("Sub2: " + sub2.getLabel());

						IIndicatorType subType3 = ServiceInterface.GetIndicatorTypes(sub2);
						JSONObject jsonCat3 = new JSONObject();
						for (IOntoBase sub3 : subType3.getSubConcepts()) {
							// IIndicatorType subType2 =
							// services.ServiceInterface.GetIndicatorTypes(sub2);
							System.out.println("Sub3: " + sub3.getLabel());
							JSONObject catLvl3 = new JSONObject();
							catLvl3.put("label", sub2.getLabel());
							catLvl3.put("resource", sub2.getResource().toString());
							catLvl3.put("type", sub2.getType().toString());
							catLvl3.put("uri", sub2.getURI());
							catLvl3.put("guiordertag", Integer.valueOf(subType3.getGUIOrderTag()).toString());
							jsonCat3.put(sub3.getLabel(), catLvl3);
						}
						JSONObject catLvl2 = new JSONObject();
						catLvl2.put("label", sub2.getLabel());
						catLvl2.put("resource", sub2.getResource().toString());
						catLvl2.put("type", sub2.getType().toString());
						catLvl2.put("uri", sub2.getURI());
						catLvl2.put("guiordertag", Integer.valueOf(subType2.getGUIOrderTag()).toString());
						// jsonCat2.put(sub2.getLabel(), "test");
						System.out.println("Sub3: label=" + sub2.getLabel());
						System.out.println("Sub3: resource=" + sub2.getResource().toString());
						System.out.println("Sub3: type=" + sub2.getType().toString());
						System.out.println("Sub3: uri=" + sub2.getURI());
						jsonCat2.put(sub2.getLabel(), catLvl2);
					}
					JSONObject catLvl1 = new JSONObject();
					catLvl1.put("label", sub.getLabel());
					catLvl1.put("resource", sub.getResource().toString());
					catLvl1.put("type", sub.getType().toString());
					catLvl1.put("uri", sub.getURI());
					catLvl1.put("guiordertag", Integer.valueOf(subType.getGUIOrderTag()).toString());
					catLvl1.put("subcat", jsonCat2);
					jsonCat1.put(sub.getLabel(), catLvl1);
				}
				JSONObject catLvl0 = new JSONObject();
				catLvl0.put("label", ind.getLabel());
				catLvl0.put("resource", ind.getResource().toString());
				catLvl0.put("type", ind.getType().toString());
				catLvl0.put("uri", ind.getURI());
				catLvl0.put("guiordertag", Integer.valueOf(ind.getGUIOrderTag()).toString());
				catLvl0.put("subcat", jsonCat1);
				json.put(ind.getLabel(), catLvl0);
			}
			System.out.println("GetIndicatorsHierarchy()->json=" + json.toString());
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return json.toString();
	}

	public static String GetIndicatorConcepts() {

		JSONObject json = new JSONObject();
		try {
			List<IIndicator> indicators = ServiceInterface.GetIndicatorConcepts();
			for (IIndicator ind : indicators) {
				JSONObject jInd = WebInterfaceUtils.convertIIndicatorToJSON(ind);
				json.put(ind.getURI(), jInd);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return json.toString();
	}

	/**
	 * For a given concept uri, the WS returns the links where this concept is a
	 * valid origin or destination
	 * 
	 * @param conceptUri
	 * @param withInstancesStr
	 * @return
	 */
	public static String GetLinksBoth(String conceptUri, String withInstancesStr) {

		JSONObject json = new JSONObject();

		boolean withInstances = Boolean.parseBoolean(withInstancesStr);
		// Get Links From
		System.out.println("GetLinksBoth(concept=" + conceptUri + ")");
		try {
			IOntoBase iobConcept = ServiceInterface.GetConcept(conceptUri);

			// links where the Concept is the domain
			List<IOntoLink> links;

			links = ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT, iobConcept, null, withInstances);

			JSONArray jAsDomain = new JSONArray();
			for (IOntoLink l : links) {
				// get all the possible rangeuri's
				jAsDomain.put(l.getRange().getURI());
			}
			json.put("asdomain", jAsDomain);

			// links where the Concept is the range

			links = ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT, null, iobConcept, withInstances);

			JSONArray jAsRange = new JSONArray();
			for (IOntoLink l : links) {
				// get all the possible domainuri's
				jAsRange.put(l.getDomain().getURI());
			}
			json.put("asrange", jAsRange);

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * Gets all the links with the domain and range specified as functions
	 * parameters
	 * 
	 * @param domainUri
	 *            is domain uri
	 * @param rangeUri
	 *            is range uri
	 * @return json containing all the links with the specified domain and range
	 */
	// public static String GetLinksFromTo(String domainUri, String rangeUri,
	// String withInstancesStr) {
	public static String GetLinksFromTo(String params) {
		JSONObject jsonOutput = null;
		try {
			jsonOutput = new JSONObject();
			JSONObject jsonInput = new JSONObject(params);
			String domainUri = jsonInput.getString("domainuri");
			String rangeUri = jsonInput.getString("rangeuri");
			boolean withInstances = jsonInput.getBoolean("populated");

			// Get Links From
			System.out.println("GetLinksFromTo(domain=" + domainUri + ", range=" + rangeUri + ")");
			IOntoBase iobDomain = null;
			IOntoBase iobRange = null;
			if (domainUri != null) {
				iobDomain = ServiceInterface.GetConcept(domainUri);
			}
			if (rangeUri != null) {
				iobRange = ServiceInterface.GetConcept(rangeUri);
			}
			boolean xx = ServiceInterface.isGeoConcept(iobDomain);
			boolean yy = ServiceInterface.isGeoConcept(iobRange);
			List<IOntoLink> links = null;

			// if (iobRange.isAreaInstance() && iobDomain.isAreaInstance()) {
			// IOntoArea rangeArea = ServiceInterface.GetArea(iobRange);
			// IOntoArea domainArea = ServiceInterface.GetArea(iobDomain);
			// links =
			// ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT,
			// domainArea, rangeArea,
			// withInstances);
			// } else if (iobRange.isAreaInstance()) {
			// IOntoArea rangeArea = ServiceInterface.GetArea(iobRange);
			// links =
			// ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT,
			// iobDomain, rangeArea,
			// withInstances);
			// } else if (iobDomain.isAreaInstance()) {
			// IOntoArea domainArea = ServiceInterface.GetArea(iobDomain);
			// links =
			// ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT,
			// domainArea, iobRange,
			// withInstances);
			// } else {
			// links =
			// ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT,
			// iobDomain, iobRange,
			// withInstances);
			// }

			links = ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT, iobDomain, iobRange,
					withInstances);

			for (IOntoLink l : links) {
				JSONObject jlinks = WebInterfaceUtils.convertLinkToJSON(l);
				String key = WebInterfaceUtils.linkKeyGenerator(l);
				jsonOutput.put(key, jlinks);
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonOutput.toString();
	}

	/**
	 * 
	 * @param domain
	 *            URI of the concept in the domain
	 * @return Links with the domain passed as parameter and SOBJECT relation
	 *         property
	 */
	public static String GetLinksFrom(String params) {

		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput;
		try {
			jsonInput = new JSONObject(params);
			String domain = jsonInput.getString("domainuri");
			boolean wi = jsonInput.getBoolean("populated");

			// Get Links From
			System.out.println("GetLinksFrom(domain=" + domain + ")");

			IOntoBase iobDomain = ServiceInterface.GetConcept(domain);

			List<IOntoLink> links = ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT, iobDomain, null,
					wi);

			for (IOntoLink l : links) {
				JSONObject jlinks = WebInterfaceUtils.convertLinkToJSON(l);
				String key = WebInterfaceUtils.linkKeyGenerator(l);
				jsonOutput.put(key, jlinks);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonOutput.toString();
	}
	
	/**
	 * Given a domain/range uri, returns its data properties(attributes &
	 * relations)
	 * 
	 * @param prop
	 *            the uri of a given domain/range
	 * @return json containing the data properties for the given domain/range
	 */
	public static String GetDataProperties(String params) {
		JSONObject jsonOutput = null;
		try {
			jsonOutput = new JSONObject();
			JSONObject jsonInput = new JSONObject(params);
			String domainUri = jsonInput.getString("uri");
			String rangeUri = null; // null for data-properties
			boolean withInstances = false; //jsonInput.getBoolean("populated");

			System.out.println("GetDataProperties(domain=" + domainUri + ", range=" + rangeUri + ")");
			IOntoBase iobDomain = null;
			IOntoBase iobRange = null;
			if (domainUri != null) {
				iobDomain = ServiceInterface.GetConcept(domainUri);
				boolean xx = ServiceInterface.isGeoConcept(iobDomain);
			}

			//List<IOntoLink> links = null;

			List<IOntoLink> links = ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.DATA, iobDomain, iobRange,
					withInstances);
			
			List<IOntoLink> slinks= ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.SDATA, iobDomain, iobRange,
					withInstances);
			
			links.addAll(slinks);

			for (IOntoLink l : links) {
				IOntoBase p = l.getRelationship();
				JSONObject dproperty = new JSONObject();
				dproperty.put("label", p.getLabel());
				dproperty.put("uri", p.getURI());
				jsonOutput.put(p.getURI(), dproperty);
			}
			
			ResourcesFactory rf =ResourcesFactory.getInstance(); 
			
			if( rf.identifySource(domainUri) == ResourcesSources.JENA) {
				JSONObject dproperty = new JSONObject();
				dproperty.put("label", "URI-individual");
				dproperty.put("uri", "http://www.bsc.com/OntManaging#URIs");
				jsonOutput.put("http://www.bsc.com/OntManaging#URIs", dproperty);	
			}
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonOutput.toString();
	}

	/**
	 * Given a domain/range uri, returns its data properties(attributes &
	 * relations)
	 * 
	 * @param prop
	 *            the uri of a given domain/range
	 * @return json containing the data properties for the given domain/range
	 */	 
	public static String GetDataProperties_Deprecated(String params) {
		//DEPRECATED 2019-DEC-12
		JSONObject json = null;
		String uri;
		try {
			json = new JSONObject(params);
			uri = json.getString("uri");
			IOntoBase concept = ServiceInterface.GetConcept(uri);
			Set<IOntoProperty> propertiesSet = ServiceInterface.GetDataProperties(concept);
			// Set<IOntoProperty> dataP = concept.getDataProperties();
			for (IOntoProperty p : propertiesSet) {
				JSONObject dproperty = new JSONObject();
				String nlabel = WebInterfaceUtils.genRelationshipLabel(p);
				dproperty.put("label", nlabel);
				dproperty.put("uri", p.getURI());
				json.put(p.getURI(), dproperty);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
		return json.toString();
	}

	/**
	 * Get some concept properties, useful for the GUI (label, class, uri...)
	 * 
	 * @param conceptUri
	 *            requires the concept URI
	 * @return concept attributes
	 */
	public static String GetProperties(String params) {

		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput = null;
		try {

			jsonInput = new JSONObject(params);
			String conceptUri = jsonInput.getString("concepturi");
			System.out.println("GetProperties(conceptUri=" + conceptUri + ")");
			IOntoBase iob = ServiceInterface.GetConcept(conceptUri);
			jsonOutput.put("uri", iob.getURI());
			jsonOutput.put("id", iob.getId());
			jsonOutput.put("class", iob.getClass());
			jsonOutput.put("label", iob.getLabel());
			jsonOutput.put("resource", iob.getResource());
			jsonOutput.put("type", iob.getType());
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return jsonOutput.toString();
	}

	public static String GetGraphDataURIS(JSONObject json) {
		int queryId = json.getInt("queryid");
		int nodeId = json.getInt("nodeid");
		int rangeFrom = json.getInt("rangeFrom");
		int rangeTo = json.getInt("rangeTo");
		JSONArray filters = json.getJSONArray("filters");
		JSONObject jsonOut = new JSONObject();
		List<String> resultsUris = null;
		JSONArray uris = new JSONArray();

		if (filters == null || filters.length() <= 0) {
			resultsUris = ServiceInterface.GetGraphDataURIS(nodeId, rangeFrom, rangeTo, queryId);
			uris = new JSONArray(resultsUris);
		} else {
			// TODO: filter the GetGraphDataURIs
		}
		jsonOut.put("uris", uris);

		return jsonOut.toString();
	}

	public static String GetGraphDataValuesFromCellnex(JSONObject json) {
		Integer queryId = null;
		Integer nodeId = null;
		String uriProperty = null;
		Integer rFrom = null;
		Integer rTo = null;
		JSONArray filters = null;

		if (!json.isNull("queryid")) {
			queryId = json.getInt("queryid");
		}
		if (!json.isNull("nodeid")) {
			nodeId = json.getInt("nodeid");
		}
		if (!json.isNull("uriprop")) {
			uriProperty = json.getString("uriprop");
		}
		if (!json.isNull("rangeFrom")) {
			rFrom = json.getInt("rangeFrom");
		}
		if (!json.isNull("rangeTo")) {
			rTo = json.getInt("rangeTo");
		}
		if (!json.isNull("filters")) {
			filters = json.getJSONArray("filters");
		}

		JSONArray rowsToReturn = new JSONArray();
		JSONObject jsonAll = new JSONObject();
		try {
			int subgraphId = ServiceInterface.getSubgraphId(Integer.valueOf(nodeId), queryId);
			int nrows = ServiceInterface.getRowsNumber(subgraphId, queryId);

			// TODO: Introducir el SparQLParser para hacer las llamadas a
			// Cellnex!
			String sparqlQuery = ServiceInterface.getSparql(subgraphId, queryId);
			List<Map<String, JSONObject>> results = SparQLParser.executeQuery(sparqlQuery);

			// TODO: 16/02, apply the MApacheCommons.filterRows(nodeId,
			// prop, filters) instead of the following call
			List<Set<String>> valuesNode;
			List<String> resultsUris = null;

			if (filters == null || filters.length() <= 0) {
				// no filters to apply
				if (rTo > nrows) {
					rTo = nrows - 1;
				}
				valuesNode = ServiceInterface.GetGraphDataValues(nodeId, uriProperty, rFrom, rTo, queryId);
				resultsUris = ServiceInterface.GetGraphDataURIS(nodeId, rFrom, rTo, queryId);
				jsonAll.put("nrows", nrows);
			} else {

				List<Set<String>> valuesNodeAll = ServiceInterface.GetGraphDataValues(Integer.valueOf(nodeId),
						uriProperty, 0, nrows - 1, queryId);

				List<String> resultsUrisAll = ServiceInterface.GetGraphDataURIS(Integer.valueOf(nodeId), 0, nrows - 1,
						queryId);

				// filters to apply (maybe invalid)
				List<Integer> filteredRowsIndex = WebInterfaceUtils.filterAllRows(valuesNodeAll,
						Integer.valueOf(nodeId), uriProperty, filters, queryId);

				List<Set<String>> filteredRows = new ArrayList<>();
				List<String> filteresResultsUris = new ArrayList<>();
				for (Integer i : filteredRowsIndex) {
					filteredRows.add(valuesNodeAll.get(i));
					filteresResultsUris.add(resultsUrisAll.get(i));
				}

				if (filteredRows.size() > 0) {
					if (rTo >= filteredRows.size()) {
						rTo = filteredRows.size() - 1;
					}
					valuesNode = filteredRows.subList(rFrom, rTo + 1);
					resultsUris = filteresResultsUris.subList(rFrom, rTo + 1);
				} else {
					valuesNode = filteredRows;// en empty list
					resultsUris = filteresResultsUris;
				}
				jsonAll.put("nrows", filteredRows.size());
			}

			String rowValues;
			String rowUri;
			Set<String> values;
			for (int i = 0; i < valuesNode.size(); ++i) {
				rowUri = resultsUris.get(i);
				values = (Set<String>) valuesNode.get(i);
				if (values.size() == 1) {
					rowValues = (String) values.toArray()[0];
				} else if (values.size() > 1) {
					rowValues = values.toString();
				} else {
					rowValues = "";
				}

				JSONObject rowJSON = new JSONObject();
				rowJSON.put("rowuri", rowUri);
				rowJSON.put("rowvalues", rowValues);
				rowsToReturn.put(rowJSON);
			}
			jsonAll.put("rows", rowsToReturn);

			System.out.println("GetGraphDataValues(String nodeId=" + nodeId + ", String uriProperty=" + uriProperty
					+ ")=[....] " + valuesNode.size() + " rows");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return jsonAll.toString();
	}

	public static String GetGraphDataValues(JSONObject json) {
		Integer queryId = null;
		Integer nodeId = null;
		String uriProperty = null;
		Integer rFrom = null;
		Integer rTo = null;
		JSONArray filters = null;

		if (!json.isNull("queryid")) {
			queryId = json.getInt("queryid");
		}
		if (!json.isNull("nodeid")) {
			nodeId = json.getInt("nodeid");
		}
		if (!json.isNull("uriprop")) {
			uriProperty = json.getString("uriprop");
		}
		if (!json.isNull("rangeFrom")) {
			rFrom = json.getInt("rangeFrom");
		}
		if (!json.isNull("rangeTo")) {
			rTo = json.getInt("rangeTo");
		}
		if (!json.isNull("filters")) {
			filters = json.getJSONArray("filters");
		}

		JSONArray rowsToReturn = new JSONArray();
		JSONObject jsonAll = new JSONObject();
		try {
			int subgraphId = ServiceInterface.getSubgraphId(Integer.valueOf(nodeId), queryId);
			int nrows = ServiceInterface.getRowsNumber(subgraphId, queryId);

			// TODO: 16/02, apply the MApacheCommons.filterRows(nodeId,
			// prop, filters) instead of the following call
			List<Set<String>> valuesNode;
			List<String> resultsUris = null;

			if (filters == null || filters.length() <= 0) {
				// no filters to apply
				if (rTo > nrows) {
					rTo = nrows - 1;
				}
				valuesNode = ServiceInterface.GetGraphDataValues(nodeId, uriProperty, rFrom, rTo, queryId);
				resultsUris = ServiceInterface.GetGraphDataURIS(nodeId, rFrom, rTo, queryId);
				jsonAll.put("nrows", nrows);
			} else {

				List<Set<String>> valuesNodeAll = ServiceInterface.GetGraphDataValues(Integer.valueOf(nodeId),
						uriProperty, 0, nrows - 1, queryId);

				List<String> resultsUrisAll = ServiceInterface.GetGraphDataURIS(Integer.valueOf(nodeId), 0, nrows - 1,
						queryId);

				// filters to apply (maybe invalid)
				List<Integer> filteredRowsIndex = WebInterfaceUtils.filterAllRows(valuesNodeAll,
						Integer.valueOf(nodeId), uriProperty, filters, queryId);

				List<Set<String>> filteredRows = new ArrayList<>();
				List<String> filteresResultsUris = new ArrayList<>();
				for (Integer i : filteredRowsIndex) {
					filteredRows.add(valuesNodeAll.get(i));
					filteresResultsUris.add(resultsUrisAll.get(i));
				}

				if (filteredRows.size() > 0) {
					if (rTo >= filteredRows.size()) {
						rTo = filteredRows.size() - 1;
					}
					valuesNode = filteredRows.subList(rFrom, rTo + 1);
					resultsUris = filteresResultsUris.subList(rFrom, rTo + 1);
				} else {
					valuesNode = filteredRows;// en empty list
					resultsUris = filteresResultsUris;
				}
				jsonAll.put("nrows", filteredRows.size());
			}

			String rowValues;
			String rowUri;
			Set<String> values;
			for (int i = 0; i < valuesNode.size(); ++i) {
				rowUri = resultsUris.get(i);
				values = (Set<String>) valuesNode.get(i);
				if (values.size() == 1) {
					rowValues = (String) values.toArray()[0];
				} else if (values.size() > 1) {
					rowValues = values.toString();
				} else {
					rowValues = "";
				}

				JSONObject rowJSON = new JSONObject();
				rowJSON.put("rowuri", rowUri);
				rowJSON.put("rowvalues", rowValues);
				rowsToReturn.put(rowJSON);
			}
			jsonAll.put("rows", rowsToReturn);

			System.out.println("GetGraphDataValues(String nodeId=" + nodeId + ", String uriProperty=" + uriProperty
					+ ")=[....] " + valuesNode.size() + " rows");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return jsonAll.toString();
	}

	public enum Calculus {
		Avg, Count, Max, Min, Sum
	}

	/**
	 * Returns the avg of the whole values corresponding to the subgraph,
	 * corresponding to the last query generated
	 * 
	 * @param subgraphNodesJSON
	 * @return avg of the whole values corresponding to the subgraph
	 */
	public static String GetGraphCalculus(String subgraphNodesJSON) {

		String[] requiredParams = { "node_id", "node_attribute_uri", "calculus", "filters" };
		JSONObject calculusJSON = null;
		try {
			// Extract data from JSON
			calculusJSON = new JSONObject(subgraphNodesJSON);
			boolean ipm = WebInterfaceUtils.isParamMissing(requiredParams, calculusJSON);
			if (ipm) {
				JSONObject jsonErrorMsg = new JSONObject();
				jsonErrorMsg.put("ERROR-MSG",
						"/GetGraphCalculus WS ERROR: Param missing at the JSON sent to the web service");
				return jsonErrorMsg.toString();
			}

			// Get the params
			int queryId = calculusJSON.getInt("queryid");
			int nodeId = calculusJSON.getInt("node_id");
			String nodeAttrUri = calculusJSON.getString("node_attribute_uri");
			String calcStr = calculusJSON.getString("calculus");
			Calculus calc = Calculus.valueOf(calcStr);
			JSONArray filters = calculusJSON.getJSONArray("filters");

			Integer subgraphId = ServiceInterface.getSubgraphId(Integer.valueOf(nodeId), queryId);
			Integer nrows = ServiceInterface.getRowsNumber(subgraphId, queryId);

			// Get all rows from the uri
			List<Set<String>> rows = ServiceInterface.GetGraphDataValues(nodeId, nodeAttrUri, 0, nrows - 1, queryId);

			// Apply the filter to the rows
			// No me vale el GetGraphDataValues() que solo me devuelve un
			// atributo, necesitaré el
			// Filter

			// Get list of invalid rows
			Set<Integer> invalidRowsSet = new HashSet<Integer>();
			for (int i = 0; i < filters.length(); ++i) {
				JSONObject f = filters.getJSONObject((i));
				int filterNodeId = f.getInt("nodeid");
				String filterAttrUri = f.getJSONObject("attribute").getString("uri");
				String filterType = f.getString("filtertype");
				JSONObject filterParams = (!f.isNull("params")) ? f.getJSONObject("params") : null;

				List<Set<String>> rowsToFilterFromNodeId = ServiceInterface.GetGraphDataValues(filterNodeId,
						filterAttrUri, 0, nrows - 1, queryId);
				ArrayList<Integer> invalidRows = WebInterfaceUtils.getInvalidRows(rowsToFilterFromNodeId, filterType,
						filterParams);
				invalidRowsSet.addAll(invalidRows);
			}

			// Generate a list with the valid rows
			List<Set<String>> filteredRows = new ArrayList<>();
			for (int i = 0; i < rows.size(); ++i) {
				if (!invalidRowsSet.contains(i)) {
					filteredRows.add(rows.get(i));
				}
			}

			// Calculate the calculus demanded
			// if (calc.equals("Avg")) { // Calculate the Avg
			if (calc == Calculus.Avg) { // Calculate the Avg
				int sum = 0, n = 0;
				for (Set<String> row : filteredRows) {
					for (String property : row) {
						if (WebInterfaceUtils.isNumeric(property)) {
							sum += Integer.parseInt(property);
							n++;
						}
					}
				}
				double avg = sum / n;
				calculusJSON.put("Avg", avg);
			}
			// if (calc.equals("Sum")) { // Calculate the Sum
			if (calc == Calculus.Sum) { // Calculate the Avg
				int sum = 0;
				for (Set<String> row : filteredRows) {
					for (String property : row) {
						if (WebInterfaceUtils.isNumeric(property)) {
							sum += Integer.parseInt(property);
						}
					}
				}
				calculusJSON.put("Sum", sum);
			}
			// if (calc.equals("Min")) { // Calculate the Min
			if (calc == Calculus.Min) { // Calculate the Avg
				int min = -1;
				for (Set<String> row : filteredRows) {
					for (String property : row) {
						if (WebInterfaceUtils.isNumeric(property)) {
							int val = Integer.parseInt(property);
							if (min == -1) {
								min = val;
							} else if (val < min) {
								min = val;
							}
						}
					}
				}
				calculusJSON.put("Min", min);
			}
			// if (calc.equals("Max")) { // Calculate the Max
			if (calc == Calculus.Max) { // Calculate the Avg
				int max = -1;
				for (Set<String> row : filteredRows) {
					for (String property : row) {
						if (WebInterfaceUtils.isNumeric(property)) {
							int val = Integer.parseInt(property);
							if (max == -1) {
								max = val;
							} else if (val > max) {
								max = val;
							}
						}
					}
				}
				calculusJSON.put("Max", max);
			}
			// if (calc.equals("Count")) { // Calculate the Count
			if (calc == Calculus.Count) { // Calculate the Avg
				calculusJSON.put("Count", filteredRows.size());
			}

		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return calculusJSON.toString();
	}

	/**
	 * Execute GetGraphDataValues through all the subgraph nodes to get all the
	 * data values obtained by the query. It will be used for the client-side
	 * processing by datatables. The JSON result is like: {rownumber(int):
	 * {nodeid(int): {dataproperty(URI): values(array) } } }
	 * 
	 * @param nodesId
	 *            list of nodes identifiers that compound the subgraph
	 * @return json with a collection with all the dataquery values from the
	 *         graph concepts
	 */
	public static String GetGraphAllDataValues(String subgraphNodesJSON) {

		ArrayList<String> subgraphNodes = new ArrayList<String>();
		Integer queryId = null; // to integrate
		String output = null;

		JSONArray jsonSubgraphNodes;
		try {
			jsonSubgraphNodes = new JSONArray(subgraphNodesJSON);

			for (int i = 0; i < jsonSubgraphNodes.length(); ++i) {
				subgraphNodes.add(String.valueOf(jsonSubgraphNodes.get(i)));
			}

			JSONObject jrows = new JSONObject();
			JSONObject jconcepts = new JSONObject();
			for (String node : subgraphNodes) {
				Integer subgraphId = ServiceInterface.getSubgraphId(Integer.valueOf(node), queryId);
				Integer nrows = ServiceInterface.getRowsNumber(subgraphId, queryId);
				List<HashMap<String, Set<String>>> rows;
				if (nrows > 10) // TRUNK para pruebas
					nrows = 20;

				rows = ServiceInterface.GetGraphDataValues(Integer.valueOf(node), 0, nrows - 1, queryId);
				int count = 0;
				for (HashMap<String, Set<String>> concept : rows) {
					JSONObject dProps = new JSONObject();
					for (String key : concept.keySet()) {
						// requested range of rows
						List<Set<String>> data = ServiceInterface.GetGraphDataValues(Integer.valueOf(node), key, count,
								count, queryId);
						for (Set<String> value : data) {
							JSONArray row = new JSONArray(value);
							dProps.accumulate(key, row);
							System.out.println("dProps.accumulate(" + key + ", " + row + ");");
						}
					}
					System.out.println("jconcepts.put(" + node + ", " + dProps + ");");
					System.out.println("jrows.accumulate(String.valueOf(" + (count) + "), " + jconcepts + ");");
					jconcepts.put(node, dProps);
					jrows.put(String.valueOf(count), jconcepts);
					count++;
				}

			}

			output = jrows.toString();
		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return output;
	}

	public static String GetGraphResultExport(String inputJSON, PrintWriter pw) {

		JSONObject jsonInput = null;
		String filepath = "";

		try {
			jsonInput = new JSONObject(inputJSON);
			String formatExport = jsonInput.getString("format");
			JSONArray jsonLinks = new JSONArray(jsonInput.getString("links"));
			JSONArray jsonNodesSueltos = new JSONArray(jsonInput.getString("nodessueltos"));
			Integer queryid = jsonInput.getInt("queryid");

			System.out.println("/GetGraphResultExport(input=" + jsonLinks + ", " + formatExport + ")");
			Set<IOntoBase> nodeSueltos = new HashSet<>();
			for (int i = 0; i < jsonNodesSueltos.length(); ++i) {
				JSONObject node = jsonNodesSueltos.getJSONObject(i);
				IOntoBase tmpIOB = ServiceInterface.GetConcept(node.getString("uri"));
				tmpIOB.setId(node.getInt("id"));
				nodeSueltos.add(tmpIOB);
			}

			Set<IOntoBase> nodesInLink = new HashSet<>();
			List<IOntoLink> links = new ArrayList<IOntoLink>();
			for (int i = 0; i < jsonLinks.length(); ++i) {
				JSONObject aLink = jsonLinks.getJSONObject(i);
				String domURI = String.valueOf(aLink.get("domainuri"));
				int domID = (Integer) aLink.get("domainid");
				String relURI = (String) aLink.get("relationuri");
				String ranURI = String.valueOf(aLink.get("rangeuri"));
				int ranID = (Integer) aLink.get("rangeid");
				IOntoBase dConcept = ServiceInterface.GetConcept(domURI);
				IOntoProperty pConcept = ServiceInterface.GetProperty(ServiceInterface.GetConcept(relURI));
				IOntoBase rConcept = ServiceInterface.GetConcept(ranURI);
				links.add(ServiceInterface.CreateLink(dConcept, pConcept, rConcept));

				dConcept.setId(domID);
				rConcept.setId(ranID);
				nodesInLink.add(dConcept);
				nodesInLink.add(rConcept);
			}
			System.out.println("services.ServiceInterface.GetGraphResultExport(links) Starting...");
			// Integer queryId =
			// services.ServiceInterface.GetGraphResult(nodeSueltos, links);
			// Integer queryId = 0; // testing para no volverlo a hacer
			if (queryid != null) {
				System.out.println("services.ServiceInterface.GetGraphResultExport(links)=TRUE");

				// TODO: put the data into an
				JSONArray jsa = new JSONArray();
				try {
					Set<IOntoBase> concepts = new HashSet<IOntoBase>();
					concepts.addAll(nodesInLink);
					concepts.addAll(nodeSueltos);

					ArrayList<ArrayList<String>> matrix = new ArrayList<ArrayList<String>>();
					ArrayList<String> col = new ArrayList<String>();
					ArrayList<String> headers = new ArrayList<String>();
					for (IOntoBase concept : concepts) {
						// Obtengo values de todos los atributos del concepto
						Set<IOntoProperty> propertiesSet = ServiceInterface.GetDataProperties(concept);
						for (IOntoProperty p : propertiesSet) {
							int subgraphId = ServiceInterface.getSubgraphId(concept.getId(), queryid);
							int rows = ServiceInterface.getRowsNumber(subgraphId, queryid);
							List<Set<String>> valuesNode = ServiceInterface.GetGraphDataValues(concept.getId(),
									p.getURI(), 0, rows - 1, queryid);
							col = new ArrayList<String>();
							String csvColLabel = "{" + WebInterfaceUtils.getTheLabel(concept) + "}.{"
									+ WebInterfaceUtils.genRelationshipLabel(p) + "}";
							// String csvColLabel = concept.getURI() + "_" +
							// p.getURI();
							// la primera fila de del csv son nombres de las
							// columnas (concept.dataProperty)
							headers.add(csvColLabel);

							for (Set<String> values : valuesNode) {
								String cell = "N/A";
								if (values.size() == 1) {
									for (String value : values) {
										cell = value;
									}
								} else if (values.size() > 1) {
									cell = values.toString();
								} else {
									cell = "empty";
								}
								jsa.put(cell);
								col.add(cell);
							}
							matrix.add(col);
						}
					}

					CSVFormat format = CSVFormat.DEFAULT.withHeader(headers.toArray(new String[headers.size()]))
							.withDelimiter(',');
					CSVPrinter myCSVPrinter = new CSVPrinter(pw, format);
					for (int j = 0; j < matrix.get(0).size(); j++) {
						ArrayList<String> csvrow = new ArrayList<String>();
						for (int k = 0; k < matrix.size(); k++) {
							csvrow.add(matrix.get(k).get(j));
						}
						myCSVPrinter.printRecord(csvrow); // add row to the csv
						myCSVPrinter.flush();
					}
					myCSVPrinter.close(); // close the printer

				} catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				System.out.println("services.ServiceInterface.GetGraphResultExport(links)=FALSE");
				return null;
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return filepath;
	}

	/**
	 * Creates an Area in the Ontology
	 * 
	 * @param wkt
	 *            coordinates of the Area you want to introduce, in wkt format
	 * @param label
	 *            label of the Area you want to introduce
	 * @return String data of the area created in JSON format (uri assigned in
	 *         Scribe, etc.)
	 */
	public static String CreateArea(String JSONpolygons) {
		JSONObject areaJSON = new JSONObject();
		try {
			JSONArray areasJSON = new JSONArray(JSONpolygons);
			// TODO: add the multipolygon param
			// MApacheCommons.createTheMultiPolygon(new ArrayList<String>());

			for (int i = 0; i < areasJSON.length(); ++i) {
				JSONObject areajson = areasJSON.getJSONObject(i);
				String wkt = areajson.getString("wkt");
				String label = areajson.getString("label");
				IOntoArea area = ServiceInterface.CreateArea(IOntoArea.OntArea.POLYGON, wkt, label);
				areaJSON = WebInterfaceUtils.convertIOntoAreaToJSONObject(area);
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return areaJSON.toString();
	}

	/**
	 * Executes a query with the area and the concepts
	 * 
	 * @param dataRequired
	 *            json string containing area and concepts to generate a query
	 */
	public static String GetGraphResultUC(String dataRequired) {
		// TODO:
		// 1) Create the Areas in the Ontology
		// 2) Create the Links between the concepts and different area instances

		JSONObject outputData = new JSONObject();
		try {
			JSONObject inputData = new JSONObject(dataRequired);

			// 1) Create the Areas in the Ontology
			JSONObject areasJSON = inputData.getJSONObject("area");
			String wkt = areasJSON.getString("wkt");
			String label = areasJSON.getString("label");
			IOntoArea area = ServiceInterface.CreateArea(IOntoArea.OntArea.POLYGON, wkt, label);

			// 2) Create the Links between the concepts and different area
			// instances
			List<IOntoLink> graph = new ArrayList<>();
			JSONArray nodes = inputData.getJSONArray("nodes");
			for (int i = 0; i < nodes.length(); ++i) {
				IOntoArea areaInstance = ServiceInterface.GetArea(area);
				areaInstance.setId(QueryGraphs.QueryNodeID++);
				JSONObject node = nodes.getJSONObject(i);
				String nUri = node.getString("uri");
				IOntoBase ioNode = ServiceInterface.GetConcept(nUri);
				ioNode.setId(QueryGraphs.QueryNodeID++);
				IOntoLink link = ServiceInterface.CreateGeospatialLink(ioNode, IOntoFunction.OntFunction.INTERSECTION,
						new ArrayList<String>(), areaInstance);
				graph.add(link);
			}

			Integer queryId = ServiceInterface.GetGraphResult(graph, WebInterface.sessionID);
			boolean wasAborted = ServiceInterface.OnQuery(WebInterface.sessionID);
			// boolean wasAborted = false;
			if (queryId == null && !wasAborted) {
				outputData.put("ERROR-MSG", "ERROR on /GetGraphResultUC");
			} else {
				outputData.put("queryid", queryId);
				String jsonString = WebInterfaceUtils.getConnectedGraphsJSON(graph, new ArrayList<IOntoBase>(),
						new Integer(queryId));
				JSONArray jgraph = new JSONArray(jsonString);
				outputData.put("graph", jgraph);

				// TODO: a partir de los links, devolver tb edges?
				JSONObject graphNodes = new JSONObject();
				for (IOntoLink iol : graph) {
					// domain node
					IOntoBase d = iol.getDomain();
					JSONObject dJSON = WebInterfaceUtils.convertIOBToJSONObject(d);
					graphNodes.put(String.valueOf(d.getId()), dJSON);
					// range node
					IOntoBase r = iol.getRange();
					JSONObject rJSON = WebInterfaceUtils.convertIOBToJSONObject(r);
					graphNodes.put(String.valueOf(r.getId()), rJSON);
				}
				JSONArray ja = WebInterfaceUtils.convertJSONObjectToJSONArray(graphNodes);
				outputData.put("nodes", ja);

				JSONObject jSparqQL = new JSONObject();
				JSONObject jrows = new JSONObject();
				// get how many rows are in the result
				for (int j = 0; j < jgraph.length(); ++j) {
					JSONArray subgraph = (JSONArray) jgraph.get(j);
					for (int k = 0; k < subgraph.length(); ++k) {
						Integer nodeid = (Integer) subgraph.get(k);
						int subgraphId = ServiceInterface.getSubgraphId(nodeid, queryId);
						int rows = ServiceInterface.getRowsNumber(subgraphId, queryId);
						jrows.put(String.valueOf(subgraphId), rows);
						jSparqQL.put(String.valueOf(subgraphId), ServiceInterface.getSparql(subgraphId, queryId));
						break; // unique call enough to get sparql and nrows
					}
				}
				outputData.put("nrows", jrows);
				outputData.put("sparqlquery", jSparqQL);
				outputData.put("queryid", queryId);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return outputData.toString();
	}

	/**
	 * 
	 * @param inputJSON
	 * @return
	 */
	public static String GetGraphResultEnergySynthetic(String input) {
		JSONObject inputJSON = new JSONObject(input);
		// Construct the graph
		ResultsGraph rg = new ResultsGraph(inputJSON);
		// Get synthetic results
		Set<List<JSONObject>> results = rg.makeSyntheticResults();
		// Save RG on the singleton
		ResultsGraphFactory.getInstance().setRG(rg);

		int init = 0, end = 50;
		Set<List<JSONObject>> resultsTrunked = results.stream().skip(init).limit(end).collect(Collectors.toSet());

		// TODO: convertir output al formato de la UI:
		// {"headers":["TemperatureSensor","Beach"],"data":[["88","11"],["88","24"],["88","25"],["88","26"],["88","27"],["88","17"],["88","20"],["88","21"],["68","23"],["68","25"],["68","15"],["68","27"],["68","144"],["68","20"],["68","21"],["171","22"],["171","23"],["171","24"],["171","25"],["171","15"],["171","16"],["171","17"],["171","18"],["171","19"],["171","144"],["171","20"],["194","11"],["194","13"],["194","25"],["194","18"],["194","19"],["194","21"],["130","13"],["130","15"],["130","16"],["130","144"],["198","11"],["198","26"],["198","15"],["198","19"],["198","144"],["155","23"],["155","24"],["155","26"],["155","27"],["155","16"],["155","17"],["155","19"],["155","21"],["199","22"],["199","11"],["199","12"],["199","24"],["199","14"],["199","26"],["199","15"],["199","27"],["199","17"],["199","19"],["199","20"],["199","21"],["211","11"],["211","14"],["211","27"],["211","16"],["211","144"],["211","20"],["211","21"],["156","22"],["156","12"],["156","24"],["156","14"],["156","27"],["156","21"],["212","22"],["212","11"],["212","12"],["212","13"],["212","24"],["212","15"],["212","17"],["212","19"],["212","144"],["212","20"],["138","11"],["138","12"],["138","23"],["138","24"],["138","14"],["138","15"],["138","16"],["138","17"],["138","18"],["138","21"],["30","22"],["30","11"],["30","23"],["30","25"],["30","14"],["30","20"],["11","11"],["11","23"],["11","24"],["11","25"],["11","14"],["11","26"],["11","27"],["11","16"],["11","17"],["11","18"],["11","19"],["11","20"],["16","22"],["16","11"],["16","24"],["16","26"],["16","15"],["16","17"],["16","19"],["16","21"],["142","11"],["142","12"],["142","23"],["142","13"],["142","24"],["142","25"],["142","14"],["142","16"],["142","18"],["142","20"],["100","22"],["100","11"],["100","12"],["100","24"],["100","25"],["100","26"],["100","15"],["100","16"],["100","20"],["222","11"],["222","12"],["222","13"],["222","24"],["222","14"],["222","26"],["222","27"],["222","17"],["222","18"],["222","19"],["222","20"],["222","21"],["169","12"],["169","23"],["169","26"],["169","15"],["169","17"],["169","18"],["169","19"],["169","20"],["169","21"],["226","11"],["226","23"],["226","13"],["226","14"],["226","26"],["226","15"],["226","27"],["226","17"],["226","18"],["226","19"],["226","144"],["128","12"],["128","23"],["128","13"],["128","17"],["128","18"],["128","19"],["128","144"],["128","21"],["208","13"],["208","24"],["208","25"],["208","15"],["208","18"],["208","19"],["208","20"],["82","11"],["82","14"],["82","26"],["82","17"],["82","18"],["82","19"],["82","21"],["86","13"],["86","24"],["86","25"],["86","14"],["86","26"],["86","16"],["86","17"],["43","22"],["43","11"],["43","23"],["43","13"],["43","24"],["43","25"],["43","15"],["43","16"],["43","18"],["43","20"]],"attributes":[["component","service","name","description","location","id","type","observed_properties","manufacturer","technical_details"],["end_date","provider","name","id","type","technical_details"]]}

		JSONObject outputUI = rg.toJSONUIFormat(results);

		// Serialized results (serializing the trunked results, otherwise, would
		// need a Cassandra)
		JSONArray jsonToSerialize = new JSONArray(results);
		FileWriter fw;
		try {
			fw = new FileWriter("/tmp/last_query_json.json");
			System.out.println("serializing #rows : " + jsonToSerialize.length());

			jsonToSerialize.write(fw);
			fw.flush();
			fw.close();
			System.out.println("/tmp/last_query_json.json");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return outputUI.toString();
	}

	/**
	 * 
	 * @param inputJSON
	 * @return
	 */
	public static String GetGraphResultEnergy(String input) {
		JSONObject inputJSON = new JSONObject(input);
		// Construct the graph
		ResultsGraph rg = new ResultsGraph(inputJSON);
		//Set the CellnexToken
		CellnexAPI.setCellnexToken(inputJSON.getString("usertoken"));
		
		// Execute the query By Subgraph
		ResultsDAG.executeQueryBySubGraphs(rg);
		// Cross the results
		// ResultsCrossing.ResultsGraphCrossing(rg);
		// ResultsCrossing.ResultsGraphCrossingv2(rg);
		Set<List<JSONObject>> results = ResultsCrossing.ResultsGraphCrossingByCartesian(rg);
		// Save RG on the singleton
		ResultsGraphFactory.getInstance().setRG(rg);

		int init = 0, end = 50;
		Set<List<JSONObject>> resultsTrunked = results.stream().skip(init).limit(end).collect(Collectors.toSet());

		// TODO: convertir output al formato de la UI:
		// {"headers":["TemperatureSensor","Beach"],"data":[["88","11"],["88","24"],["88","25"],["88","26"],["88","27"],["88","17"],["88","20"],["88","21"],["68","23"],["68","25"],["68","15"],["68","27"],["68","144"],["68","20"],["68","21"],["171","22"],["171","23"],["171","24"],["171","25"],["171","15"],["171","16"],["171","17"],["171","18"],["171","19"],["171","144"],["171","20"],["194","11"],["194","13"],["194","25"],["194","18"],["194","19"],["194","21"],["130","13"],["130","15"],["130","16"],["130","144"],["198","11"],["198","26"],["198","15"],["198","19"],["198","144"],["155","23"],["155","24"],["155","26"],["155","27"],["155","16"],["155","17"],["155","19"],["155","21"],["199","22"],["199","11"],["199","12"],["199","24"],["199","14"],["199","26"],["199","15"],["199","27"],["199","17"],["199","19"],["199","20"],["199","21"],["211","11"],["211","14"],["211","27"],["211","16"],["211","144"],["211","20"],["211","21"],["156","22"],["156","12"],["156","24"],["156","14"],["156","27"],["156","21"],["212","22"],["212","11"],["212","12"],["212","13"],["212","24"],["212","15"],["212","17"],["212","19"],["212","144"],["212","20"],["138","11"],["138","12"],["138","23"],["138","24"],["138","14"],["138","15"],["138","16"],["138","17"],["138","18"],["138","21"],["30","22"],["30","11"],["30","23"],["30","25"],["30","14"],["30","20"],["11","11"],["11","23"],["11","24"],["11","25"],["11","14"],["11","26"],["11","27"],["11","16"],["11","17"],["11","18"],["11","19"],["11","20"],["16","22"],["16","11"],["16","24"],["16","26"],["16","15"],["16","17"],["16","19"],["16","21"],["142","11"],["142","12"],["142","23"],["142","13"],["142","24"],["142","25"],["142","14"],["142","16"],["142","18"],["142","20"],["100","22"],["100","11"],["100","12"],["100","24"],["100","25"],["100","26"],["100","15"],["100","16"],["100","20"],["222","11"],["222","12"],["222","13"],["222","24"],["222","14"],["222","26"],["222","27"],["222","17"],["222","18"],["222","19"],["222","20"],["222","21"],["169","12"],["169","23"],["169","26"],["169","15"],["169","17"],["169","18"],["169","19"],["169","20"],["169","21"],["226","11"],["226","23"],["226","13"],["226","14"],["226","26"],["226","15"],["226","27"],["226","17"],["226","18"],["226","19"],["226","144"],["128","12"],["128","23"],["128","13"],["128","17"],["128","18"],["128","19"],["128","144"],["128","21"],["208","13"],["208","24"],["208","25"],["208","15"],["208","18"],["208","19"],["208","20"],["82","11"],["82","14"],["82","26"],["82","17"],["82","18"],["82","19"],["82","21"],["86","13"],["86","24"],["86","25"],["86","14"],["86","26"],["86","16"],["86","17"],["43","22"],["43","11"],["43","23"],["43","13"],["43","24"],["43","25"],["43","15"],["43","16"],["43","18"],["43","20"]],"attributes":[["component","service","name","description","location","id","type","observed_properties","manufacturer","technical_details"],["end_date","provider","name","id","type","technical_details"]]}

		JSONObject outputUI = rg.toJSONUIFormat(resultsTrunked);

		// Serialized results (serializing the trunked results, otherwise, would
		// need a Cassandra)
		JSONArray jsonToSerialize = new JSONArray(resultsTrunked);
		FileWriter fw;
		try {
			fw = new FileWriter("/tmp/last_query_json.json");
			System.out.println("serializing #rows : " + jsonToSerialize.length());

			jsonToSerialize.write(fw);
			fw.flush();
			fw.close();
			System.out.println("/tmp/last_query_json.json");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return outputUI.toString();
	}

	public static String GetGraphResultUpdate(String input) {

		JSONObject inputJSON = new JSONObject(input);

		// Deserializacion de la query
		List<List<JSONObject>> results = new ArrayList<>();
		JSONArray resultsJSON = null;
		String outputserialized = new String();
		JSONArray dataProps = inputJSON.getJSONArray("attributes");
		JSONArray jsonToSerialize = new JSONArray(results);

		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader("/tmp/last_query_json.json");
			br = new BufferedReader(fr);
			String sCurrentLine;
			StringBuilder sb = new StringBuilder();
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine);
			}

			resultsJSON = new JSONArray(sb.toString());
			fr.close();
			System.out.println("/tmp/last_query_json.json");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ResultsGraph rg = ResultsGraphFactory.getInstance().getRG();

		// convert JSONArray to Set<List<JSONObject>>
		Iterator<Object> iterSet = resultsJSON.iterator();
		while (iterSet.hasNext()) {
			JSONArray listJSON = (JSONArray) iterSet.next();
			Iterator<Object> iterList = listJSON.iterator();
			List<JSONObject> listOUT = new ArrayList<>();
			int attrIndex = 0;
			while (iterList.hasNext()) {
				JSONObject obj = (JSONObject) iterList.next();
				listOUT.add(obj);
			}
			results.add(listOUT);
		}

		int init = 0, end = 50;
		Set<List<JSONObject>> resultsTrunked = results.stream().skip(init).limit(end).collect(Collectors.toSet());

		JSONObject outputUI = rg.toJSONUIFormat(resultsTrunked, dataProps);

		// Set<List<String>> resultsSetOutput = new HashSet<>();
		// Iterator<Object> iterSet = resultsJSON.iterator();
		// while (iterSet.hasNext()) {
		// JSONArray listJSON = (JSONArray) iterSet.next();
		// Iterator<Object> iterList = listJSON.iterator();
		// List<String> listOUT = new ArrayList<>();
		// int attrIndex = 0;
		// while (iterList.hasNext()) {
		// JSONObject obj = (JSONObject) iterList.next();
		// String attr =
		// dataProps.getJSONObject(attrIndex++).get("attribute").toString();
		// String value = obj.get(attr).toString();
		// listOUT.add(value);
		//
		// }
		// resultsSetOutput.add(listOUT);
		// }

		// trunked
		// int init = 0, end = 50;
		// Set<List<String>> resultsTrunked =
		// resultsSetOutput.stream().skip(init).limit(end).collect(Collectors.toSet());
		//
		// JSONObject outputUI = rg.toJSONUIFormat(resultsTrunked);

		// JSONArray jsonArr = new JSONArray(resultsTrunked);
		// JSONObject outputJSON = new JSONObject();
		// outputJSON.put("headers", new JSONArray());
		// outputJSON.put("data", jsonArr);
		// outputJSON.put("attributes", new JSONArray());

		// Read the last query
		// String jsonFile = new String();
		// FileReader fr;
		// try {
		// fr = new FileReader(new File("/tmp/last_query.json"));
		// BufferedReader br = new BufferedReader(fr);
		// String sCurrentLine;
		//
		// while ((sCurrentLine = br.readLine()) != null) {
		// jsonFile.concat(sCurrentLine);
		// }
		// fr.close();
		// } catch (IOException ex) {
		// ex.printStackTrace();
		// }

		// int init = 0, end = 50;
		// JSONObject results = new JSONObject(jsonFile);

		// TODO: convertir output al formato de la UI:
		// {"headers":["TemperatureSensor","Beach"],"data":[["88","11"],["88","24"],["88","25"],["88","26"],["88","27"],["88","17"],["88","20"],["88","21"],["68","23"],["68","25"],["68","15"],["68","27"],["68","144"],["68","20"],["68","21"],["171","22"],["171","23"],["171","24"],["171","25"],["171","15"],["171","16"],["171","17"],["171","18"],["171","19"],["171","144"],["171","20"],["194","11"],["194","13"],["194","25"],["194","18"],["194","19"],["194","21"],["130","13"],["130","15"],["130","16"],["130","144"],["198","11"],["198","26"],["198","15"],["198","19"],["198","144"],["155","23"],["155","24"],["155","26"],["155","27"],["155","16"],["155","17"],["155","19"],["155","21"],["199","22"],["199","11"],["199","12"],["199","24"],["199","14"],["199","26"],["199","15"],["199","27"],["199","17"],["199","19"],["199","20"],["199","21"],["211","11"],["211","14"],["211","27"],["211","16"],["211","144"],["211","20"],["211","21"],["156","22"],["156","12"],["156","24"],["156","14"],["156","27"],["156","21"],["212","22"],["212","11"],["212","12"],["212","13"],["212","24"],["212","15"],["212","17"],["212","19"],["212","144"],["212","20"],["138","11"],["138","12"],["138","23"],["138","24"],["138","14"],["138","15"],["138","16"],["138","17"],["138","18"],["138","21"],["30","22"],["30","11"],["30","23"],["30","25"],["30","14"],["30","20"],["11","11"],["11","23"],["11","24"],["11","25"],["11","14"],["11","26"],["11","27"],["11","16"],["11","17"],["11","18"],["11","19"],["11","20"],["16","22"],["16","11"],["16","24"],["16","26"],["16","15"],["16","17"],["16","19"],["16","21"],["142","11"],["142","12"],["142","23"],["142","13"],["142","24"],["142","25"],["142","14"],["142","16"],["142","18"],["142","20"],["100","22"],["100","11"],["100","12"],["100","24"],["100","25"],["100","26"],["100","15"],["100","16"],["100","20"],["222","11"],["222","12"],["222","13"],["222","24"],["222","14"],["222","26"],["222","27"],["222","17"],["222","18"],["222","19"],["222","20"],["222","21"],["169","12"],["169","23"],["169","26"],["169","15"],["169","17"],["169","18"],["169","19"],["169","20"],["169","21"],["226","11"],["226","23"],["226","13"],["226","14"],["226","26"],["226","15"],["226","27"],["226","17"],["226","18"],["226","19"],["226","144"],["128","12"],["128","23"],["128","13"],["128","17"],["128","18"],["128","19"],["128","144"],["128","21"],["208","13"],["208","24"],["208","25"],["208","15"],["208","18"],["208","19"],["208","20"],["82","11"],["82","14"],["82","26"],["82","17"],["82","18"],["82","19"],["82","21"],["86","13"],["86","24"],["86","25"],["86","14"],["86","26"],["86","16"],["86","17"],["43","22"],["43","11"],["43","23"],["43","13"],["43","24"],["43","25"],["43","15"],["43","16"],["43","18"],["43","20"]],"attributes":[["component","service","name","description","location","id","type","observed_properties","manufacturer","technical_details"],["end_date","provider","name","id","type","technical_details"]]}

		return outputUI.toString();
	}

	/**
	 * Method to get the data from different sources (JENA-TDB, Cellnex-API...)
	 * 
	 * @param inputJSON
	 *            input with JSON containing the graph query made on the UI
	 * @return data containing the data received from different sources
	 *         (JENA-TDB, Cellnex-API...)
	 */
	public static String GetGraphResultMultiSource(String inputJSON) {

		JSONObject jOut = new JSONObject();
		JSONObject jsonQueryGraph;
		// get request data from headers
		jsonQueryGraph = new JSONObject(inputJSON);
		JSONArray jsonLinks = jsonQueryGraph.getJSONArray("links");
		JSONArray jsonNodesSueltos = jsonQueryGraph.getJSONArray("nodesSueltos");
		JSONArray nodes = jsonQueryGraph.getJSONArray("nodesDS");

		JSONObject jSparqQL = new JSONObject();
		JSONObject jrows = new JSONObject();
		Integer queryId = null;

		// TODO:
		String getGraphResultOutput = WebInterface.GetGraphResult(inputJSON);
		JSONObject ggrJSON = new JSONObject(getGraphResultOutput);
		String sparqlQuery = ggrJSON.getJSONObject("sparqlquery").getString("0");
		List<Map<String, JSONObject>> results;

		// @2017/01/18: temporal para testear UI y no tener que ejecutar
		boolean testUI = false;
		if (!testUI || !(new File("/tmp/myQueryResults.ser").exists())) {

			results = SparQLParser.executeQuery(sparqlQuery);

			List<Map<String, String>> resultSerializable = DataResultsUtils.getSerialize(results);
			// Serializo el resultado de la query (todas las instancias
			try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/tmp/myQueryResults.ser"))) {
				oos.writeObject(resultSerializable);
				System.out.println("Serializado en /tmp/myQueryResults.ser");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			// @2017/01/18: temporal para testear UI y no tener que ejecutar
			// toda la query (resultados serializados)
			results = DataResultsUtils.deSerialize("/tmp/myQueryResults.ser");
		}
		// Le pongo formato para la UI
		// JSONObject resultsUI = DataResultsUtils.toUIFormat(results);
		JSONObject resultsUI = null;
		if (jsonQueryGraph.has("attributes")) {
			JSONArray attributes = jsonQueryGraph.getJSONArray("attributes");
			JSONArray attrClean = new JSONArray();
			for (int i = 0; i < attributes.length(); ++i) {
				if (attributes.isNull(i)) {
					// this column selected by random
					attrClean.put("id");
				} else {
					attrClean.put(attributes.getJSONObject(i).getString("attribute"));
				}
			}
			resultsUI = DataResultsUtils.toUIFormat(results, attrClean);
		} else {
			resultsUI = DataResultsUtils.toUIFormat(results);
		}

		// String resultsHCoded =
		// {"headers":["TemperatureSensor","Beach"],"data":[["88","11"],["88","24"],["88","25"],["88","26"],["88","27"],["88","17"],["88","20"],["88","21"],["68","23"],["68","25"],["68","15"],["68","27"],["68","144"],["68","20"],["68","21"],["171","22"],["171","23"],["171","24"],["171","25"],["171","15"],["171","16"],["171","17"],["171","18"],["171","19"],["171","144"],["171","20"],["194","11"],["194","13"],["194","25"],["194","18"],["194","19"],["194","21"],["130","13"],["130","15"],["130","16"],["130","144"],["198","11"],["198","26"],["198","15"],["198","19"],["198","144"],["155","23"],["155","24"],["155","26"],["155","27"],["155","16"],["155","17"],["155","19"],["155","21"],["199","22"],["199","11"],["199","12"],["199","24"],["199","14"],["199","26"],["199","15"],["199","27"],["199","17"],["199","19"],["199","20"],["199","21"],["211","11"],["211","14"],["211","27"],["211","16"],["211","144"],["211","20"],["211","21"],["156","22"],["156","12"],["156","24"],["156","14"],["156","27"],["156","21"],["212","22"],["212","11"],["212","12"],["212","13"],["212","24"],["212","15"],["212","17"],["212","19"],["212","144"],["212","20"],["138","11"],["138","12"],["138","23"],["138","24"],["138","14"],["138","15"],["138","16"],["138","17"],["138","18"],["138","21"],["30","22"],["30","11"],["30","23"],["30","25"],["30","14"],["30","20"],["11","11"],["11","23"],["11","24"],["11","25"],["11","14"],["11","26"],["11","27"],["11","16"],["11","17"],["11","18"],["11","19"],["11","20"],["16","22"],["16","11"],["16","24"],["16","26"],["16","15"],["16","17"],["16","19"],["16","21"],["142","11"],["142","12"],["142","23"],["142","13"],["142","24"],["142","25"],["142","14"],["142","16"],["142","18"],["142","20"],["100","22"],["100","11"],["100","12"],["100","24"],["100","25"],["100","26"],["100","15"],["100","16"],["100","20"],["222","11"],["222","12"],["222","13"],["222","24"],["222","14"],["222","26"],["222","27"],["222","17"],["222","18"],["222","19"],["222","20"],["222","21"],["169","12"],["169","23"],["169","26"],["169","15"],["169","17"],["169","18"],["169","19"],["169","20"],["169","21"],["226","11"],["226","23"],["226","13"],["226","14"],["226","26"],["226","15"],["226","27"],["226","17"],["226","18"],["226","19"],["226","144"],["128","12"],["128","23"],["128","13"],["128","17"],["128","18"],["128","19"],["128","144"],["128","21"],["208","13"],["208","24"],["208","25"],["208","15"],["208","18"],["208","19"],["208","20"],["82","11"],["82","14"],["82","26"],["82","17"],["82","18"],["82","19"],["82","21"],["86","13"],["86","24"],["86","25"],["86","14"],["86","26"],["86","16"],["86","17"],["43","22"],["43","11"],["43","23"],["43","13"],["43","24"],["43","25"],["43","15"],["43","16"],["43","18"],["43","20"]],"attributes":[["component","service","name","description","location","id","type","observed_properties","manufacturer","technical_details"],["end_date","provider","name","id","type","technical_details"]]}
		// JSONObject resultsUI = new JSONObject(resultsHCoded);

		return resultsUI.toString();

	}

	/**
	 * Method to get the data from the Cellnex API (fake till 12/09/2016)
	 * 
	 * @param inputJSON
	 *            input with JSON containing the graph query made on the UI
	 * @return data containing the data received from Cellnex API
	 */
	public static String GetGraphResultFromCellnex(String inputJSON) {

		JSONObject jOut = new JSONObject();
		JSONObject jsonQueryGraph;
		// get request data from headers
		jsonQueryGraph = new JSONObject(inputJSON);
		JSONArray jsonLinks = jsonQueryGraph.getJSONArray("links");
		JSONArray jsonNodesSueltos = jsonQueryGraph.getJSONArray("nodesSueltos");
		JSONArray nodes = jsonQueryGraph.getJSONArray("nodesDS");

		JSONObject jSparqQL = new JSONObject();
		JSONObject jrows = new JSONObject();
		Integer queryId = null;

		// TODO:
		String getGraphResultOutput = WebInterface.GetGraphResult(inputJSON);
		JSONObject ggrJSON = new JSONObject(getGraphResultOutput);
		String sparqlQuery = ggrJSON.getJSONObject("sparqlquery").getString("0");
		List<Map<String, JSONObject>> results;

		// @2017/01/18: temporal para testear UI y no tener que ejecutar
		boolean testUI = false;
		if (!testUI || !(new File("/tmp/myQueryResults.ser").exists())) {

			results = SparQLParser.executeQuery(sparqlQuery);

			List<Map<String, String>> resultSerializable = DataResultsUtils.getSerialize(results);
			// Serializo el resultado de la query (todas las instancias
			try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/tmp/myQueryResults.ser"))) {
				oos.writeObject(resultSerializable);
				System.out.println("Serializado en /tmp/myQueryResults.ser");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			// @2017/01/18: temporal para testear UI y no tener que ejecutar
			// toda la query (resultados serializados)
			results = DataResultsUtils.deSerialize("/tmp/myQueryResults.ser");
		}
		// Le pongo formato para la UI
		// JSONObject resultsUI = DataResultsUtils.toUIFormat(results);
		JSONObject resultsUI = null;
		if (jsonQueryGraph.has("attributes")) {
			JSONArray attributes = jsonQueryGraph.getJSONArray("attributes");
			JSONArray attrClean = new JSONArray();
			for (int i = 0; i < attributes.length(); ++i) {
				if (attributes.isNull(i)) {
					// this column selected by random
					attrClean.put("id");
				} else {
					attrClean.put(attributes.getJSONObject(i).getString("attribute"));
				}
			}
			resultsUI = DataResultsUtils.toUIFormat(results, attrClean);
		} else {
			resultsUI = DataResultsUtils.toUIFormat(results);
		}

		// String resultsHCoded =
		// "{'headers':['TemperatureSensor','Beach'],'data':[['88','11'],['88','24'],['88','25'],['88','26'],['88','27'],['88','17'],['88','20'],['88','21'],['68','23'],['68','25'],['68','15'],['68','27'],['68','144'],['68','20'],['68','21'],['171','22'],['171','23'],['171','24'],['171','25'],['171','15'],['171','16'],['171','17'],['171','18'],['171','19'],['171','144'],['171','20'],['194','11'],['194','13'],['194','25'],['194','18'],['194','19'],['194','21'],['130','13'],['130','15'],['130','16'],['130','144'],['198','11'],['198','26'],['198','15'],['198','19'],['198','144'],['155','23'],['155','24'],['155','26'],['155','27'],['155','16'],['155','17'],['155','19'],['155','21'],['199','22'],['199','11'],['199','12'],['199','24'],['199','14'],['199','26'],['199','15'],['199','27'],['199','17'],['199','19'],['199','20'],['199','21'],['211','11'],['211','14'],['211','27'],['211','16'],['211','144'],['211','20'],['211','21'],['156','22'],['156','12'],['156','24'],['156','14'],['156','27'],['156','21'],['212','22'],['212','11'],['212','12'],['212','13'],['212','24'],['212','15'],['212','17'],['212','19'],['212','144'],['212','20'],['138','11'],['138','12'],['138','23'],['138','24'],['138','14'],['138','15'],['138','16'],['138','17'],['138','18'],['138','21'],['30','22'],['30','11'],['30','23'],['30','25'],['30','14'],['30','20'],['11','11'],['11','23'],['11','24'],['11','25'],['11','14'],['11','26'],['11','27'],['11','16'],['11','17'],['11','18'],['11','19'],['11','20'],['16','22'],['16','11'],['16','24'],['16','26'],['16','15'],['16','17'],['16','19'],['16','21'],['142','11'],['142','12'],['142','23'],['142','13'],['142','24'],['142','25'],['142','14'],['142','16'],['142','18'],['142','20'],['100','22'],['100','11'],['100','12'],['100','24'],['100','25'],['100','26'],['100','15'],['100','16'],['100','20'],['222','11'],['222','12'],['222','13'],['222','24'],['222','14'],['222','26'],['222','27'],['222','17'],['222','18'],['222','19'],['222','20'],['222','21'],['169','12'],['169','23'],['169','26'],['169','15'],['169','17'],['169','18'],['169','19'],['169','20'],['169','21'],['226','11'],['226','23'],['226','13'],['226','14'],['226','26'],['226','15'],['226','27'],['226','17'],['226','18'],['226','19'],['226','144'],['128','12'],['128','23'],['128','13'],['128','17'],['128','18'],['128','19'],['128','144'],['128','21'],['208','13'],['208','24'],['208','25'],['208','15'],['208','18'],['208','19'],['208','20'],['82','11'],['82','14'],['82','26'],['82','17'],['82','18'],['82','19'],['82','21'],['86','13'],['86','24'],['86','25'],['86','14'],['86','26'],['86','16'],['86','17'],['43','22'],['43','11'],['43','23'],['43','13'],['43','24'],['43','25'],['43','15'],['43','16'],['43','18'],['43','20']],'attributes':[['component','service','name','description','location','id','type','observed_properties','manufacturer','technical_details'],['end_date','provider','name','id','type','technical_details']]}";
		// JSONObject resultsUI = new JSONObject(resultsHCoded);

		return resultsUI.toString();

	}

	/**
	 * Calculate the graph results, returns a json with the subparts of the
	 * graph
	 * 
	 * @param json
	 *            containing an array with the links of the graph
	 * @return json string containing an array of array of the subparts of the
	 *         graph
	 */
	public static String GetGraphResult(String inputJSON) {
		Date dateInit = new Date();

		JSONObject jOut = new JSONObject();

		try {
			JSONObject jsonQueryGraph = new JSONObject(inputJSON);
			JSONArray jsonLinks = jsonQueryGraph.getJSONArray("links");
			JSONArray jsonNodesSueltos = jsonQueryGraph.getJSONArray("nodesSueltos");
			JSONObject jSparqQL = new JSONObject();
			JSONObject jrows = new JSONObject();
			Integer queryId = null;

			List<IOntoLink> links = new ArrayList<IOntoLink>();
			ArrayList<IOntoBase> nodesSueltos = new ArrayList<IOntoBase>();
			if (jsonNodesSueltos.length() > 0 || jsonLinks.length() > 0) {
				for (int i = 0; i < jsonNodesSueltos.length(); ++i) {
					JSONObject nodejson = jsonNodesSueltos.getJSONObject(i);
					QueryGNode gn = new QueryGNode(nodejson);
					IOntoBase nodeSueltoIOB = ServiceInterface.GetConcept(gn.getUri());
					nodeSueltoIOB.setId(nodejson.getInt("id"));
					nodesSueltos.add(nodeSueltoIOB);
				}

				// Nodos sueltos
				Set<IOntoBase> nodesSueltosSet = new HashSet<>();
				nodesSueltosSet.addAll(nodesSueltos);
				// End Nodos sueltos

				Random rand = new Random();

				for (int i = 0; i < jsonLinks.length(); ++i) {
					JSONObject aLink = jsonLinks.getJSONObject(i);
					JSONObject relation = (JSONObject) aLink.get("relation");

					QueryGNode domain = new QueryGNode(aLink.getJSONObject("domain"));
					QueryGNode range = new QueryGNode(aLink.getJSONObject("range"));
					String relURI = (String) relation.get("prop");

					IOntoBase domConcept = ServiceInterface.GetConcept(domain.getUri());
					IOntoBase ranConcept = ServiceInterface.GetConcept(range.getUri());
					IOntoBase relConcept = ServiceInterface.GetConcept(relURI);
					// TODO:
					domConcept.setId(aLink.getJSONObject("domain").getInt("id"));
					ranConcept.setId(aLink.getJSONObject("range").getInt("id"));
					relConcept.setId((int) (rand.nextInt((999999 - 0) + 1)));

					IOntoLink geoLink = null;
					// check type (property/function)
					if (relConcept.isProperty()) {
						// if property
						IOntoProperty propConcept = ServiceInterface.GetProperty(relConcept);
						propConcept.setId((int) (rand.nextInt((999999 - 0) + 1)));
						geoLink = ServiceInterface.CreateLink(domConcept, propConcept, ranConcept);
					} else if (relConcept.isFunction()) {
						// relationparams were introduced by the user, after
						// choosing a function
						List<String> propParams = new ArrayList<>();
						if (relation.has("params")) {
							JSONArray relParams = (JSONArray) relation.get("params");
							propParams = WebInterfaceUtils.jsonArrayToArrayList(relParams);
						}
						IOntoFunction funConcept = ServiceInterface.GetFunction(relConcept, propParams);
						funConcept.setId((int) (rand.nextInt((999999 - 0) + 1)));

						if (domConcept.isAreaInstance()) {
							// domain is Area
							IOntoArea area = ServiceInterface.GetArea(domConcept);
							area.setId(Integer.valueOf(domain.getId()));
							geoLink = ServiceInterface.CreateGeospatialLink(area, funConcept.getFunctionType(),
									propParams, ranConcept);
						} else if (ranConcept.isAreaInstance()) {
							// range is Area
							IOntoArea area = ServiceInterface.GetArea(ranConcept);
							area.setId(Integer.valueOf(range.getId()));
							geoLink = ServiceInterface.CreateGeospatialLink(domConcept, funConcept.getFunctionType(),
									propParams, area);
						} else {
							// domain nor range are Areas
							System.out.println("Function without areas");
							geoLink = ServiceInterface.CreateLink(domConcept, funConcept, ranConcept);
						}

					}
					links.add(geoLink);
					// domConcept.setId(Integer.valueOf(domain.getId()));
					// ranConcept.setId(Integer.valueOf(range.getId()));
				}
				// End Links (not including nodes_sueltos)

				System.out.println("services.ServiceInterface.GetGraphResult(IOntoBase) Starting...");
				queryId = ServiceInterface.GetGraphResult(nodesSueltosSet, links, WebInterface.sessionID);
				boolean wasAborted = ServiceInterface.OnQuery(WebInterface.sessionID);
				if (queryId != null && !wasAborted) {
					System.out.println("services.ServiceInterface.GetGraphResult(links)=TRUE");
					// get subgraphs compounding the whole graph
					// String jsonString =
					// MyCommons.convertSubgraphsToJSON(queryId); // Beta
					String jsonString = WebInterfaceUtils.getConnectedGraphsJSON(links, nodesSueltos, queryId);
					JSONArray jgraph = new JSONArray(jsonString);
					jOut.put("graph", jgraph);

					// NODES SUELTOS
					for (IOntoBase nodeSuelto : nodesSueltosSet) {
						int subgraphId = ServiceInterface.getSubgraphId(nodeSuelto.getId(), queryId);
						int rows = ServiceInterface.getRowsNumber(subgraphId, queryId);
						String sparql = ServiceInterface.getSparql(subgraphId, queryId);
						jrows.put(String.valueOf(nodeSuelto.getId()), rows);
						jSparqQL.put(String.valueOf(subgraphId), sparql);
					}

					// LINKS
					for (int i = 0; i < jgraph.length(); ++i) {
						JSONArray subgraph = (JSONArray) jgraph.get(i);
						for (int j = 0; j < subgraph.length(); ++j) {
							Integer nodeid = (Integer) subgraph.get(j);
							int subgraphId = ServiceInterface.getSubgraphId(nodeid, queryId);
							int rows = ServiceInterface.getRowsNumber(subgraphId, queryId);
							jrows.put(String.valueOf(nodeid), rows);
							String sparql = ServiceInterface.getSparql(subgraphId, queryId);
							jSparqQL.put(String.valueOf(subgraphId), sparql);
							// unique call enough to get sparql and nrows
							// break;
						}
					}

				} else {
					System.out.println("services.ServiceInterface.GetGraphResult(links)=FALSE");
					return null;
				}

			} else {
				JSONObject jsonErrorMsg = new JSONObject();
				jsonErrorMsg
						.put("ERROR-MSG",
								"/GetGraphResult WS ERROR: Graph must be connected. The json sendt may content at least one link.");
				return jsonErrorMsg.toString();
			}

			jOut.put("nrows", jrows);
			jOut.put("sparqlquery", jSparqQL);
			jOut.put("queryid", queryId);

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Date dateEnd = new Date();

		long timeInit = dateInit.getTime();
		long timeEnd = dateEnd.getTime();

		long difftime = timeEnd - timeInit; // miliseconds
		long diffSeconds = difftime / 1000 % 60;
		long diffMinutes = difftime / (60 * 1000) % 60;
		long diffHours = difftime / (60 * 60 * 1000) % 24;
		long diffDays = difftime / (24 * 60 * 60 * 1000);

		Date difftimeDate = new Date(difftime);
		SimpleDateFormat milliseconds = new SimpleDateFormat("SSS");// "HH:mm:ss.SSS"
		String millisecondsString = milliseconds.format(difftimeDate);

		System.out.println(diffDays + " days, " + diffHours + " hours, " + diffMinutes + " mins, " + diffSeconds
				+ " sec, " + millisecondsString + "msec.");
		jOut.put("timeexecution", diffMinutes + " mins, " + diffSeconds + "." + millisecondsString + " sec");

		return jOut.toString();
	}

	/**
	 * Returns the subclasses and superclasses of the concept's uri
	 * 
	 * @param uri
	 *            of the concept whose sub/super concepts are requested
	 * @return json string containing sub/super concepts information
	 */
	public static String GetSubSuperClasses(String jsonInput) {
		// Extract data from the input
		JSONObject jsonIn = new JSONObject(jsonInput);
		String conceptURI = jsonIn.getString("uri");
		Boolean withInstances = jsonIn.getBoolean("withinstances");

		JSONObject jsonOutput = new JSONObject();
		try {
			IOntoBase concept = ServiceInterface.GetConcept(conceptURI);
			List<IOntoBase> superc = concept.getSuperConcepts();
			List<IOntoBase> subc = concept.getSubConcepts();

			JSONArray jsonSuperclassesArray = new JSONArray();
			for (IOntoBase sc : superc) {
				JSONObject jsonSuperclass = new JSONObject();
				String id = Integer.toString(sc.getId());
				String label = sc.getLabel();
				String uri = sc.getURI();
				String type = sc.getType().toString();
				jsonSuperclass.put("id", id);
				jsonSuperclass.put("label", label);
				jsonSuperclass.put("uri", uri);
				jsonSuperclass.put("type", type);
				jsonSuperclass.put("relation", "(SUP)");
				jsonSuperclass.put("comment", "N/A"); // Mientras no haya
														// sc.getComment(),
				// lo dejo vacio
				jsonSuperclass.put("similarity", "N/A (Sub/Sup-Class added)"); // No
																				// tiene
				// sentido,
				// no se ha
				// aplicado
				// en la
				// busqueda

				Set<IOntoBase> cats = ServiceInterface.GetSearchConcept(sc, 1).getCategory();
				JSONArray jcats = new JSONArray();
				if (cats != null && cats.size() > 0) {
					for (IOntoBase cat : cats) {
						JSONObject jcat = new JSONObject();
						jcat.put("uri", cat.getURI());
						jcat.put("label", cat.getLabel());
						jcats.put(jcat);
					}
				} else {
					JSONObject jcat = new JSONObject();
					jcat.put("uri", "Other");
					jcat.put("label", "Other");
					jcats.put(jcat);
				}
				jsonSuperclass.put("categories", jcats);
				jsonSuperclassesArray.put(jsonSuperclass);

				System.out.println("SuperClasses=" + jsonSuperclass.toString());
			}

			JSONArray jsonSubclassesArray = new JSONArray();
			for (IOntoBase sc : subc) {
				JSONObject jsonSubclass = new JSONObject();
				String id = Integer.toString(sc.getId());
				String label = sc.getLabel();
				String uri = sc.getURI();
				String type = sc.getType().toString();
				jsonSubclass.put("id", id);
				jsonSubclass.put("label", label);
				jsonSubclass.put("uri", uri);
				jsonSubclass.put("type", type);
				jsonSubclass.put("relation", "(sub)");
				Set<IOntoBase> cats = ServiceInterface.GetSearchConcept(sc, 1).getCategory();
				JSONArray jcats = new JSONArray();
				if (cats != null && cats.size() > 0) {
					for (IOntoBase cat : cats) {
						JSONObject jcat = new JSONObject();
						jcat.put("uri", cat.getURI());
						jcat.put("label", cat.getLabel());
						jcats.put(jcat);
					}
				} else {
					JSONObject jcat = new JSONObject();
					jcat.put("uri", "Other");
					jcat.put("label", "Other");
					jcats.put(jcat);
				}
				jsonSubclass.put("categories", jcats);
				jsonSubclassesArray.put(jsonSubclass);

				System.out.println("Subclasses=" + jsonSubclass.toString());
			}
			jsonOutput.put("subc", jsonSubclassesArray);
			jsonOutput.put("superc", jsonSuperclassesArray);

		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return jsonOutput.toString();
	}

	public static String GetUrbanConceptsJsTree() {
		ArrayList<DataNodeJSTree> dnjst = new ArrayList<>();
		IOntoBase udc = ServiceInterface.GetUrbanDimensionConcept();
		DataJsTree djt = new DataJsTree(udc);
		String jsTreeData = djt.toJSONArray().toString();
		return jsTreeData;
	}

	public static String isGeoConcept(String paramsInJSON) {
		JSONObject jsonOut = null;
		try {
			jsonOut = new JSONObject(paramsInJSON);
			String conceptURI = jsonOut.getString("uri");
			IOntoBase iob = ServiceInterface.GetConcept(conceptURI);
			boolean udc = ServiceInterface.isGeoConcept(iob);
			jsonOut.put("isgeoconcept", udc);
			if (udc && iob.isAreaInstance()) {
				IOntoArea area = ServiceInterface.GetArea(iob);
				String wkt = area.getWKT();
				// String wkt =
				// "POLYGON ((236887.66903021314647
				// 5068821.040585480630398,236899.29181341259391
				// 5068775.162597150541842,236882.80740297655575
				// 5068792.569475973956287,236878.38361317635281
				// 5068788.431501735001802,236866.555633034178754
				// 5068801.103840194642544,236876.175287506310269
				// 5068810.1867581512779,236878.802420684573008
				// 5068812.668205762282014,236887.66903021314647
				// 5068821.040585480630398))";
				jsonOut.put("wkt", wkt);
			} else {
				jsonOut.put("wkt", false); // null
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonOut.toString();
	}

	public static String isArea(String paramsInJSON) {
		JSONObject jsonOut = null;
		try {
			jsonOut = new JSONObject(paramsInJSON);
			String uri = jsonOut.getString("uri");
			IOntoBase iOB = ServiceInterface.GetConcept(uri);
			boolean isArea = iOB.isAreaInstance();
			jsonOut.put("isArea", isArea);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonOut.toString();
	}

	public static String getPaths(String paramsInJSON) {
		JSONObject jsonOut = null;
		try {
			jsonOut = new JSONObject(paramsInJSON);
			String domainURI = jsonOut.getString("domainuri");
			String rangeURI = jsonOut.getString("rangeuri");
			int maxDeep = jsonOut.getInt("maxdeep");
			boolean populated = jsonOut.getBoolean("populated");

			IOntoBase domainIOB = ServiceInterface.GetConcept(domainURI);
			IOntoBase rangeIOB = ServiceInterface.GetConcept(rangeURI);

			Map<Integer, Set<String[]>> pathResults = ServiceInterface
					.GetPaths(domainIOB, rangeIOB, maxDeep, populated);

			JSONArray pathsJSON = new JSONArray();
			/**
			 * PATCH TEMPORAL PARA MAXDEPTH == 1 En este caso haga una llamada a
			 * GetLinksFromTo()
			 */
			if (maxDeep == 1) {

				List<IOntoLink> links;
				try {
					links = ServiceInterface.GetDirectLinks(IOntoProperty.OntProperty.OBJECT, domainIOB, rangeIOB,
							populated);
					if (links.size() > 0) {
						JSONArray jarr = new JSONArray();
						JSONObject node = new JSONObject();
						node.put("id", domainIOB.getId());
						node.put("uri", domainIOB.getURI());
						node.put("label", domainIOB.getLabel());
						node.put("type", domainIOB.getType().toString());
						// dJSON.put("isarea", d.toString());
						node.put("isfunction", Boolean.valueOf(domainIOB.isFunction()).toString());
						node.put("isproperty", Boolean.valueOf(domainIOB.isProperty()).toString());
						jarr.put(node);

						node = new JSONObject();
						node.put("id", rangeIOB.getId());
						node.put("uri", rangeIOB.getURI());
						node.put("label", rangeIOB.getLabel());
						node.put("type", rangeIOB.getType().toString());
						// dJSON.put("isarea", d.toString());
						node.put("isfunction", Boolean.valueOf(rangeIOB.isFunction()).toString());
						node.put("isproperty", Boolean.valueOf(rangeIOB.isProperty()).toString());
						jarr.put(node);

						String pathLabel = "";
						final String separator = " :: ";
						for (int i = 0; i < jarr.length(); ++i) {
							if (i < (jarr.length() - 1)) {
								pathLabel += jarr.getJSONObject(i).getString("label") + separator;
							} else {
								pathLabel += jarr.getJSONObject(i).getString("label");
							}
						}
						JSONObject ss = new JSONObject();
						ss.put("label", pathLabel);
						ss.put("depth", maxDeep);
						ss.put("path", jarr);
						// resultsByDepthJSON.put(ss);
						pathsJSON.put(ss);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/**
				 * END PATCH TEMPORAL PARA MAXDEPTH == 1
				 */
			} else {

				/**
				 * PATCH TEMPORAL PARA MAXDEPTH == 1 En este caso haga una
				 * llamada a GetLinksFromTo()
				 */
				// Si A y B geoconcepts, hay relaciones geograficas entre ellos
				// (ditance, sfWithin,...)
				boolean a = ServiceInterface.isGeoConcept(domainIOB);
				boolean b = ServiceInterface.isGeoConcept(rangeIOB);
				if ((!pathResults.keySet().contains(1)) && a && b) {
					JSONArray jarr = new JSONArray();
					JSONObject node = new JSONObject();
					node.put("id", domainIOB.getId());
					node.put("uri", domainIOB.getURI());
					node.put("label", domainIOB.getLabel());
					node.put("type", domainIOB.getType().toString());
					node.put("isfunction", Boolean.valueOf(domainIOB.isFunction()).toString());
					node.put("isproperty", Boolean.valueOf(domainIOB.isProperty()).toString());
					jarr.put(node);

					node = new JSONObject();
					node.put("id", rangeIOB.getId());
					node.put("uri", rangeIOB.getURI());
					node.put("label", rangeIOB.getLabel());
					node.put("type", rangeIOB.getType().toString());
					node.put("isfunction", Boolean.valueOf(rangeIOB.isFunction()).toString());
					node.put("isproperty", Boolean.valueOf(rangeIOB.isProperty()).toString());
					jarr.put(node);

					String pathLabel = "";
					final String separator = " :: ";
					for (int i = 0; i < jarr.length(); ++i) {
						if (i < (jarr.length() - 1)) {
							pathLabel += jarr.getJSONObject(i).getString("label") + separator;
						} else {
							pathLabel += jarr.getJSONObject(i).getString("label");
						}
					}
					JSONObject ss = new JSONObject();
					ss.put("label", pathLabel);
					ss.put("depth", maxDeep);
					ss.put("path", jarr);
					// resultsByDepthJSON.put(ss);
					pathsJSON.put(ss);
				}

				/**
				 * END PATCH TEMPORAL PARA MAXDEPTH == 1
				 */

				for (Integer depth : pathResults.keySet()) {
					// Get results by depth
					Set<String[]> resultsByDepth = pathResults.get(depth);

					JSONArray resultsByDepthJSON = new JSONArray();
					for (String[] nodesPaths : resultsByDepth) {
						JSONArray jarr = new JSONArray();
						ArrayList<String> pathLabelAL = new ArrayList<>();
						// String pathLabel = "[";
						for (String nodePathUri : nodesPaths) {
							if (nodePathUri != null) {
								IOntoBase nodeIOB = ServiceInterface.GetConcept(nodePathUri);
								// JSONObject node2 =
								// MApacheCommons.convertIOBToJSONObject(nodeIOB);
								JSONObject node = new JSONObject();
								node.put("id", nodeIOB.getId());
								node.put("uri", nodeIOB.getURI());
								node.put("label", nodeIOB.getLabel());
								node.put("type", nodeIOB.getType().toString());
								// dJSON.put("isarea", d.toString());
								node.put("isfunction", Boolean.valueOf(nodeIOB.isFunction()).toString());
								node.put("isproperty", Boolean.valueOf(nodeIOB.isProperty()).toString());

								jarr.put(node);
								pathLabelAL.add(nodeIOB.getLabel());
							}
						}
						String pathLabel = "";
						final String separator = " :: ";
						for (int i = 0; i < pathLabelAL.size(); ++i) {
							if (i < (pathLabelAL.size() - 1)) {
								pathLabel += pathLabelAL.get(i) + separator;
							} else {
								pathLabel += pathLabelAL.get(i);
							}
						}
						JSONObject ss = new JSONObject();
						ss.put("label", pathLabel);
						ss.put("depth", depth);
						ss.put("path", jarr);
						// resultsByDepthJSON.put(ss);
						pathsJSON.put(ss);
					}
					// pathsJSON.put(depth.toString(), resultsByDepthJSON);
				}
			}
			jsonOut.put("paths", pathsJSON);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonOut.toString();
	}

	public static String GetSnippet(String params) {
		JSONObject jsonOutput = null;

		JSONObject jsonInput;
		String output = null;
		jsonInput = new JSONObject(params);

		String uri = jsonInput.getString("uri");
		String query = jsonInput.getString("query");
		String openlabel = jsonInput.getString("openlabel");
		String closelabel = jsonInput.getString("closelabel");
		Map<String, Map<String, String>> snippets = ServiceInterface.GetSnippet(uri, query, openlabel, closelabel);

		for (Map.Entry<String, Map<String, String>> eachSnippet : snippets.entrySet()) {
			String snippetUri = eachSnippet.getKey();
			Map<String, String> v = eachSnippet.getValue();
			for (Map.Entry<String, String> vx : v.entrySet()) {
				String kx = vx.getKey();
				String kv = vx.getValue();
				jsonOutput.put(snippetUri, kx);
				jsonOutput.put(snippetUri, kv);
			}
		}
		output = jsonOutput.toString();
		return output;
	}

	/**
	 * Returns JSON containing the anchor areas that can be associated with an
	 * Indicator
	 * 
	 * @return JSON containing the anchor areas that can be associated with an
	 *         Indicator
	 */
	public static String GetIndicatorDescriptors() {
		JSONObject jsonOutput = new JSONObject();
		JSONArray idsjson = new JSONArray();
		try {
			for (IIndicatorDescriptor iidescr : ServiceInterface.GetIndicatorDescriptors()) {
				JSONObject idJSON = WebInterfaceUtils.iiDescriptorToJSON(iidescr);
				idsjson.put(idJSON);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jsonOutput.put("indicator_descriptors", idsjson);
		return jsonOutput.toString();

	}

	/**
	 * Returns JSON containing the anchor areas that can be associated with an
	 * Indicator
	 * 
	 * @return JSON containing the anchor areas that can be associated with an
	 *         Indicator
	 */
	public static String GetIndicatorAnchorAreas() {
		JSONObject jsonOutput = new JSONObject();
		JSONArray aaJSON = new JSONArray();
		ANCHOR_AREAS[] aas = IIndicator.ANCHOR_AREAS.values();
		for (ANCHOR_AREAS aa : aas) {
			aaJSON.put(aa.toString());
		}
		jsonOutput.put("anchor_areas", aaJSON);
		return jsonOutput.toString();

	}

	public static String indicatorRemove(String inputParams) {
		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput;
		try {
			jsonInput = new JSONObject(inputParams);
			JSONArray indicatorURIs = jsonInput.getJSONArray("indicator_uris");
			for (int i = 0; i < indicatorURIs.length(); ++i) {
				String uri = indicatorURIs.getString(i);
				IOntoBase IOind = ServiceInterface.GetConcept(uri);
				IIndicator iind = ServiceInterface.GetIndicator(IOind);
				ServiceInterface.RemoveIndicator(iind);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonOutput.toString();

	}

	public static String indicatorUpdateValue(String inputParams) {
		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput;
		try {
			jsonInput = new JSONObject(inputParams);
			String uri = jsonInput.getString("indicator_uri");
			IOntoBase IOind = ServiceInterface.GetConcept(uri);
			IIndicator iind = ServiceInterface.GetIndicator(IOind);
			String value = jsonInput.getString("value");
			iind.editIndicatorValue(Float.valueOf(value));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonOutput.toString();

	}

	public static String CancelQuery(String params) {
		// TODO Auto-generated method stub
		JSONObject jsonOutput = null;
		JSONObject jsonInput = null;
		try {
			jsonOutput = new JSONObject();
			jsonInput = new JSONObject(params);
			boolean queryCanceled = ServiceInterface.CancelQuery(WebInterface.sessionID);
			jsonOutput.put("wascanceled", queryCanceled);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonOutput.toString();
	}

}
