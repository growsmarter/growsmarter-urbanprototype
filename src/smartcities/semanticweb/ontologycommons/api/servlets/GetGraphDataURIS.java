package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;

/**
 * Servlet implementation class GetGraphDataURIS
 */
@WebServlet("/GetGraphDataURIS")
public class GetGraphDataURIS extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetGraphDataURIS() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//String nodeid = request.getParameter("myparams");
		// String params = request.getParameter("params");
		String postBody = null;
		if (request.getParameter("params") == null) {
			postBody = WebInterfaceUtils.readPostMessage(request);
		} else {
			postBody = request.getParameter("params");
		}
		JSONObject inputJSON = new JSONObject(postBody);
		
		String json = WebInterface.GetGraphDataURIS(inputJSON);
		response.setContentType("application/json");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(json);
	}

}
