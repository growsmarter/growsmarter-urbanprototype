/*
 * Ajax response for the retrieval of the features checked on the Urban Planning Concepts (tab1)
 */
package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import smartcities.semanticweb.ontologycommons.api.WebInterface;

/**
 * Servlet implementation class GetFeatures
 */
@WebServlet("/GetFeatures")
public class GetFeatures extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFeatures() {
        super();
        // TODO Auto-generated constructor stub
    }
    
   
    public String JsonResponse(String strURIItem, String wktFeature){
 	   return WebInterface.Json_InstancesIntersectPolygon(strURIItem, wktFeature);
    	//return "";
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// String concept = request.getParameter("conceptSelected");
		String strFeature = request.getParameter("selectedFeature");
		String strURIItem=request.getParameter("URIItem");
		String json = JsonResponse(strURIItem, strFeature); 
		response.setContentType("application/json");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(json);
		
	}

}
