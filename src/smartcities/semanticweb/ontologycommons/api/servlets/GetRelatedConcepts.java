package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import smartcities.semanticweb.ontologycommons.api.WebInterface;

/**
 * Servlet implementation class GetRelatedConcepts
 */
@WebServlet("/GetRelatedConcepts")
public class GetRelatedConcepts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetRelatedConcepts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ptype = request.getParameter("propertytype");
		String uriclass = request.getParameter("uriclass");
		String populated= request.getParameter("populated");
		String json = WebInterface.GetRelatedConcepts(ptype, uriclass, populated);
		response.setContentType("application/json");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(json);
	}

}
