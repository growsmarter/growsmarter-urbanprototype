package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import smartcities.semanticweb.ontologycommons.api.WebInterface;

/**
 * Servlet implementation class GetUrbanConceptsJsTree
 */
@WebServlet("/GetUrbanConceptsJsTree")
public class GetUrbanConceptsJsTree extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUrbanConceptsJsTree() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
// 		String uconcepts = WebInterface.GetUrbanPlanningPerspectiveConcepts(); 
//		String uri = request.getParameter("uri");
//		String withInstances = request.getParameter("withinstances");
		String json = WebInterface.GetUrbanConceptsJsTree();
		response.setContentType("application/json");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(json);

	}

}
