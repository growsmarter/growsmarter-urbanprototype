package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;

/**
 * Servlet implementation class GetLabel
 */
@WebServlet("/GetLabel")
public class GetLabel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetLabel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String postBody = null;
		if (request.getParameter("params") == null) {
			postBody = WebInterfaceUtils.readPostMessage(request);
		} else {
			postBody = request.getParameter("params");
		}
		JSONObject jsonInput = new JSONObject(postBody);
		String uri = jsonInput.getString("uri");
		String label = WebInterface.GetLabel(uri); 
		response.setContentType("application/text");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(label);
	}

}
