package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;

/**
 * Servlet implementation class GetGraphResultFromCellnex
 */
@WebServlet("/GetGraphResultEnergy")
public class GetGraphResultEnergy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetGraphResultEnergy() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		// TODO Auto-generated method stub
		// String params = request.getParameter("params");
		String postbody = WebInterfaceUtils.readPostMessage(request);
		JSONObject postbodyJSON = new JSONObject(postbody);
		String json = null;
		if (postbodyJSON.getBoolean("synthetic")) {
			json = WebInterface.GetGraphResultEnergySynthetic(postbody);
		} else {
			json = WebInterface.GetGraphResultEnergy(postbody);
		}

		if (json != null) {
			response.setContentType("application/json");
			// ServletOutputStream sos = response.getOutputStream();
			// sos.println(json);
			PrintWriter pw = response.getWriter();
			pw.println(json);

		} else {
			response.getWriter().print("ERROR on GetGraphResultEnergy");
			// HttpServletResponse.SC_INTERNAL_SERVER_ERROR = 500
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
}
