package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator.ANCHOR_AREAS;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.input.BOMInputStream;

/**
 * Servlet implementation class IndicatorUpdateFileUploader
 */
@WebServlet("/IndicatorUpdateFileUploader")
public class IndicatorUpdateFileUploader extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndicatorUpdateFileUploader() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Check that we have a file upload request
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();

		// Configure a repository (to ensure a secure temp location is used)
		ServletContext servletContext = this.getServletConfig().getServletContext();

		File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");

		factory.setRepository(repository);

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		try {
			List<FileItem> items = upload.parseRequest(request);
			Map<String, String> params = new HashMap<>();
			// Process the uploaded items
			for (FileItem item : items) {
				if (item.isFormField()) {
					String name = item.getFieldName();
					String value = item.getString();
					params.put(name, value);
					// processFormField(item);
				} else {
					InputStream is = item.getInputStream();
					Reader reader = new InputStreamReader(is);
					CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
					Map<String, Integer> headers = parser.getHeaderMap();
					try {

						ANCHOR_AREAS aa = IIndicator.ANCHOR_AREAS.valueOf(params.get("anchor_area"));
						String indicatorDescriptorUri = params.get("indicator_descriptor");
						IOntoBase IOBIdescr = ServiceInterface.GetConcept(indicatorDescriptorUri);
						IIndicatorDescriptor idescr = ServiceInterface.GetIndicatorDescriptor(IOBIdescr);

						for (CSVRecord record : parser) {
							String areaid = record.get(0);
							String value = record.get(1);
							IIndicator ind = ServiceInterface.CreateIndicator(aa, idescr, areaid, Float.valueOf(value));
							System.out.println(
									"areaid=" + areaid + ", value=" + value + ", new-indicator-uri=" + ind.getURI());
						}

					} finally {
						parser.close();
						reader.close();
					}

					// File file = new File("tmp.csv");
					// item.write(file);
					// processUploadedFile(item);
				}
			}
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// response.setStatus(200);

	}
}
