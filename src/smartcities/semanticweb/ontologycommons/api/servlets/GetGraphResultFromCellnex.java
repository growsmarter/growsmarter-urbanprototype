package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;

/**
 * Servlet implementation class GetGraphResultFromCellnex
 */
@WebServlet("/GetGraphResultFromCellnex")
public class GetGraphResultFromCellnex extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetGraphResultFromCellnex() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// String params = request.getParameter("params");
		// String params = request.getParameter("params");
		String postBody = null;
		if (request.getParameter("params") == null) {
			postBody = WebInterfaceUtils.readPostMessage(request);
		} else {
			postBody = request.getParameter("params");
		}

		String json = WebInterface.GetGraphResultFromCellnex(postBody);

		if (json != null) {
			response.setContentType("application/json");
//			ServletOutputStream sos = response.getOutputStream();
//			sos.println(json);
			PrintWriter pw = response.getWriter();
			pw.println(json);
			
			
		} else {
			response.getWriter().print("ERROR on GetGraphResult");
			// HttpServletResponse.SC_INTERNAL_SERVER_ERROR = 500
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

}
