package smartcities.semanticweb.ontologycommons.api.servlets;

import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

/**
 * Application Lifecycle Listener implementation class AppServletContextListener
 * 
 */
@WebListener
public class AppServletContextListener implements ServletContextListener {
	// main ontology
	final static String ONTFILE = "Ontologies/SUM_V0/SUM/BCNEcology.ttl";
	// file or directory containing files with the geographic information
	final static String INSTFILES = "Ontologies/SUM_V0/SUM/BCNEcologyINST.ttl";

	final static String INDFILES = "Ontologies/SUM_V0/SUM/BCNIndicatorInstances.ttl";
	final static String ONTURI = "http://www.ibm.com/BCNEcology";
	// policy file for the imported ontologies (prefixes and locations)
	final static String POLICYFILE = "policy.rdf";
	final static String NAMEBROKER = "UrbanModel";
	// final static String INFERENCEFILE = "inference.sparql";
	// policy file for the imported ontologies (prefixes and locations)
	final static String MODELMAPPATH = "Maps";

	final static String SEARCHPATH = "SearchIndex";

	public static String applicationPath = null;

	/**
	 * Default constructor.
	 */
	public AppServletContextListener() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("ServletContextListener started");

		applicationPath = arg0.getServletContext().getRealPath("/"); // localization
																		// of
																		// resources
		// String resourcesPath = applicationPath + "WEB-INF/resources/";
		// System.out.println("Resources path:" + resourcesPath);
		// System.out.println("TDB:" + resourcesPath + "/TDB");
		// System.out.println("Path policy file:" +
		// arg0.getServletContext().getRealPath(POLICYFILE));
		// System.out.println("Path model maps files:" + resourcesPath +
		// MODELMAPPATH);
		try {

			String configFileEnvName = "CONFIG_FILE_URBANPROJECT";
			String configFileEnvNameMC = "CONFIG_FILE_URBANPROJECT_MC";
			Map<String, String> env = System.getenv();
			String configfile = null;

			if (env.get(configFileEnvNameMC) != null) {
				// Read MC ontology Resources
				configfile = env.get(configFileEnvNameMC);
			} else {
				if (env.get(configFileEnvName) != null) {
					// Read ontology Resources
					configfile = env.get(configFileEnvName);
				} else {
					// Read local ontology resources
					configfile = "/home/smendoza/eclipse-workspace/UrbanPrototypeResources/config.properties";
				}
			}

			// PATCH para usar maven:
			if (configfile == null) {
				// configfile =
				// "/home/smendoza/case-smart-cities/UrbanPrototypeResources/config.properties";
				configfile = "/home/smendoza/eclipse-workspace/UrbanPrototypeResources/config.properties";
				System.out.println(configFileEnvName + "->null (default: " + configfile + ")");
			} else {
				System.out.println(configFileEnvName + "->" + configfile);
			}

			// services.ServiceInterface.CreateOntBroker(NAMEBROKER, ONTURI,
			// ONTFILE, POLICYFILE, resourcesPath,
			// INSTFILES, INDFILES, MODELMAPPATH, SEARCHPATH);

			ServiceInterface.CreateOntBroker(configfile);
			System.out.println("Created OntoBroker");
			System.out.println("App ready");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		try {
			smartcities.semanticweb.ontologycommons.services.ServiceInterface.Finalize();
		} catch (Exception e) {
			System.out.println("contextDestroyed error:" + e.getMessage());
		}

		System.out.println("ServletContextListener destroyed");
	}

}