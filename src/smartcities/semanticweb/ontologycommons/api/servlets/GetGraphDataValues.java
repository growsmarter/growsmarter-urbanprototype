package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;

/**
 * Servlet implementation class GetGraphDataValues
 */
@WebServlet("/GetGraphDataValues")
public class GetGraphDataValues extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetGraphDataValues() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		// TODO Auto-generated method stub

		String postBody = null;
		if (request.getParameter("params") != null) {
			postBody = request.getParameter("params");
		} else {
			postBody = WebInterfaceUtils.readPostMessage(request);
		}

		JSONObject myParamsJSON;
		myParamsJSON = new JSONObject(postBody);
		String json = WebInterface.GetGraphDataValues(myParamsJSON);
		response.setContentType("application/json");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(json);

	}

}
