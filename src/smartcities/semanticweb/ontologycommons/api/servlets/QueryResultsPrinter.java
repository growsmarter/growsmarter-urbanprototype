package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;

public class QueryResultsPrinter {
	private static String htmlSelectProperties(int nodeId, Set<IOntoProperty> propertyList) {
		StringBuffer sb = new StringBuffer();
		sb.append("<select onchange=\"htmlData(" + nodeId + ",this.value, false)\">");
		for (IOntoProperty p : propertyList) {
			String label = p.getLabel();
			if (label == null || label == "")
				label = p.getURI();
			sb.append("<option value=\"" + p.getURI() + "\">" + label + "</option>");
		}
		sb.append("</select>");
		return sb.toString();
	}

	private static String htmlData(int id, String uriProperty, boolean inference, int queryId) throws IOException {
		StringBuffer sb = new StringBuffer();
		int subgraphId = smartcities.semanticweb.ontologycommons.services.ServiceInterface.getSubgraphId(id, queryId);
		int rows = smartcities.semanticweb.ontologycommons.services.ServiceInterface.getRowsNumber(subgraphId, queryId);
		List<Set<String>> values = smartcities.semanticweb.ontologycommons.services.ServiceInterface.GetGraphDataValues(id, uriProperty, 0, rows - 1, queryId);
		for (Set<String> v : values) {
			sb.append("<tr>");
			sb.append("<td>");
			for (String item : v) {
				sb.append(item);
				sb.append(" ");
			}
			sb.append("</td>");
			sb.append("</tr>");
		}
		return sb.toString();
	}

	public static String htmlTable(IOntoBase node, int queryId) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("<table>");
		sb.append("<tr>");
		sb.append("<th>");
		String label = node.getLabel();
		if (label == null || label == "")
			label = node.getURI();
		sb.append(label);
		sb.append("</th>");
		sb.append("</tr>");
		sb.append("<tr>");
		Set<IOntoProperty> properties = smartcities.semanticweb.ontologycommons.services.ServiceInterface.GetDataProperties(node);
		sb.append(htmlSelectProperties(node.getId(), properties));
		sb.append("</tr>");
		sb.append("</table>");

		// TO TEST
		for (IOntoProperty p : properties)
			System.out.println(htmlData(node.getId(), p.getURI(), false, queryId));
		return sb.toString();
	}

	public static List<String> GetGraphResults(List<IOntoLink> graph, int queryId) {
		List<String> tables = new ArrayList<String>();

		try {
			for (IOntoBase node : smartcities.semanticweb.ontologycommons.services.ServiceInterface.GetGraphNodesToShow(graph)) {
				StringBuffer sb = new StringBuffer();
				sb.append(htmlTable(node, queryId));
				tables.add(sb.toString());
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return tables;
	}

	// public static String GetGraphResultsJSON(List<IOntoLink> graph) {
	//
	// JSONObject jnodeProps = new JSONObject();
	//
	// try {
	//
	// Set<IOntoBase> nodes = new HashSet<IOntoBase>();
	// for (IOntoLink link : graph) {
	// // TODO: eliminar repetidos según id
	// nodes.add(link.getDomain());
	// nodes.add(link.getRange());
	// }
	// List<IOntoBase> nodeList = new ArrayList<IOntoBase>(nodes);
	// nodeList = services.ServiceInterface.OrderConceptsById(nodeList);
	// int lastId = -1;
	//
	// for (IOntoBase node : nodeList) {
	// if (node.getId() != lastId) {
	// JSONObject jprops = new JSONObject();
	// Set<IOntoProperty> properties =
	// services.ServiceInterface.GetDataProperties(node);
	// for (IOntoProperty p : properties) {
	// // obtiene valores de la propiedad p, del nodo node
	// List<ArrayList<String>> values =
	// services.ServiceInterface.GetGraphDataValues(node.getId(),
	// p.getURI(), false);
	// JSONArray jvalues = new JSONArray();
	// int vals = 0;
	// for (ArrayList<String> v : values) {
	// //TO TRUNCATE LONG RESULTS
	// if (vals++ >= 10)
	// break;
	// //END TO TRUNCATE LONG RESULTS
	//
	// // concateno values de una fila
	// StringBuffer sb = new StringBuffer();
	// for (String item : v) {
	// // concateno values de una fila
	// sb.append(item);
	// sb.append(" ");
	// }
	// jvalues.put(sb.toString());
	//
	// }
	// jprops.put(p.getLabel(), jvalues);
	// }
	// lastId = node.getId();
	// jnodeProps.put(node.getURI(), jprops);
	// }
	// }
	//
	// } catch (Exception e) {
	// System.out.println(e.getMessage());
	// }
	// return jnodeProps.toString();
	// }

}
