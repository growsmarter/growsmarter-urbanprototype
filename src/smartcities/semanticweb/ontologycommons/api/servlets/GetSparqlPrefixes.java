package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import smartcities.semanticweb.ontologycommons.api.WebInterface;

/**
 * Servlet implementation class GetSparqlPrefixes
 */
@WebServlet("/GetSparqlPrefixes")
public class GetSparqlPrefixes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetSparqlPrefixes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String prefixes = WebInterface.PrefixBlock(); 
 		response.setContentType("application/text");
 		ServletOutputStream sos = response.getOutputStream();
 		sos.println(prefixes);
	}

}
