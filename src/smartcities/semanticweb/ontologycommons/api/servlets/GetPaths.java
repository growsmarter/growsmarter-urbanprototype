package smartcities.semanticweb.ontologycommons.api.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterface;
import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;

/**
 * Servlet implementation class GetPaths
 */
@WebServlet("/GetPaths")
public class GetPaths extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetPaths() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		// TODO Auto-generated method stub
		String postBody = null;
		if (request.getParameter("params") == null) {
			postBody = WebInterfaceUtils.readPostMessage(request);
		} else {
			postBody = request.getParameter("params");
		}
		
		String json = WebInterface.getPaths(postBody);
		response.setContentType("application/json");
		ServletOutputStream sos = response.getOutputStream();
		sos.println(json);
	}

}
