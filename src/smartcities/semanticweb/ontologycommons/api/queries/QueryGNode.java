package smartcities.semanticweb.ontologycommons.api.queries;

import java.io.IOException;
import java.util.Iterator;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class QueryGNode {

	protected String nodeID = null;
	protected Integer id = null;
	protected String label = null;
	protected String uri = null;
	protected String type = null;
	protected String attributes = null;
	protected String wkt = null; // just area will have it
	IOntoBase iobNode = null;

	public QueryGNode() {

	}

	public QueryGNode(JSONObject jo) {
		Iterator<String> x = jo.keys();
		while (x.hasNext()) {
			String element = x.next();
		}
		// this.nodeID = jo.getString("nodeID");
		this.id = jo.getInt("id");
		this.label = jo.getString("label");
		this.uri = jo.getString("uri");
		if (jo.has("type") && jo.has("wkt")) {
			this.type = jo.getString("type");
			this.wkt = jo.getString("wkt");
		}
	}

	public boolean isArea() {
		// if (this.type != null && this.type == "AREA") {
		if (this.wkt != null) {
			return true;
		} else {
			return false;
		}
	}

	public String getWkt() {
		return wkt;
	}

	public void setWkt(String wkt) {
		this.wkt = wkt;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNodeID() {
		return this.nodeID;
	};

	public String getLabel() {
		return this.label;
	};

	public String getUri() {
		return this.uri;
	};

	public String getType() {
		return this.type;
	}

	public static IOntoBase generateIOBase(JSONObject node) {
		IOntoBase nodeInstance = null;
		QueryGNode x = new QueryGNode(node);
		try {
			nodeInstance = ServiceInterface.GetConcept(x.getUri());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return nodeInstance;
	}

}
