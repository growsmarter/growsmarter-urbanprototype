package smartcities.semanticweb.ontologycommons.api.queries;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONObject;

public class QueryGraph {
	protected ArrayList<QueryGSubgraph> subgraphs;
	protected ArrayList<IOntoLink> QGEdges;
	protected ArrayList<IOntoBase> QGNodes;

	protected int queryID; // id returned by getGraphResult() to identify its
							// corresponding query while executing the
							// getgraphdataresults, etc.

	public QueryGraph(String queryGraph) {

		JSONObject jsonQueryGraph;
		try {
			jsonQueryGraph = new JSONObject(queryGraph);

			// JSONArray jsonLinks = jsonQueryGraph.getJSONArray("links");
			// JSONArray jsonNodesSueltos =
			// jsonQueryGraph.getJSONArray("nodesSueltos");
			JSONArray edges = jsonQueryGraph.getJSONArray("edgesDS");
			JSONArray nodes = jsonQueryGraph.getJSONArray("nodesDS");

			// TODO: 1.1)Anyadir la creacion de areas aqui
			for (int i = 0; i < nodes.length(); ++i) {
				JSONObject node = nodes.getJSONObject(i);
				QueryGNode x = new QueryGNode(node);
				if (x.isArea()) {
					// Create the area at the ontology
					String wkt = x.getWkt();
					String label = x.getLabel();
					IOntoArea areaConcept = ServiceInterface.CreateArea(IOntoArea.OntArea.POLYGON, wkt, label);
					x.setUri(areaConcept.getURI());
				}
			}

			// TODO: 2.2)GetGraphResults() de links
			for (int i = 0; i < edges.length(); ++i) {
				JSONObject edge = edges.getJSONObject(i);
				IOntoLink x = QueryGLink.generateIOLink(edge);
				this.QGEdges.add(x);
				// from: "1"
				// id: "36d3919a-f0a3-8b5b-4d93-508efd6fe684"
				// label: "has door"
				// prop: "http://www.ibm.com/ICP/Scribe/CoreV2#buildingHasDoor"
				// to: 2
			}

			// TODO: 2.1)GetGraphResults() de nodos_sueltos
			for (int i = 0; i < nodes.length(); ++i) {
				JSONObject node = nodes.getJSONObject(i);
				IOntoBase x = QueryGNode.generateIOBase(node);
				this.QGNodes.add(x);

				// from: "1"
				// id: "36d3919a-f0a3-8b5b-4d93-508efd6fe684"
				// label: "has door"
				// prop: "http://www.ibm.com/ICP/Scribe/CoreV2#buildingHasDoor"
				// to: 2
			}

			// TODO: 2.3)GetGraphResults() de nodos_sueltos&links

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ArrayList<QueryGSubgraph> getGraph() {
		return subgraphs;
	}

	public void setGraph(ArrayList<QueryGSubgraph> graph) {
		this.subgraphs = graph;
	}
}
