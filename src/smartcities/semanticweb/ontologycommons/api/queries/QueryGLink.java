package smartcities.semanticweb.ontologycommons.api.queries;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.api.WebInterfaceUtils;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class QueryGLink {
	protected String linkId = null;
	protected QueryGNode domain = null;
	protected QueryGNode range = null;
	protected QueryGProperty property = null;
	IOntoLink ioLink = null;

	public QueryGLink(QueryGNode inputDomain, QueryGNode inputRange, QueryGProperty inputProperty) {
		this.domain = inputDomain;
		this.range = inputRange;
		this.property = inputProperty;
	}

	public QueryGLink(JSONObject inputDomain, JSONObject inputRange, JSONObject inputProperty) {
		this.domain = new QueryGNode(inputDomain);
		this.range = new QueryGNode(inputRange);
		this.property = new QueryGProperty(inputProperty);
	}

	public static IOntoLink generateIOLink(JSONObject edge) {
		// from: "1"
		// id: "36d3919a-f0a3-8b5b-4d93-508efd6fe684"
		// label: "has door"
		// prop: "http://www.ibm.com/ICP/Scribe/CoreV2#buildingHasDoor"
		// to: 2
		IOntoLink ioEdgeLink = null;
		String label = edge.getString("label");
		String edgeId = edge.getString("id");
		String domainUri = edge.getString("from");
		String rangeUri = edge.getString("to");
		String propUri = edge.getString("prop");

		try {
			IOntoBase domConcept = ServiceInterface.GetConcept(domainUri);
			IOntoBase ranConcept = ServiceInterface.GetConcept(rangeUri);
			IOntoBase relConcept = ServiceInterface.GetConcept(propUri);
			domConcept.setId(Integer.valueOf(QueryGraphs.QueryNodeID++));
			ranConcept.setId(Integer.valueOf(QueryGraphs.QueryNodeID++));

			// check type (property/function)
			if (relConcept.isProperty()) {
				// if property
				IOntoProperty propConcept;
				propConcept = ServiceInterface.GetProperty(relConcept);
				ioEdgeLink = ServiceInterface.CreateLink(domConcept, propConcept, ranConcept);
			} else if (relConcept.isFunction()) {
				// relationparams introduced by user, when choosing a function
				JSONArray relParams = edge.getJSONArray("params");
				ArrayList<String> paramsArray = WebInterfaceUtils.jsonArrayToArrayList(relParams);
				IOntoFunction funConcept = ServiceInterface.GetFunction(relConcept, paramsArray);

				if (domConcept.getType() != null && domConcept.getType().equals("AREA")) {
					IOntoArea area = ServiceInterface.GetArea(domConcept);
					area.setId(QueryGraphs.QueryNodeID++);
					ioEdgeLink = ServiceInterface.CreateGeospatialLink(area, funConcept.getFunctionType(), paramsArray,
							ranConcept);
				} else if (ranConcept.getType() != null && ranConcept.getType().equals("AREA")) {
					IOntoArea area = ServiceInterface.GetArea(ranConcept);
					area.setId(QueryGraphs.QueryNodeID++);
					ioEdgeLink = ServiceInterface.CreateGeospatialLink(domConcept, funConcept.getFunctionType(),
							paramsArray, area);
				} else {
					System.out.println("Function without areas");
					ioEdgeLink = ServiceInterface.CreateLink(domConcept, funConcept, ranConcept);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ioEdgeLink;

	}

}
