package smartcities.semanticweb.ontologycommons.api.queries;

import java.util.ArrayList;

import org.json.JSONObject;

public class QueryGraphs {
	public static ArrayList<QueryGraph> graphs;
	public static int QueryNodeID = 1;
	public static int QueryGraphID = 1;
	public static int QuerySubgraphID = 1;

	// var nodeData = {
	// id : $scope.nodecounter,
	// label : addlabel,
	// uri : adduri,
	// type : addtype,
	// };
	/**
	 * Function that must add a new Node in the graph&subgraph specified at the
	 * input json
	 * 
	 * @param jsonGN
	 */
	public static void addNode(JSONObject jsonGN) {
		int graphID = jsonGN.getInt("graphID");
		int subGraphID = jsonGN.getInt("subgraphID");

		// gotta generate an ID for the JSON Object, before
		jsonGN.put("id", QueryGraphs.QueryNodeID++);
		QueryGNode gN = new QueryGNode(jsonGN);

		QueryGraphs.graphs.get(graphID).subgraphs.get(subGraphID).nodeSueltos.add(gN);
	}

	public static void addLink(JSONObject jsonGL) {
		int graphID = jsonGL.getInt("graphID");
		int subGraphID = jsonGL.getInt("subgraphID");
//		QueryGLink gL = new QueryGLink(jsonGL);

//		QueryGraphs.graphs.get(graphID).subgraphs.get(subGraphID).links.add(gL);
	}

	public static String getNewNodeIDToJSON() {
		JSONObject x = new JSONObject();
		x.put("nodeID", QueryNodeID);
		return x.toString();
	}

}
