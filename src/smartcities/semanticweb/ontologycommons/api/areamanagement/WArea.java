package smartcities.semanticweb.ontologycommons.api.areamanagement;

import org.json.JSONObject;

public class WArea {
	protected String wkt;
	protected String label;

	public WArea(JSONObject json) {
		this.wkt = json.getString("wkt"); // POLYGON((236760.6909516373
											// 5068581.795028944,...))
		this.label = json.getString("label");
	}

	public String getWkt() {
		return wkt;
	}

	public void setWkt(String wkt) {
		this.wkt = wkt;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
