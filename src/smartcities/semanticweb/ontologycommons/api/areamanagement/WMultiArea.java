package smartcities.semanticweb.ontologycommons.api.areamanagement;

import java.util.ArrayList;

import org.json.JSONArray;

public class WMultiArea {
	protected ArrayList<WArea> w = new ArrayList<>();

	public WMultiArea(ArrayList<WArea> inputw) {
		this.w = inputw;
	}

	public WMultiArea(JSONArray areasJSON) {
		for (int i = 0; i < areasJSON.length(); ++i) {
			WArea x = new WArea(areasJSON.getJSONObject(i));
			this.w.add(x);
		}
	}

	public String genMultipolygonStr() {
		String mWkt = "";
		for (int i = 0; i < this.w.size(); ++i) {
			// Remove POLYGON part from the wkt String
			String waWkt = this.w.get(i).getWkt();
			int iParenthesis = waWkt.indexOf("(", 0);
			waWkt = waWkt.substring(iParenthesis);

			if (i > 0) {
				mWkt.concat(",");
			}
			mWkt.concat(waWkt);
		}

		return "MULTIPOLYGON (" + mWkt + ")";
	}

}
