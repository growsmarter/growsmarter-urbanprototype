package smartcities.semanticweb.ontologycommons.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import se.resources.MyStemmer;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicatorDescriptor;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoArea;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoFunction;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoLink;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoProperty;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoRelationship;
import smartcities.semanticweb.ontologycommons.ontoconcepts.ISearchResult;
import smartcities.semanticweb.ontologycommons.ontoconcepts.IIndicator.ANCHOR_AREAS;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

/**
 * Common functions around the WebInterface
 * 
 * @author smendoza
 * 
 */
public class WebInterfaceUtils {

	// Sort ISearchResult List by its similatity
	public static void iSRSortBySimilarity(List<ISearchResult> ISRList) {
		Collections.sort(ISRList, new Comparator<ISearchResult>() {
			public int compare(ISearchResult a, ISearchResult b) {
				// Write your logic here.
				if (a.getSimilarity() == b.getSimilarity()) {
					return 0;
				} else if (a.getSimilarity() < b.getSimilarity()) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		// return ISRList;
	}

	public static JSONObject toJSONIOntoProperty(IOntoProperty iop) {
		JSONObject jsonOutput = new JSONObject();
		jsonOutput.put("uri", iop.getURI());
		jsonOutput.put("id", iop.getId());
		jsonOutput.put("uri", iop.getURI());
		jsonOutput.put("label", iop.getLabel());
		jsonOutput.put("type", iop.getType().toString());
		return jsonOutput;
	}

	public static String readPostMessage(HttpServletRequest request) {
		BufferedReader reader;
		try {
			reader = request.getReader();
			StringBuilder builder = new StringBuilder();
			String aux = "";
			while ((aux = reader.readLine()) != null) {
				builder.append(aux);
			}
			return builder.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static JSONArray recalculusRecursive(ArrayList<String> queryTokenized) {
		JSONArray graphNodesURI = new JSONArray();
		String query = StringUtils.join(queryTokenized, " ");
		// Recalculus:
		List<ISearchResult> resultsUntrunked;
		try {
			resultsUntrunked = ServiceInterface.SearchConcepts("5", query, true);
			// Sort by similarity
			WebInterfaceUtils.iSRSortBySimilarity(resultsUntrunked);
			// Trunk
			List<ISearchResult> resultsTrunked = new ArrayList<>();
			int trunkingTo = 50;
			if (resultsUntrunked.size() > trunkingTo) {
				resultsTrunked = resultsUntrunked.subList(0, trunkingTo);
			} else {
				resultsTrunked = resultsUntrunked;
			}

			ISearchResult res = null;
			if (resultsTrunked.size() > 0) {
				res = resultsTrunked.get(0);
			} else {
				// BASE CASE:
				return new JSONArray();
			}

			// Data from each result
			String uri = res.getOntoConcept().getURI();
			int maxNodes = (int) (queryTokenized.size() * 0.9);
			if ((res.getSimilarity() > 2) && (graphNodesURI.length() <= maxNodes)) {

				String reason = null;
				Map<String, Map<String, String>> snippets = ServiceInterface.GetSnippet(uri, query, "<label>",
						"</label>");

				JSONObject jSnippet = WebInterfaceUtils.snippetToJSON(snippets);

				if (jSnippet.has("label")) {
					Iterator<String> keysToCopyIterator = jSnippet.getJSONObject("label").keys();
					while (keysToCopyIterator.hasNext()) {
						String key = (String) keysToCopyIterator.next();
						reason = jSnippet.getJSONObject("label").getString(key);
					}
				} else if (jSnippet.has("related")) {
					Iterator<String> keysToCopyIterator = jSnippet.getJSONObject("related").keys();
					while (keysToCopyIterator.hasNext()) {
						String key = (String) keysToCopyIterator.next();
						reason = jSnippet.getJSONObject("related").getString(key);
					}
				} else if (jSnippet.has("property")) {
					Iterator<String> keysToCopyIterator = jSnippet.getJSONObject("property").keys();
					while (keysToCopyIterator.hasNext()) {
						String key = (String) keysToCopyIterator.next();
						reason = jSnippet.getJSONObject("property").getString(key);
					}
				} else {
					reason = "";
				}

				String opener = "<label>";
				String closer = "</label>";
				reason = reason.trim().toLowerCase();

				if (reason.length() == 0) {
					// If nothing to be replaced, stop adding nodes!
				}
				ArrayList<String> toRemoveFromQueryTokenized = new ArrayList<>();
				Pattern patternBetween = Pattern.compile("\\" + opener + "(.*?)\\" + closer);
				Matcher matcher = patternBetween.matcher(reason);
				while (matcher.find()) {
					String toRemoveFromQuery = matcher.group();
					toRemoveFromQuery = toRemoveFromQuery.replace(opener, "").replace(closer, "");
					toRemoveFromQueryTokenized.add(toRemoveFromQuery);
				}

				boolean addNode = WebInterfaceUtils.removeStemming(queryTokenized, toRemoveFromQueryTokenized);
				JSONArray AA = WebInterfaceUtils.recalculusRecursive(queryTokenized);
				if (addNode) {
					graphNodesURI.put(uri);
					return AA.put(graphNodesURI);
				} else {
					// BASE CASE: No node to add
					return new JSONArray();
				}

			} else {
				// BASE CASE: Not enough query
				return new JSONArray();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return graphNodesURI;
	}

	/**
	 * Removes from the query the words with the same origin
	 * 
	 * @param queryTokenized
	 * @param reasonTokenized
	 * @return
	 */
	public static boolean removeStemming(ArrayList<String> queryTokenized, ArrayList<String> reasonTokenized) {
		MyStemmer stemming = new MyStemmer();
		Iterator<String> j = queryTokenized.iterator();
		Iterator<String> i = reasonTokenized.iterator();
		int removesMade = 0;
		int removesExpected = reasonTokenized.size();
		while (i.hasNext()) {
			String reason = i.next();
			String reasonStemmed = stemming.process(reason);
			while (j.hasNext()) {
				String query = j.next();
				String queryStemmed = stemming.process(query);
				if (stemming.process(queryStemmed).equals(reasonStemmed)) {
					i.remove();
					j.remove();
					removesMade++;
					break; // pass to the next reason
				} else {
					// do nothing
				}
			}
		}

		if (removesMade == removesExpected) {
			return true;
		} else {
			return false;
		}

		// for (int i = 0; i < queryTokenized.size();) {
		// String query = queryTokenized.get(i);
		// for (int j = 0; j < reasonTokenized.size();) {
		// String reason = reasonTokenized.get(j);
		// // Si tienen la misma raiz, la elimino
		// String reasonStemmed = stemming.process(reason);
		// String queryStemmed = stemming.process(query);
		// if (stemming.process(queryStemmed).equals(reasonStemmed)) {
		// queryTokenized.remove(i);
		// reasonTokenized.remove(j);
		// } else {
		// // do nothing
		// i++;
		// j++;
		// }
		// }
		// }

	}

	public static String getTheLabel(IOntoBase concept) {
		String l = concept.getLabel();
		String csvColLabel = new String();
		// strange label cases
		if (l == null || l.isEmpty() || l.trim().isEmpty()) {
			String[] splitRslt = concept.getURI().split("#");
			if (splitRslt.length > 1) {
				csvColLabel = splitRslt[1];
			}
		} else {
			csvColLabel = l;
		}

		return csvColLabel;
	}

	public static String genRelationshipLabel(IOntoRelationship rel) {
		String l = rel.getLabel();
		String csvColLabel = new String();
		// strange label cases
		if (l == null || l.isEmpty() || l.trim().isEmpty()) {
			if (rel.getURI().contains("#")) {
				String[] splitRslt = rel.getURI().split("#");
				if (splitRslt.length > 1) {
					csvColLabel = splitRslt[1];
				}
			} else {
				String[] splitRslt = rel.getURI().split("/");
				if (splitRslt.length > 1) {
					csvColLabel = splitRslt[splitRslt.length - 1];
				}
			}
		} else {
			csvColLabel = l;
		}

		return csvColLabel;
	}

	public static String genEdgeLabel(IOntoRelationship rel) {
		String labelDefault = rel.getLabel();
		String label = new String();
		// strange label cases
		if (labelDefault == null || labelDefault.isEmpty() || labelDefault.trim().isEmpty()) {
			if (rel.getURI().contains("#")) {
				String[] splitRslt = rel.getURI().split("#");
				if (splitRslt.length > 1) {
					label = splitRslt[1];
				}
			} else {
				String[] splitRslt = rel.getURI().split("/");
				if (splitRslt.length > 1) {
					label = splitRslt[splitRslt.length - 1];
				}
			}
		} else {
			String[] splitBarra = rel.getURI().split("/");
			String[] splitHashtag = rel.getURI().split("#");

			if (splitHashtag.length > 1) {
				label = labelDefault + " (#" + splitHashtag[1] + ")";
			} else if (splitBarra.length > 1) {
				label = labelDefault + " (/" + splitBarra[splitBarra.length - 1] + ")";
			} else {
				label = labelDefault;
			}
		}

		return label;
	}

	/**
	 * Converts the area objecto into a json
	 * 
	 * @param a
	 *            area instance
	 * @return json object with the interesting area information
	 */
	public static JSONObject convertIOntoAreaToJSONObject(IOntoArea a) {
		JSONObject jarea = new JSONObject();
		Random randomGenerator = new Random();
		int areaInstanceId = randomGenerator.nextInt(999999);
		areaInstanceId = Math.abs(areaInstanceId);
		int aid = a.getId();
		jarea.put("id", areaInstanceId);
		jarea.put("label", a.getLabel());
		jarea.put("wkt", a.getWKT());
		jarea.put("areatype", a.getAreaType().toString());
		jarea.put("uri", a.getURI());
		jarea.put("type", a.getType().toString());
		jarea.put("class", a.getClass().toString());

		return jarea;
	}

	public static String uriToAnchorArea(String uri) {

		switch (uri) {
		case IIndicator.ROAD_SEGMENT:
			return "ROAD_SEGMENT";
		case IIndicator.SUPERBLOCK:
			return "SUPERBLOCK";
		case IIndicator.LANDLOT:
			return "LANDLOT";
		case IIndicator.MESH:
			return "MESH";
		case IIndicator.CITY:
			return "CITY";
		case IIndicator.NEIGHBORHOOD:
			return "NEIGHBORHOOD";
		}
		return null;
	}

	public static String convertSubgraphsToJSON(Integer idGraph) {

		JSONArray output = new JSONArray();
		List<List<IOntoLink>> subgraphs = smartcities.semanticweb.ontologycommons.services.ServiceInterface
				.getSubgraphs(idGraph);

		for (List<IOntoLink> subgraph : subgraphs) {
			JSONArray subgraphJSON = new JSONArray();
			for (IOntoLink link : subgraph) {
				JSONObject linkJSON = WebInterfaceUtils.convertLinkToJSON(link);
				subgraphJSON.put(linkJSON);
			}
			output.put(subgraphJSON);
		}

		return output.toString();
	}

	/**
	 * Extracts data from an Indicator returning a JSONObject
	 * 
	 * @param ind
	 *            indicator that you want to extract data from
	 * @return JSONObject which contains the indicator information
	 */
	public static JSONObject convertIIndicatorToJSON(IIndicator ind) {

		JSONObject jInd = new JSONObject();
		IIndicatorDescriptor idesc = ind.getIndDescriptor();

		jInd.put("label", ind.getIndDescriptor().getLabel());
		jInd.put("value", ind.getIndValue());
		// get the label of the type of area associated to the concept (Road
		// Segment, Building,...)
		Set<IOntoBase> x = ind.getIndArea().getClassConcept(false);
		for (IOntoBase area : x) {
			jInd.put("areaid", area.getId());
			jInd.put("areaidontology", area.getOntologyIdentifier());
			jInd.put("areaclass", area.getLabel());
			String anchorArea = WebInterfaceUtils.uriToAnchorArea(area.getURI());
			jInd.put("areaanchor", anchorArea);
		}

		jInd.put("uri", ind.getURI());
		jInd.put("comment", idesc.getComment());
		jInd.put("categoryuri", idesc.getIndicatorType().getURI());
		jInd.put("categorylabel", idesc.getLabel());
		jInd.put("categoryguiordertag", idesc.getIndicatorType().getGUIOrderTag());
		IOntoBase areaIOB = ind.getIndArea();
		jInd.put("area", areaIOB.getLabel());
		try {
			if (areaIOB.isAreaInstance()) {
				IOntoArea area = smartcities.semanticweb.ontologycommons.services.ServiceInterface.GetArea(areaIOB);
				jInd.put("areawkt", area.getWKT());
			} else {
				jInd.put("areawkt", false); // null
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONArray minVals = new JSONArray();
		for (String min : idesc.getMinValues()) {
			minVals.put(min);
		}
		JSONArray bVals = new JSONArray();
		for (String best : idesc.getBestValues()) {
			bVals.put(best);
		}
		JSONArray entAss = new JSONArray();
		for (IOntoBase assoc : idesc.getEntitiesAssoc()) {
			entAss.put(assoc.getLabel());
		}
		jInd.put("dminvals", minVals);
		jInd.put("dbestvals", bVals);
		jInd.put("dentass", entAss);
		return jInd;
	}

	public static JSONObject convertLinkToJSON(IOntoLink l) {
		IOntoBase domain = l.getDomain();
		IOntoBase range = l.getRange();
		IOntoRelationship rel = l.getRelationship();

		JSONObject jlinks = new JSONObject();
		jlinks.put("domainuri", domain.getURI());
		jlinks.put("domainlabel", WebInterfaceUtils.getTheLabel(domain));
		jlinks.put("domainresource", domain.getResource().toString());
		jlinks.put("domaintype", domain.getType().toString());
		jlinks.put("rangeuri", range.getURI());
		jlinks.put("rangelabel", WebInterfaceUtils.getTheLabel(range));
		jlinks.put("rangeresource", range.getResource().toString());
		jlinks.put("rangetype", range.getType().toString());
		jlinks.put("propertyuri", rel.getURI());
		jlinks.put("propertylabel", WebInterfaceUtils.genRelationshipLabel(rel));
		jlinks.put("propertylabelextended", WebInterfaceUtils.genEdgeLabel(rel));
		jlinks.put("propertyresource", rel.getResource().toString());
		jlinks.put("propertytype", rel.getType().toString());

		if (rel.isFunction()) {
			IOntoFunction func = (IOntoFunction) l.getRelationship();
			int nparams = func.getParamNumber();
			jlinks.put("relationshippnumbers", nparams);
			System.out.println("func.getParamNumber(" + func.getURI() + ")=" + func.getParamNumber());

		} else {
			// if not a relationship function, the param will be 0
			jlinks.put("relationshippnumbers", String.valueOf(0));
		}
		return jlinks;
	}

	/**
	 * Generates a key to identify the links with a unique key. It concatenates
	 * domain, relation and range uris with a random number has to be
	 * introduced. The random number is necessary because there can be more than
	 * one relation with the same domain and range.
	 * 
	 * @param l
	 *            the link we want to identify. Contains a domain, relation and
	 *            range
	 * @return returns a string unique key for the link
	 */
	public static String linkKeyGenerator(IOntoLink l) {
		String uuid[] = UUID.randomUUID().toString().split("-");
		String key = uuid[0] + l.getDomain().getURI() + l.getRelationship().getURI() + l.getRange().getURI();
		return key;
	}

	public static ArrayList<String> jsonArrayToArrayList(JSONArray input) {
		ArrayList<String> output = new ArrayList<String>();
		for (int i = 0; i < input.length(); ++i) {
			output.add((String) input.get(i));
		}

		return output;
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	public static boolean isNumeric(String str) {
		return str.matches("-?\\d+(\\.\\d+)?"); // match a number with optional
												// '-' and decimal.
	}

	public enum FilterTypes {
		Contains, Range
	}

	/**
	 * Rows to Filter from a specific node
	 * 
	 * @param rowsToFilterFromNodeId
	 * @return Collection of indexes of invalid rows to remove from the
	 */
	public static ArrayList<Integer> getInvalidRows(List<Set<String>> rowsToFilterFromNodeId, String filterType,
			JSONObject filterParams) {
		ArrayList<Integer> invalidRows = new ArrayList<Integer>();
		FilterTypes ft = FilterTypes.valueOf(filterType);

		// apply filter to the value
		if (filterParams != null) {
			if (ft == FilterTypes.Contains) {
				if (filterParams.has("text")) {
					String c = filterParams.getString("text");
					if (c.isEmpty()) {
						// nothing to filter
					} else {
						for (int i = 0; i < rowsToFilterFromNodeId.size(); ++i) {
							boolean validRow = false;
							for (String property : rowsToFilterFromNodeId.get(i)) {
								validRow = validRow || property.contains(c);
							}
							if (!validRow) {
								invalidRows.add(i);
							}
						}
					}
				}
			} else if (ft == FilterTypes.Range) {
				if (filterParams.has("max") && filterParams.has("min")) {
					if (filterParams.getString("max").isEmpty() || filterParams.getString("min").isEmpty()) {
						// nothing to filter
					} else {
						int max = filterParams.getInt("max");
						int min = filterParams.getInt("min");
						for (int i = 0; i < rowsToFilterFromNodeId.size(); ++i) {
							for (String property : rowsToFilterFromNodeId.get(i)) {
								if (min > Float.valueOf(property) || max < Float.valueOf(property)) {
									invalidRows.add(i);
								}
							}
						}
					}
				}
			}
		}
		return invalidRows;
	}

	public static boolean isParamMissing(String[] params, JSONObject jsonReceived) {
		for (String param : params) {
			if (!jsonReceived.has(param)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return rows containing the properties prop from the node nodeId, that
	 * passes the specified filters
	 * 
	 * @param nodeId
	 * @param prop
	 * @param filters
	 * @return
	 */
	public static List<Integer> filterAllRows(List<Set<String>> valuesNodeAll, int nodeId, String prop,
			JSONArray filters, int queryNodeId) {
		Integer subgraphId = smartcities.semanticweb.ontologycommons.services.ServiceInterface.getSubgraphId(nodeId,
				queryNodeId);
		Integer nrows = smartcities.semanticweb.ontologycommons.services.ServiceInterface.getRowsNumber(subgraphId,
				queryNodeId);

		// Get all rows from the uri
		List<Integer> filteredRowsIndex = new ArrayList<>();
		List<Set<String>> rows;
		try {
			// rows = services.ServiceInterface.GetGraphDataValues(nodeId,
			// prop,0, nrows - 1, queryNodeId);
			rows = valuesNodeAll;

			// Apply the filter to the rows
			// No me vale el GetGraphDataValues() que solo me devuelve un
			// atributo, necesitaré el
			// Filter

			// Get list of invalid rows
			Set<Integer> invalidRowsSet = new HashSet<Integer>();
			for (int i = 0; i < filters.length(); ++i) {
				JSONObject f = filters.getJSONObject((i));

				// filter with all the parameteres (maybe valid, maybe not)
				if (!f.isNull("nodeid") && !f.isNull("attribute") && !f.getJSONObject("attribute").isNull("uri")
						&& !f.isNull("filtertype") && !f.isNull("params")) {
					int filterNodeId = f.getInt("nodeid");
					String filterAttrUri = f.getJSONObject("attribute").getString("uri");
					String filterType = f.getString("filtertype");
					JSONObject filterParams = f.getJSONObject("params");

					List<Set<String>> rowsToFilterFromNodeId = smartcities.semanticweb.ontologycommons.services.ServiceInterface
							.GetGraphDataValues(filterNodeId, filterAttrUri, 0, nrows - 1, queryNodeId);
					ArrayList<Integer> invalidRows = WebInterfaceUtils.getInvalidRows(rowsToFilterFromNodeId,
							filterType, filterParams);
					invalidRowsSet.addAll(invalidRows);
				}
			}
			// Generate a list with the valid rows
			for (int i = 0; i < rows.size(); ++i) {
				if (!invalidRowsSet.contains(i)) {
					// filteredRows.add(rows.get(i));
					filteredRowsIndex.add(i);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// return filteredRows;
		return filteredRowsIndex;

	}

	public static String createTheMultiPolygon(ArrayList<String> x) {
		String result = new String();
		for (int i = 0; i < x.size(); ++i) {
			if (i > 0) {
				result.concat(",");
			}
			result.concat(x.get(i));
		}
		result = "MULTIPOLYGON (" + result + ")";
		return result;
	}

	/**
	 * Returns an array containing the a list of nodes with the non-connected
	 * subparts of the graph [[],...,[]]
	 * 
	 * @param graph
	 *            : nodes of the graph
	 * @return array of array of the parts of the graph
	 */
	public static String getConnectedGraphsJSON(List<IOntoLink> graph, ArrayList<IOntoBase> nodesSueltos,
			int queryNodeId) {

		JSONArray jgraph = new JSONArray();
		List<List<IOntoLink>> lgraph = smartcities.semanticweb.ontologycommons.services.ServiceInterface
				.GetConnectedGraphs(graph, queryNodeId);
		if (graph != null) {
			for (List<IOntoLink> lsubgraph : lgraph) {
				JSONArray jSubgraph = new JSONArray();
				Set<Integer> subgraphSet = new HashSet<Integer>(); // check
																	// object
																	// not
																	// already
																	// in
																	// jsonarray
				for (IOntoLink nodo : lsubgraph) {
					if (!subgraphSet.contains(nodo.getDomain().getId()))
						jSubgraph.put(nodo.getDomain().getId());
					if (!subgraphSet.contains(nodo.getRange().getId()))
						jSubgraph.put(nodo.getRange().getId());
					subgraphSet.add(nodo.getDomain().getId());
					subgraphSet.add(nodo.getRange().getId());
				}
				jgraph.put(jSubgraph);
			}
		}
		if (nodesSueltos != null) {
			for (IOntoBase ns : nodesSueltos) {
				JSONArray jSubgraph = new JSONArray();
				jSubgraph.put(ns.getId());
				jgraph.put(jSubgraph);
			}
		}

		return jgraph.toString();
	}

	public static JSONArray convertJSONObjectToJSONArray(JSONObject json) {
		JSONArray jsonArray = new JSONArray();
		Iterator<String> itr = json.keys();
		while (itr.hasNext()) {
			String key = itr.next();
			jsonArray.put(json.get(key));
		}
		return jsonArray;
	}

	public static JSONObject convertIOBToJSONObject(IOntoBase d) {
		JSONObject dJSON = new JSONObject();
		dJSON.put("id", d.getId());
		dJSON.put("uri", d.getURI());
		dJSON.put("label", d.getLabel());
		dJSON.put("type", d.getType().toString());
		// dJSON.put("isarea", d.toString());
		dJSON.put("isfunction", Boolean.valueOf(d.isFunction()).toString());
		dJSON.put("isproperty", Boolean.valueOf(d.isProperty()).toString());

		try {
			Set<IOntoProperty> dataP = smartcities.semanticweb.ontologycommons.services.ServiceInterface
					.GetDataProperties(d);
			JSONArray dproperties = new JSONArray();
			for (IOntoProperty p : dataP) {
				JSONObject dproperty = new JSONObject();
				String nlabel = WebInterfaceUtils.genRelationshipLabel(p);
				dproperty.put("label", nlabel);

				dproperty.put("uri", p.getURI());
				dproperties.put(dproperty);
			}
			dJSON.put("properties", dproperties);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dJSON;
	}

	public static Map<String, Set<String>> mapperParentsToChildrens(String conceptURI,
			Map<String, Set<String>> parentsMap) {
		Map<String, Set<String>> childrenMap = new HashMap<>();
		try {
			Set<String> parentsSet = parentsMap.get(conceptURI);
			if (parentsSet.size() == 0) {
				// Base CASE: return empty
				Set x = new HashSet<>();
				childrenMap.put(conceptURI, x);
				return childrenMap;
			} else {
				for (String parent : parentsSet) {
					IOntoBase parentIOB = ServiceInterface.GetConcept(parent);

					childrenMap = mapperParentsToChildrens(parentIOB.getURI(), parentsMap);
					try {
						if (childrenMap.get(parentIOB.getURI()) == null) {
							Set<String> childrenSet = new HashSet<>();
							childrenMap.put(conceptURI, childrenSet);
						} else {
							childrenMap.get(parentIOB.getURI()).add(conceptURI);
						}

					} catch (Exception e) {
						System.out.println("conceptURI: " + conceptURI + ", parent: " + parent + ", parentsSet: "
								+ parentsSet.toString());
						System.out.println("childrenSet: " + childrenMap + ", childrenSet.get(" + parentIOB.getURI()
								+ "): " + childrenMap.get(parentIOB.getURI()));
					}

				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return childrenMap;

	}

	/**
	 * Converts the parentsMap [child1 -> [parents1,parent2...], child2->...] to
	 * childrenMap [parent1 -> [child1,...],parent2-> [child1,...], ...] After
	 * that, it generates the jstree from the childrenMap
	 * 
	 * @param parentsMap
	 *            [child1 -> [parents1,parent2...], child2->...]
	 * @return jstree
	 */
	public static JSONArray jsTreeGeneratorFromParentMap(Map<String, Set<String>> parentsMap) {
		Random randomGenerator = new Random();
		JSONArray jstree = new JSONArray();
		Map<String, Set<String>> childrenMapAllConcepts = new HashMap<>();
		try {

			Set<String> rootURIs = new HashSet<>();
			for (Entry<String, Set<String>> parentEntry : parentsMap.entrySet()) {
				String conceptURI = parentEntry.getKey();
				Set<String> parentSet = parentEntry.getValue();

				if (parentSet.size() == 0) {
					// will be used to generate the jstree
					rootURIs.add(conceptURI);
				}

				if (childrenMapAllConcepts.get(conceptURI) == null) {
					Set<String> newSet = new HashSet<>();
					childrenMapAllConcepts.put(conceptURI, newSet);
				} else {
					// already exists, not necessary to create
				}

				for (String parentURI : parentSet) {
					Set<String> childrenSet;
					if (childrenMapAllConcepts.get(parentURI) == null) {
						childrenSet = new HashSet<>();
						childrenMapAllConcepts.put(parentURI, childrenSet);
					} else {
						// already exists, not necessary to create
						childrenSet = childrenMapAllConcepts.get(parentURI);
					}

					childrenSet.add(conceptURI);
				}
			}
			JSONArray branches = new JSONArray();
			for (Entry<String, Set<String>> childrenEntry : childrenMapAllConcepts.entrySet()) {

				String conceptURI = childrenEntry.getKey();

				if (rootURIs.contains(conceptURI)) {
					// if root, generate the branch
					IOntoBase conceptIOB = ServiceInterface.GetConcept(conceptURI);
					// JSONObject jstreeNode = new JSONObject();
					// jstreeNode.put("id", randomGenerator.nextInt(9999999));
					// jstreeNode.put("uri", conceptIOB.getURI());
					// jstreeNode.put("label", conceptIOB.getLabel());
					// jstreeNode.put("state", new JSONObject("{opened:true}"));
					JSONArray branch = jsTreeGenerator(conceptURI, childrenMapAllConcepts);
					// jstreeNode.put("children", children);
					// jstree.put(jstreeNode);
					branches.put(branch);
				}
			}

			// Merge all the branches to create a unique jstree
			for (int i = 0; i < branches.length(); ++i) {
				JSONArray tmpBranch = branches.getJSONArray(i);
				for (int j = 0; j < tmpBranch.length(); ++j) {
					JSONObject tmpRootNode = tmpBranch.getJSONObject(j);
					jstree.put(tmpRootNode);
				}
			}

		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jstree;

	}

	public static void jstreeRetocator(JSONArray jstree, JSONObject json) {
		try {
			for (int i = 0; i < jstree.length(); ++i) {
				JSONObject concept = jstree.getJSONObject(i);
				String uri = concept.getString("uri");
				String label = concept.getString("label");
				Double similarity = json.getJSONObject(uri).getDouble("similarity");

				// similarity adapt to %
				similarity = similarity * 100.0 / 5.0;
				// end similarity adapt to %
				// similarity trunk to 1 cipher
				DecimalFormat df = new DecimalFormat("##.#");
				df.setRoundingMode(RoundingMode.DOWN);
				String simStr = df.format(similarity);
				// end similarity trunk
				String txt = label + " [similarity: " + simStr + "%]";
				concept.put("text", txt);
				// clean categories array
				JSONArray categories = json.getJSONObject(uri).getJSONArray("categories");
				JSONObject jstreenodeCategories = new JSONObject();
				for (int j = 0; j < categories.length(); ++j) {
					JSONObject a = categories.getJSONObject(j);
					jstreenodeCategories.put(a.getString("uri"), a);
				}
				// end clean categories array

				// jstree node extra data
				JSONObject data = new JSONObject();
				data.put("uri", uri);
				data.put("label", label);
				data.put("type", WebInterfaceUtils.getTypeCustomized(uri));
				data.put("similarity", similarity);
				data.put("categories", jstreenodeCategories);
				concept.put("data", data);

				// continue with the children
				jstreeRetocator(concept.getJSONArray("children"), json);
			}
		} catch (Exception e) {
			System.out.println("Exception en WIU.jstreeRetocator()");
		}
	}

	public static JSONArray jsTreeGenerator(String conceptURI, Map<String, Set<String>> childrenMap) {
		Random randomGenerator = new Random();
		try {
			JSONArray jstreeNodes = new JSONArray();
			Set<String> childrenSet = childrenMap.get(conceptURI);
			IOntoBase conceptIOB = ServiceInterface.GetConcept(conceptURI);
			WebInterfaceUtils.checkConceptIOB(conceptIOB);
			if (childrenSet.size() == 0) {
				// BASE CASE: leaf node
				JSONObject jstreeNode = new JSONObject();
				jstreeNode.put("id", randomGenerator.nextInt(9999999));
				jstreeNode.put("uri", conceptIOB.getURI());
				jstreeNode.put("label", conceptIOB.getLabel());
				jstreeNode.put("state", new JSONObject("{opened:true}"));
				jstreeNode.put("children", new JSONArray());
				return jstreeNodes.put(jstreeNode);

			} else {
				// Generate the children jstree's
				JSONArray childJSTrees = new JSONArray();
				for (String child : childrenSet) {
					JSONArray childJSTree = new JSONArray();
					IOntoBase childIOB = ServiceInterface.GetConcept(child);
					childJSTree = jsTreeGenerator(childIOB.getURI(), childrenMap);
					// TODO: get all the childJSTree's and merge them after
					// taking them into a unique JSONArray that will be used as
					childJSTrees.put(childJSTree);
				}

				// Merge all childrens branches to create a unique jstree
				JSONArray childJSTreesAll = new JSONArray();
				for (int i = 0; i < childJSTrees.length(); ++i) {
					JSONArray tmpChildJSTree = childJSTrees.getJSONArray(i);
					for (int j = 0; j < tmpChildJSTree.length(); ++j) {
						JSONObject tmpBranch = tmpChildJSTree.getJSONObject(j);
						childJSTreesAll.put(tmpBranch);
						// for (int k = 0; k < tmpChildJSTree.length(); ++k) {
						// JSONObject tmpRootNode = tmpBranch.getJSONObject(k);
						// childJSTreesAll.put(tmpRootNode);
						// }
					}
				}

				// id : 1,
				// text : "Folder 1",
				// state : {selected : false},
				// children : []
				JSONObject jstreeNode = new JSONObject();
				jstreeNode.put("id", randomGenerator.nextInt(9999999));
				jstreeNode.put("uri", conceptIOB.getURI());
				jstreeNode.put("label", conceptIOB.getLabel());
				jstreeNode.put("state", new JSONObject("{opened:true}"));
				jstreeNode.put("children", childJSTreesAll);
				jstreeNodes.put(jstreeNode);

				return jstreeNodes;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void checkConceptIOB(IOntoBase conceptIOB) {
		// required to have an URI defined!
		assert conceptIOB.getURI() != null;

		if (conceptIOB.getLabel() == null) {
			conceptIOB.setLabel(conceptIOB.getURI().split("#")[1] + " (*local_name)");
		} else {
			// do nothing
		}

	}

	/**
	 * 
	 * @param d
	 * @return
	 */
	public static void jsTreeChildrenMapFactory(Map<String, Set<String>> iOBParents,
			Map<IOntoBase, Set<IOntoBase>> iOBChildrens, String conceptURI, Set<String> rootsUris) {
		//
		// Random randomGenerator = new Random();
		// Set<String> parentsSIOB = iOBParents.get(conceptURI);
		// try {
		// if (parentsSIOB.size() > 0) {
		// for (String gBHParent : parentsSIOB) {
		// IOntoBase childIOB = ServiceInterface.GetConcept(conceptURI);
		// childIOB.setId(randomGenerator.nextInt(9999999));
		//
		// WebInterfaceUtils.jsTreeChildrenMapFactory(iOBParents, iOBChildrens,
		// gBHParent, rootsUris);
		// Set<IOntoBase> childrenSet = iOBChildrens.get(gBHParent);
		// if (childrenSet == null) {
		// childrenSet = new HashSet<>();
		// }
		// childrenSet.add(childIOB);
		// iOBChildrens.put(gBHParent, childrenSet);
		// }
		// } else {
		// // root node
		// conceptURI.setId(randomGenerator.nextInt(9999999));
		// rootsUris.add(conceptURI);
		// if (iOBChildrens.get(conceptURI) == null) {
		// // System.out.println("conceptIOB Root: " +
		// // conceptIOB.getURI()
		// // + " ");
		// iOBChildrens.put(conceptURI, new HashSet<IOntoBase>());
		// } else {
		// // do nothing
		// }
		// }
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	public static Set<IOntoBase> jsTreeFilterTillNextParents(Map<IOntoBase, Set<IOntoBase>> iOBParents,
			Set<IOntoBase> filtered, IOntoBase object) {
		Set<IOntoBase> filteredParents = new HashSet<>();
		if (filtered.contains(object)) {
			// look for another not filtered
			Set<IOntoBase> parents = iOBParents.get(object);
			Set<IOntoBase> xxx = null;
			for (IOntoBase parent : parents) {
				xxx = WebInterfaceUtils.jsTreeFilterTillNextParents(iOBParents, filtered, parent);
				filteredParents.addAll(xxx);
			}
			System.out.println("Filtering " + object.getURI());

		} else {
			// ok, no problem
			Set<IOntoBase> x = iOBParents.get(object); // get actual parents
			filteredParents.addAll(x);
			// filteredParents.add(object);
		}
		return filteredParents;
	}

	public static void jsTreeChildrenMapFactoryByIdentifiers(Map<IOntoBase, Set<IOntoBase>> iOBParents,
			Map<IOntoBase, Set<IOntoBase>> iOBChildrens, IOntoBase conceptIOB, Set<IOntoBase> rootsUris) {

		for (Entry<IOntoBase, Set<IOntoBase>> gBHParent : iOBParents.entrySet()) {
			Set<IOntoBase> x = gBHParent.getValue();
			for (IOntoBase y : x) {
				if (y.isIndicator()) {
					// do nothing, keep it at Map
				} else {
					// remove from Map
					iOBParents.remove(y);
				}
			}
		}
		// WebInterfaceUtils.jsTreeChildrenMapFactory(iOBParents, iOBChildrens,
		// conceptIOB, rootsUris);
	}

	public static void jsTreeChildrenMapFactoryByEnumType(Map<IOntoBase, Set<IOntoBase>> iOBParents,
			Map<IOntoBase, Set<IOntoBase>> iOBChildrens, IOntoBase conceptIOB, Set<IOntoBase> rootsUris) {

		for (Entry<IOntoBase, Set<IOntoBase>> gBHParent : iOBParents.entrySet()) {
			Set<IOntoBase> x = gBHParent.getValue();
			for (IOntoBase y : x) {
				if (y.getType().toString() == "ECLASS") {
					// do nothing, keep it at Map
				} else {
					// remove from Map
					iOBParents.remove(y);
				}
			}
		}
		// WebInterfaceUtils.jsTreeChildrenMapFactory(iOBParents, iOBChildrens,
		// conceptIOB, rootsUris);
	}

	public static void jsTreeChildrenMapFactoryByEntities(Map<IOntoBase, Set<IOntoBase>> iOBParents,
			Map<IOntoBase, Set<IOntoBase>> iOBChildrens, Set<IOntoBase> rootsUris) {

		Map<String, Set<String>> parentURIs = new HashMap<>();
		for (Entry<IOntoBase, Set<IOntoBase>> anEntry : iOBParents.entrySet()) {
			anEntry.getKey();
			Set<String> Uris = new HashSet<>();
			for (IOntoBase iob : anEntry.getValue()) {
				Uris.add(iob.getURI());
			}
			parentURIs.put(anEntry.getKey().getURI(), Uris);
		}

		Map<String, Set<String>> childrensURIs = new HashMap<>();
		Set<String> rootURIs = new HashSet<>();
		for (String conceptIOB : parentURIs.keySet()) {
			// WebInterfaceUtils.jsTreeChildrenMapFactory(parentURIs,childrensURIs,
			// conceptIOB, rootURIs);
		}

	}

	public static String getTypeCustomized(String uri) {
		String typeC = null;
		// TODO:
		IOntoBase IOBConcept;
		try {
			IOBConcept = smartcities.semanticweb.ontologycommons.services.ServiceInterface.GetConcept(uri);
			if (IOBConcept.isIndicator() || IOBConcept.isIndicatorClass() || IOBConcept.isIndicatorDescriptorClass()) {
				if (IOBConcept.isIndicatorClass())
					System.out.println("INDICATOR isIndicatorClass(" + uri + ")->" + IOBConcept.isIndicatorClass());
				if (IOBConcept.isIndicatorDescriptorClass())
					System.out.println("INDICATOR isIndicatorDescriptorClass(" + uri + ")-> "
							+ IOBConcept.isIndicatorDescriptorClass());
				if (IOBConcept.isIndicator())
					System.out.println("INDICATOR IOBConcept.isIndicator(" + uri + ")->" + IOBConcept.isIndicator());
				typeC = "IND";
				return typeC;
			} else {
				// Otherwise
				typeC = IOBConcept.getType().toString();
				return typeC;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return typeC;
	}

	public static JSONObject snippetToJSON(Map<String, Map<String, String>> snippets) {
		JSONObject jSnippet = new JSONObject();
		for (Map.Entry<String, Map<String, String>> eachSnippet : snippets.entrySet()) {
			String snippetUri = eachSnippet.getKey();
			Map<String, String> v = eachSnippet.getValue();
			for (Map.Entry<String, String> vx : v.entrySet()) {
				JSONObject property = new JSONObject();
				String kx = vx.getKey();
				String kv = vx.getValue();
				property.put(kx, kv);
				jSnippet.put(snippetUri, property);
			}
		}
		return jSnippet;
	}

	public static JSONObject iiDescriptorToJSON(IIndicatorDescriptor idescr) {
		JSONObject json = new JSONObject();
		json.put("id", idescr.getId());
		json.put("label", idescr.getLabel());
		json.put("uri", idescr.getURI());
		json.put("id_ontology", idescr.getOntologyIdentifier());
		json.put("comment", idescr.getComment());
		return json;
	}

	public static void filterByType(List<ISearchResult> resultsUntrunked, String type) {
		for (ISearchResult isr : resultsUntrunked) {

		}

	}

	public static HashMap<String, Set<IOntoBase>> filterByClasses(Set<IOntoBase> conceptsToGroupByHierarchy) {

		HashMap<String, Set<IOntoBase>> clasification = new HashMap<>();
		clasification.put("ALL", new HashSet<IOntoBase>());
		clasification.put("IND", new HashSet<IOntoBase>());
		clasification.put("ECLASS", new HashSet<IOntoBase>());
		clasification.put("CLASS", new HashSet<IOntoBase>());

		// 0) Identify ALL (¬¬)
		clasification.put("ALL", new HashSet<>(conceptsToGroupByHierarchy));

		// 1) Identify INDICATORS
		Iterator<IOntoBase> iterator = conceptsToGroupByHierarchy.iterator();
		Set<IOntoBase> listIND = new HashSet<>();
		while (iterator.hasNext()) {
			IOntoBase resIOB = iterator.next();
			if (resIOB.isIndicatorClass() || resIOB.isIndicatorDescriptor() || resIOB.isIndicatorDescriptorClass()) {
				listIND.add(resIOB);
				// AVOIDING IND, ECLASS, CLASS Overlapping
				iterator.remove();
			} else {
				// Wait steps 2,3
			}
		}
		clasification.put("IND", listIND);

		// 2,3) Identify ECLASS, CLASS
		iterator = conceptsToGroupByHierarchy.iterator();
		Set<IOntoBase> list = new HashSet<>();
		while (iterator.hasNext()) {
			IOntoBase resIOB = iterator.next();
			String type = resIOB.getType().toString();
			list = clasification.get(type);
			if (list != null) {
				list.add(resIOB);
			} else {
				list = new HashSet<>();
				list.add(resIOB);
				clasification.put(type, list);
			}
			// AVOIDING IND, ECLASS, CLASS Overlapping
			iterator.remove();
		}
		return clasification;
	}

}
