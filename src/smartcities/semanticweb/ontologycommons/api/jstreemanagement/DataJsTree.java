package smartcities.semanticweb.ontologycommons.api.jstreemanagement;

import java.util.ArrayList;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;
import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class DataJsTree {

	ArrayList<DataNodeJSTree> djt = new ArrayList<>();

	public DataJsTree(IOntoBase udc) {
		Random randomGenerator = new Random();
		udc.setId(randomGenerator.nextInt(999999999));
		for (IOntoBase childOB : udc.getSubConcepts()) {
			childOB.setId(randomGenerator.nextInt(999999999));
			DataNodeJSTree child = new DataNodeJSTree(childOB, "#");
			this.djt.add(child);
			this.djt.addAll(DataJsTree.jsTreeGeneration(childOB));
		}
	}

	public static ArrayList<DataNodeJSTree> jsTreeGeneration(IOntoBase parentOB) {

		if (parentOB == null) {
			return new ArrayList<DataNodeJSTree>();
		} else {
			ArrayList<DataNodeJSTree> tmp = new ArrayList<>();
			Random randomGenerator = new Random();
			// parentOB.setId(randomGenerator.nextInt(999999999));
			IOntoBase parentDescendants = ServiceInterface.GetSub(parentOB);
			System.out.println("parentDescendants.getSubConcepts() URI: " + parentOB.getURI());
			for (IOntoBase childOB : parentDescendants.getSubConcepts()) {
				if (childOB.getId() == 0) {
					childOB.setId(randomGenerator.nextInt(999999999));
					// DataNodeJSTree child = new DataNodeJSTree(childOB,
					// parentOB);
					DataNodeJSTree child = new DataNodeJSTree(childOB, String.valueOf(parentOB.getId()));
					tmp.add(child);
					tmp.addAll(jsTreeGeneration(childOB));

				}
			}
			return tmp;
		}
	}

	public JSONArray toJSONArray() {
		JSONArray jTree = new JSONArray();

		for (DataNodeJSTree node : this.djt) {
			JSONObject jsonNode = node.toJSONObject();
			jTree.put(jsonNode);
		}

		return jTree;
	}

}
