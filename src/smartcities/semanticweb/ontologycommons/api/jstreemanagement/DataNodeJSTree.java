package smartcities.semanticweb.ontologycommons.api.jstreemanagement;

import java.util.Random;

import org.json.JSONObject;

import smartcities.semanticweb.ontologycommons.ontoconcepts.IOntoBase;

public class DataNodeJSTree {
	// Alternative format of the node (id & parent are required)
	// {
	// id : "string" // required
	// parent : "string" // required
	// text : "string" // node text
	// icon : "string" // string for custom
	// state : {
	// opened : boolean // is the node open
	// disabled : boolean // is the node disabled
	// selected : boolean // is the node selected
	// },
	// li_attr : {} // attributes for the generated LI node
	// a_attr : {} // attributes for the generated A node
	// }
	Random randomGenerator = new Random();
	String id = null;
	String parent = null;
	String text = null;
	String icon = null;
	String uri = null; // required by smendoza
	DNodeState state = null;
	JSONObject a_attr = new JSONObject();
	JSONObject li_attr = new JSONObject();

	private class DNodeState {
		boolean opened = false; // is the node open
		boolean disabled = false; // is the node disabled
		boolean selected = false; // is the node selected
	}

	// public DataNodeJSTree(ArrayList<String> x) {
	// this.id = null;
	// this.parent = null;
	// this.text = null;
	// this.icon = null;
	// this.state = null;
	// }

	public DataNodeJSTree(IOntoBase iob) {

		// checkbox plugin required
		iob.setId(randomGenerator.nextInt(999999999));
		this.id = String.valueOf(iob.getId());
		this.parent = null;
		this.text = iob.getLabel();
		this.icon = null;
		this.state = new DNodeState();
		this.a_attr.put("uri", iob.getURI());
		// personal
		// this.uri = iob.getURI();
	}

	public DataNodeJSTree(IOntoBase iob, IOntoBase parentIOB) {
		// this.id = iob.getLabel() + parent.getLabel();
		this.id = String.valueOf(iob.getId());
		// this.parent = parent.getLabel();
		this.parent = String.valueOf(parentIOB.getId());
		this.text = iob.getLabel();
		this.icon = null;
		this.state = new DNodeState();
		this.a_attr.put("uri", iob.getURI());
		// personal
		// this.uri = iob.getURI();
	}

	public DataNodeJSTree(IOntoBase iob, String parentID) {
		this.id = String.valueOf(iob.getId());
		this.parent = parentID;
		this.text = iob.getLabel();
		this.icon = null;
		this.state = new DNodeState();
		this.a_attr.put("uri", iob.getURI());
		// personal
		// this.uri = iob.getURI();
	}

	public JSONObject toJSONObject() {
		JSONObject output = new JSONObject();
		// {
		// id : "string" // required
		// parent : "string" // required
		// text : "string" // node text
		// icon : "string" // string for custom
		// state : {
		// opened : boolean // is the node open
		// disabled : boolean // is the node disabled
		// selected : boolean // is the node selected
		// },
		// li_attr : {} // attributes for the generated LI node
		// a_attr : {} // attributes for the generated A node
		// }

		output.put("id", this.id);
		output.put("parent", this.parent);
		output.put("text", this.text);
		output.put("uri", this.uri);
		output.put("a_attr", this.a_attr);
		// output.put("li_attr", this.li_attr);
		// output.put("icon", this.icon);
		// JSONObject jsonState = new JSONObject();
		// jsonState.put("opened", this.state.opened);
		// jsonState.put("disabled", this.state.disabled);
		// jsonState.put("selected", this.state.selected);
		// output.put("state", jsonState);

		return output;
	}

}
