package smartcities.semanticweb.ontologycommons.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.tdb.TDBFactory;

import smartcities.semanticweb.ontologycommons.services.JenaOntology;
import smartcities.semanticweb.ontologycommons.services.ModelMap;
import smartcities.semanticweb.ontologycommons.services.ParliamentOntology;
import smartcities.semanticweb.ontologycommons.services.UrbanOntology;

public class DataGeneration {
	
//	private static void insertInstancesGraph(String parliamentlocation, String parliamentmodel, Model ontology) throws IOException{
//		ParliamentOntology parliament = new ParliamentOntology(parliamentlocation);
//		//RemoteModel pmodel = ParliamentUtils.getPmodel("http://redrock.bsc.es:8080");
////		if (ParliamentUtils.existGraph(pmodel, mapping_graph))
////			ParliamentUtils.deleteGraph(pmodel, mapping_graph);
//		//flush
//		
//		if (parliament.existGraph(parliamentmodel))
//			parliament.deleteGraph(parliamentmodel);
//		parliament.insertStatements(parliamentmodel, ontology);
//		//index
//		parliament.indexGraph(parliamentmodel);
//		
//	}
	
	


	public static UrbanOntology GenerateData(Properties properties) throws Exception {
		String baseuri = properties.getProperty("baseuri").trim();
		String indicatorsinstancesuri = properties.getProperty("indicatorsinstancesuri").trim();
		String instancesuri = properties.getProperty("instancesuri").trim();
		String instancesontology = properties.getProperty("instancesontology").trim();
		String ontologiespolicy = properties.getProperty("ontologiespolicy").trim();
		String datapath = properties.getProperty("dataPath").trim();
		String tdbPath = properties.getProperty("tdbPath").trim();
		String datacreationlog = properties.getProperty("datacreationlog").trim();
		String ontologiespath = properties.getProperty("ontologiesPath").trim();
		String superblocks = properties.getProperty("superblocks").trim();
		int sb = Integer.parseInt(superblocks);
		
		File f = new File(datacreationlog);
		if (!f.exists()){
			String parent = f.getParent();
			if (parent != null){
				Path path = Paths.get(parent);
				if (Files.notExists(path))
					Files.createDirectories(path);
			}
		}
		FileWriter logFile = new FileWriter(datacreationlog,false);
		logFile.write(properties.toString()+"\n");
		
		//get tbox in memory
		Model tbox = JenaOntology.getTBox(ontologiespolicy,baseuri,indicatorsinstancesuri,ontologiespath);
		
		//get tdb with instances
		Dataset tdb = Mapping.MapData(sb, baseuri, indicatorsinstancesuri,instancesuri,instancesontology,ontologiespolicy,datapath,tdbPath,logFile, tbox);
		
		//create resources, including new urban inferenced statements in main model
		UrbanOntology uOntology = new UrbanOntology(properties, tdb,tbox);
		
		if (uOntology != null)
			logFile.write("UrbanOntology created successfully");
		logFile.close();

		return uOntology;
	}

}
