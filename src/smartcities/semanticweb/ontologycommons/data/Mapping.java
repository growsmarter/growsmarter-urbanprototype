package smartcities.semanticweb.ontologycommons.data;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bbn.parliament.jena.joseki.client.RemoteModel;
import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.TypeMapper;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.impl.PropertyImpl;
import com.hp.hpl.jena.rdf.model.impl.ResourceImpl;
import com.hp.hpl.jena.rdf.model.impl.StatementImpl;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;
import com.hp.hpl.jena.tdb.store.DatasetGraphTDB;
import com.hp.hpl.jena.tdb.sys.TDBInternal;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.LocationMapper;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.XSD;

import smartcities.semanticweb.ontologycommons.services.ParliamentOntology;

import com.hp.hpl.jena.query.ResultSet;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPListParseEngine;
import org.apache.commons.net.ftp.FTPReply;
//import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.commons.io.FileUtils;

public class Mapping {

	public static int contInst = 0;
	public static int contGeom = 0;
	public static int contMeasure = 0;
	public static int contIndicator = 0;

	public static int countIncorrectPolygons = 0;

	static Dataset dbModel = null;
	static Model model = null;

	// public HashMap<String, HashMap<Integer,String>> bcnEcologyData = null;
	// //file->(field position->ontology entity) field position = -1 is the
	// individual

	// Map File with the entities of the ontology in the first column and the
	// correspondent field of bcnecology files in the third column
	// As result, HashMap with field->entity
	public static HashMap<String, HashMap<String, String>> ReadMapFile(URL path) throws IOException, URISyntaxException {
		HashMap<String, HashMap<String, String>> map = new HashMap<String, HashMap<String, String>>();
		BufferedReader reader = Files.newBufferedReader((new File(path.toURI())).toPath(), Charset.defaultCharset());
		String line = reader.readLine(); // headers
		while ((line = reader.readLine()) != null) {
			String[] tokens = line.split("\t");
			String entity = tokens[0];
			String field = tokens[2].toUpperCase(); // FIELDS AND FILE NAME FROM
													// MAPPING FILE NORMALIZED
													// TO UPPER CASE
			String file = tokens[3].toUpperCase();
			if (!file.equals("Notfound")) {
				HashMap<String, String> innerMap = null;
				if (map.containsKey(file))
					innerMap = map.get(file);
				else {
					innerMap = new HashMap<String, String>();
					map.put(file, innerMap);
				}
				innerMap.put(field, entity);
			}
		}
		reader.close();
		return map;
	}

	// private static String[] getBCNFields(BufferedReader reader) throws
	// IOException{
	// String firstline = reader.readLine();
	// String[] tokens = firstline.split(",");
	// return tokens;
	// }

	// For each field located in the map file, the correspondent entity is
	// incorporated in the adequate position
	// private static HashMap<Integer,String> getMappingPosition(String
	// individual, String[] fields, HashMap<String,String> mapFile){
	// HashMap<Integer,String> map = new HashMap<Integer,String>();
	// for (int i=0;i<fields.length;i++){
	// if (mapFile.containsKey(fields[i]))
	// map.put(i, mapFile.get(fields[i]));
	// }
	// if (mapFile.containsKey(individual))
	// map.put(-1, mapFile.get(individual));
	// return map;
	// }

	private static String NormalizeNumber(String value) {
		// return value.replaceFirst("^0+", "");
		value = value.replaceFirst("\\.0+$", "");
		return value;

	}

	private static ArrayList<String> NormalizedSplit(String line) {
		// String[] t = line.split("\"",-1);
		String[] tokens = line.split(",", -1);
		if (tokens.length <= 1)
			tokens = line.split(";", -1);
		ArrayList<String> output = new ArrayList<String>();
		boolean inField = false;
		String field = "";
		for (String tok : tokens) {
			tok = tok.trim();
			if (tok.startsWith(("\"")) && tok.endsWith("\"") && !inField) {
				inField = false;
				output.add(tok.substring(1, tok.length() - 1)); // remove "
				field = "";
			} else if (tok.endsWith("\"") && inField) {
				inField = false;
				String newtoken = field + tok;
				output.add(newtoken.substring(1, newtoken.length() - 1)); // remove
																			// "
				field = "";
			} else if (tok.startsWith("\"") && !inField) {
				inField = true;
				field = tok + ",";
			} else if (inField) {
				field = field + tok + ",";
			} else {
				output.add(tok);
			}

		}
		return output;
	}

	private static String ClosePolygon(String wkt) {
		String pattern = "\\((?<first>[\\d\\s\\.]+)(?<middle>\\,([\\d\\s\\.]+\\,)*+)(?<last>[\\d\\s\\.]+)\\)";

		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		int wrong = 0;
		// Now create matcher object.
		Matcher m = r.matcher(wkt);
		while (m.find()) {
			String polygon = m.group();
			String firstPoint = m.group("first");
			String middlePoints = m.group("middle");
			String lastPoint = m.group("last");
			if (!firstPoint.equals(lastPoint)) {
				wrong = 1;
				String newPolygon = "(" + firstPoint + middlePoints + lastPoint + "," + firstPoint + ")";
				// System.out.println("Incorrect polygon:"+wkt);
				wkt = wkt.replace(polygon, newPolygon);

				// m.replaceFirst(newPolygon);
				// System.out.println("Correct polygon:"+wkt);
			}
		}
		if (wrong == 1)
			System.out.println("Incorrect Polygon");
		countIncorrectPolygons += wrong;
		return wkt;
	}

	private static void AddGeometry(Resource ind, String baseURI, String wkt) {
		String geometryClass = null;
		if (wkt.startsWith("POLYGON")) {
			geometryClass = "Polygon";
			wkt = ClosePolygon(wkt);
		} else if (wkt.startsWith("POINT"))
			geometryClass = "Point";
		else if (wkt.startsWith("LINESTRING"))
			geometryClass = "LineString";
		else if (wkt.startsWith("MULTIPOLYGON")) {
			geometryClass = "MultiPolygon";
			wkt = ClosePolygon(wkt);
		} else if (wkt.startsWith("MULTIPOINT"))
			geometryClass = "MultiPoint";

		wkt = "<http://www.opengis.net/def/crs/EPSG/0/3049> " + wkt; // SI NO SE
																		// AÑADEN
																		// LAS
																		// COORDENADAS
																		// NO SE
																		// CALCULAN
																		// LAS
																		// DISTANCIAS.
																		// NO SE
																		// SI
																		// VALEN
																		// OTRAS
																		// COORD.

		if (geometryClass != null) {
			Resource geometryInd = model.createResource(baseURI + "#geometry" + contGeom++,
					new ResourceImpl(model.getNsPrefixURI("sf"), geometryClass));
			RDFDatatype wktType = TypeMapper.getInstance().getSafeTypeByName(model.getNsPrefixURI("sf") + "wktLiteral");
			geometryInd.addProperty(new PropertyImpl(model.getNsPrefixURI("geo"), "asWKT"),
					ResourceFactory.createTypedLiteral(wkt, wktType));
			ind.addProperty(new PropertyImpl(model.getNsPrefixURI("geo"), "hasGeometry"), geometryInd);
		}
	}

	private static RDFDatatype GetDataType(Property prop, Model tbox) {
		RDFDatatype rdfdtype = null;
		for (NodeIterator iterator = tbox.listObjectsOfProperty(prop, RDFS.range); iterator.hasNext();) {
			ResourceImpl type = (ResourceImpl) iterator.next();
			rdfdtype = TypeMapper.getInstance().getSafeTypeByName(type.getURI());
		}
		if (rdfdtype == null)
			rdfdtype = TypeMapper.getInstance().getSafeTypeByName(model.getNsPrefixURI("xsd") + "string");
		return rdfdtype;
	}

	private static void CreateAreaInstance(String baseURI, Resource bcnInstance, Resource actualClass, String entity,
			String value, Model tbox) {
		String prefixCoreWeatherV2 = model.getNsPrefixURI("CoreWeatherV2");
		String prefixCoreV2 = model.getNsPrefixURI("CoreV2");
		Resource ind = model.createResource(baseURI + "#measureInstance" + contMeasure++, actualClass);
		// OntProperty measuremValue =
		// ontology.getOntProperty(prefixCoreWeatherV2+"measurementFloatValue");
		Property measuremValue = tbox.getProperty(prefixCoreV2 + "measurementFloatValue");
		ind.addProperty(measuremValue, value, GetDataType(measuremValue, tbox));

		Property measuremUnit = tbox.getProperty(prefixCoreV2 + "measurementUnit");
		ind.addProperty(measuremUnit, "Square meters", GetDataType(measuremUnit, tbox));

		Property measuremAbb = tbox.getProperty(prefixCoreV2 + "abbreviation");
		ind.addProperty(measuremAbb, value + " sq. m.", GetDataType(measuremAbb, tbox));

		Property measuremDesc = tbox.getProperty(prefixCoreV2 + "textDescription");
		ind.addProperty(measuremDesc, value + " square meters", GetDataType(measuremDesc, tbox));

		Property prop = tbox.getProperty(prefixCoreV2 + "hasArea");
		bcnInstance.addProperty(prop, ind);
		Property invProp = tbox.getProperty(prefixCoreV2 + "areaOf");
		ind.addProperty(invProp, bcnInstance);
	}

	private static void CreateHightInstance(String baseURI, Resource bcnInstance, Resource actualClass, String entity,
			String value, Model tbox) {
		String prefixCoreWeatherV2 = model.getNsPrefixURI("CoreWeatherV2");
		String prefixCoreV2 = model.getNsPrefixURI("CoreV2");
		Resource ind = model.createResource(baseURI + "#measureInstance" + contMeasure++, actualClass);
		// OntProperty measuremValue =
		// ontology.getOntProperty(prefixCoreWeatherV2+"measurementFloatValue");
		Property measuremValue = tbox.getProperty(prefixCoreV2 + "measurementFloatValue");
		ind.addProperty(measuremValue, value, GetDataType(measuremValue, tbox));

		Property measuremUnit = tbox.getProperty(prefixCoreV2 + "measurementUnit");
		ind.addProperty(measuremUnit, "meter", GetDataType(measuremUnit, tbox));

		Property measuremAbb = tbox.getProperty(prefixCoreV2 + "abbreviation");
		ind.addProperty(measuremAbb, value + " m.", GetDataType(measuremAbb, tbox));

		Property measuremDesc = tbox.getProperty(prefixCoreV2 + "textDescription");
		ind.addProperty(measuremDesc, value + " meters", GetDataType(measuremDesc, tbox));

		Property prop = tbox.getProperty(prefixCoreV2 + "hasHeight");
		bcnInstance.addProperty(prop, ind);
		Property invProp = tbox.getProperty(prefixCoreV2 + "heightOf");
		ind.addProperty(invProp, bcnInstance);
	}

	private static void CreateVehiclesInstance(String baseURI, Resource bcnInstance, Resource actualClass,
			String entity, String value, Model tbox) {
		String prefixCoreV2 = model.getNsPrefixURI("CoreV2");
		Resource ind = model.createResource(baseURI + "#measureInstance" + contMeasure++, actualClass);
		Property measuremValue = tbox.getProperty(prefixCoreV2 + "measurementFloatValue");
		ind.addProperty(measuremValue, value, GetDataType(measuremValue, tbox));
		Property prop = tbox.getProperty(prefixCoreV2 + "hasTrafficVolume");
		bcnInstance.addProperty(prop, ind);
		Property invProp = tbox.getProperty(prefixCoreV2 + "trafficVolumeOf");
		ind.addProperty(invProp, bcnInstance);
	}

	private static void AddIndicator(String baseURI, Resource bcnInstance, Resource actualClass, String entity,
			String value, String field, Model tbox) throws Exception {
		String prefixCoreV2 = model.getNsPrefixURI("CoreV2");
		String prefixRDFS = model.getNsPrefixURI("rdfs");
		String prefixBCN = model.getNsPrefixURI("BCNEcology");
		Resource ind = model.createResource(baseURI + "#indicatorInstance" + contIndicator++, actualClass); // indicator
		Property measuremValue = tbox.getProperty(prefixCoreV2 + "measurementFloatValue");
		ind.addProperty(measuremValue, value, GetDataType(measuremValue, tbox));
		Property indicatorOf = tbox.getProperty(prefixCoreV2 + "IndicatorOf");
		ind.addProperty(indicatorOf, bcnInstance);

		Property indicatorOfInv = tbox.getProperty(prefixCoreV2 + "hasIndicator");
		bcnInstance.addProperty(indicatorOfInv, ind);

		String labelValue = "";
		Resource indType = null;
		Property label = tbox.getProperty(prefixRDFS + "label");
		Property descriptor = tbox.getProperty(prefixCoreV2 + "hasIndicatorDescriptor");
		Resource descInd = null;
		if (field.contains("AIRE")) {
			labelValue = "Air quality indicator";
			descInd = tbox.getResource(prefixBCN + "AirQuality_INST");
		} else if (field.contains("RUID")) {
			labelValue = "Noise quality indicator";
			descInd = tbox.getResource(prefixBCN + "AcousticComfort_INST");

		} else
			throw new Exception("AddIndicator Error: field " + field + " not found");
		Property associatedToInd = tbox.getProperty(prefixCoreV2 + "associatedToIndicatorType");
		Property associatedToIndInv = tbox.getProperty(prefixCoreV2 + "indicatorTypeAssociatedTo");
		bcnInstance.addProperty(associatedToInd, descInd);
		descInd.addProperty(associatedToIndInv, bcnInstance);

		ind.addProperty(label, labelValue, GetDataType(label, tbox));
		ind.addProperty(descriptor, descInd);
		Property descriptorInv = tbox.getProperty(prefixCoreV2 + "indicatorDescriptorOf");
		descInd.addProperty(descriptorInv, ind);
	}

	// private static void CreatePublicSpaceInstance(String baseURI, OntModel
	// ontology, Individual bcnInstance, OntClass actualClass){
	// OntClass generalClass = bcnInstance.getOntClass();
	// bcnInstance.removeOntClass(generalClass);
	// bcnInstance.addOntClass(actualClass);
	// }

	private static boolean CreateClassInstance(String baseURI, Resource bcnInstance, String nsPrefix, String entity,
			String value, String field, Model tbox) throws Exception {
		Resource actualClass = tbox.getResource(nsPrefix + entity);
		if (actualClass == null)
			return false;
		switch (entity) {
		case "HeightMeasurement":
			CreateHightInstance(baseURI, bcnInstance, actualClass, entity, value, tbox);
			break;
		case "BuildUnitAreaMeasurement":
			CreateAreaInstance(baseURI, bcnInstance, actualClass, entity, value, tbox);
			break;
		case "BuiltStructureOccupiedSurfaceMeasurement":
			CreateAreaInstance(baseURI, bcnInstance, actualClass, entity, value, tbox);
			break;
		case "BuiltStructureFootPrintMeasurement":
			CreateAreaInstance(baseURI, bcnInstance, actualClass, entity, value, tbox);
			break;
		case "VehiclesPerDayCurrent":
			CreateVehiclesInstance(baseURI, bcnInstance, actualClass, entity, value, tbox);
			break;
		case "VehiclesPerDayFuture":
			CreateVehiclesInstance(baseURI, bcnInstance, actualClass, entity, value, tbox);
			break;
		case "Geometry":
			AddGeometry(bcnInstance, baseURI, value);
			break;// Add wkt field if exist (only if it is in the first field)
		case "Indicator":
			AddIndicator(baseURI, bcnInstance, actualClass, entity, value, field, tbox);
			break;// Add wkt field if exist (only if it is in the first field)
		default:
			return false;
			// case "AreaLocalUse": CreatePublicSpaceInstance(baseURI,ontology,
			// bcnInstance, actualClass);break;
			// case "BikeLane": CreatePublicSpaceInstance(baseURI,ontology,
			// bcnInstance, actualClass);break;
			// case "BlockExternalSpace":
			// CreatePublicSpaceInstance(baseURI,ontology, bcnInstance,
			// actualClass);break;
			// case "BlockInnerSpace":
			// CreatePublicSpaceInstance(baseURI,ontology, bcnInstance,
			// actualClass);break;
			// case "Boulevard": CreatePublicSpaceInstance(baseURI,ontology,
			// bcnInstance, actualClass);break;
			// case "Bypath": CreatePublicSpaceInstance(baseURI,ontology,
			// bcnInstance, actualClass);break;
			// case "PedestrianMall":
			// CreatePublicSpaceInstance(baseURI,ontology, bcnInstance,
			// actualClass);break;
			// case "PedestrianStreet":
			// CreatePublicSpaceInstance(baseURI,ontology, bcnInstance,
			// actualClass);break;
			// case "Promenade": CreatePublicSpaceInstance(baseURI,ontology,
			// bcnInstance, actualClass);break;
			// case "ResidualSpace": CreatePublicSpaceInstance(baseURI,ontology,
			// bcnInstance, actualClass);break;
		}
		return true;
	}

	public static ArrayList<Resource> InsertData(BufferedReader reader, String fileName,
			HashMap<String, String> innerMap, String baseURI, Model tbox) throws Exception {
		int attempts = 0;
		ArrayList<Resource> toDelete = null;
		boolean success = false;
		do {
			attempts++;
			try {
				String line = null;
				toDelete = new ArrayList<Resource>();
				// BufferedReader reader = Files.newBufferedReader(f.toPath(),
				// Charset.defaultCharset());

				String firstLine = reader.readLine();

				ArrayList<String> fields = NormalizedSplit(firstLine);
				String classURI = innerMap.get(fileName);
				String[] tokClassURI = classURI.split(":");
				String nsClass = model.getNsPrefixURI(tokClassURI[0]);
				Resource cl = tbox.getResource(nsClass + tokClassURI[1]);
				Resource r = null;
				Property p = null;
				if (!tbox.contains(cl, p, r) && !tbox.contains(r, p, cl)) {
					tokClassURI = classURI.split(":");
					String prefix = tokClassURI[0];
					nsClass = model.getNsPrefixURI(prefix);
					cl = tbox.createResource(nsClass + tokClassURI[1]);
					tbox.add(cl, RDF.type, OWL.Class);
					toDelete.add(cl);
					System.out.println("Class " + nsClass + tokClassURI[1] + " not found: created");
				}
				int count = 0;
				while ((line = reader.readLine()) != null) {
					System.out.println(++count + ":" + line);
					Resource ind = model.createResource(baseURI + "#bcnInstance" + contInst++, new ResourceImpl(
							nsClass, tokClassURI[1]));
					ArrayList<String> tokens = NormalizedSplit(line);
					if (tokens.size() != fields.size())
						throw new Exception("Tokenization problems in File " + fileName);
					for (int i = 0; i < tokens.size(); i++) {
						String field = fields.get(i).toUpperCase(); // FIELDS
																	// FROM
																	// TABLE
																	// NORMALIZED
																	// TO UPPER
																	// CASE
						if (innerMap.containsKey(field)) {
							String entity = innerMap.get(field);
							String value = NormalizeNumber(tokens.get(i).trim());
							if (!value.equals("")) {
								String[] tokProperty = entity.split(":");
								String prefix = tokProperty[0];
								String nsProp = model.getNsPrefixURI(prefix);
								Property prop = tbox.getProperty(nsProp + tokProperty[1]);
								if (!CreateClassInstance(baseURI, ind, nsProp, tokProperty[1], value, field, tbox)) {
									if (!tbox.contains(prop, p, r) && !tbox.contains(r, p, prop)) {
										prop = tbox.createProperty(nsProp + tokProperty[1]);
										tbox.add(prop, RDF.type, OWL.DatatypeProperty);
										toDelete.add(prop);
										System.out.println("Property " + nsProp + tokProperty[1]
												+ " not found: created");
									}
									RDFDatatype dtype = GetDataType(prop, tbox);
									if (dtype.getURI().equals(XSD.integer.getURI())) {
										// Integer valueInt =
										// Integer.parseInt(value);
										value = value.replaceAll("\\.\\d+", "");
									}
									ind.addProperty(prop, value, dtype);
								}
							}

						}
					}
				}
				success = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				if (attempts >= 3)
					throw (e);
			}
		} while (!success && attempts < 3);
		return toDelete;
	}

	public static ArrayList<Resource> processFile(BufferedReader reader, String fName,
			HashMap<String, HashMap<String, String>> map, String baseURI, Model tbox) throws Exception {
		// HashMap<String, HashMap<Integer,String>> bcnData = new
		// HashMap<String, HashMap<Integer,String>>();
		ArrayList<Resource> toDelete = new ArrayList<Resource>();
		String fileName = (fName.replaceFirst("\\..+", "")).toUpperCase(); // FILE
																			// NORMALIZED
																			// TO
																			// UPPER
																			// CASE

		// String[] fields = getBCNFields(reader);
		if (map.containsKey(fileName)) {
			HashMap<String, String> innerMap = map.get(fileName);
			if (innerMap.containsKey(fileName)) // it is an instance
				// bcnData.put(fileName, getMappingPosition(fileName,fields,
				// map.get(fileName)));
				// toDelete.addAll(InsertData(reader,instOntology,bcnData.get(fileName),baseURI));
				toDelete.addAll(InsertData(reader, fileName, innerMap, baseURI, tbox));
			// reader.close();
		}
		return toDelete;
	}

	// public static ArrayList<Resource> processFile(File f,
	// HashMap<String,HashMap<String,String>> map, String baseURI) throws
	// Exception{
	// //HashMap<String, HashMap<Integer,String>> bcnData = new HashMap<String,
	// HashMap<Integer,String>>();
	// ArrayList<Resource> toDelete = new ArrayList<Resource>();
	// String fileName = f.getName().replaceFirst("\\..+", "");
	//
	// //String[] fields = getBCNFields(reader);
	// if (map.containsKey(fileName)){
	// HashMap<String, String> innerMap = map.get(fileName);
	// if (innerMap.containsKey(fileName)) //it is an instance
	// //bcnData.put(fileName, getMappingPosition(fileName,fields,
	// map.get(fileName)));
	// //toDelete.addAll(InsertData(reader,instOntology,bcnData.get(fileName),baseURI));
	// toDelete.addAll(InsertData(f, fileName, innerMap,baseURI));
	// //reader.close();
	// }
	// return toDelete;
	// }

	public static ArrayList<Resource> ImportEcologyDataLocal(String path, HashMap<String, HashMap<String, String>> map,
			String baseURI, Model tbox) throws Exception {
		ArrayList<Resource> toDelete = new ArrayList<Resource>();
		// HashMap<String,String> ontologyMap = readMapFile();
		File[] resources = new File(path).listFiles();
		for (File res : resources) {
			if (res.isDirectory()) {
				File[] files = res.listFiles();
				for (File f : files) {
					if (f.isFile() && f.getName().endsWith(".csv")) {
						FileInputStream stream = new FileInputStream(f.getAbsolutePath());
						// BufferedReader reader = new BufferedReader(new
						// InputStreamReader(stream, "UTF-8"));
						BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "ISO-8859-1"));
						toDelete.addAll(processFile(reader, f.getName(), map, baseURI, tbox));
						reader.close();
					}
				}
			} else if (res.isFile() && res.getName().endsWith(".csv")) {
				FileInputStream stream = new FileInputStream(res.getAbsolutePath());
				// BufferedReader reader = new BufferedReader(new
				// InputStreamReader(stream, "UTF-8"));
				BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "ISO-8859-1"));
				toDelete.addAll(processFile(reader, res.getName(), map, baseURI, tbox));
				reader.close();
			}
		}
		return toDelete;
	}

	public static String getLang(String pathFile) {
		if (pathFile.endsWith("rdf"))
			return "RDF/XML";
		else if (pathFile.endsWith("ttl"))
			return "TURTLE"; // "N3";
		System.out.println("Unknown ontology format " + pathFile);
		return null;
	}

	// public static OntModel CreateOntologyAlternative(File basePath, File
	// urlmapping, String baseURI) throws IOException{
	// OntModel instOntology =
	// ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
	// OntDocumentManager dm = instOntology.getDocumentManager();
	// dm.setProcessImports(false);
	// instOntology.read(new FileInputStream(basePath), baseURI,
	// getLang(basePath.getAbsolutePath()));
	// BufferedReader reader = Files.newBufferedReader(urlmapping.toPath(),
	// Charset.defaultCharset());
	// String line = null;
	// while ((line = reader.readLine()) != null){
	// String[] tokens = line.split("\t");
	// dm.addAltEntry(tokens[0], tokens[1]);
	// Model impModel = ModelFactory.createDefaultModel();
	// String mapurl = tokens[1];
	// if (mapurl.startsWith("file:"))
	// impModel.read(new FileInputStream(new File(mapurl.replace("file:", ""))),
	// baseURI, getLang(mapurl));
	// else if (mapurl.startsWith("http:"))
	// impModel.read(mapurl, baseURI, getLang(mapurl));
	// instOntology.addSubModel(impModel);
	// }

	public static void CreateOntologyAlternative(String tdbPath, URL basePath, String bcnURI, String bcnIndicatorsURI,
			String policyFile) throws IOException {

		// CREATE INSTANCES TDB MODEL

		Path path = Paths.get(tdbPath + "/model");
		if (Files.notExists(path.getParent()))
			Files.createDirectories(path);
		else
			FileUtils.cleanDirectory(new File(tdbPath + "/model"));
		dbModel = TDBFactory.createDataset(tdbPath + "/model");
		if (dbModel.getDefaultModel().isEmpty()) {
			dbModel.begin(ReadWrite.WRITE);
			FileManager fm = FileManager.get();
			String fileURL = basePath.getFile();
			// fileURL =
			// "/home/smendoza/workspace/ontologycommons/target/classes/smartcities/semanticweb/ontologycommons/data/ontologyheader.ttl";
			InputStream in1 = fm.open(fileURL); // open instancesfile
												// as dataset
			DatasetGraphTDB dsg = TDBInternal.getBaseDatasetGraphTDB(dbModel.asDatasetGraph());
			TDBLoader tdb = new TDBLoader();
			tdb.loadGraph(dsg.getDefaultGraphTDB(), in1);
			dbModel.commit();
			dbModel.end();
			dbModel.begin(ReadWrite.WRITE);
			model = dbModel.getDefaultModel();

		} else { // tdbmodel exists
			dbModel.begin(ReadWrite.WRITE);
			dbModel.getDefaultModel().removeAll();
			// System.out.println("Tdb already exists. No processing is done");
			// dbModel.begin(ReadWrite.READ);
			// model = dbModel.getDefaultModel();
			// PrintInfoModel();
			dbModel.commit();
			dbModel.end();
			// System.exit(1);
		}
	}

	public static OntModel CreateOntology(String basePath, String baseURI, String datasetPath)
			throws FileNotFoundException {
		// String ns = new String("http://www.ibm.com/BCNEcologyINST#");
		Dataset dsmodel = TDBFactory.createDataset(datasetPath);
		// this.dBaseModel =
		// TDBFactory.createDataset("/home/mmarrero/Downloads/TDB/baseModel");
		// //TODO: change in server
		Model ontology = null;
		ontology = dsmodel.getDefaultModel();
		if (ontology.isEmpty()) {
			OntModel instOntology = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
			OntDocumentManager dm = instOntology.getDocumentManager();
			dm.setCacheModels(true);
			dm.setProcessImports(true);
			dm.addAltEntry("http://www.ibm.com/BCNEcology",
					"file:/home/mmarrero/EclipseWorkspace2/SemanticUrbanModel2/Resources/Ontologies/SUM_V0/SUM/BCNEcology.ttl");
			dm.addAltEntry("http://www.businessactivities.com/BusAc",
					"file:/home/mmarrero/EclipseWorkspace2/SemanticUrbanModel2/Resources/Ontologies/SUM_V0/core/EconomicActivities.ttl");
			dm.addAltEntry("http://www.ibm.com/ICP/Scribe/CoreV2",
					"file:/home/mmarrero/EclipseWorkspace2/SemanticUrbanModel2/Resources/Ontologies/SUM_V0/core/CoreV2.ttl");
			dm.addAltEntry("http://www.ibm.com/ICP/Scribe/CoreGeoV2",
					"file:/home/mmarrero/EclipseWorkspace2/SemanticUrbanModel2/Resources/Ontologies/SUM_V0/core/CoreGeoV2.ttl");
			dm.addAltEntry("http://www.ibm.com/ICP/Scribe/CoreWeatherV2",
					"file:/home/mmarrero/EclipseWorkspace2/SemanticUrbanModel2/Resources/Ontologies/SUM_V0/core/CoreWeatherV2.ttl");
			instOntology.read(new FileInputStream(new File(basePath)), baseURI, "N3");
			return instOntology;
		} else {
			// OntModel instOntology =
			// ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, ontology);
			OntModel instOntology = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, ontology);
			// instOntology.add(ontology);
			instOntology.setNsPrefixes(ontology.getNsPrefixMap());
			return instOntology;
		}
	}

	// XML Results as a list of variable values separated by the tab character
	// (\t)
	private static ArrayList<String> ProcessXMLResults(String xmlResults) throws UnsupportedEncodingException,
			SAXException, IOException, ParserConfigurationException {
		ArrayList<String> output = new ArrayList<String>();
		Document xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
				.parse(new InputSource(new ByteArrayInputStream(xmlResults.getBytes("utf-8"))));
		NodeList results = xmlDoc.getElementsByTagName("result");
		for (int i = 0; i < results.getLength(); i++) {
			Element result = (Element) results.item(i);
			NodeList bindings = result.getElementsByTagName("binding");
			String resultRow = "";
			for (int j = 0; j < bindings.getLength(); j++) {
				Element bind = (Element) bindings.item(j);
				String value = bind.getTextContent();
				resultRow += value + "\t";
			}
			output.add(resultRow);
		}
		return output;
	}

	// private static String XMLQueryParliament(RemoteModel pmodel, String
	// query) throws IOException{
	// String output=null;
	// ResultSet rs = pmodel.selectQuery(query);
	// output = ResultSetFormatter.asXMLString(rs);
	// return output;
	// }

	// private static RemoteModel GetParliamentOntology(String sparqlURL, String
	// bulkURL, String graphURL, OntModel ontology) throws IOException{
	// RemoteModel pmodel = new RemoteModel(sparqlURL,bulkURL);
	// boolean exist = false;
	// for (Iterator<String> iterator = pmodel.getAvailableNamedGraphs();
	// iterator.hasNext();) {
	// String pargraph = (String) iterator.next();
	// if (pargraph.equals(graphURL))
	// exist = true;
	// }
	// if (!exist)
	// pmodel.insertStatements(ontology,graphURL);
	// else
	// System.out.println("Parliament "+graphURL+" exists and won't be replaced. Delete/Rename it before the mapping process");
	// return pmodel;
	// }

	// public static OntResource CreateGeographicObjProperty(OntModel ontology,
	// String baseURL, String temporalProp, String sparqlURL, String bulkURL,
	// String graphURL) throws IOException, SAXException,
	// ParserConfigurationException{
	// RemoteModel pmodel = Parliament.getPmodel(sparqlURL,bulkURL);
	// if (Parliament.existGraph(pmodel, graphURL))
	// Parliament.deleteGraph(pmodel, graphURL);
	// Parliament.insertStatements(pmodel, graphURL, ontology);
	// String prefixes
	// ="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX geo: <http://www.opengis.net/ont/geosparql#> PREFIX geof: <http://www.opengis.net/def/function/geosparql/> PREFIX sf: <http://www.opengis.net/ont/sf#> PREFIX CoreV2: <http://www.ibm.com/ICP/Scribe/CoreV2#> PREFIX : <http://www.ibm.com/BCNEcologyINST#>  ";
	// //String query
	// ="SELECT DISTINCT ?building ?local WHERE {GRAPH <"+graphURL+"> {?building rdf:type CoreV2:Building . ?building geo:hasGeometry ?bgeo . ?bgeo geo:asWKT ?bwkt . ?building :temporal2 ?cadastra . ?local rdf:type CoreV2:BuildingUseUnit . ?local geo:hasGeometry ?lgeo . ?lgeo geo:asWKT ?lwkt . ?local :temporal4 ?cadastra . FILTER (geof:sfIntersects(?bwkt,?lwkt)) }}";
	// String query
	// ="SELECT DISTINCT ?building ?local WHERE {GRAPH <"+graphURL+"> {?building rdf:type CoreV2:Building . ?building geo:hasGeometry ?bgeo . ?bgeo geo:asWKT ?bwkt . ?local rdf:type CoreV2:BuildingUseUnit . ?local geo:hasGeometry ?lgeo . ?lgeo geo:asWKT ?lwkt . FILTER (geof:sfWithin(?bwkt,?lwkt)) }}";
	// String xmlresults = Parliament.XMLQueryParliament(pmodel,prefixes+query);
	// ArrayList<String> results = ProcessXMLResults(xmlresults);
	// //String nsProp = ontology.getNsPrefixURI("");
	// OntProperty prop = ontology.createOntProperty(baseURL+"#"+temporalProp);
	// for (String lineResult: results){
	// String[] tokResult = lineResult.split("\t");
	// Individual building = ontology.getIndividual(tokResult[0].trim());
	// Individual local = ontology.getIndividual(tokResult[1].trim());
	// building.addProperty(prop, local);
	// }
	// return prop;
	// }

	private static void AddObjectRelationsFromQuery(String query) {

		Query q = QueryFactory.create(query);
		QueryExecution qexec = com.hp.hpl.jena.query.QueryExecutionFactory.create(q, model);
		Model newModel = qexec.execConstruct();
		System.out.println(newModel.listStatements().toList().size());
		for (Statement st : newModel.listStatements().toList()) {
			if (!model.contains(st))
				model.add(st);
		}
	}

	// public static String GetUrbanSpaceQueryConstruct(String code){
	// String query = null;
	// String entity = null;
	// switch (code){
	// case "EVR_07": entity = "CoreV2:AreaLocalUse";break;
	// case "VVH_04": entity = "CoreV2:BikeLane";break;
	// case "EVR_04": entity = "CoreV2:BlockExternalSpace";break;
	// case "EVR_03": entity = "CoreV2:BlockInnerSpace";break;
	// case "VPT_05": entity = "CoreV2:Boulevard";break;
	// case "VPT_07": entity = "CoreV2:Bypath";break;
	// case "VPT_04": entity = "CoreV2:PedestrianMall";break;
	// case "VPT_03": entity = "CoreV2:PedestrianStreet";break;
	// case "VPT_06": entity = "CoreV2:Promenade";break;
	// case "OTR_01": entity = "CoreV2:ResidualSpace";break;
	// default: return null;//entity = prefixCoreV2+"UrbanOpenSpace";break;
	// }
	// query =
	// "CONSTRUCT {?ind rdf:type "+entity+"} WHERE {?ind :temporal8 \""+code+"\" .}";
	// return query;
	// }
	//
	// public static void UrbanSpaceQueryDelete(OntModel ontology){
	// String prefixes
	// ="PREFIX :<http://www.ibm.com/BCNEcologyINST#> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX geo: <http://www.opengis.net/ont/geosparql#> PREFIX geof: <http://www.opengis.net/def/function/geosparql/> PREFIX sf: <http://www.opengis.net/ont/sf#> PREFIX CoreV2: <http://www.ibm.com/ICP/Scribe/CoreV2#> ";
	// String query =
	// prefixes+"SELECT ?ind WHERE {?ind :temporal8 ?code . ?ind rdf:type CoreV2:UrbanOpenSpace .}";
	// Query q = QueryFactory.create(query);
	// QueryExecution qexec =
	// com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
	// ResultSet result = qexec.execSelect();
	// String prefixCoreV2 = ontology.getNsPrefixURI("CoreV2");
	// for (Iterator iterator = result; iterator.hasNext();) {
	// QuerySolution res = (QuerySolution) iterator.next();
	// Resource ind = res.getResource("ind");
	// RDFNode rnode = new ResourceImpl(prefixCoreV2+"UrbanOpenSpace");
	// ontology = (OntModel) ontology.removeAll(ind, RDF.type, rnode) ;
	// }
	// }

	public static void AddObjectProperties(String temporalPropParl, URL queriesFile) throws IOException,
			URISyntaxException {
		BufferedReader reader = Files.newBufferedReader((new File(queriesFile.toURI())).toPath(),
				Charset.defaultCharset());
		String line = null;
		StringBuffer prefixes = new StringBuffer();
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("PREFIX")) {
				prefixes.append(line);
				prefixes.append(" ");
			} else if (line.startsWith("CONSTRUCT")) {
				// AddObjectRelationsFromQuery(ontology, prefixes+line);
				System.out.println("Processing query: " + line);
				AddObjectRelationsFromQuery(prefixes.toString() + line);
			}

		}
		// TOTEST
		// String query =
		// prefixes+"SELECT count(*) WHERE {?entity geo:hasGeometry ?geo}";
		// Query q = QueryFactory.create(query);
		// QueryExecution qexec =
		// com.hp.hpl.jena.query.QueryExecutionFactory.create(q, ontology);
		// ResultSet res = qexec.execSelect();
		// //System.out.println(res.().toList().size());
		//
		// while (res.hasNext()){
		// System.out.println(res.next().toString());
		// }

	}

	// public static void AddObjectRelations (OntModel ontology, String
	// temporalPropParl){
	// String prefixes
	// ="PREFIX BCNEcology: <http://www.ibm.com/BCNEcology#> PREFIX :<http://www.ibm.com/BCNEcologyINST#> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX geo: <http://www.opengis.net/ont/geosparql#> PREFIX geof: <http://www.opengis.net/def/function/geosparql/> PREFIX sf: <http://www.opengis.net/ont/sf#> PREFIX CoreV2: <http://www.ibm.com/ICP/Scribe/CoreV2#> ";
	// String query1
	// ="CONSTRUCT {?building CoreV2:buildingHasUnit ?local} WHERE {?building "+":"+temporalPropParl+" ?local . ?building :temporal2 ?cadastra . ?local :temporal4 ?cadastra}";
	// //String query2 =
	// "CONSTRUCT {?building CoreV2:numberOfDwellings ?dwell; CoreV2:numberOfResidents ?res; CoreV2:streetName ?sn; CoreV2:hasArea ?area1; CoreV2:hasArea ?area2 . ?area1 CoreV2:areaOf ?building . ?area2 CoreV2:areaOf ?building} WHERE {?building :temporal2 ?cadastra . ?building :temporal3 ?cilla . ?finques :temporal6 ?cadastra . ?finques :temporal7 ?cilla . ?finques CoreV2:numberOfDwellings ?dwell . ?finques CoreV2:numberOfResidents ?res . ?finques CoreV2:streetName ?sn . ?finques CoreV2:hasArea ?area1 . ?finques CoreV2:hasArea ?area2 . ?area1 rdf:type CoreV2:BuiltStructureOccupiedSurfaceMeasurement . ?area2 rdf:type CoreV2:BuiltStructureFootPrintMeasurement}";
	// String query2 =
	// "CONSTRUCT {?landlot BCNEcology:numberOfResidentsPerLandLot ?res} WHERE {?landlot CoreV2:landLotID ?cadastra . ?landlot :temporal1 ?cilla . ?finques :temporal6 ?cadastra . ?finques :temporal7 ?cilla . ?finques CoreV2:numberOfResidents ?res}";
	// String query3 =
	// "CONSTRUCT {?landlot CoreV2:landLotHasBuiltStructure ?building} WHERE {?landlot :temporal1 ?cilla . ?building :temporal3 ?cilla . ?landlot CoreV2:landLotID ?cadastra . ?building :temporal2 ?cadastra}";
	// String query4 =
	// "CONSTRUCT {?landlot CoreV2:landPolygonInsideOf ?block} WHERE {?landlot :temporal1 ?cilla . ?block CoreV2:UrbanBlockID ?cilla}";
	// String query5 =
	// "CONSTRUCT {?local CoreV2:hasAssetDescriptor ?ind} WHERE {?local :temporal9 ?identifier . ?ind CoreV2:identifier ?identifier}";
	// String query6 =
	// "CONSTRUCT {?local CoreV2:hasAssetDescriptor ?ind} WHERE {?local :temporal10 ?identifier . ?ind CoreV2:identifier ?identifier}";
	// AddObjectRelationsFromQuery(ontology, prefixes+query1);
	// AddObjectRelationsFromQuery(ontology, prefixes+query2);
	// AddObjectRelationsFromQuery(ontology, prefixes+query3);
	// AddObjectRelationsFromQuery(ontology, prefixes+query4);
	// AddObjectRelationsFromQuery(ontology, prefixes+query5);
	// AddObjectRelationsFromQuery(ontology, prefixes+query6);
	// ArrayList<String> public_space_codes = new ArrayList<String>();
	// public_space_codes.add("EVR_03");
	// public_space_codes.add("EVR_04");
	// public_space_codes.add("EVR_07");
	// public_space_codes.add("VVH_04");
	// public_space_codes.add("VPT_03");
	// public_space_codes.add("VPT_04");
	// public_space_codes.add("VPT_05");
	// public_space_codes.add("VPT_06");
	// public_space_codes.add("VPT_07");
	// public_space_codes.add("OTR_01");
	// for (String code: public_space_codes){
	// AddObjectRelationsFromQuery(ontology,
	// prefixes+GetUrbanSpaceQueryConstruct(code));
	// }
	// }

	// TIENE QUE LANZARSE DESPUÉS DE CREAR LAS RELACIONES ENTRE ENTIDADES
	public static void FilterBlocks(int maxsuperblocks, String baseURI) {
		// final int MAXSUPERBLOCKS = 1;

		List<Resource> instancesToDelete = DisconnectedEntities();
		Set<String> superblocks = new HashSet<String>();
		String prefixCoreV2 = model.getNsPrefixURI("CoreV2");
		String prefixBCNEcology = model.getNsPrefixURI("BCNEcology");
		// Property p_block = model.getProperty(prefixCoreV2 +"UrbanBlockID");
		Property p_superblock = model.getProperty(prefixBCNEcology + "superblockID");
		Resource rsegment = model.getResource(prefixCoreV2 + "RoadSegment");
		List<Resource> blocks = new ArrayList<Resource>();
		Resource cl = null;
		RDFNode object = null;
		for (Iterator iterator = model.listStatements(cl, p_superblock, object); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			String id_superblock = st.getObject().asLiteral().getValue().toString();
			// RDFNode node_idsuperblock = st.getObject();
			if (superblocks.size() < maxsuperblocks && !superblocks.contains(id_superblock)) {
				superblocks.add(id_superblock);
				// BLOCKS
				Property p1 = model.getProperty(prefixCoreV2 + "hasLocationSuperBlock");
				for (Iterator iterator2 = model.listStatements(cl, p1, st.getSubject()); iterator2.hasNext();) {
					Statement st2 = (Statement) iterator2.next();
					blocks.add(st2.getSubject());
				}

			} else {
				instancesToDelete.add(st.getSubject());

			}
		}
		Set<Statement> stToDelete = new HashSet<Statement>();
		while (instancesToDelete.size() > 0) {
			Resource inst = instancesToDelete.remove(instancesToDelete.size() - 1);
			Property p = null;
			for (Iterator iterator = model.listStatements(inst, p, object); iterator.hasNext();) {
				Statement st = (Statement) iterator.next();

				if (st.getObject().isURIResource() && st.getObject().asResource().getNameSpace().equals(baseURI + "#")) {
					boolean remove = true;

					// System.out.println("Entity:"+st.getObject().toString());
					// for (Iterator iterator2 =
					// model.listStatements(inst,RDF.type,object);
					// iterator2.hasNext();){
					// Statement st2 = (Statement) iterator2.next();
					// System.out.println("Type:"+st2.getObject().toString());
					// if
					// (st2.getObject().asResource().getURI().contains("RoadSegment"))
					// System.out.println("stop");
					// }
					if (model.contains(st.getObject().asResource(), RDF.type, rsegment))
						for (Resource block : blocks)
							if (model.contains(block, p, st.getObject())) {
								remove = false;
								break;
							}
					if (remove) {
						instancesToDelete.add(st.getObject().asResource());
					}
				}
				stToDelete.add(st);

			}
			model = model.remove(stToDelete.toArray(new Statement[stToDelete.size()]));
			stToDelete.clear();

			Resource res = null;
			for (Iterator iterator = model.listStatements(res, p, inst); iterator.hasNext();) {
				Statement st = (Statement) iterator.next();

				if (st.getSubject().getNameSpace().equals(baseURI + "#")) {
					boolean remove = true;
					if (model.contains(st.getSubject(), RDF.type, rsegment))
						for (Resource block : blocks)
							if (model.contains(block, p, st.getSubject())) {
								remove = false;
								break;
							}
					if (remove) {
						instancesToDelete.add(st.getSubject());
					}
				}
				stToDelete.add(st);
			}
			model = model.remove(stToDelete.toArray(new Statement[stToDelete.size()]));
			stToDelete.clear();

		}
		// model = model.remove(stToDelete.toArray(new
		// Statement[stToDelete.size()]));

	}

	// String[] entityType = {"http://www.ibm.com/BCNEcology#Superblock",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#LegalEntity",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#Building",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#BuildingUseUnit",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#LandLot","http://www.ibm.com/ICP/Scribe/CoreV2#RoadSegment",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#UrbanBlock",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#UrbanOpenSpace",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#BikeParkingFacility",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#AreaLocalUse",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#BikeLane",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#BlockExternalSpace",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#BlockInnerSpace",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#Boulevard",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#Bypath",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#PedestrianMall",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#PedestrianStreet",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#Promenade",
	// "http://www.ibm.com/ICP/Scribe/CoreV2#ResidualSpace"};

	public static List<Resource> DisconnectedEntities() {

		List<Resource> instancesToDelete = new ArrayList<Resource>();
		Resource res = null;
		RDFNode node = null;
		String[][] entityPairs = {
				{ "http://www.ibm.com/BCNEcology#Superblock", "http://www.ibm.com/ICP/Scribe/CoreV2#UrbanBlock",
						"http://www.ibm.com/ICP/Scribe/CoreV2#locationBlockOf" },
				{ "http://www.ibm.com/ICP/Scribe/CoreV2#UrbanBlock", "http://www.ibm.com/BCNEcology#Superblock",
						"http://www.ibm.com/ICP/Scribe/CoreV2#hasLocationSuperBlock" },
				{ "http://www.ibm.com/ICP/Scribe/CoreV2#LandLot", "http://www.ibm.com/ICP/Scribe/CoreV2#UrbanBlock",
						"http://www.ibm.com/ICP/Scribe/CoreV2#landPolygonInsideOf" },
				{ "http://www.ibm.com/ICP/Scribe/CoreV2#LegalEntity", "http://www.ibm.com/ICP/Scribe/CoreV2#LandLot",
						"http://www.ibm.com/ICP/Scribe/CoreV2#legalEntityAssocToAsset" },
				{ "http://www.ibm.com/ICP/Scribe/CoreV2#BuildingUseUnit",
						"http://www.ibm.com/ICP/Scribe/CoreV2#LandLot",
						"http://www.ibm.com/ICP/Scribe/CoreV2#bldgUseUnitInLandLot" },
				{ "http://www.ibm.com/ICP/Scribe/CoreV2#RoadSegment",
						"http://www.ibm.com/ICP/Scribe/CoreV2#UrbanBlock",
						"http://www.ibm.com/ICP/Scribe/CoreV2#routeAdjacentToArea" },
				{ "http://www.ibm.com/ICP/Scribe/CoreV2#BikeParkingFacility",
						"http://www.ibm.com/ICP/Scribe/CoreV2#RoadSegment",
						"http://www.ibm.com/ICP/Scribe/CoreV2#locatedIn" } };

		for (String[] pair : entityPairs) {
			for (Iterator iterator = model.listStatements(res, RDF.type, model.getResource(pair[0])); iterator
					.hasNext();) {
				Property p = model.getProperty(pair[2]);
				StatementImpl st = (StatementImpl) iterator.next();
				if (!model.contains(st.getSubject(), p, node))
					instancesToDelete.add(st.getSubject());
			}
		}
		// List<Statement> stToRemove = new ArrayList<Statement>();
		// for (Resource inst: instancesToDelete){
		// for (Iterator iterator = model.listStatements(inst,p,node);
		// iterator.hasNext();) {
		// StatementImpl st = (StatementImpl) iterator.next();
		// stToRemove.add(st);
		// }
		// for (Iterator iterator = model.listStatements(res,p,inst);
		// iterator.hasNext();) {
		// StatementImpl st = (StatementImpl) iterator.next();
		// stToRemove.add(st);
		// }
		// }
		// model.remove(stToRemove);

		return instancesToDelete;
	}

	public static void DeleteTemporalRelations(ArrayList<Resource> resToDelete, Model tbox) {
		// UrbanSpaceQueryDelete(ontology);

		for (Resource res : resToDelete) {
			Resource cl = tbox.getResource(res.getURI());
			Property prop = tbox.getProperty(res.getURI());
			Resource r = null;
			Property p = null;
			if (tbox.contains(cl, RDF.type, OWL.Class)) {
				RDFNode rdfn = null;
				prop = null;
				ArrayList<StatementImpl> temporalResources = new ArrayList<StatementImpl>();
				for (Iterator iterator = model.listStatements(r, RDF.type, cl); iterator.hasNext();) {

					StatementImpl st = (StatementImpl) iterator.next();

					RDFNode rdfnode = null;
					boolean otherTypes = false;
					for (Iterator iterator2 = model.listStatements(st.getSubject(), RDF.type, rdfnode); iterator2
							.hasNext();) {
						Statement st2 = (Statement) iterator2.next();
						if (st2.getObject().isResource() && !st2.getObject().asResource().getURI().equals(cl.getURI()))
							otherTypes = true;
					}
					if (!otherTypes) {
						Resource ind = st.getSubject();
						for (Iterator iterator2 = model.listStatements(ind, prop, rdfn); iterator2.hasNext();) {
							StatementImpl st2 = (StatementImpl) iterator2.next();
							temporalResources.add(st2);
						}
						for (Iterator iterator2 = model.listStatements(r, prop, ind); iterator2.hasNext();) {
							StatementImpl st2 = (StatementImpl) iterator2.next();
							temporalResources.add(st2);
						}
					}

					temporalResources.add(st);
				}

				for (Iterator iterator = model.listStatements(r, prop, cl); iterator.hasNext();) {
					StatementImpl st = (StatementImpl) iterator.next();
					// ontology = (OntModel) ontology.remove(st);
					temporalResources.add(st);
				}

				// for (StatementImpl rtemp: temporalResources){
				model.remove(new ArrayList<Statement>(temporalResources));
				// ontology = (OntModel) ontology.remove(rtemp);

				// }
				// cl.remove();

			} else if (tbox.contains(prop, RDF.type, OWL.DatatypeProperty)) {
				RDFNode rdfn = null;
				model.remove(model.listStatements(r, prop, rdfn));
				// prop.removeProperties();
			}
		}
	}

	public static void savePopulationData(Model ontology) {
		ArrayList<String> uris = new ArrayList<String>();
		HashMap<String, Integer> populationData = new HashMap<String, Integer>();

		for (Iterator iterator = ontology.listStatements(); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			Property p = st.getPredicate();
			uris.add(p.getURI());
		}
		Resource subject = null;
		RDFNode subjectclass = null;
		for (Iterator iterator = ontology.listStatements(subject, RDF.type, subjectclass); iterator.hasNext();) {
			Statement st = (Statement) iterator.next();
			if (st.getObject().isURIResource())
				uris.add(st.getObject().asResource().getURI());
		}
		for (String uri : uris) {
			int count = 1;
			if (populationData.containsKey(uri)) {
				count = populationData.get(uri) + 1;
				populationData.remove(uri);
			}
			populationData.put(uri, count);
		}
		String prefixCoreV2 = ontology.getNsPrefixURI("CoreV2");
		for (String uri : populationData.keySet()) {
			String stringvalue = populationData.get(uri).toString();
			// System.out.println(uri+" - "+stringvalue);
			Literal value = ontology.createTypedLiteral(stringvalue);
			Statement newst = ontology.createStatement(ontology.getResource(uri), new PropertyImpl(prefixCoreV2
					+ "currentlyPopulated"), value);
			ontology.add(newst);
		}
	}

	public static ArrayList<Resource> ImportEcologyDataFTP(String urlinfo,
			HashMap<String, HashMap<String, String>> map, String baseURI, Model tbox) throws Exception {
		// String host = "redrock.bsc.es";
		// String user = "bscftpshareduser";
		// String pass = "nTm.TxLyW6(C4N";
		// String directory = "TOT_BCN2WKT";

		String host = null;
		String user = "";
		String pass = "";
		String directory = "";

		URL url = new URL(urlinfo);
		host = url.getHost();
		String[] userinfo = url.getUserInfo().split(":");
		if (userinfo != null && userinfo.length > 0) {
			user = userinfo[0];
			if (userinfo.length > 1)
				pass = userinfo[1];
		}
		directory = url.getPath();
		int port = url.getPort();
		if (port < 0)
			port = url.getDefaultPort();

		ArrayList<Resource> toDelete = new ArrayList<Resource>();
		FTPClient ftp = new FTPClient();
		int attempts = 0;
		boolean success = false;
		do {
			attempts++;
			try {
				FTPClientConfig config = new FTPClientConfig();
				ftp.configure(config);

				int reply;
				ftp.connect(host, port);
				ftp.enterLocalPassiveMode();
				ftp.login(user, pass);

				System.out.print("Connected to " + directory + ".");
				System.out.println(ftp.getReplyString());
				reply = ftp.getReplyCode();

				if (!FTPReply.isPositiveCompletion(reply)) {
					ftp.disconnect();
					throw (new IOException("FTP server refused connection."));
				}
				listDirectory(ftp, directory, "", 2, map, baseURI, toDelete, tbox);
				success = true;
				ftp.logout();
				ftp.disconnect();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				if (attempts >= 3)
					throw (e);
			} finally {
				if (ftp.isConnected())
					ftp.disconnect();
			}
		} while (!success && attempts < 3);
		return toDelete;
	}

	private static void listDirectory(FTPClient ftpClient, String parentDir, String currentDir, int level,
			HashMap<String, HashMap<String, String>> map, String baseURI, ArrayList<Resource> toDelete, Model tbox)
			throws Exception {
		int attempts = 0;
		FTPFile[] subFiles;
		boolean success = false;
		String dirToList = parentDir;
		if (!currentDir.equals(""))
			dirToList += "/" + currentDir;
		do {
			attempts++;
			try {
				subFiles = ftpClient.listFiles(dirToList);
				if (subFiles != null && subFiles.length > 0) {
					for (FTPFile aFile : subFiles) {
						String currentFileName = aFile.getName();
						if (currentFileName.equals(".") || currentFileName.equals("..")) {
							continue;
						}
						for (int i = 0; i < level; i++) {
							System.out.print("\t");
						}
						if (aFile.isDirectory()) {
							System.out.println("[" + currentFileName + "]");
							listDirectory(ftpClient, dirToList, currentFileName, level + 1, map, baseURI, toDelete,
									tbox);
						} else {
							System.out.println(currentFileName);
							if (currentFileName.endsWith(".csv")) {
								System.out.println("Processing " + currentFileName);
								InputStream stream = ftpClient.retrieveFileStream(parentDir + "/" + currentDir + "/"
										+ currentFileName);
								BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
								toDelete.addAll(processFile(reader, currentFileName, map, baseURI, tbox));
								reader.close();
								stream.close();
								ftpClient.completePendingCommand();
							}
						}
					}
				}
				success = true;
			} catch (IOException e) {
				System.out.println(e.getMessage());
				if (attempts >= 3)
					throw (e);
			}
		} while (!success && attempts < 3);

	}

	public static void PrintInfoModel() {
		String[] entityType = { "http://www.ibm.com/BCNEcology#Superblock",
				"http://www.ibm.com/ICP/Scribe/CoreV2#LegalEntity", "http://www.ibm.com/ICP/Scribe/CoreV2#Building",
				"http://www.ibm.com/ICP/Scribe/CoreV2#BuildingUseUnit", "http://www.ibm.com/ICP/Scribe/CoreV2#LandLot",
				"http://www.ibm.com/ICP/Scribe/CoreV2#RoadSegment", "http://www.ibm.com/ICP/Scribe/CoreV2#UrbanBlock",
				"http://www.ibm.com/ICP/Scribe/CoreV2#UrbanOpenSpace",
				"http://www.ibm.com/ICP/Scribe/CoreV2#BikeParkingFacility",
				"http://www.ibm.com/ICP/Scribe/CoreV2#AreaLocalUse", "http://www.ibm.com/ICP/Scribe/CoreV2#BikeLane",
				"http://www.ibm.com/ICP/Scribe/CoreV2#BlockExternalSpace",
				"http://www.ibm.com/ICP/Scribe/CoreV2#BlockInnerSpace",
				"http://www.ibm.com/ICP/Scribe/CoreV2#Boulevard", "http://www.ibm.com/ICP/Scribe/CoreV2#Bypath",
				"http://www.ibm.com/ICP/Scribe/CoreV2#PedestrianMall",
				"http://www.ibm.com/ICP/Scribe/CoreV2#PedestrianStreet",
				"http://www.ibm.com/ICP/Scribe/CoreV2#Promenade", "http://www.ibm.com/ICP/Scribe/CoreV2#ResidualSpace" };

		Resource entity = null;
		for (String type : entityType) {
			Resource entityClass = model.getResource(type);
			int count = 0;
			for (Iterator iterator = model.listStatements(entity, RDF.type, entityClass); iterator.hasNext();) {
				iterator.next();
				count++;
			}
			System.out.println("Entity " + type);
			System.out.println("Count " + count);
			System.out.println("***");
		}
	}

	public static Dataset MapData(int superblocks, String baseuri, String indicatorsinstancesuri, String instancesuri,
			String instancesontology, String ontologiespolicy, String datapath, String tdbPath, FileWriter logFile,
			Model tbox) throws Exception {
		final String temporalPropParliament = "temporal100";
		try {
			URL mapping = Mapping.class.getResource("mapEntities_Eixample.txt");
			URL header = Mapping.class.getResource("ontologyheader.ttl");
			URL queries = Mapping.class.getResource("queriesEixample.sparql");

			CreateOntologyAlternative(tdbPath, header, baseuri, indicatorsinstancesuri, ontologiespolicy);
			HashMap<String, HashMap<String, String>> map = ReadMapFile(mapping);
			ArrayList<Resource> resToDelete;
			if (datapath.startsWith("file") || datapath.startsWith("/"))
				resToDelete = ImportEcologyDataLocal(datapath, map, instancesuri, tbox);
			else if (datapath.startsWith("ftp"))
				resToDelete = ImportEcologyDataFTP(datapath, map, instancesuri, tbox);
			else {
				logFile.close();
				throw (new Exception("Not recognized  " + datapath + ". Only ftp or local files supported"));
			}

			logFile.write("Incorrect figures:" + countIncorrectPolygons + "\n");
			model.add(tbox);

			dbModel.commit();
			dbModel.end();

			dbModel.begin(ReadWrite.WRITE);
			model = dbModel.getDefaultModel();

			logFile.write("Statements before new relationships: " + model.size() + "\n");
			PrintInfoModel();

			AddObjectProperties(temporalPropParliament, queries);

			PrintInfoModel();

			// DeleteDisconnectedEntities();

			// PrintInfoModel();

			FilterBlocks(superblocks, instancesuri);

			PrintInfoModel();

			model.remove(tbox); // REMOVE TBOX

			logFile.write("Statements before delete: " + model.size() + "\n");
			DeleteTemporalRelations(resToDelete, tbox);

			dbModel.commit();
			dbModel.end();
			dbModel.begin(ReadWrite.WRITE);
			model = dbModel.getDefaultModel();
			logFile.write("Statements after delete: " + model.size() + "\n");

			PrintInfoModel();

			java.io.FileOutputStream fos = new java.io.FileOutputStream(new File(instancesontology), false);
			model.write(fos, "TURTLE");
			// insertInstancesGraph(parliamentlocation, parliamentmodel, model);
			dbModel.commit();
			dbModel.end();

		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw (e);
		}

		return dbModel;
	}

}
