package smartcities.semanticweb.ontologycommons;

import smartcities.semanticweb.ontologycommons.services.ServiceInterface;

public class GenerateResources {

	public static void main(String[] args) {
		GenerateResourcesTest();
	}

	public static void GenerateResourcesTest() {
		// redrock environment?
		// String configfile = "/home/smendoza/UrbanPrototypeResources-mc/UrbanPrototypeResources/config.properties";

		//laptop environment
		String configfile = "/home/smendoza/eclipse-workspace/UrbanPrototypeResources/config.properties";

		System.out.println("Generating Resources. config.properties("
				+ configfile + ")...");

		try {
			ServiceInterface.GenerateResources(configfile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Generating Resources... FINISHED");
	}

}