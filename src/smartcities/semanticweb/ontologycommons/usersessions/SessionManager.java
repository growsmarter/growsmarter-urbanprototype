package smartcities.semanticweb.ontologycommons.usersessions;

import java.util.HashMap;
import java.util.Map;

import es.bsc.casedep.gs.sal.apis.APIClient;

public class SessionManager {

	private static SessionManager instance = null;
	private Map<String, Session> usersSessions = new HashMap<>();
	public static final String tokenCellnexAPIDefault = "eed7f5d59c1057e24d3b37598c7192ae";

	protected SessionManager() {
		// Exists only to defeat instantiation.
	}

	public SessionManager getInstance() {
		if (instance == null) {
			instance = new SessionManager();
		}
		return instance;
	}

	public void addSession(Session newSession) {
		this.usersSessions.put(newSession.getUser(), newSession);
	}

	public void addSession(String user, String token) {
		Session newSession = new Session(user, token);
		this.addSession(newSession);
	}

	public Session getSession(String user) {
		return this.usersSessions.get(user);
	}

}
