package smartcities.semanticweb.ontologycommons.usersessions;

import es.bsc.casedep.gs.sal.apis.APIClient;

public class Session {

	private String user = null;
	private String tokenCellnexAPI = SessionManager.tokenCellnexAPIDefault;



	public Session() {

	}

	public Session(String user, String token) {
		this.user = user;
		this.tokenCellnexAPI = token;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getTokenCellnexAPI() {
		return tokenCellnexAPI;
	}

	public void setTokenCellnexAPI(String tokenCellnexAPI) {
		this.tokenCellnexAPI = tokenCellnexAPI;
	}

}
