#!/bin/bash

#SHELL SCRIPT TO RESTART TOMCAT at redrock.bsc.es 
#1.STARTS PARLIAMENT
#2.WAITS FOR PARLIAMENT DATA TO BE LOADED
#3.STARTS TOMCAT
#3.STARTS WEBAPP RUNNED IN TOMCAT

TOMCAT_WEBAPPS="/srv/tomcat/webapps"
MY_WEBAPP=$TOMCAT_WEBAPPS"/UrbanPrototype.war"

#stop the tomcat server
sudo /usr/share/tomcat/bin/catalina.sh stop
#remove the webapp directory
sudo rm -rf $TOMCAT_WEBAPPS/UrbanPrototype
#change .war webapp extension to avoid its deployment
sudo mv $MY_WEBAPP $MY_WEBAPP.nodep
#go to tomcat bin dir
sudo su - root -c "cd /usr/share/tomcat/bin/; ./catalina.sh start"

#wait one minute, till parliament starts
sleep 30
#allow webapp deployment
sudo mv $MY_WEBAPP.nodep $MY_WEBAPP
