#!/bin/bash

#SHELL SCRIPT TO RESTART TOMCAT at redrock.bsc.es 
#1.STARTS PARLIAMENT
#2.WAITS FOR PARLIAMENT DATA TO BE LOADED
#3.STARTS TOMCAT
#3.STARTS WEBAPP RUNNED IN TOMCAT

TOMCAT_WEBAPPS="/srv/tomcat/webapps";
MY_WEBAPP=$TOMCAT_WEBAPPS"/UrbanPrototype.war"

#stop the tomcat server
#remove the webapp directory
#change .war webapp extension to avoid its deployment
#go to tomcat bin dir
#wait one minute, till parliament starts
#allow webapp deployment

CMD_RESTART_REDROCK_TOMCAT="
sudo /usr/share/tomcat/bin/catalina.sh stop;
sudo rm -rf $TOMCAT_WEBAPPS/UrbanPrototype;
sudo mv $MY_WEBAPP $MY_WEBAPP.nodep;
sudo su - root -c \"cd /usr/share/tomcat/bin/; ./catalina.sh start\";
sleep 1m;
sudo mv $MY_WEBAPP.nodep $MY_WEBAPP;
"
#RUN RESTART COMMANDS AT redrock.bsc.es
ssh smendoza@redrock.bsc.es $CMD_RESTART_REDROCK_TOMCAT