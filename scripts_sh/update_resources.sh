#!/bin/bash

#SHELL SCRIPT TO UPDATE URBANRESOURCES IN TOMCAT
#1.ZIP's THE LAST URBANRESOURCES (THEY MUST BE ALREADY GENERATED)
#2.UPLOAD's AT REDROCK.BSC.ES
#3.MOVES URBANRESOURCES TO TOMCAT SERVER
#3.SETS THE URBANRESOURCES REDROCK.BSC.ES config.properties FILE

TOMCAT_WEBAPPS="/srv/tomcat/webapps"
UPR_HOME="/home/smendoza/UrbanPrototypeResources-mc"
NOW=$(date +"%Y%m%d")
ZIP_FILENAME=UrbanPrototypeResources-$NOW.zip
ZIP_FILE_PATH=$UPR_HOME/$ZIP_FILENAME
cd $UPR_HOME
zip $ZIP_FILE_PATH UrbanPrototypeResources -r
scp $ZIP_FILE_PATH smendoza@redrock.bsc.es:~/$ZIP_FILENAME

CMD_REDROCK="
sudo /usr/share/tomcat/bin/catalina.sh stop ;
sudo mv ~/$ZIP_FILENAME ~/backups_UrbanPrototypeResources ;
sudo rm -rf $TOMCAT_WEBAPPS/UrbanPrototypeResources ;
sudo unzip ~/backups_UrbanPrototypeResources/$ZIP_FILENAME -d $TOMCAT_WEBAPPS ;
sudo cp $TOMCAT_WEBAPPS/UrbanPrototypeResources/config.properties.redrock $TOMCAT_WEBAPPS/UrbanPrototypeResources/config.properties ;
"
echo "EXECUTING AT REDROCK...."
echo $CMD_REDROCK
ssh smendoza@redrock.bsc.es $CMD_REDROCK > /tmp/output.ssh.tmp
echo "FINISHED!"

